class: FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FwChildMode_CLASS

    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM not_in_state Excluded ) then
                if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                    insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETSTATES
                    insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETSTATES
                insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETACTIONS
            endif
            move_to Included
        action: Manual
            do Manual FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
            insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETSTATES
            remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
            move_to Manual
        action: Ignore
            do Ignore FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
            insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETACTIONS
            remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
            move_to Ignored
        action: LockOut
            move_to LockedOut
        action: Exclude
            do Exclude FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
            remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
            remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
            move_to Excluded
    state: Included	!color: FwStateOKPhysics
        when ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM in_state Excluded )  move_to EXCLUDED
        when ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM in_state Ignored )  move_to IGNORED
        when ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM in_state Manual )  move_to MANUAL
        action: Exclude(string OWNER = "")
            if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM not_in_state Included ) then
                if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
                remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
            insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETSTATES
            remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")
            do Ignore(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
            insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETACTIONS
            remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
                remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
            insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETSTATES
            insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
        action: ExcludePerm(string OWNER = "")
            if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM not_in_state Included ) then
                if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
                remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
            endif
            move_to ExcludedPerm
    state: Manual	!color: FwStateOKNotPhysics
        when ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM in_state Included )  move_to INCLUDED
        when ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
              insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETSTATES
              insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETACTIONS
            endif
            move_to Manual
        action: Exclude(string OWNER = "")
            if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM not_in_state Included ) then
                if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
                remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
            endif
            move_to Manual
        action: Ignore
            do Ignore FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
            insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETACTIONS
            remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
            move_to Manual
        action: ExcludeAll(string OWNER = "")
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
            if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM not_in_state InManual ) then
              do ExcludeAll(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
              remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
              remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
            endif
            move_to Manual
    state: Ignored	!color: FwStateOKNotPhysics
        when ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM in_state Included )  move_to INCLUDED
        when ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
            insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETSTATES
            insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETACTIONS
            move_to Included
        action: Exclude(string OWNER = "")
            if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM not_in_state Included ) then
                if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
                remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
            insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETSTATES
            remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")
            if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
                    remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETSTATES
                remove FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER from FWCAENCRATESY1527_FWSETACTIONS
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut
            move_to Excluded
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM not_in_state Excluded ) then
                if ( FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                    insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETSTATES
                    insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM
                insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETSTATES
                insert FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER in FWCAENCRATESY1527_FWSETACTIONS
            endif
            move_to Included

object: FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM is_of_class FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FwChildMode_CLASS

objectset: FWCHILDMODE_FWSETSTATES {FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM }
objectset: FWCHILDMODE_FWSETACTIONS {FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM }

class: ASS_FwCaenCrateSY1527_CLASS/associated
    state: OFF	!color: FwStateOKNotPhysics
        action: KILL
        action: RESET
    state: ON	!color: FwStateOKPhysics
        action: KILL
        action: RESET
    state: NO_CONTROL	!color: FwStateAttention2
    state: WA_REPAIR	!color: FwStateAttention1
        action: KILL
        action: RESET
    state: ER_REPAIR	!color: FwStateAttention2
        action: KILL
        action: RESET
    state: PWS_FAULT	!color: FwStateAttention3
        action: KILL
        action: RESET
    state: INTERLOCK	!color: FwStateAttention3
        action: KILL
        action: RESET
    state: INTERLOCK_WENT	!color: FwStateAttention2
        action: KILL
        action: RESET

object: FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER is_of_class ASS_FwCaenCrateSY1527_CLASS

objectset: FWCAENCRATESY1527_FWSETSTATES
objectset: FWCAENCRATESY1527_FWSETACTIONS

class: ASS_FwChildrenMode_CLASS/associated
    state: Complete	!color: _3DFace
    state: Incomplete	!color: FwStateAttention2
    state: IncompleteDev	!color: FwStateAttention1

object: FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWCNM is_of_class ASS_FwChildrenMode_CLASS

class: FwChildrenMode_CLASS
    state: Complete	!color: _3DFace
        when ( ( any_in FWCHILDRENMODE_FWSETSTATES in_state Incomplete ) )  move_to Incomplete
        when ( ( any_in FWCHILDRENMODE_FWSETSTATES in_state IncompleteDev ) )  move_to IncompleteDev
        when ( ( any_in FWCHILDMODE_FWSETSTATES not_in_state {Included,ExcludedPerm} ) )  move_to Incomplete
    state: Incomplete	!color: FwStateAttention2
        when (  ( ( all_in FWCHILDMODE_FWSETSTATES in_state {Included,ExcludedPerm} ) ) and
       ( ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state Incomplete ) )  )  move_to Complete
    state: IncompleteDev	!color: FwStateAttention1
        when (  ( ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state IncompleteDev ) )  ) move_to Complete
        when (  ( ( any_in FWCHILDMODE_FWSETSTATES not_in_state {Included,ExcludedPerm} ) ) or
       ( ( any_in FWCHILDRENMODE_FWSETSTATES in_state Incomplete ) )  )  move_to Incomplete

object: FMD_INFR_RACK1_FWCNM is_of_class FwChildrenMode_CLASS

objectset: FWCHILDRENMODE_FWSETSTATES {FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWCNM }

class: ASS_FwMode_CLASS/associated
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Manual
        action: Ignore
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")
        action: Manual(string OWNER = "")
        action: Ignore(string OWNER = "")
        action: ExcludeAll(string OWNER = "")
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Free(string OWNER = "")
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
        action: ReleaseAll(string OWNER = "")
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Take(string OWNER = "")
        action: Exclude(string OWNER = "")
        action: Ignore
        action: Free(string OWNER = "")
        action: ExcludeAll(string OWNER = "")
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: ReleaseAll(string OWNER = "")
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include
        action: Exclude(string OWNER = "")
        action: Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: Free(string OWNER = "")
        action: ExcludeAll(string OWNER = "")

object: FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER::FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM is_of_class ASS_FwMode_CLASS

class: FwMode_CLASS
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to InLocal
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Manual
            move_to Manual
        action: Ignore
            move_to Ignored
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Manual(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: ReleaseAll(string OWNER = "")
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Take(string OWNER = "")
            do Include(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to InManual
        action: Exclude(string OWNER = "")
            do Exclude(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Ignore
            move_to Ignored
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: ExcludeAll(string OWNER = "")
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
        action: ReleaseAll(string OWNER = "")
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include
            move_to Included
        action: Exclude(string OWNER = "")
            do Exclude(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Manual
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: ExcludeAll(string OWNER = "")
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded

object: FMD_INFR_RACK1_FWM is_of_class FwMode_CLASS

class: TOP_FmdPowerRack_CLASS
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FWCAENCRATESY1527_FWSETSTATES in_state ON ) )  move_to READY
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state WA_REPAIR ) )  move_to WA_REPAIR
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state ER_REPAIR ) )  move_to ER_REPAIR
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state {PWS_FAULT,PLC_FAULT} ) )  move_to FAULT
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state INTERLOCK ) )  move_to INTERLOCK
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state INTERLOCK_WENT ) )  move_to INTERLOCK_WENT
        action: KILL
            do KILL all_in FWCAENCRATESY1527_FWSETACTIONS
        action: RESET
            do RESET all_in FWCAENCRATESY1527_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state WA_REPAIR ) )  move_to WA_REPAIR
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state ER_REPAIR ) )  move_to ER_REPAIR
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state {PWS_FAULT,PLC_FAULT} ) )  move_to FAULT
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state INTERLOCK ) )  move_to INTERLOCK
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state INTERLOCK_WENT ) )  move_to INTERLOCK_WENT
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state OFF ) )  move_to OFF
        action: KILL
            do KILL all_in FWCAENCRATESY1527_FWSETACTIONS
        action: RESET
            do RESET all_in FWCAENCRATESY1527_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FWCAENCRATESY1527_FWSETSTATES in_state ON ) )  move_to READY
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state WA_REPAIR ) )  move_to WA_REPAIR
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state ER_REPAIR ) )  move_to ER_REPAIR
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state {PWS_FAULT,PLC_FAULT} ) )  move_to FAULT
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state INTERLOCK ) )  move_to INTERLOCK
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state INTERLOCK_WENT ) )  move_to INTERLOCK_WENT
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state OFF ) )  move_to OFF
        action: RESET
            do RESET all_in FWCAENCRATESY1527_FWSETACTIONS
    state: WA_REPAIR	!color: FwStateAttention1
        when ( ( all_in FWCAENCRATESY1527_FWSETSTATES in_state ON ) )  move_to READY
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state ER_REPAIR ) )  move_to ER_REPAIR
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state {PWS_FAULT,PLC_FAULT} ) )  move_to FAULT
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state INTERLOCK ) )  move_to INTERLOCK
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state INTERLOCK_WENT ) )  move_to INTERLOCK_WENT
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state OFF ) )  move_to OFF
        action: KILL
            do KILL all_in FWCAENCRATESY1527_FWSETACTIONS
        action: RESET
            do RESET all_in FWCAENCRATESY1527_FWSETACTIONS
    state: ER_REPAIR	!color: FwStateAttention2
        when ( ( all_in FWCAENCRATESY1527_FWSETSTATES in_state ON ) )  move_to READY
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state WA_REPAIR ) )  move_to WA_REPAIR
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state {PWS_FAULT,PLC_FAULT} ) )  move_to FAULT
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state INTERLOCK ) )  move_to INTERLOCK
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state INTERLOCK_WENT ) )  move_to INTERLOCK_WENT
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state OFF ) )  move_to OFF
        action: KILL
            do KILL all_in FWCAENCRATESY1527_FWSETACTIONS
        action: RESET
            do RESET all_in FWCAENCRATESY1527_FWSETACTIONS
    state: FAULT	!color: FwStateAttention3
        when ( ( all_in FWCAENCRATESY1527_FWSETSTATES in_state ON ) )  move_to READY
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state WA_REPAIR ) )  move_to WA_REPAIR
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state ER_REPAIR ) )  move_to ER_REPAIR
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state INTERLOCK ) )  move_to INTERLOCK
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state INTERLOCK_WENT ) )  move_to INTERLOCK_WENT
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state OFF ) )  move_to OFF
        action: KILL
            do KILL all_in FWCAENCRATESY1527_FWSETACTIONS
        action: RESET
            do RESET all_in FWCAENCRATESY1527_FWSETACTIONS
    state: INTERLOCK	!color: FwStateAttention3
        when ( ( all_in FWCAENCRATESY1527_FWSETSTATES in_state ON ) )  move_to READY
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state WA_REPAIR ) )  move_to WA_REPAIR
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state ER_REPAIR ) )  move_to ER_REPAIR
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state {PWS_FAULT,PLC_FAULT} ) )  move_to FAULT
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state INTERLOCK_WENT ) )  move_to INTERLOCK_WENT
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state OFF ) )  move_to OFF
        action: KILL
            do KILL all_in FWCAENCRATESY1527_FWSETACTIONS
        action: RESET
            do RESET all_in FWCAENCRATESY1527_FWSETACTIONS
    state: INTERLOCK_WENT	!color: FwStateAttention2
        when ( ( all_in FWCAENCRATESY1527_FWSETSTATES in_state ON ) )  move_to READY
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state WA_REPAIR ) )  move_to WA_REPAIR
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state ER_REPAIR ) )  move_to ER_REPAIR
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state {PWS_FAULT,PLC_FAULT} ) )  move_to FAULT
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state INTERLOCK ) )  move_to INTERLOCK
        when ( ( any_in FWCAENCRATESY1527_FWSETSTATES in_state OFF ) )  move_to OFF
        action: KILL
            do KILL all_in FWCAENCRATESY1527_FWSETACTIONS
        action: RESET
            do RESET all_in FWCAENCRATESY1527_FWSETACTIONS

object: FMD_INFR_RACK1 is_of_class TOP_FmdPowerRack_CLASS

