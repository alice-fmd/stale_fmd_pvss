class: ASS_FwCaenChannel_CLASS/associated
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: SWITCH_ON
        action: SET(string SetNum = "1")
        action: GO_READY
        action: GO_BEAM_TUNING
        action: RESET
    state: ON	!color: FwStateOKPhysics
        action: SWITCH_OFF
        action: SET(string SetNum = "1")
        action: GO_BEAM_TUNING
        action: GO_STANDBY
        action: RESET
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET
    state: INTERMEDIATE	!color: FwStateOKNotPhysics
        action: SWITCH_OFF
        action: SET(string SetNum = "1")
        action: GO_READY
        action: GO_STANDBY
        action: RESET
    state: RAMP_UP_ON	!color: FwStateAttention1
        action: SWITCH_OFF
        action: RESET
    state: RAMP_UP_INTER	!color: FwStateAttention1
        action: SWITCH_OFF
        action: RESET
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: SWITCH_ON
        action: RESET
    state: RAMP_DW_INTER	!color: FwStateAttention1
        action: SWITCH_ON
        action: RESET
    state: TRIPPED	!color: FwStateAttention2
        action: SET
        action: RESET
        action: SWITCH_OFF
        action: SWITCH_ON
        action: GO_READY
        action: GO_BEAM_TUNING
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET
    state: CHAN_FAULT	!color: FwStateAttention3
        action: SWITCH_ON
        action: SWITCH_OFF
        action: SET
        action: RESET
