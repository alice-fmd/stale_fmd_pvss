class: ASS_FwCaenChannelLV_CLASS/associated
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: SWITCH_ON
        action: SET(string SetNum = "1")
        action: GO_READY
        action: RESET
    state: ON	!color: FwStateOKPhysics
        action: SWITCH_OFF
        action: SET(string SetNum = "1")
        action: GO_STANDBY
        action: RESET
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET
    state: TRIPPED	!color: FwStateAttention2
        action: SET(string SetNum = "1")
        action: RESET
        action: SWITCH_ON
        action: SWITCH_OFF
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET
    state: CHAN_FAULT	!color: FwStateAttention3
        action: SWITCH_ON
        action: SWITCH_OFF
        action: SET
        action: RESET
    state: RAMP_UP	!color: FwStateAttention1
        action: SET
        action: SWITCH_OFF
        action: RESET
    state: RAMP_DW	!color: FwStateAttention1
        action: SET
        action: SWITCH_ON
        action: RESET
