class: ASS_FwFSMConfDBDUT_CLASS/associated
    state: NOT_READY	!color: FwStateOKNotPhysics
        action: LOAD(string sMode = "PHYSICS")
    state: READY	!color: FwStateOKPhysics
        action: APPLY_RECIPE
        action: LOAD(string sMode = "PHYSICS")
        action: UNLOAD
    state: ERROR	!color: FwStateAttention3
        action: RECOVER
