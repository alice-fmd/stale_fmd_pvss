class: ASS_FwMode_CLASS/associated
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Manual
        action: Ignore
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")
        action: Manual(string OWNER = "")
        action: Ignore(string OWNER = "")
        action: ExcludeAll(string OWNER = "")
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Free(string OWNER = "")
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
        action: ReleaseAll(string OWNER = "")
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Take(string OWNER = "")
        action: Exclude(string OWNER = "")
        action: Ignore
        action: Free(string OWNER = "")
        action: ExcludeAll(string OWNER = "")
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: ReleaseAll(string OWNER = "")
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include
        action: Exclude(string OWNER = "")
        action: Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: Free(string OWNER = "")
        action: ExcludeAll(string OWNER = "")
