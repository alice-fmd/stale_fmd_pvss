class: TOP_FmdPowerRack_CLASS
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( all_in FWCAENCHANNELEPC_FWSETSTATES in_state ON ) )  move_to READY
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state WA_REPAIR ) )  move_to WA_REPAIR
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state ER_REPAIR ) )  move_to ER_REPAIR
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state {PWS_FAULT,PLC_FAULT} ) )  move_to FAULT
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state INTERLOCK ) )  move_to INTERLOCK
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state INTERLOCK_WENT ) )  move_to INTERLOCK_WENT
        action: KILL
            do KILL all_in FWCAENCHANNELEPC_FWSETACTIONS
        action: RESET
            do RESET all_in FWCAENCHANNELEPC_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state WA_REPAIR ) )  move_to WA_REPAIR
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state ER_REPAIR ) )  move_to ER_REPAIR
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state {PWS_FAULT,PLC_FAULT} ) )  move_to FAULT
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state INTERLOCK ) )  move_to INTERLOCK
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state INTERLOCK_WENT ) )  move_to INTERLOCK_WENT
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state OFF ) )  move_to OFF
        action: KILL
            do KILL all_in FWCAENCHANNELEPC_FWSETACTIONS
        action: RESET
            do RESET all_in FWCAENCHANNELEPC_FWSETACTIONS
    state: NO_CONTROL	!color: FwStateAttention2
        when ( ( all_in FWCAENCHANNELEPC_FWSETSTATES in_state ON ) )  move_to READY
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state WA_REPAIR ) )  move_to WA_REPAIR
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state ER_REPAIR ) )  move_to ER_REPAIR
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state {PWS_FAULT,PLC_FAULT} ) )  move_to FAULT
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state INTERLOCK ) )  move_to INTERLOCK
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state INTERLOCK_WENT ) )  move_to INTERLOCK_WENT
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state OFF ) )  move_to OFF
        action: RESET
            do RESET all_in FWCAENCHANNELEPC_FWSETACTIONS
    state: WA_REPAIR	!color: FwStateAttention1
        when ( ( all_in FWCAENCHANNELEPC_FWSETSTATES in_state ON ) )  move_to READY
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state ER_REPAIR ) )  move_to ER_REPAIR
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state {PWS_FAULT,PLC_FAULT} ) )  move_to FAULT
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state INTERLOCK ) )  move_to INTERLOCK
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state INTERLOCK_WENT ) )  move_to INTERLOCK_WENT
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state OFF ) )  move_to OFF
        action: KILL
            do KILL all_in FWCAENCHANNELEPC_FWSETACTIONS
        action: RESET
            do RESET all_in FWCAENCHANNELEPC_FWSETACTIONS
    state: ER_REPAIR	!color: FwStateAttention2
        when ( ( all_in FWCAENCHANNELEPC_FWSETSTATES in_state ON ) )  move_to READY
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state WA_REPAIR ) )  move_to WA_REPAIR
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state {PWS_FAULT,PLC_FAULT} ) )  move_to FAULT
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state INTERLOCK ) )  move_to INTERLOCK
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state INTERLOCK_WENT ) )  move_to INTERLOCK_WENT
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state OFF ) )  move_to OFF
        action: KILL
            do KILL all_in FWCAENCHANNELEPC_FWSETACTIONS
        action: RESET
            do RESET all_in FWCAENCHANNELEPC_FWSETACTIONS
    state: FAULT	!color: FwStateAttention3
        when ( ( all_in FWCAENCHANNELEPC_FWSETSTATES in_state ON ) )  move_to READY
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state WA_REPAIR ) )  move_to WA_REPAIR
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state ER_REPAIR ) )  move_to ER_REPAIR
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state INTERLOCK ) )  move_to INTERLOCK
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state INTERLOCK_WENT ) )  move_to INTERLOCK_WENT
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state OFF ) )  move_to OFF
        action: KILL
            do KILL all_in FWCAENCHANNELEPC_FWSETACTIONS
        action: RESET
            do RESET all_in FWCAENCHANNELEPC_FWSETACTIONS
    state: INTERLOCK	!color: FwStateAttention3
        when ( ( all_in FWCAENCHANNELEPC_FWSETSTATES in_state ON ) )  move_to READY
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state WA_REPAIR ) )  move_to WA_REPAIR
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state ER_REPAIR ) )  move_to ER_REPAIR
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state {PWS_FAULT,PLC_FAULT} ) )  move_to FAULT
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state INTERLOCK_WENT ) )  move_to INTERLOCK_WENT
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state OFF ) )  move_to OFF
        action: KILL
            do KILL all_in FWCAENCHANNELEPC_FWSETACTIONS
        action: RESET
            do RESET all_in FWCAENCHANNELEPC_FWSETACTIONS
    state: INTERLOCK_WENT	!color: FwStateAttention2
        when ( ( all_in FWCAENCHANNELEPC_FWSETSTATES in_state ON ) )  move_to READY
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state WA_REPAIR ) )  move_to WA_REPAIR
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state ER_REPAIR ) )  move_to ER_REPAIR
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state {PWS_FAULT,PLC_FAULT} ) )  move_to FAULT
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state INTERLOCK ) )  move_to INTERLOCK
        when ( ( any_in FWCAENCHANNELEPC_FWSETSTATES in_state OFF ) )  move_to OFF
        action: KILL
            do KILL all_in FWCAENCHANNELEPC_FWSETACTIONS
        action: RESET
            do RESET all_in FWCAENCHANNELEPC_FWSETACTIONS

object: FMD_INFR_RACK2 is_of_class TOP_FmdPowerRack_CLASS

class: FwChildrenMode_CLASS
    state: Complete	!color: _3DFace
        when ( ( any_in FWDEVMODE_FWSETSTATES in_state DISABLED ) )  move_to IncompleteDev
    state: Incomplete	!color: FwStateAttention2
    state: IncompleteDev	!color: FwStateAttention1
        when (  ( ( all_in FWDEVMODE_FWSETSTATES not_in_state DISABLED ) )  ) move_to Complete

object: FMD_INFR_RACK2_FWCNM is_of_class FwChildrenMode_CLASS

class: FwMode_CLASS
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")
            move_to InLocal
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            move_to Included
        action: Manual
            move_to Manual
        action: Ignore
            move_to Ignored
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")
            move_to Excluded
        action: Manual(string OWNER = "")
            move_to Manual
        action: Ignore(string OWNER = "")
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            move_to Included
        action: Free(string OWNER = "")
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
            move_to Excluded
        action: ReleaseAll(string OWNER = "")
            move_to Excluded
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            move_to Included
        action: Take(string OWNER = "")
            move_to InManual
        action: Exclude(string OWNER = "")
            move_to Excluded
        action: Ignore
            move_to Ignored
        action: Free(string OWNER = "")
            move_to Excluded
        action: ExcludeAll(string OWNER = "")
            move_to Excluded
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: ReleaseAll(string OWNER = "")
            move_to Excluded
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include
            move_to Included
        action: Exclude(string OWNER = "")
            move_to Excluded
        action: Manual
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: Free(string OWNER = "")
            move_to Included
        action: ExcludeAll(string OWNER = "")
            move_to Excluded

object: FMD_INFR_RACK2_FWM is_of_class FwMode_CLASS

class: FwCaenChannelEPC_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWCAENCHANNELEPC_FWSETSTATES
            remove &VAL_OF_Device from FWCAENCHANNELEPC_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWCAENCHANNELEPC_FWSETSTATES
            insert &VAL_OF_Device in FWCAENCHANNELEPC_FWSETACTIONS
            move_to READY

object: FwCaenChannelEPC_FWDM is_of_class FwCaenChannelEPC_FwDevMode_CLASS


class: FwCaenChannelEPC_CLASS/associated
    state: OFF	!color: FwStateOKPhysics
        action: SWITCH_ON
        action: RESET
    state: ON	!color: FwStateOKPhysics
        action: SWITCH_OFF
        action: RESET
    state: NO_CONTROL	!color: FwStateAttention2
    state: WA_REPAIR	!color: FwStateAttention1
        action: SWITCH_ON
        action: SWITCH_OFF
        action: RESET
    state: ER_REPAIR	!color: FwStateAttention2
        action: SWITCH_ON
        action: SWITCH_OFF
        action: RESET
    state: PWS_FAULT	!color: FwStateAttention3
        action: RESET
    state: INTERLOCK	!color: 
        action: SWITCH_ON
        action: SWITCH_OFF
        action: RESET
        action: KILL

object: FMD_DCS:FMD_INFR_RACK2:LVPS_CONTROLLER0 is_of_class FwCaenChannelEPC_CLASS

object: FMD_DCS:FMD_INFR_RACK2:LVPS_CONTROLLER1 is_of_class FwCaenChannelEPC_CLASS

objectset: FWCAENCHANNELEPC_FWSETSTATES {FMD_DCS:FMD_INFR_RACK2:LVPS_CONTROLLER0,
	FMD_DCS:FMD_INFR_RACK2:LVPS_CONTROLLER1 }
objectset: FWCAENCHANNELEPC_FWSETACTIONS {FMD_DCS:FMD_INFR_RACK2:LVPS_CONTROLLER0,
	FMD_DCS:FMD_INFR_RACK2:LVPS_CONTROLLER1 }

class: FwDevMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWDEVMODE_FWSETSTATES
            remove &VAL_OF_Device from FWDEVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWDEVMODE_FWSETSTATES
            insert &VAL_OF_Device in FWDEVMODE_FWSETACTIONS
            move_to READY

object: FwDevMode_FWDM is_of_class FwDevMode_FwDevMode_CLASS


class: FwDevMode_CLASS/associated
    state: ENABLED	!color: FwStateOKPhysics
    state: DISABLED	!color: FwStateAttention1

object: FMD_INFR_RACK2_FWDM is_of_class FwDevMode_CLASS

objectset: FWDEVMODE_FWSETSTATES {FMD_INFR_RACK2_FWDM }
objectset: FWDEVMODE_FWSETACTIONS {FMD_INFR_RACK2_FWDM }

