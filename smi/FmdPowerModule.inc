class: ASS_FmdPowerModule_CLASS/associated
    state: MIXED	!color: 
        action: NV_SHUTOFF
        action: RESET
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_STANDBY
        action: RESET
    state: DOWNLOADING	!color: FwStateAttention1
        action: RESET
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        action: RESET
        action: GO_READY
        action: GO_BEAM_TUNING
    state: MOVING_BEAM_TUNING	!color: FwStateAttention1
        action: RESET
        action: NV_START_BT
    state: MOVING_READY	!color: FwStateAttention1
        action: RESET
        action: NV_START_RD
    state: READY	!color: FwStateOKPhysics
        action: RESET
        action: GO_STANDBY
        action: GO_BEAM_TUNING
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        action: RESET
        action: GO_STANDBY
        action: GO_READY
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        action: RESET
        action: NV_SHUTOFF_ST
    state: WA_REPAIR	!color: FwStateAttention1
        action: RESET
        action: ACKNOLEDGE
    state: ER_REPAIR	!color: FwStateAttention2
        action: RESET
        action: ACKNOLEDGE
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET
    state: SYS_FAULT	!color: FwStateAttention3
        action: RESET
