class: FMD1_MOD_FwChildMode_CLASS

    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD1_MOD::FMD1_MOD_FWM not_in_state Excluded ) then
                if ( FMD1_MOD::FMD1_MOD_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD1_MOD::FMD1_MOD_FWM
                    insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETSTATES
                    insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD1_MOD::FMD1_MOD_FWM
                insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETSTATES
                insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Included
        action: Manual
            do Manual FMD1_MOD::FMD1_MOD_FWM
            insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETSTATES
            remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
            move_to Manual
        action: Ignore
            do Ignore FMD1_MOD::FMD1_MOD_FWM
            insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETACTIONS
            remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
            move_to Ignored
        action: LockOut
            move_to LockedOut
        action: Exclude
            do Exclude FMD1_MOD::FMD1_MOD_FWM
            remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
            remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
            move_to Excluded
    state: Included	!color: FwStateOKPhysics
        when ( FMD1_MOD::FMD1_MOD_FWM in_state Excluded )  move_to EXCLUDED
        when ( FMD1_MOD::FMD1_MOD_FWM in_state Ignored )  move_to IGNORED
        when ( FMD1_MOD::FMD1_MOD_FWM in_state Manual )  move_to MANUAL
        action: Exclude(string OWNER = "")
            if ( FMD1_MOD::FMD1_MOD_FWM not_in_state Included ) then
                if ( FMD1_MOD::FMD1_MOD_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
                remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
            insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETSTATES
            remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")
            do Ignore(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
            insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETACTIONS
            remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            if ( FMD1_MOD::FMD1_MOD_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD1_MOD::FMD1_MOD_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
                remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD1_MOD::FMD1_MOD_FWM
            insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETSTATES
            insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD1_MOD::FMD1_MOD_FWM
        action: ExcludePerm(string OWNER = "")
            if ( FMD1_MOD::FMD1_MOD_FWM not_in_state Included ) then
                if ( FMD1_MOD::FMD1_MOD_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
                remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to ExcludedPerm
    state: Manual	!color: FwStateOKNotPhysics
        when ( FMD1_MOD::FMD1_MOD_FWM in_state Included )  move_to INCLUDED
        when ( FMD1_MOD::FMD1_MOD_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD1_MOD::FMD1_MOD_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD1_MOD::FMD1_MOD_FWM
              insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETSTATES
              insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Manual
        action: Exclude(string OWNER = "")
            if ( FMD1_MOD::FMD1_MOD_FWM not_in_state Included ) then
                if ( FMD1_MOD::FMD1_MOD_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
                remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Manual
        action: Ignore
            do Ignore FMD1_MOD::FMD1_MOD_FWM
            insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETACTIONS
            remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
            move_to Manual
        action: ExcludeAll(string OWNER = "")
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
            if ( FMD1_MOD::FMD1_MOD_FWM not_in_state InManual ) then
              do ExcludeAll(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
              remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
              remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Manual
    state: Ignored	!color: FwStateOKNotPhysics
        when ( FMD1_MOD::FMD1_MOD_FWM in_state Included )  move_to INCLUDED
        when ( FMD1_MOD::FMD1_MOD_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD1_MOD::FMD1_MOD_FWM
            insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETSTATES
            insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETACTIONS
            move_to Included
        action: Exclude(string OWNER = "")
            if ( FMD1_MOD::FMD1_MOD_FWM not_in_state Included ) then
                if ( FMD1_MOD::FMD1_MOD_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
                remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
            insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETSTATES
            remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD1_MOD::FMD1_MOD_FWM
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")
            if ( FMD1_MOD::FMD1_MOD_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD1_MOD::FMD1_MOD_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD1_MOD::FMD1_MOD_FWM
                remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD1_MOD::FMD1_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut
            move_to Excluded
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD1_MOD::FMD1_MOD_FWM not_in_state Excluded ) then
                if ( FMD1_MOD::FMD1_MOD_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD1_MOD::FMD1_MOD_FWM
                    insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETSTATES
                    insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD1_MOD::FMD1_MOD_FWM
                insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETSTATES
                insert FMD1_MOD::FMD1_MOD in FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Included

object: FMD1_MOD_FWM is_of_class FMD1_MOD_FwChildMode_CLASS

class: FMD2_MOD_FwChildMode_CLASS

    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD2_MOD::FMD2_MOD_FWM not_in_state Excluded ) then
                if ( FMD2_MOD::FMD2_MOD_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD2_MOD::FMD2_MOD_FWM
                    insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETSTATES
                    insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD2_MOD::FMD2_MOD_FWM
                insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETSTATES
                insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Included
        action: Manual
            do Manual FMD2_MOD::FMD2_MOD_FWM
            insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETSTATES
            remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
            move_to Manual
        action: Ignore
            do Ignore FMD2_MOD::FMD2_MOD_FWM
            insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETACTIONS
            remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
            move_to Ignored
        action: LockOut
            move_to LockedOut
        action: Exclude
            do Exclude FMD2_MOD::FMD2_MOD_FWM
            remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
            remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
            move_to Excluded
    state: Included	!color: FwStateOKPhysics
        when ( FMD2_MOD::FMD2_MOD_FWM in_state Excluded )  move_to EXCLUDED
        when ( FMD2_MOD::FMD2_MOD_FWM in_state Ignored )  move_to IGNORED
        when ( FMD2_MOD::FMD2_MOD_FWM in_state Manual )  move_to MANUAL
        action: Exclude(string OWNER = "")
            if ( FMD2_MOD::FMD2_MOD_FWM not_in_state Included ) then
                if ( FMD2_MOD::FMD2_MOD_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
                remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
            insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETSTATES
            remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")
            do Ignore(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
            insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETACTIONS
            remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            if ( FMD2_MOD::FMD2_MOD_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD2_MOD::FMD2_MOD_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
                remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD2_MOD::FMD2_MOD_FWM
            insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETSTATES
            insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD2_MOD::FMD2_MOD_FWM
        action: ExcludePerm(string OWNER = "")
            if ( FMD2_MOD::FMD2_MOD_FWM not_in_state Included ) then
                if ( FMD2_MOD::FMD2_MOD_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
                remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to ExcludedPerm
    state: Manual	!color: FwStateOKNotPhysics
        when ( FMD2_MOD::FMD2_MOD_FWM in_state Included )  move_to INCLUDED
        when ( FMD2_MOD::FMD2_MOD_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD2_MOD::FMD2_MOD_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD2_MOD::FMD2_MOD_FWM
              insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETSTATES
              insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Manual
        action: Exclude(string OWNER = "")
            if ( FMD2_MOD::FMD2_MOD_FWM not_in_state Included ) then
                if ( FMD2_MOD::FMD2_MOD_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
                remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Manual
        action: Ignore
            do Ignore FMD2_MOD::FMD2_MOD_FWM
            insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETACTIONS
            remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
            move_to Manual
        action: ExcludeAll(string OWNER = "")
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
            if ( FMD2_MOD::FMD2_MOD_FWM not_in_state InManual ) then
              do ExcludeAll(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
              remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
              remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Manual
    state: Ignored	!color: FwStateOKNotPhysics
        when ( FMD2_MOD::FMD2_MOD_FWM in_state Included )  move_to INCLUDED
        when ( FMD2_MOD::FMD2_MOD_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD2_MOD::FMD2_MOD_FWM
            insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETSTATES
            insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETACTIONS
            move_to Included
        action: Exclude(string OWNER = "")
            if ( FMD2_MOD::FMD2_MOD_FWM not_in_state Included ) then
                if ( FMD2_MOD::FMD2_MOD_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
                remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
            insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETSTATES
            remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD2_MOD::FMD2_MOD_FWM
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")
            if ( FMD2_MOD::FMD2_MOD_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD2_MOD::FMD2_MOD_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD2_MOD::FMD2_MOD_FWM
                remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD2_MOD::FMD2_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut
            move_to Excluded
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD2_MOD::FMD2_MOD_FWM not_in_state Excluded ) then
                if ( FMD2_MOD::FMD2_MOD_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD2_MOD::FMD2_MOD_FWM
                    insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETSTATES
                    insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD2_MOD::FMD2_MOD_FWM
                insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETSTATES
                insert FMD2_MOD::FMD2_MOD in FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Included

object: FMD2_MOD_FWM is_of_class FMD2_MOD_FwChildMode_CLASS

class: FMD3_MOD_FwChildMode_CLASS

    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD3_MOD::FMD3_MOD_FWM not_in_state Excluded ) then
                if ( FMD3_MOD::FMD3_MOD_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_MOD::FMD3_MOD_FWM
                    insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETSTATES
                    insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_MOD::FMD3_MOD_FWM
                insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETSTATES
                insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Included
        action: Manual
            do Manual FMD3_MOD::FMD3_MOD_FWM
            insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETSTATES
            remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
            move_to Manual
        action: Ignore
            do Ignore FMD3_MOD::FMD3_MOD_FWM
            insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETACTIONS
            remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
            move_to Ignored
        action: LockOut
            move_to LockedOut
        action: Exclude
            do Exclude FMD3_MOD::FMD3_MOD_FWM
            remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
            remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
            move_to Excluded
    state: Included	!color: FwStateOKPhysics
        when ( FMD3_MOD::FMD3_MOD_FWM in_state Excluded )  move_to EXCLUDED
        when ( FMD3_MOD::FMD3_MOD_FWM in_state Ignored )  move_to IGNORED
        when ( FMD3_MOD::FMD3_MOD_FWM in_state Manual )  move_to MANUAL
        action: Exclude(string OWNER = "")
            if ( FMD3_MOD::FMD3_MOD_FWM not_in_state Included ) then
                if ( FMD3_MOD::FMD3_MOD_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
                remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
            insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETSTATES
            remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")
            do Ignore(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
            insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETACTIONS
            remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            if ( FMD3_MOD::FMD3_MOD_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD3_MOD::FMD3_MOD_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
                remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_MOD::FMD3_MOD_FWM
            insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETSTATES
            insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_MOD::FMD3_MOD_FWM
        action: ExcludePerm(string OWNER = "")
            if ( FMD3_MOD::FMD3_MOD_FWM not_in_state Included ) then
                if ( FMD3_MOD::FMD3_MOD_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
                remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to ExcludedPerm
    state: Manual	!color: FwStateOKNotPhysics
        when ( FMD3_MOD::FMD3_MOD_FWM in_state Included )  move_to INCLUDED
        when ( FMD3_MOD::FMD3_MOD_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD3_MOD::FMD3_MOD_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_MOD::FMD3_MOD_FWM
              insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETSTATES
              insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Manual
        action: Exclude(string OWNER = "")
            if ( FMD3_MOD::FMD3_MOD_FWM not_in_state Included ) then
                if ( FMD3_MOD::FMD3_MOD_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
                remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Manual
        action: Ignore
            do Ignore FMD3_MOD::FMD3_MOD_FWM
            insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETACTIONS
            remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
            move_to Manual
        action: ExcludeAll(string OWNER = "")
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
            if ( FMD3_MOD::FMD3_MOD_FWM not_in_state InManual ) then
              do ExcludeAll(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
              remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
              remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Manual
    state: Ignored	!color: FwStateOKNotPhysics
        when ( FMD3_MOD::FMD3_MOD_FWM in_state Included )  move_to INCLUDED
        when ( FMD3_MOD::FMD3_MOD_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_MOD::FMD3_MOD_FWM
            insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETSTATES
            insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETACTIONS
            move_to Included
        action: Exclude(string OWNER = "")
            if ( FMD3_MOD::FMD3_MOD_FWM not_in_state Included ) then
                if ( FMD3_MOD::FMD3_MOD_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
                remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
            insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETSTATES
            remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_MOD::FMD3_MOD_FWM
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")
            if ( FMD3_MOD::FMD3_MOD_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD3_MOD::FMD3_MOD_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
                    remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD3_MOD::FMD3_MOD_FWM
                remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETSTATES
                remove FMD3_MOD::FMD3_MOD from FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut
            move_to Excluded
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD3_MOD::FMD3_MOD_FWM not_in_state Excluded ) then
                if ( FMD3_MOD::FMD3_MOD_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_MOD::FMD3_MOD_FWM
                    insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETSTATES
                    insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_MOD::FMD3_MOD_FWM
                insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETSTATES
                insert FMD3_MOD::FMD3_MOD in FMDPOWERMODULE_FWSETACTIONS
            endif
            move_to Included

object: FMD3_MOD_FWM is_of_class FMD3_MOD_FwChildMode_CLASS

class: FMD_INFR_RACK1_FwChildMode_CLASS

    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM not_in_state Excluded ) then
                if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                    insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETSTATES
                    insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETSTATES
                insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Included
        action: Manual
            do Manual FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
            insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETSTATES
            remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
            move_to Manual
        action: Ignore
            do Ignore FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
            insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETACTIONS
            remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
            move_to Ignored
        action: LockOut
            move_to LockedOut
        action: Exclude
            do Exclude FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
            remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
            remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
            move_to Excluded
    state: Included	!color: FwStateOKPhysics
        when ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM in_state Excluded )  move_to EXCLUDED
        when ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM in_state Ignored )  move_to IGNORED
        when ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM in_state Manual )  move_to MANUAL
        action: Exclude(string OWNER = "")
            if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM not_in_state Included ) then
                if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
                remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
            insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETSTATES
            remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")
            do Ignore(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
            insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETACTIONS
            remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
                remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
            insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETSTATES
            insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
        action: ExcludePerm(string OWNER = "")
            if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM not_in_state Included ) then
                if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
                remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
            endif
            move_to ExcludedPerm
    state: Manual	!color: FwStateOKNotPhysics
        when ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM in_state Included )  move_to INCLUDED
        when ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
              insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETSTATES
              insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Manual
        action: Exclude(string OWNER = "")
            if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM not_in_state Included ) then
                if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
                remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Manual
        action: Ignore
            do Ignore FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
            insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETACTIONS
            remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
            move_to Manual
        action: ExcludeAll(string OWNER = "")
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
            if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM not_in_state InManual ) then
              do ExcludeAll(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
              remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
              remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Manual
    state: Ignored	!color: FwStateOKNotPhysics
        when ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM in_state Included )  move_to INCLUDED
        when ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
            insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETSTATES
            insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETACTIONS
            move_to Included
        action: Exclude(string OWNER = "")
            if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM not_in_state Included ) then
                if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
                remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
            insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETSTATES
            remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")
            if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETSTATES
                remove FMD_INFR_RACK1::FMD_INFR_RACK1 from FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut
            move_to Excluded
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM not_in_state Excluded ) then
                if ( FMD_INFR_RACK1::FMD_INFR_RACK1_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                    insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETSTATES
                    insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK1::FMD_INFR_RACK1_FWM
                insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETSTATES
                insert FMD_INFR_RACK1::FMD_INFR_RACK1 in FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Included

object: FMD_INFR_RACK1_FWM is_of_class FMD_INFR_RACK1_FwChildMode_CLASS

class: FMD_INFR_RACK2_FwChildMode_CLASS

    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM not_in_state Excluded ) then
                if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                    insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETSTATES
                    insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETSTATES
                insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Included
        action: Manual
            do Manual FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
            insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETSTATES
            remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
            move_to Manual
        action: Ignore
            do Ignore FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
            insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETACTIONS
            remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
            move_to Ignored
        action: LockOut
            move_to LockedOut
        action: Exclude
            do Exclude FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
            remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
            remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
            move_to Excluded
    state: Included	!color: FwStateOKPhysics
        when ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM in_state Excluded )  move_to EXCLUDED
        when ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM in_state Ignored )  move_to IGNORED
        when ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM in_state Manual )  move_to MANUAL
        action: Exclude(string OWNER = "")
            if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM not_in_state Included ) then
                if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
                remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
            insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETSTATES
            remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")
            do Ignore(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
            insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETACTIONS
            remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
                remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
            insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETSTATES
            insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
        action: ExcludePerm(string OWNER = "")
            if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM not_in_state Included ) then
                if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
                remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
            endif
            move_to ExcludedPerm
    state: Manual	!color: FwStateOKNotPhysics
        when ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM in_state Included )  move_to INCLUDED
        when ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
              insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETSTATES
              insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Manual
        action: Exclude(string OWNER = "")
            if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM not_in_state Included ) then
                if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
                remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Manual
        action: Ignore
            do Ignore FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
            insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETACTIONS
            remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
            move_to Manual
        action: ExcludeAll(string OWNER = "")
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
            if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM not_in_state InManual ) then
              do ExcludeAll(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
              remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
              remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Manual
    state: Ignored	!color: FwStateOKNotPhysics
        when ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM in_state Included )  move_to INCLUDED
        when ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
            insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETSTATES
            insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETACTIONS
            move_to Included
        action: Exclude(string OWNER = "")
            if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM not_in_state Included ) then
                if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
                remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
            insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETSTATES
            remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")
            if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
                    remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETSTATES
                remove FMD_INFR_RACK2::FMD_INFR_RACK2 from FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut
            move_to Excluded
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM not_in_state Excluded ) then
                if ( FMD_INFR_RACK2::FMD_INFR_RACK2_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                    insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETSTATES
                    insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD_INFR_RACK2::FMD_INFR_RACK2_FWM
                insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETSTATES
                insert FMD_INFR_RACK2::FMD_INFR_RACK2 in FMDPOWERRACK_FWSETACTIONS
            endif
            move_to Included

object: FMD_INFR_RACK2_FWM is_of_class FMD_INFR_RACK2_FwChildMode_CLASS

objectset: FWCHILDMODE_FWSETSTATES {FMD1_MOD_FWM,
	FMD2_MOD_FWM,
	FMD3_MOD_FWM,
	FMD_INFR_RACK1_FWM,
	FMD_INFR_RACK2_FWM }
objectset: FWCHILDMODE_FWSETACTIONS {FMD1_MOD_FWM,
	FMD2_MOD_FWM,
	FMD3_MOD_FWM,
	FMD_INFR_RACK1_FWM,
	FMD_INFR_RACK2_FWM }

class: ASS_FmdPowerModule_CLASS/associated
    state: MIXED	!color: 
        action: NV_SHUTOFF
        action: RESET
    state: OFF	!color: FwStateOKNotPhysics
        action: GO_STANDBY
        action: RESET
    state: DOWNLOADING	!color: FwStateAttention1
        action: RESET
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        action: RESET
        action: GO_READY
        action: GO_BEAM_TUNING
    state: MOVING_BEAM_TUNING	!color: FwStateAttention1
        action: RESET
        action: NV_START_BT
    state: MOVING_READY	!color: FwStateAttention1
        action: RESET
        action: NV_START_RD
    state: READY	!color: FwStateOKPhysics
        action: RESET
        action: GO_STANDBY
        action: GO_BEAM_TUNING
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        action: RESET
        action: GO_STANDBY
        action: GO_READY
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        action: RESET
        action: NV_SHUTOFF_ST
    state: WA_REPAIR	!color: FwStateAttention1
        action: RESET
        action: ACKNOLEDGE
    state: ER_REPAIR	!color: FwStateAttention2
        action: RESET
        action: ACKNOLEDGE
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET
    state: SYS_FAULT	!color: FwStateAttention3
        action: RESET

object: FMD1_MOD::FMD1_MOD is_of_class ASS_FmdPowerModule_CLASS

object: FMD2_MOD::FMD2_MOD is_of_class ASS_FmdPowerModule_CLASS

object: FMD3_MOD::FMD3_MOD is_of_class ASS_FmdPowerModule_CLASS

objectset: FMDPOWERMODULE_FWSETSTATES
objectset: FMDPOWERMODULE_FWSETACTIONS

class: ASS_FwChildrenMode_CLASS/associated
    state: Complete	!color: _3DFace
    state: Incomplete	!color: FwStateAttention2
    state: IncompleteDev	!color: FwStateAttention1

object: FMD1_MOD::FMD1_MOD_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: FMD2_MOD::FMD2_MOD_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: FMD3_MOD::FMD3_MOD_FWCNM is_of_class ASS_FwChildrenMode_CLASS

class: FwChildrenMode_CLASS
    state: Complete	!color: _3DFace
        when ( ( any_in FWCHILDRENMODE_FWSETSTATES in_state Incomplete ) )  move_to Incomplete
        when ( ( any_in FWCHILDRENMODE_FWSETSTATES in_state IncompleteDev ) )  move_to IncompleteDev
        when ( ( any_in FWCHILDMODE_FWSETSTATES not_in_state {Included,ExcludedPerm} ) )  move_to Incomplete
    state: Incomplete	!color: FwStateAttention2
        when (  ( ( all_in FWCHILDMODE_FWSETSTATES in_state {Included,ExcludedPerm} ) ) and
       ( ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state Incomplete ) )  )  move_to Complete
    state: IncompleteDev	!color: FwStateAttention1
        when (  ( ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state IncompleteDev ) )  ) move_to Complete
        when (  ( ( any_in FWCHILDMODE_FWSETSTATES not_in_state {Included,ExcludedPerm} ) ) or
       ( ( any_in FWCHILDRENMODE_FWSETSTATES in_state Incomplete ) )  )  move_to Incomplete

object: FMD_DCS_FWCNM is_of_class FwChildrenMode_CLASS

object: FMD_INFR_RACK1::FMD_INFR_RACK1_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: FMD_INFR_RACK2::FMD_INFR_RACK2_FWCNM is_of_class ASS_FwChildrenMode_CLASS

objectset: FWCHILDRENMODE_FWSETSTATES {FMD1_MOD::FMD1_MOD_FWCNM,
	FMD2_MOD::FMD2_MOD_FWCNM,
	FMD3_MOD::FMD3_MOD_FWCNM,
	FMD_INFR_RACK1::FMD_INFR_RACK1_FWCNM,
	FMD_INFR_RACK2::FMD_INFR_RACK2_FWCNM }

class: ASS_FwMode_CLASS/associated
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Manual
        action: Ignore
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")
        action: Manual(string OWNER = "")
        action: Ignore(string OWNER = "")
        action: ExcludeAll(string OWNER = "")
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Free(string OWNER = "")
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
        action: ReleaseAll(string OWNER = "")
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Take(string OWNER = "")
        action: Exclude(string OWNER = "")
        action: Ignore
        action: Free(string OWNER = "")
        action: ExcludeAll(string OWNER = "")
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: ReleaseAll(string OWNER = "")
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include
        action: Exclude(string OWNER = "")
        action: Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: Free(string OWNER = "")
        action: ExcludeAll(string OWNER = "")

object: FMD1_MOD::FMD1_MOD_FWM is_of_class ASS_FwMode_CLASS

object: FMD2_MOD::FMD2_MOD_FWM is_of_class ASS_FwMode_CLASS

object: FMD3_MOD::FMD3_MOD_FWM is_of_class ASS_FwMode_CLASS

class: FwMode_CLASS
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to InLocal
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Manual
            move_to Manual
        action: Ignore
            move_to Ignored
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Manual(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: ReleaseAll(string OWNER = "")
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Take(string OWNER = "")
            do Include(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to InManual
        action: Exclude(string OWNER = "")
            do Exclude(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Ignore
            move_to Ignored
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: ExcludeAll(string OWNER = "")
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
        action: ReleaseAll(string OWNER = "")
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include
            move_to Included
        action: Exclude(string OWNER = "")
            do Exclude(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Manual
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: ExcludeAll(string OWNER = "")
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded

object: FMD_DCS_FWM is_of_class FwMode_CLASS

object: FMD_INFR_RACK1::FMD_INFR_RACK1_FWM is_of_class ASS_FwMode_CLASS

object: FMD_INFR_RACK2::FMD_INFR_RACK2_FWM is_of_class ASS_FwMode_CLASS

class: TOP_FmdHVLVSubSystem_CLASS
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMDPOWERMODULE_FWSETSTATES in_state SYS_FAULT ) ) or ( ( any_in FMDPOWERRACK_FWSETSTATES in_state FAULT ) )  )  move_to ERROR
        when ( ( any_in FMDPOWERMODULE_FWSETSTATES not_in_state READY ) )  move_to NOT_READY
        action: GO_READY
            do GO_READY all_in FMDPOWERMODULE_FWSETACTIONS
        action: GO_STANDBY
            do GO_STANDBY all_in FMDPOWERMODULE_FWSETACTIONS
        action: GO_BEAMTUNING
            do GO_BEAM_TUNING all_in FMDPOWERMODULE_FWSETACTIONS
    state: NOT_READY	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMDPOWERMODULE_FWSETSTATES in_state SYS_FAULT ) ) or ( ( any_in FMDPOWERRACK_FWSETSTATES in_state FAULT ) )  )  move_to ERROR
        when (  ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state READY ) ) or ( ( all_in FMDPOWERMODULE_FWSETSTATES in_state BEAM_TUNING ) )  )  move_to READY
        action: GO_READY
            do GO_READY all_in FMDPOWERMODULE_FWSETACTIONS
        action: GO_STANDBY
            do GO_STANDBY all_in FMDPOWERMODULE_FWSETACTIONS
        action: GO_BEAMTUNING
            do GO_BEAM_TUNING all_in FMDPOWERMODULE_FWSETACTIONS
    state: ERROR	!color: FwStateAttention3
        when (  ( ( all_in FMDPOWERMODULE_FWSETSTATES not_in_state SYS_FAULT ) ) and ( ( all_in FMDPOWERRACK_FWSETSTATES not_in_state FAULT ) )  )  move_to NOT_READY

object: FMD_DCS is_of_class TOP_FmdHVLVSubSystem_CLASS

class: ASS_FmdPowerRack_CLASS/associated
    state: OFF	!color: FwStateOKNotPhysics
        action: KILL
        action: RESET
    state: READY	!color: FwStateOKPhysics
        action: KILL
        action: RESET
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET
    state: WA_REPAIR	!color: FwStateAttention1
        action: KILL
        action: RESET
    state: ER_REPAIR	!color: FwStateAttention2
        action: KILL
        action: RESET
    state: FAULT	!color: FwStateAttention3
        action: KILL
        action: RESET
    state: INTERLOCK	!color: FwStateAttention3
        action: KILL
        action: RESET
    state: INTERLOCK_WENT	!color: FwStateAttention2
        action: KILL
        action: RESET

object: FMD_INFR_RACK1::FMD_INFR_RACK1 is_of_class ASS_FmdPowerRack_CLASS

object: FMD_INFR_RACK2::FMD_INFR_RACK2 is_of_class ASS_FmdPowerRack_CLASS

objectset: FMDPOWERRACK_FWSETSTATES
objectset: FMDPOWERRACK_FWSETACTIONS

