class: FwChildrenMode_CLASS
    state: Complete	!color: _3DFace
        when ( ( any_in FWDEVMODE_FWSETSTATES in_state DISABLED ) )  move_to IncompleteDev
    state: Incomplete	!color: FwStateAttention2
    state: IncompleteDev	!color: FwStateAttention1
        when (  ( ( all_in FWDEVMODE_FWSETSTATES not_in_state DISABLED ) )  ) move_to Complete

object: FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWCNM is_of_class FwChildrenMode_CLASS

class: FwMode_CLASS
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")
            move_to InLocal
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            move_to Included
        action: Manual
            move_to Manual
        action: Ignore
            move_to Ignored
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")
            move_to Excluded
        action: Manual(string OWNER = "")
            move_to Manual
        action: Ignore(string OWNER = "")
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            move_to Included
        action: Free(string OWNER = "")
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
            move_to Excluded
        action: ReleaseAll(string OWNER = "")
            move_to Excluded
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            move_to Included
        action: Take(string OWNER = "")
            move_to InManual
        action: Exclude(string OWNER = "")
            move_to Excluded
        action: Ignore
            move_to Ignored
        action: Free(string OWNER = "")
            move_to Excluded
        action: ExcludeAll(string OWNER = "")
            move_to Excluded
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: ReleaseAll(string OWNER = "")
            move_to Excluded
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include
            move_to Included
        action: Exclude(string OWNER = "")
            move_to Excluded
        action: Manual
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: Free(string OWNER = "")
            move_to Included
        action: ExcludeAll(string OWNER = "")
            move_to Excluded

object: FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWM is_of_class FwMode_CLASS

class: FwCaenCrateSY1527_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWCAENCRATESY1527_FWSETSTATES
            remove &VAL_OF_Device from FWCAENCRATESY1527_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWCAENCRATESY1527_FWSETSTATES
            insert &VAL_OF_Device in FWCAENCRATESY1527_FWSETACTIONS
            move_to READY

object: FwCaenCrateSY1527_FWDM is_of_class FwCaenCrateSY1527_FwDevMode_CLASS


class: TOP_FwCaenCrateSY1527_CLASS/associated
    state: OFF	!color: FwStateOKNotPhysics
        action: KILL
        action: RESET
    state: ON	!color: FwStateOKPhysics
        action: KILL
        action: RESET
    state: NO_CONTROL	!color: FwStateAttention2
    state: WA_REPAIR	!color: FwStateAttention1
        action: KILL
        action: RESET
    state: ER_REPAIR	!color: FwStateAttention2
        action: KILL
        action: RESET
    state: PWS_FAULT	!color: FwStateAttention3
        action: KILL
        action: RESET
    state: INTERLOCK	!color: FwStateAttention3
        action: KILL
        action: RESET
    state: INTERLOCK_WENT	!color: FwStateAttention2
        action: KILL
        action: RESET

object: FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER is_of_class TOP_FwCaenCrateSY1527_CLASS

class: FwDevMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWDEVMODE_FWSETSTATES
            remove &VAL_OF_Device from FWDEVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWDEVMODE_FWSETSTATES
            insert &VAL_OF_Device in FWDEVMODE_FWSETACTIONS
            move_to READY

object: FwDevMode_FWDM is_of_class FwDevMode_FwDevMode_CLASS


class: FwDevMode_CLASS/associated
    state: ENABLED	!color: FwStateOKPhysics
    state: DISABLED	!color: FwStateAttention1

object: FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWDM is_of_class FwDevMode_CLASS

objectset: FWDEVMODE_FWSETSTATES {FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWDM }
objectset: FWDEVMODE_FWSETACTIONS {FMD_DCS:FMD_INFR_RACK1:HVPS_CONTROLLER_FWDM }

