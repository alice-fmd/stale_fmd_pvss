class: TOP_FmdPowerModule_CLASS
    state: MIXED	!color: 
        when (  ( ( any_in FMDSECTOR_FWSETSTATES in_state SYS_FAULT ) )  )  move_to SYS_FAULT
        when ( ( any_in FMDSECTOR_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FMDSECTORLARGE_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when (  ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) )  )  do NV_SHUTOFF
        when (  ( ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_WARNING ) )  )  move_to WA_REPAIR
        when (  ( ( all_in FMDSECTOR_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  ( ( all_in FMDSECTOR_FWSETSTATES in_state BEAM_TUNING ) )  )  move_to BEAM_TUNING
        when (  ( ( all_in FMDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) )  )  move_to STBY_CONFIGURED
        when ( ( all_in FMDSECTOR_FWSETSTATES in_state OFF ) and
          ( all_in FMDSECTORLARGE_FWSETSTATES in_state OFF ) )  move_to OFF
        action: NV_SHUTOFF
            do GO_STANDBY all_in FMDSECTOR_FWSETACTIONS
            move_to ER_REPAIR
        action: RESET
            do RESET all_in FMDSECTOR_FWSETACTIONS 
            do RESET all_in FMDSECTORLARGE_FWSETACTIONS 
    state: OFF	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMDSECTOR_FWSETSTATES in_state SYS_FAULT ) )  )  move_to SYS_FAULT
        when ( ( any_in FMDSECTOR_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FMDSECTORLARGE_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when (  ( ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_WARNING ) )  )  move_to WA_REPAIR
        when ( ( all_in FMDSECTOR_FWSETSTATES not_in_state OFF ) and
          ( all_in FMDSECTORLARGE_FWSETSTATES not_in_state OFF ) )  move_to MIXED
        action: GO_STANDBY
            do GO_STANDBY all_in FMDSECTOR_FWSETACTIONS 
            do GO_STANDBY all_in FMDSECTORLARGE_FWSETACTIONS 
            move_to DOWNLOADING
        action: RESET
            do RESET all_in FMDSECTOR_FWSETACTIONS 
            do RESET all_in FMDSECTORLARGE_FWSETACTIONS 
    state: DOWNLOADING	!color: FwStateAttention1
        when (  ( ( any_in FMDSECTOR_FWSETSTATES in_state SYS_FAULT ) )  )  move_to SYS_FAULT
        when ( ( any_in FMDSECTOR_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FMDSECTORLARGE_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when (  ( ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_WARNING ) )  )  move_to WA_REPAIR
        when (  ( ( all_in FMDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) )  )  move_to STBY_CONFIGURED
        action: RESET
            do RESET all_in FMDSECTOR_FWSETACTIONS 
            do RESET all_in FMDSECTORLARGE_FWSETACTIONS 
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMDSECTOR_FWSETSTATES in_state SYS_FAULT ) )  )  move_to SYS_FAULT
        when ( ( any_in FMDSECTOR_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FMDSECTORLARGE_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when (  ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) )  )  move_to MIXED
        when (  ( ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_WARNING ) )  )  move_to WA_REPAIR
        when (  ( ( any_in FMDSECTOR_FWSETSTATES not_in_state STBY_CONFIGURED ) )  )  move_to MIXED
        action: RESET
            do RESET all_in FMDSECTOR_FWSETACTIONS 
            do RESET all_in FMDSECTORLARGE_FWSETACTIONS 
        action: GO_READY
            move_to MOVING_READY
        action: GO_BEAM_TUNING
            move_to MOVING_BEAM_TUNING
    state: MOVING_BEAM_TUNING	!color: FwStateAttention1
        when (  ( ( any_in FMDSECTOR_FWSETSTATES in_state SYS_FAULT ) )  )  move_to SYS_FAULT
        when ( ( any_in FMDSECTOR_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FMDSECTORLARGE_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when (  ( ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_WARNING ) )  )  move_to WA_REPAIR
        when (  ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) )  )  move_to MIXED
        when (  ( ( all_in FMDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) )  )  do NV_START_BT
        when (  ( ( all_in FMDSECTOR_FWSETSTATES in_state BEAM_TUNING ) )  )  move_to BEAM_TUNING
        action: RESET
            do RESET all_in FMDSECTOR_FWSETACTIONS
            do RESET all_in FMDSECTORLARGE_FWSETACTIONS
        action: NV_START_BT
            do GO_BEAM_TUNING all_in FMDSECTOR_FWSETACTIONS
    state: MOVING_READY	!color: FwStateAttention1
        when (  ( ( any_in FMDSECTOR_FWSETSTATES in_state SYS_FAULT ) )  )  move_to SYS_FAULT
        when ( ( any_in FMDSECTOR_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FMDSECTORLARGE_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when (  ( ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_WARNING ) )  )  move_to WA_REPAIR 
        when (  ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) )  )  move_to MIXED
        when (  ( ( all_in FMDSECTOR_FWSETSTATES in_state READY ) )  )  move_to READY
        when (  ( ( all_in FMDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) )  )  do NV_START_RD
        action: RESET
            do RESET all_in FMDSECTOR_FWSETACTIONS 
            do RESET all_in FMDSECTORLARGE_FWSETACTIONS 
        action: NV_START_RD
            do GO_READY all_in FMDSECTOR_FWSETACTIONS
    state: READY	!color: FwStateOKPhysics
        when (  ( ( any_in FMDSECTOR_FWSETSTATES in_state SYS_FAULT ) )  )  move_to SYS_FAULT
        when ( ( any_in FMDSECTOR_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FMDSECTORLARGE_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when (  ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) )  )  move_to MIXED
        when (  ( ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_WARNING ) ) )  move_to WA_REPAIR
        when (  ( ( any_in FMDSECTOR_FWSETSTATES not_in_state READY ) )  )  move_to MIXED
        action: RESET
            do RESET all_in FMDSECTOR_FWSETACTIONS 
            do RESET all_in FMDSECTORLARGE_FWSETACTIONS 
        action: GO_STANDBY
            do GO_STANDBY all_in FMDSECTOR_FWSETACTIONS
            move_to MOVING_STBY_CONF
        action: GO_BEAM_TUNING
            do GO_BEAM_TUNING all_in FMDSECTOR_FWSETACTIONS
            move_to MOVING_BEAM_TUNING
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when (  ( ( any_in FMDSECTOR_FWSETSTATES in_state SYS_FAULT ) )  )  move_to SYS_FAULT
        when ( ( any_in FMDSECTOR_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FMDSECTORLARGE_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when (  ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) )  )  move_to MIXED
        when (  ( ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_WARNING ) )   )  move_to WA_REPAIR
        when (  ( ( any_in FMDSECTOR_FWSETSTATES not_in_state BEAM_TUNING ) )  )  move_to MIXED
        action: RESET
            do RESET all_in FMDSECTOR_FWSETACTIONS 
            do RESET all_in FMDSECTORLARGE_FWSETACTIONS 
        action: GO_STANDBY
            do GO_STANDBY all_in FMDSECTOR_FWSETACTIONS
            move_to MOVING_STBY_CONF
        action: GO_READY
            do GO_READY all_in FMDSECTOR_FWSETACTIONS
            move_to MOVING_READY
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        when (  ( ( any_in FMDSECTOR_FWSETSTATES in_state SYS_FAULT ) )  )  move_to SYS_FAULT
        when ( ( any_in FMDSECTOR_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FMDSECTORLARGE_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when (  ( ( all_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_ERROR ) )  )  move_to MIXED
        when (  ( ( any_in FWDEVMAJORITY_FWSETSTATES in_state MAJORITY_WARNING ) ) )  move_to WA_REPAIR
        when (  ( ( all_in FMDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) )  )  move_to STBY_CONFIGURED
        when ( ( ( all_in FMDSECTOR_FWSETSTATES in_state STBY_CONFIGURED ) )  )  do NV_SHUTOFF_ST
        action: RESET
            do RESET all_in FMDSECTOR_FWSETACTIONS 
            do RESET all_in FMDSECTORLARGE_FWSETACTIONS 
        action: NV_SHUTOFF_ST
    state: WA_REPAIR	!color: FwStateAttention1
        when (  ( ( any_in FMDSECTOR_FWSETSTATES in_state SYS_FAULT ) )  )  move_to SYS_FAULT
        when ( ( any_in FMDSECTOR_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FMDSECTORLARGE_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when (  ( ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_WARNING ) )  )  move_to MIXED
        action: RESET
            do RESET all_in FMDSECTOR_FWSETACTIONS
            do RESET all_in FMDSECTORLARGE_FWSETACTIONS
        action: ACKNOLEDGE
            move_to MIXED
    state: ER_REPAIR	!color: FwStateAttention2
        when (  ( ( any_in FMDSECTOR_FWSETSTATES in_state SYS_FAULT ) )  )  move_to SYS_FAULT
        when ( ( any_in FMDSECTOR_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FMDSECTORLARGE_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when (  ( ( all_in FWDEVMAJORITY_FWSETSTATES not_in_state MAJORITY_ERROR ) )  )  move_to MIXED
        action: RESET
            do RESET all_in FMDSECTOR_FWSETACTIONS 
            do RESET all_in FMDSECTORLARGE_FWSETACTIONS 
        action: ACKNOLEDGE
            move_to MIXED
    state: NO_CONTROL	!color: FwStateAttention2
        when (  ( ( any_in FMDSECTOR_FWSETSTATES in_state SYS_FAULT ) )  )  move_to SYS_FAULT
        when (  not ( ( any_in FMDSECTOR_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FMDSECTORLARGE_FWSETSTATES in_state NO_CONTROL ) )  ) move_to MIXED
        action: RESET
            do RESET all_in FMDSECTOR_FWSETACTIONS 
            do RESET all_in FMDSECTORLARGE_FWSETACTIONS 
    state: SYS_FAULT	!color: FwStateAttention3
        when (  ( ( all_in FMDSECTOR_FWSETSTATES not_in_state SYS_FAULT ) )  )  move_to MIXED
        action: RESET
            do RESET all_in FMDSECTOR_FWSETACTIONS 
            do RESET all_in FMDSECTORLARGE_FWSETACTIONS 

object: FMD3_MOD is_of_class TOP_FmdPowerModule_CLASS

class: FwChildrenMode_CLASS
    state: Complete	!color: _3DFace
        when ( ( any_in FWCHILDRENMODE_FWSETSTATES in_state Incomplete ) )  move_to Incomplete
        when ( ( any_in FWCHILDRENMODE_FWSETSTATES in_state IncompleteDev ) )  move_to IncompleteDev
        when ( ( any_in FWCHILDMODE_FWSETSTATES not_in_state {Included,ExcludedPerm} ) )  move_to Incomplete
        when ( ( any_in FWDEVMODE_FWSETSTATES in_state DISABLED ) )  move_to IncompleteDev
    state: Incomplete	!color: FwStateAttention2
        when (  ( ( all_in FWCHILDMODE_FWSETSTATES in_state {Included,ExcludedPerm} ) ) and
       ( ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state Incomplete ) )  )  move_to Complete
    state: IncompleteDev	!color: FwStateAttention1
        when (  ( ( all_in FWDEVMODE_FWSETSTATES not_in_state DISABLED ) ) and
       ( ( all_in FWCHILDRENMODE_FWSETSTATES not_in_state IncompleteDev ) )  ) move_to Complete
        when (  ( ( any_in FWCHILDMODE_FWSETSTATES not_in_state {Included,ExcludedPerm} ) ) or
       ( ( any_in FWCHILDRENMODE_FWSETSTATES in_state Incomplete ) )  )  move_to Incomplete

object: FMD3_MOD_FWCNM is_of_class FwChildrenMode_CLASS

class: ASS_FwChildrenMode_CLASS/associated
    state: Complete	!color: _3DFace
    state: Incomplete	!color: FwStateAttention2
    state: IncompleteDev	!color: FwStateAttention1

object: FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWCNM is_of_class ASS_FwChildrenMode_CLASS

object: FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWCNM is_of_class ASS_FwChildrenMode_CLASS

objectset: FWCHILDRENMODE_FWSETSTATES {FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWCNM,
	FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWCNM,
	FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWCNM,
	FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWCNM }

class: FwMode_CLASS
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to InLocal
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Manual
            move_to Manual
        action: Ignore
            move_to Ignored
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Manual(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: ReleaseAll(string OWNER = "")
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: Take(string OWNER = "")
            do Include(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to InManual
        action: Exclude(string OWNER = "")
            do Exclude(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Ignore
            move_to Ignored
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: ExcludeAll(string OWNER = "")
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
        action: ReleaseAll(string OWNER = "")
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include
            move_to Included
        action: Exclude(string OWNER = "")
            do Exclude(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded
        action: Manual
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) all_in FWCHILDMODE_FWSETACTIONS
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Included
        action: ExcludeAll(string OWNER = "")
            do ExcludeAll(OWNER=OWNER) all_in FWCHILDMODE_FWSETACTIONS
            move_to Excluded

object: FMD3_MOD_FWM is_of_class FwMode_CLASS

class: ASS_FwMode_CLASS/associated
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Manual
        action: Ignore
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")
        action: Manual(string OWNER = "")
        action: Ignore(string OWNER = "")
        action: ExcludeAll(string OWNER = "")
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Free(string OWNER = "")
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
        action: ReleaseAll(string OWNER = "")
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Take(string OWNER = "")
        action: Exclude(string OWNER = "")
        action: Ignore
        action: Free(string OWNER = "")
        action: ExcludeAll(string OWNER = "")
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: ReleaseAll(string OWNER = "")
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include
        action: Exclude(string OWNER = "")
        action: Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: Free(string OWNER = "")
        action: ExcludeAll(string OWNER = "")

object: FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM is_of_class ASS_FwMode_CLASS

object: FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM is_of_class ASS_FwMode_CLASS

object: FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM is_of_class ASS_FwMode_CLASS

object: FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM is_of_class ASS_FwMode_CLASS

class: FMD3_PW_INNER_BOTTOM_FwChildMode_CLASS

    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM not_in_state Excluded ) then
                if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                    insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETSTATES
                    insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETSTATES
                insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETACTIONS
            endif
            move_to Included
        action: Manual
            do Manual FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
            insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETSTATES
            remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore
            do Ignore FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
            insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETACTIONS
            remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
            move_to Ignored
        action: LockOut
            move_to LockedOut
        action: Exclude
            do Exclude FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
            remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
            remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
            move_to Excluded
    state: Included	!color: FwStateOKPhysics
        when ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM in_state Excluded )  move_to EXCLUDED
        when ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM in_state Ignored )  move_to IGNORED
        when ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM in_state Manual )  move_to MANUAL
        action: Exclude(string OWNER = "")
            if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM not_in_state Included ) then
                if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
                remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
            insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETSTATES
            remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")
            do Ignore(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
            insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETACTIONS
            remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
                remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
            insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETSTATES
            insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
        action: ExcludePerm(string OWNER = "")
            if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM not_in_state Included ) then
                if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
                remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
            endif
            move_to ExcludedPerm
    state: Manual	!color: FwStateOKNotPhysics
        when ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM in_state Included )  move_to INCLUDED
        when ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
              insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETSTATES
              insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETACTIONS
            endif
            move_to Manual
        action: Exclude(string OWNER = "")
            if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM not_in_state Included ) then
                if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
                remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
            endif
            move_to Manual
        action: Ignore
            do Ignore FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
            insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETACTIONS
            remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
            move_to Manual
        action: ExcludeAll(string OWNER = "")
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
            if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM not_in_state InManual ) then
              do ExcludeAll(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
              remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
              remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
            endif
            move_to Manual
    state: Ignored	!color: FwStateOKNotPhysics
        when ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM in_state Included )  move_to INCLUDED
        when ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
            insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETSTATES
            insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETACTIONS
            move_to Included
        action: Exclude(string OWNER = "")
            if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM not_in_state Included ) then
                if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
                remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
            insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETSTATES
            remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")
            if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETSTATES
                remove FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM from FMDSECTOR_FWSETACTIONS
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut
            move_to Excluded
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM not_in_state Excluded ) then
                if ( FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                    insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETSTATES
                    insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM_FWM
                insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETSTATES
                insert FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM in FMDSECTOR_FWSETACTIONS
            endif
            move_to Included

object: FMD3_PW_INNER_BOTTOM_FWM is_of_class FMD3_PW_INNER_BOTTOM_FwChildMode_CLASS

class: FMD3_PW_INNER_TOP_FwChildMode_CLASS

    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM not_in_state Excluded ) then
                if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                    insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETSTATES
                    insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETSTATES
                insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETACTIONS
            endif
            move_to Included
        action: Manual
            do Manual FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
            insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETSTATES
            remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore
            do Ignore FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
            insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETACTIONS
            remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
            move_to Ignored
        action: LockOut
            move_to LockedOut
        action: Exclude
            do Exclude FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
            remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
            remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
            move_to Excluded
    state: Included	!color: FwStateOKPhysics
        when ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM in_state Excluded )  move_to EXCLUDED
        when ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM in_state Ignored )  move_to IGNORED
        when ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM in_state Manual )  move_to MANUAL
        action: Exclude(string OWNER = "")
            if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM not_in_state Included ) then
                if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
                remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
            insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETSTATES
            remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")
            do Ignore(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
            insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETACTIONS
            remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
                remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
            insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETSTATES
            insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
        action: ExcludePerm(string OWNER = "")
            if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM not_in_state Included ) then
                if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
                remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
            endif
            move_to ExcludedPerm
    state: Manual	!color: FwStateOKNotPhysics
        when ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM in_state Included )  move_to INCLUDED
        when ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
              insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETSTATES
              insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETACTIONS
            endif
            move_to Manual
        action: Exclude(string OWNER = "")
            if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM not_in_state Included ) then
                if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
                remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
            endif
            move_to Manual
        action: Ignore
            do Ignore FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
            insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETACTIONS
            remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
            move_to Manual
        action: ExcludeAll(string OWNER = "")
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
            if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM not_in_state InManual ) then
              do ExcludeAll(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
              remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
              remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
            endif
            move_to Manual
    state: Ignored	!color: FwStateOKNotPhysics
        when ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM in_state Included )  move_to INCLUDED
        when ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
            insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETSTATES
            insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETACTIONS
            move_to Included
        action: Exclude(string OWNER = "")
            if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM not_in_state Included ) then
                if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
                remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
            insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETSTATES
            remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")
            if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
                    remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETSTATES
                remove FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP from FMDSECTOR_FWSETACTIONS
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut
            move_to Excluded
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM not_in_state Excluded ) then
                if ( FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                    insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETSTATES
                    insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP_FWM
                insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETSTATES
                insert FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP in FMDSECTOR_FWSETACTIONS
            endif
            move_to Included

object: FMD3_PW_INNER_TOP_FWM is_of_class FMD3_PW_INNER_TOP_FwChildMode_CLASS

class: FMD3_PW_OUTER_BOTTOM_FwChildMode_CLASS

    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM not_in_state Excluded ) then
                if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                    insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETSTATES
                    insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETSTATES
                insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Included
        action: Manual
            do Manual FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
            insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETSTATES
            remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
            move_to Manual
        action: Ignore
            do Ignore FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
            insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETACTIONS
            remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
            move_to Ignored
        action: LockOut
            move_to LockedOut
        action: Exclude
            do Exclude FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
            remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
            remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
            move_to Excluded
    state: Included	!color: FwStateOKPhysics
        when ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM in_state Excluded )  move_to EXCLUDED
        when ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM in_state Ignored )  move_to IGNORED
        when ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM in_state Manual )  move_to MANUAL
        action: Exclude(string OWNER = "")
            if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM not_in_state Included ) then
                if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
                remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
            insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETSTATES
            remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")
            do Ignore(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
            insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETACTIONS
            remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
                remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
            insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETSTATES
            insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
        action: ExcludePerm(string OWNER = "")
            if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM not_in_state Included ) then
                if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
                remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to ExcludedPerm
    state: Manual	!color: FwStateOKNotPhysics
        when ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM in_state Included )  move_to INCLUDED
        when ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
              insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETSTATES
              insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Manual
        action: Exclude(string OWNER = "")
            if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM not_in_state Included ) then
                if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
                remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Manual
        action: Ignore
            do Ignore FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
            insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETACTIONS
            remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
            move_to Manual
        action: ExcludeAll(string OWNER = "")
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
            if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM not_in_state InManual ) then
              do ExcludeAll(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
              remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
              remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Manual
    state: Ignored	!color: FwStateOKNotPhysics
        when ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM in_state Included )  move_to INCLUDED
        when ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
            insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETSTATES
            insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETACTIONS
            move_to Included
        action: Exclude(string OWNER = "")
            if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM not_in_state Included ) then
                if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
                remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
            insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETSTATES
            remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")
            if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETSTATES
                remove FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM from FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut
            move_to Excluded
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM not_in_state Excluded ) then
                if ( FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                    insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETSTATES
                    insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM_FWM
                insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETSTATES
                insert FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM in FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Included

object: FMD3_PW_OUTER_BOTTOM_FWM is_of_class FMD3_PW_OUTER_BOTTOM_FwChildMode_CLASS

class: FMD3_PW_OUTER_TOP_FwChildMode_CLASS

    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM not_in_state Excluded ) then
                if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                    insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETSTATES
                    insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETSTATES
                insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Included
        action: Manual
            do Manual FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
            insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETSTATES
            remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
            move_to Manual
        action: Ignore
            do Ignore FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
            insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETACTIONS
            remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
            move_to Ignored
        action: LockOut
            move_to LockedOut
        action: Exclude
            do Exclude FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
            remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
            remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
            move_to Excluded
    state: Included	!color: FwStateOKPhysics
        when ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM in_state Excluded )  move_to EXCLUDED
        when ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM in_state Ignored )  move_to IGNORED
        when ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM in_state Manual )  move_to MANUAL
        action: Exclude(string OWNER = "")
            if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM not_in_state Included ) then
                if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
                remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
            insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETSTATES
            remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
            move_to Manual
        action: Ignore(string OWNER = "")
            do Ignore(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
            insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETACTIONS
            remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
                remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
            insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETSTATES
            insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETACTIONS
            move_to Included
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
        action: ExcludePerm(string OWNER = "")
            if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM not_in_state Included ) then
                if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
                remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to ExcludedPerm
    state: Manual	!color: FwStateOKNotPhysics
        when ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM in_state Included )  move_to INCLUDED
        when ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM not_in_state InManual ) then
              do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
              insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETSTATES
              insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Manual
        action: Exclude(string OWNER = "")
            if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM not_in_state Included ) then
                if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
                remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Manual
        action: Ignore
            do Ignore FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
            insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETACTIONS
            remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
            move_to Ignored
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
            move_to Manual
        action: ExcludeAll(string OWNER = "")
            !    else
            !        move_to Included
            !    endif
            !else
            !endif
            if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM not_in_state InManual ) then
              do ExcludeAll(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
              remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
              remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Manual
    state: Ignored	!color: FwStateOKNotPhysics
        when ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM in_state Included )  move_to INCLUDED
        when ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM in_state Excluded ) move_to EXCLUDED
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
            insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETSTATES
            insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETACTIONS
            move_to Included
        action: Exclude(string OWNER = "")
            if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM not_in_state Included ) then
                if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM in_state InManual ) then
                    do Release(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
                else
                    do Exclude(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
                endif
            else
                do Exclude(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
                remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Excluded
        action: Manual(string OWNER = "")
            do Manual(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
            insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETSTATES
            remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
            do SetMode(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
        action: Free(string OWNER = "")
            do Free(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
            move_to Included
        action: ExcludeAll(string OWNER = "")
            if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM not_in_state {Included,Ignored,Manual} ) then
                if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM in_state InManual ) then
                    do ReleaseAll(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
                    remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
                else
                    move_to Included
                endif
            else
                do ExcludeAll(OWNER=OWNER) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETSTATES
                remove FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP from FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Excluded
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut
            move_to Excluded
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM not_in_state Excluded ) then
                if ( FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM in_state Manual ) then
                    do Take(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                    insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETSTATES
                    insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETACTIONS
                else
                    move_to Excluded
                endif
            else
                do Include(OWNER=OWNER,EXCLUSIVE=EXCLUSIVE) FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP_FWM
                insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETSTATES
                insert FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP in FMDSECTORLARGE_FWSETACTIONS
            endif
            move_to Included

object: FMD3_PW_OUTER_TOP_FWM is_of_class FMD3_PW_OUTER_TOP_FwChildMode_CLASS

objectset: FWCHILDMODE_FWSETSTATES {FMD3_PW_INNER_BOTTOM_FWM,
	FMD3_PW_INNER_TOP_FWM,
	FMD3_PW_OUTER_BOTTOM_FWM,
	FMD3_PW_OUTER_TOP_FWM }
objectset: FWCHILDMODE_FWSETACTIONS {FMD3_PW_INNER_BOTTOM_FWM,
	FMD3_PW_INNER_TOP_FWM,
	FMD3_PW_OUTER_BOTTOM_FWM,
	FMD3_PW_OUTER_TOP_FWM }

class: ASS_FmdSector_CLASS/associated
    state: MIXED	!color: FwStateAttention1
        action: RESET
        action: NV_SHUTOFF
        action: NV_SWINTERLOCK
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        action: RESET
        action: CONFIGURE
        action: GO_READY
        action: GO_BEAM_TUNING
    state: OFF	!color: FwStateOKNotPhysics
        action: RESET
        action: GO_STANDBY
    state: DOWNLOADING	!color: FwStateAttention1
        action: RESET
    state: READY	!color: FwStateOKPhysics
        action: RESET
        action: GO_STANDBY
        action: GO_BEAM_TUNING
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        action: RESET
        action: GO_STANDBY
        action: GO_READY
        action: CONFIGURE
    state: MOVING_READY	!color: FwStateAttention1
        action: RESET
        action: NV_HVBOOT
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        action: RESET
        action: NV_LVSHUTDW
    state: MOVING_BEAM_TUNING	!color: FwStateAttention1
        action: RESET
        action: NV_HVBOOT2
    state: TRIPPED	!color: FwStateAttention2
        action: RESET
        action: ACKNOLEDGE
        action: NV_SHUTDOWNT
        action: NV_SHUTDOWNTH
    state: SYS_FAULT	!color: FwStateAttention3
        action: RESET
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET

object: FMD3_PW_INNER_BOTTOM::FMD3_PW_INNER_BOTTOM is_of_class ASS_FmdSector_CLASS

object: FMD3_PW_INNER_TOP::FMD3_PW_INNER_TOP is_of_class ASS_FmdSector_CLASS

objectset: FMDSECTOR_FWSETSTATES
objectset: FMDSECTOR_FWSETACTIONS

class: ASS_FmdSectorLarge_CLASS/associated
    state: MIXED	!color: FwStateAttention1
        action: RESET
        action: NV_SHUTOFF
        action: NV_SWINTERLOCK
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        action: RESET
        action: CONFIGURE
        action: GO_READY
        action: GO_BEAM_TUNING
    state: OFF	!color: FwStateOKNotPhysics
        action: RESET
        action: GO_STANDBY
    state: DOWNLOADING	!color: FwStateAttention1
        action: RESET
    state: READY	!color: FwStateOKPhysics
        action: RESET
        action: GO_STANDBY
        action: GO_BEAM_TUNING
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        action: RESET
        action: GO_STANDBY
        action: GO_READY
        action: CONFIGURE
    state: MOVING_READY	!color: FwStateAttention1
        action: RESET
        action: NV_HVBOOT
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        action: RESET
        action: NV_LVSHUTDW
    state: MOVING_BEAM_TUNING	!color: FwStateAttention1
        action: RESET
        action: NV_HVBOOT2
    state: TRIPPED	!color: FwStateAttention2
        action: RESET
        action: ACKNOLEDGE
        action: NV_SHUTDOWNT
        action: NV_SHUTDOWNTH
    state: SYS_FAULT	!color: FwStateAttention3
        action: RESET
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET

object: FMD3_PW_OUTER_BOTTOM::FMD3_PW_OUTER_BOTTOM is_of_class ASS_FmdSectorLarge_CLASS

object: FMD3_PW_OUTER_TOP::FMD3_PW_OUTER_TOP is_of_class ASS_FmdSectorLarge_CLASS

objectset: FMDSECTORLARGE_FWSETSTATES
objectset: FMDSECTORLARGE_FWSETACTIONS

class: FwDevMajority_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWDEVMAJORITY_FWSETSTATES
            remove &VAL_OF_Device from FWDEVMAJORITY_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWDEVMAJORITY_FWSETSTATES
            insert &VAL_OF_Device in FWDEVMAJORITY_FWSETACTIONS
            move_to READY

object: FwDevMajority_FWDM is_of_class FwDevMajority_FwDevMode_CLASS


class: FwDevMajority_CLASS/associated
    state: MAJORITY_OK	!color: FwStateOKPhysics
    state: MAJORITY_WARNING	!color: FwStateAttention2
    state: MAJORITY_ERROR	!color: FwStateAttention3

object: FMD3_MOD:FmdSector_FWMAJ is_of_class FwDevMajority_CLASS

objectset: FWDEVMAJORITY_FWSETSTATES {FMD3_MOD:FmdSector_FWMAJ }
objectset: FWDEVMAJORITY_FWSETACTIONS {FMD3_MOD:FmdSector_FWMAJ }

class: FwDevMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWDEVMODE_FWSETSTATES
            remove &VAL_OF_Device from FWDEVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWDEVMODE_FWSETSTATES
            insert &VAL_OF_Device in FWDEVMODE_FWSETACTIONS
            move_to READY

object: FwDevMode_FWDM is_of_class FwDevMode_FwDevMode_CLASS


class: FwDevMode_CLASS/associated
    state: ENABLED	!color: FwStateOKPhysics
    state: DISABLED	!color: FwStateAttention1

object: FMD3_MOD_FWDM is_of_class FwDevMode_CLASS

objectset: FWDEVMODE_FWSETSTATES {FMD3_MOD_FWDM }
objectset: FWDEVMODE_FWSETACTIONS {FMD3_MOD_FWDM }

