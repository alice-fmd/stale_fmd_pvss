class: ASS_FwChildMode_CLASS/associated
    state: Excluded	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Manual
        action: Ignore
        action: LockOut
        action: Exclude
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")
        action: Manual(string OWNER = "")
        action: Ignore(string OWNER = "")
        action: ExcludeAll(string OWNER = "")
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Free(string OWNER = "")
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: ExcludePerm(string OWNER = "")
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Exclude(string OWNER = "")
        action: Ignore
        action: Free(string OWNER = "")
        action: ExcludeAll(string OWNER = "")
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
        action: Exclude(string OWNER = "")
        action: Manual(string OWNER = "")
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: Free(string OWNER = "")
        action: ExcludeAll(string OWNER = "")
    state: LockedOut	!color: FwStateOKNotPhysics
        action: UnLockOut
    state: ExcludedPerm	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
