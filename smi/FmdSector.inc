class: ASS_FmdSector_CLASS/associated
    state: MIXED	!color: FwStateAttention1
        action: RESET
        action: NV_SHUTOFF
        action: NV_SWINTERLOCK
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        action: RESET
        action: CONFIGURE
        action: GO_READY
        action: GO_BEAM_TUNING
    state: OFF	!color: FwStateOKNotPhysics
        action: RESET
        action: GO_STANDBY
    state: DOWNLOADING	!color: FwStateAttention1
        action: RESET
    state: READY	!color: FwStateOKPhysics
        action: RESET
        action: GO_STANDBY
        action: GO_BEAM_TUNING
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        action: RESET
        action: GO_STANDBY
        action: GO_READY
        action: CONFIGURE
    state: MOVING_READY	!color: FwStateAttention1
        action: RESET
        action: NV_HVBOOT
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        action: RESET
        action: NV_LVSHUTDW
    state: MOVING_BEAM_TUNING	!color: FwStateAttention1
        action: RESET
        action: NV_HVBOOT2
    state: TRIPPED	!color: FwStateAttention2
        action: RESET
        action: ACKNOLEDGE
        action: NV_SHUTDOWNT
        action: NV_SHUTDOWNTH
    state: SYS_FAULT	!color: FwStateAttention3
        action: RESET
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET
