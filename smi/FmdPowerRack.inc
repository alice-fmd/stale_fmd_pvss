class: ASS_FmdPowerRack_CLASS/associated
    state: OFF	!color: FwStateOKNotPhysics
        action: KILL
        action: RESET
    state: READY	!color: FwStateOKPhysics
        action: KILL
        action: RESET
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET
    state: WA_REPAIR	!color: FwStateAttention1
        action: KILL
        action: RESET
    state: ER_REPAIR	!color: FwStateAttention2
        action: KILL
        action: RESET
    state: FAULT	!color: FwStateAttention3
        action: KILL
        action: RESET
    state: INTERLOCK	!color: FwStateAttention3
        action: KILL
        action: RESET
    state: INTERLOCK_WENT	!color: FwStateAttention2
        action: KILL
        action: RESET
