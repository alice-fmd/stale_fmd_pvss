class: TOP_FmdSector_CLASS
    state: MIXED	!color: FwStateAttention1
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state CHAN_FAULT ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to SYS_FAULT
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state TRIPPED ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FWCAENCHANNEL_FWSETSTATES in_state OFF ) and
          ( all_in FWCAENCHANNELLV_FWSETSTATES in_state OFF ) )  do NV_SHUTOFF
        when ( ( all_in FWCAENCHANNEL_FWSETSTATES in_state ON ) and
          ( all_in FWCAENCHANNELLV_FWSETSTATES in_state ON ) )  move_to READY
        when ( ( all_in FWCAENCHANNEL_FWSETSTATES in_state {ON,INTERMEDIATE} ) and
          ( all_in FWCAENCHANNELLV_FWSETSTATES in_state {ON,INTERMEDIATE} ) )  move_to BEAM_TUNING
        when (  ( ( all_in FWCAENCHANNEL_FWSETSTATES in_state {OFF,RAMP_UP_ON,RAMP_UP_INTER} ) ) and ( ( all_in FWCAENCHANNELLV_FWSETSTATES in_state ON ) )  )  move_to MIXED
        when (  ( ( all_in FWCAENCHANNEL_FWSETSTATES in_state {ON,RAMP_UP_ON,RAMP_UP_INTER} ) ) and ( ( all_in FWCAENCHANNELLV_FWSETSTATES in_state OFF ) )  )  do NV_SWINTERLOCK
        action: RESET
            do SET(SetNum="1") all_in FWCAENCHANNEL_FWSETACTIONS
            do SET(SetNum="1") all_in FWCAENCHANNELLV_FWSETACTIONS
            do RESET all_in FWCAENCHANNEL_FWSETACTIONS
            do RESET all_in FWCAENCHANNELLV_FWSETACTIONS
            move_to MIXED
        action: NV_SHUTOFF
            do SET(SetNum="1") all_in FWCAENCHANNEL_FWSETACTIONS
            do SET(SetNum="1") all_in FWCAENCHANNELLV_FWSETACTIONS
            move_to STBY_CONFIGURED
        action: NV_SWINTERLOCK
            do SWITCH_OFF all_in FWCAENCHANNEL_FWSETACTIONS
            move_to TRIPPED
    state: STBY_CONFIGURED	!color: FwStateOKNotPhysics
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state CHAN_FAULT ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to SYS_FAULT
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state TRIPPED ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES not_in_state OFF ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES not_in_state OFF ) )  move_to MIXED
        action: RESET
            do SET(SetNum="1") all_in FWCAENCHANNEL_FWSETACTIONS
            do SET(SetNum="1") all_in FWCAENCHANNELLV_FWSETACTIONS
            do RESET all_in FWCAENCHANNEL_FWSETACTIONS
            do RESET all_in FWCAENCHANNELLV_FWSETACTIONS
            move_to MIXED
        action: CONFIGURE
            move_to DOWNLOADING
        action: GO_READY
            do GO_READY all_in FWCAENCHANNELLV_FWSETACTIONS
            move_to MOVING_READY
        action: GO_BEAM_TUNING
            do GO_READY all_in FWCAENCHANNELLV_FWSETACTIONS
            move_to MOVING_BEAM_TUNING
    state: OFF	!color: FwStateOKNotPhysics
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state CHAN_FAULT ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to SYS_FAULT
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state TRIPPED ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES not_in_state OFF ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES not_in_state OFF ) )  move_to MIXED
        action: RESET
            do SET(SetNum="1") all_in FWCAENCHANNEL_FWSETACTIONS
            do SET(SetNum="1") all_in FWCAENCHANNELLV_FWSETACTIONS
            do RESET all_in FWCAENCHANNEL_FWSETACTIONS
            do RESET all_in FWCAENCHANNELLV_FWSETACTIONS
            move_to MIXED
        action: GO_STANDBY
            move_to DOWNLOADING
    state: DOWNLOADING	!color: FwStateAttention1
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state CHAN_FAULT ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to SYS_FAULT
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state TRIPPED ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FWCAENCHANNEL_FWSETSTATES in_state OFF ) and
          ( all_in FWCAENCHANNELLV_FWSETSTATES in_state OFF ) )  move_to STBY_CONFIGURED
        when (  ( ( all_in FWCAENCHANNEL_FWSETSTATES in_state INTERMEDIATE ) ) and ( ( all_in FWCAENCHANNELLV_FWSETSTATES in_state ON ) )  ) move_to BEAM_TUNING
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES not_in_state OFF ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES not_in_state OFF ) )  move_to MIXED
        action: RESET
            do SET(SetNum="1") all_in FWCAENCHANNEL_FWSETACTIONS
            do SET(SetNum="1") all_in FWCAENCHANNELLV_FWSETACTIONS
            do RESET all_in FWCAENCHANNEL_FWSETACTIONS
            do RESET all_in FWCAENCHANNELLV_FWSETACTIONS
            move_to MIXED
    state: READY	!color: FwStateOKPhysics
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state CHAN_FAULT ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to SYS_FAULT
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state TRIPPED ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES not_in_state ON ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES not_in_state ON ) )  move_to MIXED
        action: RESET
            do SET(SetNum="1") all_in FWCAENCHANNEL_FWSETACTIONS
            do SET(SetNum="1") all_in FWCAENCHANNELLV_FWSETACTIONS
            do RESET all_in FWCAENCHANNEL_FWSETACTIONS
            do RESET all_in FWCAENCHANNELLV_FWSETACTIONS
            move_to MIXED
        action: GO_STANDBY
            do GO_STANDBY all_in FWCAENCHANNEL_FWSETACTIONS
            move_to MOVING_STBY_CONF
        action: GO_BEAM_TUNING
            do GO_BEAM_TUNING all_in FWCAENCHANNEL_FWSETACTIONS
            move_to MOVING_BEAM_TUNING
    state: BEAM_TUNING	!color: FwStateOKNotPhysics
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state CHAN_FAULT ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to SYS_FAULT
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state TRIPPED ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when (  ( ( any_in FWCAENCHANNEL_FWSETSTATES not_in_state INTERMEDIATE ) ) or ( ( any_in FWCAENCHANNELLV_FWSETSTATES not_in_state ON ) )  )  move_to MIXED
        action: RESET
            do SET(SetNum="1") all_in FWCAENCHANNEL_FWSETACTIONS
            do SET(SetNum="1") all_in FWCAENCHANNELLV_FWSETACTIONS
            do RESET all_in FWCAENCHANNEL_FWSETACTIONS
            do RESET all_in FWCAENCHANNELLV_FWSETACTIONS
            move_to MIXED
        action: GO_STANDBY
            do GO_STANDBY all_in FWCAENCHANNEL_FWSETACTIONS
            move_to MOVING_STBY_CONF
        action: GO_READY
            do GO_READY all_in FWCAENCHANNEL_FWSETACTIONS
            move_to MOVING_READY
        action: CONFIGURE
            move_to DOWNLOADING
    state: MOVING_READY	!color: FwStateAttention1
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state CHAN_FAULT ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to SYS_FAULT
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state TRIPPED ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when (  ( ( all_in FWCAENCHANNEL_FWSETSTATES in_state ON ) ) and ( ( all_in FWCAENCHANNELLV_FWSETSTATES in_state ON ) )  )  move_to READY
        when (  ( ( all_in FWCAENCHANNEL_FWSETSTATES in_state OFF ) ) and ( ( all_in FWCAENCHANNELLV_FWSETSTATES in_state ON ) )  ) do NV_HVBOOT
        when (  ( ( any_in FWCAENCHANNEL_FWSETSTATES not_in_state OFF ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES not_in_state OFF ) ) and ( ( all_in FWCAENCHANNEL_FWSETSTATES not_in_state RAMP_UP_ON ) ) or ( ( all_in FWCAENCHANNELLV_FWSETSTATES in_state OFF ) )  ) move_to MIXED
        action: RESET
            do SET(SetNum="1") all_in FWCAENCHANNEL_FWSETACTIONS
            do SET(SetNum="1") all_in FWCAENCHANNELLV_FWSETACTIONS
            do RESET all_in FWCAENCHANNEL_FWSETACTIONS
            do RESET all_in FWCAENCHANNELLV_FWSETACTIONS
            move_to MIXED
        action: NV_HVBOOT
            do GO_READY all_in FWCAENCHANNEL_FWSETACTIONS
    state: MOVING_STBY_CONF	!color: FwStateAttention1
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state CHAN_FAULT ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to SYS_FAULT
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state TRIPPED ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when ( ( all_in FWCAENCHANNEL_FWSETSTATES in_state OFF ) and
          ( all_in FWCAENCHANNELLV_FWSETSTATES in_state OFF ) )  move_to STBY_CONFIGURED
        when (  ( ( all_in FWCAENCHANNEL_FWSETSTATES in_state OFF ) ) and ( ( all_in FWCAENCHANNELLV_FWSETSTATES in_state ON ) )  ) do NV_LVSHUTDW
        action: RESET
            do SET(SetNum="1") all_in FWCAENCHANNEL_FWSETACTIONS
            do SET(SetNum="1") all_in FWCAENCHANNELLV_FWSETACTIONS
            do RESET all_in FWCAENCHANNEL_FWSETACTIONS
            do RESET all_in FWCAENCHANNELLV_FWSETACTIONS
            move_to MIXED
        action: NV_LVSHUTDW
            do GO_STANDBY all_in FWCAENCHANNELLV_FWSETACTIONS
    state: MOVING_BEAM_TUNING	!color: FwStateAttention1
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state CHAN_FAULT ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to SYS_FAULT
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state TRIPPED ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state TRIPPED ) )  move_to TRIPPED
        when (  ( ( all_in FWCAENCHANNEL_FWSETSTATES in_state INTERMEDIATE ) ) and ( ( all_in FWCAENCHANNELLV_FWSETSTATES in_state ON ) )  )  move_to BEAM_TUNING
        when (  ( ( all_in FWCAENCHANNEL_FWSETSTATES in_state OFF ) ) and ( ( all_in FWCAENCHANNELLV_FWSETSTATES in_state ON ) )  ) do NV_HVBOOT2
        when (  ( ( any_in FWCAENCHANNEL_FWSETSTATES not_in_state OFF ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES not_in_state OFF ) ) and ( ( any_in FWCAENCHANNEL_FWSETSTATES not_in_state ON ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES not_in_state ON ) ) and ( ( all_in FWCAENCHANNEL_FWSETSTATES not_in_state {RAMP_UP_INTER,RAMP_DW_INTER} ) ) or ( ( all_in FWCAENCHANNELLV_FWSETSTATES in_state OFF ) )  ) move_to MIXED
        action: RESET
            do SET(SetNum="1") all_in FWCAENCHANNEL_FWSETACTIONS
            do SET(SetNum="1") all_in FWCAENCHANNELLV_FWSETACTIONS
            do RESET all_in FWCAENCHANNEL_FWSETACTIONS
            do RESET all_in FWCAENCHANNELLV_FWSETACTIONS
            move_to MIXED
        action: NV_HVBOOT2
            do GO_BEAM_TUNING all_in FWCAENCHANNEL_FWSETACTIONS
    state: TRIPPED	!color: FwStateAttention2
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state CHAN_FAULT ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state CHAN_FAULT ) )  move_to SYS_FAULT
        when ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state NO_CONTROL ) )  move_to NO_CONTROL
        when (  ( ( any_in FWCAENCHANNELLV_FWSETSTATES in_state TRIPPED ) ) and ( ( any_in FWCAENCHANNELLV_FWSETSTATES in_state ON ) ) ) do NV_SHUTDOWNT
        when (  ( ( any_in FWCAENCHANNELLV_FWSETSTATES in_state OFF ) ) and ( ( any_in FWCAENCHANNELLV_FWSETSTATES in_state ON ) )  )  do NV_SHUTDOWNT
        when (  ( ( any_in FWCAENCHANNEL_FWSETSTATES not_in_state {OFF,TRIPPED,RAMP_DW_OFF,RAMP_DW_INTER} ) ) and ( ( all_in FWCAENCHANNELLV_FWSETSTATES in_state {OFF,TRIPPED} ) )  )  do NV_SHUTDOWNTH
        action: RESET
            do SET(SetNum="1") all_in FWCAENCHANNEL_FWSETACTIONS
            do SET(SetNum="1") all_in FWCAENCHANNELLV_FWSETACTIONS
            do RESET all_in FWCAENCHANNEL_FWSETACTIONS
            do RESET all_in FWCAENCHANNELLV_FWSETACTIONS
            move_to MIXED
        action: ACKNOLEDGE
            move_to MIXED
        action: NV_SHUTDOWNT
            do SWITCH_OFF all_in FWCAENCHANNELLV_FWSETACTIONS
        action: NV_SHUTDOWNTH
            do SWITCH_OFF all_in FWCAENCHANNEL_FWSETACTIONS
    state: SYS_FAULT	!color: FwStateAttention3
        when ( ( all_in FWCAENCHANNEL_FWSETSTATES not_in_state CHAN_FAULT ) and
          ( all_in FWCAENCHANNELLV_FWSETSTATES not_in_state CHAN_FAULT ) )  move_to MIXED
        action: RESET
            do SET(SetNum="1") all_in FWCAENCHANNEL_FWSETACTIONS
            do SET(SetNum="1") all_in FWCAENCHANNELLV_FWSETACTIONS
            do RESET all_in FWCAENCHANNEL_FWSETACTIONS
            do RESET all_in FWCAENCHANNELLV_FWSETACTIONS
            move_to MIXED
    state: NO_CONTROL	!color: FwStateAttention2
        when (  not ( ( any_in FWCAENCHANNEL_FWSETSTATES in_state NO_CONTROL ) or
          ( any_in FWCAENCHANNELLV_FWSETSTATES in_state NO_CONTROL ) )  ) move_to MIXED
        action: RESET
            do SET(SetNum="1") all_in FWCAENCHANNEL_FWSETACTIONS
            do SET(SetNum="1") all_in FWCAENCHANNELLV_FWSETACTIONS
            do RESET all_in FWCAENCHANNEL_FWSETACTIONS
            do RESET all_in FWCAENCHANNELLV_FWSETACTIONS
            move_to MIXED

object: FMD1_PW_INNER_BOTTOM is_of_class TOP_FmdSector_CLASS

class: FwChildrenMode_CLASS
    state: Complete	!color: _3DFace
        when ( ( any_in FWDEVMODE_FWSETSTATES in_state DISABLED ) )  move_to IncompleteDev
    state: Incomplete	!color: FwStateAttention2
    state: IncompleteDev	!color: FwStateAttention1
        when (  ( ( all_in FWDEVMODE_FWSETSTATES not_in_state DISABLED ) )  ) move_to Complete

object: FMD1_PW_INNER_BOTTOM_FWCNM is_of_class FwChildrenMode_CLASS

class: FwMode_CLASS
    state: Excluded	!color: FwStateOKNotPhysics
        action: Take(string OWNER = "",string EXCLUSIVE = "YES")
            move_to InLocal
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            move_to Included
        action: Manual
            move_to Manual
        action: Ignore
            move_to Ignored
    state: Included	!color: FwStateOKPhysics
        action: Exclude(string OWNER = "")
            move_to Excluded
        action: Manual(string OWNER = "")
            move_to Manual
        action: Ignore(string OWNER = "")
            move_to Ignored
        action: ExcludeAll(string OWNER = "")
            move_to Excluded
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            move_to Included
        action: Free(string OWNER = "")
            move_to Included
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
    state: InLocal	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
            move_to Excluded
        action: ReleaseAll(string OWNER = "")
            move_to Excluded
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
    state: Manual	!color: FwStateOKNotPhysics
        action: Include(string OWNER = "",string EXCLUSIVE = "YES")
            move_to Included
        action: Take(string OWNER = "")
            move_to InManual
        action: Exclude(string OWNER = "")
            move_to Excluded
        action: Ignore
            move_to Ignored
        action: Free(string OWNER = "")
            move_to Excluded
        action: ExcludeAll(string OWNER = "")
            move_to Excluded
    state: InManual	!color: FwStateOKNotPhysics
        action: Release(string OWNER = "")
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: ReleaseAll(string OWNER = "")
            move_to Excluded
    state: Ignored	!color: FwStateOKNotPhysics
        action: Include
            move_to Included
        action: Exclude(string OWNER = "")
            move_to Excluded
        action: Manual
            move_to Manual
        action: SetMode(string OWNER = "",string EXCLUSIVE = "YES")
        action: Free(string OWNER = "")
            move_to Included
        action: ExcludeAll(string OWNER = "")
            move_to Excluded

object: FMD1_PW_INNER_BOTTOM_FWM is_of_class FwMode_CLASS

class: FwDevMode_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWDEVMODE_FWSETSTATES
            remove &VAL_OF_Device from FWDEVMODE_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWDEVMODE_FWSETSTATES
            insert &VAL_OF_Device in FWDEVMODE_FWSETACTIONS
            move_to READY

object: FwDevMode_FWDM is_of_class FwDevMode_FwDevMode_CLASS


class: FwDevMode_CLASS/associated
    state: ENABLED	!color: FwStateOKPhysics
    state: DISABLED	!color: FwStateAttention1

object: FMD1_PW_INNER_BOTTOM_FWDM is_of_class FwDevMode_CLASS

objectset: FWDEVMODE_FWSETSTATES {FMD1_PW_INNER_BOTTOM_FWDM }
objectset: FWDEVMODE_FWSETACTIONS {FMD1_PW_INNER_BOTTOM_FWDM }

class: FwCaenChannel_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWCAENCHANNEL_FWSETSTATES
            remove &VAL_OF_Device from FWCAENCHANNEL_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWCAENCHANNEL_FWSETSTATES
            insert &VAL_OF_Device in FWCAENCHANNEL_FWSETACTIONS
            move_to READY

object: FwCaenChannel_FWDM is_of_class FwCaenChannel_FwDevMode_CLASS


class: FwCaenChannel_CLASS/associated
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: SWITCH_ON
        action: SET(string SetNum = "1")
        action: GO_READY
        action: GO_BEAM_TUNING
        action: RESET
    state: ON	!color: FwStateOKPhysics
        action: SWITCH_OFF
        action: SET(string SetNum = "1")
        action: GO_BEAM_TUNING
        action: GO_STANDBY
        action: RESET
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET
    state: INTERMEDIATE	!color: FwStateOKNotPhysics
        action: SWITCH_OFF
        action: SET(string SetNum = "1")
        action: GO_READY
        action: GO_STANDBY
        action: RESET
    state: RAMP_UP_ON	!color: FwStateAttention1
        action: SWITCH_OFF
        action: RESET
    state: RAMP_UP_INTER	!color: FwStateAttention1
        action: SWITCH_OFF
        action: RESET
    state: RAMP_DW_OFF	!color: FwStateAttention1
        action: SWITCH_ON
        action: RESET
    state: RAMP_DW_INTER	!color: FwStateAttention1
        action: SWITCH_ON
        action: RESET
    state: TRIPPED	!color: FwStateAttention2
        action: SET
        action: RESET
        action: SWITCH_OFF
        action: SWITCH_ON
        action: GO_READY
        action: GO_BEAM_TUNING
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET
    state: CHAN_FAULT	!color: FwStateAttention3
        action: SWITCH_ON
        action: SWITCH_OFF
        action: SET
        action: RESET

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel000 is_of_class FwCaenChannel_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel001 is_of_class FwCaenChannel_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel002 is_of_class FwCaenChannel_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel003 is_of_class FwCaenChannel_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel004 is_of_class FwCaenChannel_CLASS

objectset: FWCAENCHANNEL_FWSETSTATES {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel000,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel001,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel002,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel003,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel004 }
objectset: FWCAENCHANNEL_FWSETACTIONS {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel000,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel001,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel002,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel003,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:HvChannel004 }

class: FwCaenChannelLV_FwDevMode_CLASS
    state: READY
        action: Disable(Device)
            remove &VAL_OF_Device from FWCAENCHANNELLV_FWSETSTATES
            remove &VAL_OF_Device from FWCAENCHANNELLV_FWSETACTIONS
            move_to READY
        action: Enable(Device)
            insert &VAL_OF_Device in FWCAENCHANNELLV_FWSETSTATES
            insert &VAL_OF_Device in FWCAENCHANNELLV_FWSETACTIONS
            move_to READY

object: FwCaenChannelLV_FWDM is_of_class FwCaenChannelLV_FwDevMode_CLASS


class: FwCaenChannelLV_CLASS/associated
    parameters: string SetNum = "0"
    state: OFF	!color: FwStateOKNotPhysics
        action: SWITCH_ON
        action: SET(string SetNum = "1")
        action: GO_READY
        action: RESET
    state: ON	!color: FwStateOKPhysics
        action: SWITCH_OFF
        action: SET(string SetNum = "1")
        action: GO_STANDBY
        action: RESET
    state: UNDEFINED	!color: FwStateAttention3
        action: RESET
    state: TRIPPED	!color: FwStateAttention2
        action: SET(string SetNum = "1")
        action: RESET
        action: SWITCH_ON
        action: SWITCH_OFF
    state: NO_CONTROL	!color: FwStateAttention2
        action: RESET
    state: CHAN_FAULT	!color: FwStateAttention3
        action: SWITCH_ON
        action: SWITCH_OFF
        action: SET
        action: RESET
    state: RAMP_UP	!color: FwStateAttention1
        action: SET
        action: SWITCH_OFF
        action: RESET
    state: RAMP_DW	!color: FwStateAttention1
        action: SET
        action: SWITCH_ON
        action: RESET

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel000 is_of_class FwCaenChannelLV_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel001 is_of_class FwCaenChannelLV_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel002 is_of_class FwCaenChannelLV_CLASS

object: FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel003 is_of_class FwCaenChannelLV_CLASS

objectset: FWCAENCHANNELLV_FWSETSTATES {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel000,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel001,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel002,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel003 }
objectset: FWCAENCHANNELLV_FWSETACTIONS {FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel000,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel001,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel002,
	FMD_DCS:FMD1_MOD:FMD1_PW_INNER_BOTTOM:LvChannel003 }

