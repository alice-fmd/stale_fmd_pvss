#uses "fwInstallation.ctl"
main()
{
	
	// if there are any postInstallation files execute them

	int iReturn;
	int i;

	dyn_string dynPostInstallFiles_all;

	if(dpExists("fwInstallationInfo"))
	{
			// get all the post install init files
			dpGet("fwInstallationInfo.postInstallFiles:_original.._value", dynPostInstallFiles_all);
			
			
			// for each post install init file
			if( dynlen(dynPostInstallFiles_all) > 0)
			{
				
				for(i = 1; i <= dynlen(dynPostInstallFiles_all); i++)	
				{
					// execute the file	
					fwInstallation_evalScriptFile(dynPostInstallFiles_all[i] , iReturn);
				
					// check the return value
					if(iReturn == -1)
					{
						Debug("   Error executing : " + dynPostInstallFiles_all[i] + " file.");
					}
					else
					{
						Debug("  " + dynPostInstallFiles_all[i] + " - OK ");

					}
				}	
			
				// all the files were executed - if there were any errors the user has been informed
				// clearing the fwInstallationInfo.postInstallFiles:_original.._value
				
				dynClear(dynPostInstallFiles_all);
			
				dpSet("fwInstallationInfo.postInstallFiles:_original.._value", dynPostInstallFiles_all);
			}

			// get all the post delete files
			dpGet("fwInstallationInfo.postDeleteFiles:_original.._value", dynPostInstallFiles_all);
			
			// for each post delete file
			for(i = 1; i <= dynlen(dynPostInstallFiles_all); i++)
			{
				fwInstallation_showMessage(makeDynString("Executing post delete  files ..."));
				
				// execute the file	
				fwInstallation_evalScriptFile(dynPostInstallFiles_all[i] , iReturn);
				
				// check the return value
				if(iReturn == -1)
				{
					fwInstallation_showMessage(makeDynString("   Error executing : " + dynPostInstallFiles_all[i] + " file."));
				}
				else
				{
					fwInstallation_showMessage(makeDynString("  " + dynPostInstallFiles_all[i] + " - OK "));
					fwInstallation_deleteFiles(dynPostInstallFiles_all[i], "");

				}
					
			}
			
			// all the files were executed - if there were any errors the user has been informed
			// clearing the fwInstallationInfo.postInstallFiles:_original.._value
			
			dynClear(dynPostInstallFiles_all);
			
			dpSet("fwInstallationInfo.postDeleteFiles:_original.._value", dynPostInstallFiles_all);
			
		}

	dpSet("_Managers.Exit", 1280+myManNum());
}

// This function is copied from the fwInstallationMain.pnl


fwInstallation_evalScriptFile(string componentInitFile , int & iReturn)
{
	
	bool fileLoaded;
	string fileInString;
	anytype retVal;
	int result;
	
	fileLoaded = fileToString(componentInitFile, fileInString);
	
	if (! fileLoaded )
	{
	
		Debug("\n Cannot load " + componentInitFile + " file");
		iReturn =  -1;
	}
	else 
	{
		
		// the evalScript 
		iReturn = evalScript(retVal, fileInString, makeDynString("$value:12345"));
		
	}
	

	
}