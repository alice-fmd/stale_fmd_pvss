
main()
{
	int i,j;	
	// This is a Script running at StartUp 

	DebugN("*******************************************");
	DebugN("*               FMD DCS V. 1.0          *");
	DebugN("*-----------------------------------------*");
	DebugN("*  StartUp manager program v.0.1          *");
	DebugN("*                                         *");
	DebugN("*                                         *");
	DebugN("*  Start the FSM engine ...               *");
	fwFsmTree_startTree();

	DebugN("*  ... FSM engine started ...             *");
	
	bool flag = true;
	string sStatus;
// 	while(flag)
// 	{
// 		dpGet("ToDo.status:_original.._value",sStatus);
// 		flag = (sStatus == "working") ? true: false;
// 		delay(10,0);
// 	}
	DebugN("*  ... FSM engine start DONE !            *");

	DebugN("*-----------------------------------------*");

	DebugN("*    Take the tree ...                    *");
	fwCU_takeTree("FMD_DCS","");
	delay(2,0);
	DebugN("*    Exclude sub trees...                 *");
        
        fwCU_excludeFullTree("FMD_INFR_RACK2","FMD_INFR_RACK2","");
 	delay(1,0);
 	DebugN("*    FMD_INFR_RACK2 -> Excluded !        *");
        
        fwCU_excludeFullTree("LVPS_CONTROLLER0","LVPS_CONTROLLER0","");
 	delay(1,0);
 	DebugN("*    LVPS_CONTROLLER0 -> Excluded !        *");

        fwCU_excludeFullTree("LVPS_CONTROLLER1","LVPS_CONTROLLER1","");
 	delay(1,0);
 	DebugN("*    LVPS_CONTROLLER1 -> Excluded !        *");
        
        
// 	for(i=0;i<=6;i++)
// 	{
// 		fwCU_excludeFullTree("HMP_MP"+i+"_PW","HMP_MP"+i+"_PW","");
// 		delay(1,0);
// 		DebugN("*    HMP_MP"+i+"_PW -> Excluded !         *");
// 	}
// 	fwCU_excludeFullTree("HMP_INFR_CR4Y11","HMP_INFR_CR4Y11","");
// 	delay(1,0);
// 	DebugN("*    HMP_INFR_CR4Y11 -> Excluded !        *");
// 	fwCU_excludeFullTree("HMP_INFR_I2","HMP_INFR_I2","");
// 	delay(1,0);
// 	DebugN("*    HMP_INFR_I2 -> Excluded !            *");
// 	fwCU_excludeFullTree("HMP_INFR_I1","HMP_INFR_I1","");
// 	delay(1,0);
// 	DebugN("*    HMP_INFR_I1 -> Excluded !            *");
// 
 	fwCU_releaseTree("FMD_DCS","");
 	DebugN("*    RELEASE FMD_DCS !!               *");
 	delay(2,0);

	DebugN("*-----------------------------------------*");
	DebugN("*  END of StartUp procedure               *");
	DebugN("*******************************************");
	

}

