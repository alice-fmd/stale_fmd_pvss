#uses "fmdBaselibrary.ctl"

int main(){
  
  
	DebugN("*******************************************");
	DebugN("*              FMD DCS V. 1.0             *");
	DebugN("*-----------------------------------------*");
	DebugN("*  StartUp manager program v.0.1          *");
	DebugN("*                                         *");
	DebugN("*                                         *");
  
         
        int ret = __fmdBase_CreatefwCaenStockSetDpe();
        DebugTN("ret = ",ret);
        
        DebugN("*-----------------------------------------*");
	DebugN("* END of StartUp procedure                *");
	DebugN("*******************************************");
	
        return ret;
        
  
  }
