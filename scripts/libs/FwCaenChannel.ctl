#uses "fmdCAENlibrary.ctl"  
string fwPreviousState;
FwCaenChannel_initialize(string domain, string device)
{ 
int status;  
string fwState,SetNumber;  
 
// and set the new status  
//dpGet(device+".actual.status:_original.._value",status); 
//FwCaenChannel_valueChanged(domain, device, status, fwState);  
fwDU_getState(domain, device, fwPreviousState);  
  
} 

FwCaenChannel_valueChanged( string domain, string device, 
      int actual_dot_status, string &fwState ) 
{ 
int SetNum;  
string SetNumber;  
dyn_float dfValues;
    
	if (actual_dot_status & CAEN_TRIPMASK )     
	{     
		fwState = "TRIPPED";     
	}    
	else if (actual_dot_status & CAEN_ERRORMASK )     
	{     
		fwState = "CHAN_FAULT";     
	}   
	else if (actual_dot_status & CAEN_NOCONTROLMASK )      
	{      
		fwState = "NO_CONTROL";      
	}      
	else if (actual_dot_status & CAEN_IGNOREDMASK)        
	{        
		fwState = (fwPreviousState != "") ? fwPreviousState : (actual_dot_status ^ CAEN_ONMASK) ? "ON" : "OFF" ;        
            DebugN(">>-FwCaenChannel_valueChanged->IGNORED STATE ! Domain"+domain+"  device:"+device+" status:"+actual_dot_status+" state:"+fwState);   
	}        
	else if (!(actual_dot_status ^ CAEN_OFFMASK))
	{     
		fwState = "OFF";     
	}     
	else if (!(actual_dot_status ^ CAEN_ONMASK))
	{       
            SetNum = fmdCAEN_GetFwCaenChannelStockNumber("",device,SetNumber);
            switch(SetNum)  
            {  
              case 1:  
              case 4:  
                fwState = "ON";  
                fmdCAEN_FwCaenChannelSetStock("",device,5,false); 
                break;  
              case 5:  
              case 7: 
                fwState = "ON";  
                break;  
              case 2:  
              case 6:  
                fwState = "INTERMEDIATE";  
                fmdCAEN_FwCaenChannelSetStock("",device,3,false);  
                break;  
              case 3:  
                fwState = "INTERMEDIATE";  
                break;  
              default: 
                DebugN(">>-FwCaenChannel_valueChanged->SetNum="+SetNum+" undefined, assuming ON ! Domain"+domain+"  device:"+device+" status:"+actual_dot_status+" state:"+fwState);   
                fwState = "ON";   
                break;   
            }   
 	}     
	else if (!(actual_dot_status ^ CAEN_RAMPUPMASK ))
	{      
            SetNum = fmdCAEN_GetFwCaenChannelStockNumber("",device,SetNumber); 
		fwState = (SetNum==2)?"RAMP_UP_INTER":"RAMP_UP_ON";         
      }     
	else if (!(actual_dot_status ^ CAEN_RAMPDOWNMASK ))
	{       
            SetNum = fmdCAEN_GetFwCaenChannelStockNumber("",device,SetNumber); 
		fwState = (SetNum==6)?"RAMP_DW_INTER":"RAMP_DW_OFF";         
	}     
	else      
	{     
            DebugN(">>-FwCaenChannel_valueChanged->UNDEFINED STATE ! Domain"+domain+"  device:"+device+" status:"+actual_dot_status+" state:"+fwState);  
		fwState = "UNDEFINED";     
	}     
      fwPreviousState = fwState;   

}
FwCaenChannel_doCommand(string domain, string device, string command) 
{  
string number;  
int num;  
string fwState;  
dyn_float dfValues; 
 
      switch(command) 
      {
      	case "SWITCH_ON":
			dpSet(device+".settings.onOff",TRUE);  
                  break; 
            case "SWITCH_OFF":
 		      dpSet(device+".settings.onOff",FALSE); 
                  break; 
            case "SET":
                  fwDU_getCommandParameter(domain,device,"SetNum",number);    
                  fmdCAEN_FwCaenChannelSetStock("",device,number,true); 
                  break;
            case "GO_READY":
                  fwDU_getState(domain,device,fwState);  
                  if(fwState == "OFF")  
                  {  
                       fmdCAEN_FwCaenChannelSetStock("",device,1,false); 
  		           dpSet(device+".settings.onOff",TRUE); 
                           DebugN("switching on device 1 :",device);
                  } else {  
// ------   fmdCAEN_FwCaenChannelSetStock("",device,4,true);
                       fmdCAEN_FwCaenChannelSetStock("",device,4,true); 
                        DebugN("switching on device 4 :",device);
                  } 
                  break; 
            case "GO_BEAM_TUNING":
                  fwDU_getState(domain,device,fwState);  
                  if(fwState == "OFF")  
                  {  
                       fmdCAEN_FwCaenChannelSetStock("",device,2,false); 
  		           dpSet(device+".settings.onOff",TRUE);  
                  } else { 
                       fmdCAEN_FwCaenChannelSetStock("",device,6,true); 
                  } 
                  break;
	      case "GO_STANDBY":
                  fmdCAEN_FwCaenChannelSetStock("",device,7,false); 
                  dpSet(device+".settings.onOff",FALSE);   
                  break;
	      case "RESET": 
                  // This is a complex Reset procedure 
                  // put in order to perform an FSM exit 
                  // in the tripped state 
                  // 
                  // first set to 0 the Voltage 
                  dpSetWait(device+".settings.v0",0.0);     
                  // then perform the SwitchON/OFF
                  dpSetWait(device+".settings.onOff",TRUE);    
                  dpSetWait(device+".settings.onOff",FALSE);     
                  // finally set the Channel with the set 1 
                  fmdCAEN_FwCaenChannelSetStock("",device,1,true);  
                  // -----------------------------
                  break; 
      }   
} 
