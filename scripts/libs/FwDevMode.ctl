int Busy = 0; 
 
dyn_dyn_string Dis_ids; 
dyn_string Children; 
dyn_int Indexes;
dyn_string Lunits; 
dyn_dyn_string Incompletes;
dyn_string ChildrenInc; 
dyn_string Refered, Reference;

FwDevMode_initialize(string domain, string device)
{ 
dyn_string objs, children, items, exInfo; 
int index, i, pos; 
string lunit, lunit_name; 
dyn_int lus, flags; 
string subdomain, subobj; 
 
    while(Busy)  
	delay(0, 100);  
    Busy = 1;  
    pos = strpos(device,"_FWDM"); 
    lunit = substr(device, 0, pos); 
    lunit_name = lunit; 
    if((pos = strpos(lunit,"/")) >= 0) 
        lunit_name = substr(lunit, pos+1);
    children = fwFsm_getLogicalUnitDevices(domain, lunit_name); 
    for(i = 1; i <= dynlen(children); i++) 
        dynAppend(lus,0); 
    objs = fwFsm_getLogicalUnitObjects(domain, lunit_name); 
    for(i = 1; i <= dynlen(objs); i++) 
    {
	  items = fwFsm_getObjChildren(domain, objs[i], flags); 
        if(dynlen(items)) 
            dynAppend(lus,1); 
        else 
            dynAppend(lus,0); 
    } 
    dynAppend(children, objs);
    if(index = dynContains(children, device)) 
    {
        dynRemove(children, index); 
        dynRemove(lus, index); 
    }
    dpSetWait(lunit+"_FWDM.n_devices",dynlen(children)); 
    dpSetWait(lunit+"_FWDM.disabled",makeDynString());  
    if(!(index = dynContains(Lunits, lunit))) 
    { 
        index = dynAppend(Lunits, lunit); 
        Dis_ids[index] = makeDynString(); 
        Incompletes[index] = makeDynString(); 
    }
    for(i = 1; i <= dynlen(children); i++) 
    { 
        fwUi_connectEnabled("change_state",domain, children[i]); 
        if(lus[i]) 
        { 
            fwUi_connectDUModeBits("change_incomplete", domain, children[i], 0); 
        } 
/*
	  if(fwFsm_isAssociated(children[i]))  
        {
            subdomain = fwFsm_getAssociatedDomain(children[i]); 
            subobj = fwFsm_getAssociatedObj(children[i]); 
            if(subdomain != subobj) 
                fwUi_connectDUModeBits("change_reference", subdomain, subobj, 0);  
            else 
                fwUi_connectCUModeBits("change_reference", subdomain, subobj, 0, 0);    
DebugN("check ref",domain, children[i]);  
              dynAppend(Refered,subdomain+"|"+subobj); 
              dynAppend(Reference, domain+"|"+children[i]); 
        } 
*/  
        strreplace(children[i],"::","|"); 
        dynAppend(Children, children[i]); 
        dynAppend(Indexes, index);
    } 
    Busy = 0;
}
 
change_state(string dp, int value) 
{ 
int n, index, lunit_index; 
dyn_string items;
string domain, id, lunit; 
int done = 0; 
 
    items = strsplit(dp,":|.");  
    domain = items[2];  
    id = items[3]; 
    if(items[4] != "mode") 
        id += "|"+items[4];
 
    while(Busy) 
	delay(0, 100); 
    Busy = 1; 
  
    compute(domain, id, value); 
    Busy = 0; 
} 
 
change_incomplete(string dp, bit32 value)  
{  
int n, index, lunit_index, inc;  
dyn_string items; 
string domain, id, lunit;  
int done = 0; 
bit32 bits;
  
    items = strsplit(dp,":|.");   
    domain = items[2];   
    id = items[3];  
    if(items[4] != "mode")  
        id += "|"+items[4]; 
  
    while(Busy)  
	delay(0, 100);  
    Busy = 1; 
  
    inc = getBit(value, FwIncompleteDevBit);  
    compute(domain, id, !inc, "Tree"); 
    Busy = 0;  
} 
 
change_reference(string dp, bit32 value)   
{   
int n, index, lunit_index, inc;   
dyn_string items;  
string domain, id, lunit, obj;   
int done = 0;  
bit32 bits; 
   
    items = strsplit(dp,":|.");    
    domain = items[2];    
    id = items[3];   
    if(items[4] != "mode")   
        id += "|"+items[4];  
   
    while(Busy)   
	delay(0, 100);   
    Busy = 1;  
   
    if(strpos(domain, "fwCU_") == 0) 
    { 
        domain = substr(domain, 5); 
        id = domain; 
    } 
    if((index = dynContains(Refered, domain+"|"+id))) 
        obj = Reference[index];
DebugN(dp, domain, id, obj, value); 
//    inc = getBit(value, FwIncompleteDevBit);   
//    compute(domain, id, !inc, "Tree");  
    Busy = 0;   
}  
  

compute(string domain, string id, int value, string inv = "") 
{  
int index, lunit_index;   
string lunit;   
int done = 0;  
 
    index = dynContains(Children, id);  
    lunit_index = Indexes[index];  
      
//    dpGet(domain+"_FWDM.disabled", dis_ids); 
    id += inv;
    if(value == 1)  
    {  
        if(index = dynContains(Dis_ids[lunit_index], id))  
        { 
            dynRemove(Dis_ids[lunit_index], index);  
            done = 1;  
        }   
    }  
    else if(value == 0)
    {  
        if(!dynContains(Dis_ids[lunit_index], id))  
        {   
            dynAppend(Dis_ids[lunit_index], id);  
            done = 1;  
        }   
    }  
    else if(value == -1) 
    {   
        if(index = dynContains(Dis_ids[lunit_index], id))   
        {  
            dynRemove(Dis_ids[lunit_index], index);   
            done = 1;   
        }    
    }   
    if(done)  
    { 
        lunit = Lunits[lunit_index];   
        dpSetWait(lunit+"_FWDM.disabled", Dis_ids[lunit_index]);   
    } 
}
FwDevMode_valueChanged( string domain, string device,
      dyn_string disabled, string &fwState )
{
	if (dynlen(disabled))
	{
		fwState = "DISABLED";
	}
	else 
	{
		fwState = "ENABLED";
	}
}


FwDevMode_doCommand(string domain, string device, string command)
{
}

