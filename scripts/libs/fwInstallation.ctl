#uses "CtrlPv2Admin"
#uses "pmon.ctl"
#uses "fwInstallationDB.ctl"

///////////////////////////////////////////////////
const string csFwInstallationToolVersion = "3.0.2";
const string csFwInstallationLibVersion = "3.0.2";
///////////////////////////////////////////////////


dyn_bool    gButtonsEnabled;
string      gUserName;
string      gPassword;
string      gDebugFlag;
int         gSelectedMan;
int         gManShifted;
bool        gRefreshManagerList;
int         gRefreshSec;
int         gRefreshMilli;
int         gRefreshTime;

string      gTcpHostName;
int         gTcpPortNumber;
int         gTcpFileDescriptor;
int         gTcpFileDescriptor2;
string      gTcpFifo;

string      gTestVariable;
bool        gShowLicenseWarning;
int         gErrorCounter;
bool        gCloseEnabled;
dyn_string	gParams;

const int g_manNumNV = 10;
const int g_manNumCtrl = 10;

const string gFwInstallationComponentName = "fwInstallation";
const string gFwInstallationConfigFile = "fwInstallation.config";

const int gFwInstallationError = -1;
const int gFwInstallationOK = 0;

bool gFwYesToAll = "FALSE";

const int EXPIRED_REQUEST_ACTION = 1;
const int EXPIRED_REQUEST_NAME = 2;
const int EXPIRED_REQUEST_VERSION = 3;
const int EXPIRED_REQUEST_EXECUTION_DATE = 4;


/** This function needs to be called before the first use of the installation library and after each installation.

@return 0 - "success"  -1 - error 
@author S. Schmeling
*/


int fwInstallation_init()
{
	dyn_string dataPointTypes;
	int dataPointTypeExists;
	int dpCreateResult;
	dyn_string 	dynPostInstallFiles_all; // all the postInstall init files to be executed
	dyn_string dynScripts;
	int i;
	int iReturn;
	dyn_float dreturnf;
	dyn_string dreturns;
	string testString, startMode;
	dyn_string libs;
	
	// check if library exists in config file otherwise add it
	paCfgReadValueList(PROJ_PATH + CONFIG_REL_PATH + "config", "ui", "LoadCtrlLibs", libs);
	if(dynContains(libs, "fwInstallation.ctl") == 0)
		paCfgInsertValue(PROJ_PATH + CONFIG_REL_PATH + "config", "ui", "LoadCtrlLibs", "fwInstallation.ctl");		

	paCfgReadValueList(PROJ_PATH + CONFIG_REL_PATH + "config", "ctrl", "LoadCtrlLibs", libs);
	if(dynContains(libs, "fwInstallation.ctl") == 0)
		paCfgInsertValue(PROJ_PATH + CONFIG_REL_PATH + "config", "ctrl", "LoadCtrlLibs", "fwInstallation.ctl");		
	
	// get the dpts from the system	
	dataPointTypes = dpTypes();
	
	// check whether the _FwInstallationComponents dpt exists
	dataPointTypeExists = dynContains(dataPointTypes, "_FwInstallationComponents");
	
	
	if ( dataPointTypeExists > 0)
	{
		// if it exists
		
		// SMS: Perform a check if the new datapoint format has already been applied

		dpCreate("fw_InstallationProbing","_FwInstallationComponents");		
		if(!dpExists("fwInstallation_fwInstallationProbing.helpFile"))
		{
			// change the installation tool internal data points	
			_fwInstallation_createDataPointTypes(FALSE, dpCreateResult);
		}	
		dpDelete("fw_InstallationProbing");
		
		// check whether there are some postInstall init files to be executed

		
		if(dpExists("fwInstallationInfo"))
		{
			// get all the post install init files
			dpGet("fwInstallationInfo.postInstallFiles:_original.._value", dynPostInstallFiles_all);
			// for each post install init file
			for(i = 1; i <= dynlen(dynPostInstallFiles_all); i++)
			{
				DebugN("Executing post install init files ...");
				
				// execute the file	
				fwInstallation_evalScriptFile(dynPostInstallFiles_all[i] , iReturn);
				
				// check the return value
				if(iReturn == -1)
				{
					DebugN("   Error executing : " + dynPostInstallFiles_all[i] + " file.");
				}
				else
				{
					DebugN("  " + dynPostInstallFiles_all[i] + " - OK ");

				}
					
			}
			
			// all the files were executed - if there were any errors the user has been informed
			// clearing the fwInstallationInfo.postInstallFiles:_original.._value
			
			dynClear(dynPostInstallFiles_all);
			
			dpSet("fwInstallationInfo.postInstallFiles:_original.._value", dynPostInstallFiles_all);
			
		}
	}
	else
	{
		//	Debug("\n The data point types for the installation tool do not exist");
		// starting the installatio tool for the first time
		
		DebugN("Starting the Installation Tool for the first time");
		DebugN("Importing the Data Point Types ... ");
	
		// create the installation tool internal data points	
		_fwInstallation_createDataPointTypes(TRUE, dpCreateResult);
		
		// check the result
		if (dpCreateResult == 0)
		{
			// display the info for the user
			DebugN("Internal data points for the installation tool created");
			//Added by FVR on 05/07/2006
			dpCreate("fwInstallationInfo" , "_FwInstallationInformation");
		}
		else
		{
			// display the error information
			DebugN("ERROR: in creating the internal data points. Please restart the application");
		}

		

		// check if fwScripts.lst exists
		// if not generate it and add an entry to it
				
		if(access(getPath(SCRIPTS_REL_PATH) + "fwScripts.lst", F_OK) == 0)
		{

		}
		else
		{
			// the file does not exist - create it
			dynAppend(dynScripts, "fwInstallationFakeScript.ctl");
			fwInstallation_saveFile( dynScripts, getPath(SCRIPTS_REL_PATH) + "fwScripts.lst");
		}
	}

  //Install installation agent:
  if(!fwInstallation_isAgentInstalled())      
    if(fwInstallation_installAgent())
    {
      DebugN("ERROR: fwInstallation_init() -> Could not install FW installation Agent");
      return -1;
    }
    
  fwInstallation_updateVersion();
  
  return 0;
}

int fwInstallation_updateVersion()
{
  string version;
  
  int error = fwInstallation_getToolVersion(version);
  
  if(error != 0){
    DebugN("ERROR: fwInstallation_getToolVersion() -> Could not update the installation tool version");
    return -1;
  }
    
  if(version != csFwInstallationToolVersion)
    dpSet(getSystemName() + "fwInstallationInfo.version", csFwInstallationToolVersion);

  return 0;
}

int fwInstallation_getToolVersion(string &version, string systemName = "")
{
  if(systemName == "")
    systemName = getSystemName();
    
  if(!patternMatch("*:", systemName))
    systemName += ":";
  
  if(!dpExists(systemName + "fwInstallationInfo.version")){
    version = "";
    return -1;
  }
  else {
    dpGet(systemName + "fwInstallationInfo.version", version);    
  }
  
  return 0;
}

bool fwInstallation_checkToolVersion(string systemName = "")
{
  bool versionOK = true;
  string version; 
  dyn_string ds1, ds2;
  int a, b;
  int max;
  
  if(systemName == "")
    systemName = getSystemName();
    
  if(!patternMatch("*:", systemName))
    systemName += ":";
    
  if(fwInstallation_getToolVersion(version, systemName) != 0)
    return false;
  
  ds1 = strsplit(version, ".");
  ds2 = strsplit(csFwInstallationToolVersion, ".");
  
  if(dynlen(ds1) > 0){

    if(dynlen(ds1) > dynlen(ds2))
      max = dynlen(ds2);
    else
      max = dynlen(ds1);
      
    for(int i = 1; i <= max; i++){

      sscanf(ds1[i], "%d", a);
      sscanf(ds2[i], "%d", b);

      if(b > a)
        versionOK = false;
      
    }  
  }else
    versionOK = false;  

  return versionOK;

}

bool fwInstallation_isAgentInstalled()
{
   if(dpExists(getSystemName() + "fwInstallation_agentParametrization."))
     return true;
   else
     return false;  
}





int  fwInstallation_installAgent()
{
  int error = 0;


  dyn_string dataPointTypes = dpTypes();

  // check whether the _FwInstallation_agentParametrization dpt exists
	
  if(dynContains(dataPointTypes, "_FwInstallation_agentParametrization") <= 0)
    error = _fwInstallation_createAgentDataPointType("_FwInstallation_agentParametrization", true);

  if(error){
    DebugN("ERROR: fwInstallation_installAgent() -> Could not create DPT for FW installation agent");
    return -1;
  }
  
  // check whether the fwInstallation_agentParametrization dp exists
	
  if(!dpExists("fwInstallation_agentParametrization"))
  {
    error = dpCreate("fwInstallation_agentParametrization", "_FwInstallation_agentParametrization");
    if(error){
      DebugN("ERROR: fwInstallation_installAgent() -> Could not create DP for FW installation agent");
      return -1;
    }
  }

  //Add Agent Ctrl and NV managers to the PVSS console:
  
  // add a control manager for the fwInstallationAgent.lst
  fwInstallation_addManager("PVSS00ctrl", "always", 30, 3, 3, "-num "+ g_manNumCtrl +" -f fwInstallationAgent.lst");

  // add a control manager for the FW Installation Agent
  //fwInstallation_addManager("PVSS00NV", "manual", 30, 3, 3, "-num "+ g_manNumNV +" -p fwInstallation/fwInstallationAgent.pnl -iconBar -menuBar");

  //Debug DPs:
  if(!dpExists("_CtrlDebug_CTRL_" + g_manNumCtrl)){
    dpCreate("_CtrlDebug_CTRL_" + g_manNumCtrl, "_CtrlDebug");
  }
  if(!dpExists("_CtrlDebug_UI_" + g_manNumCtrl))
    dpCreate("_CtrlDebug_UI_" + g_manNumCtrl, "_CtrlDebug");


  return error;
}


/** This function creates the Installation Component DPT and DP

@param result: this is the return value  0 - "success"   -1 - "error"

@author F. Varela
*/

int _fwInstallation_createAgentDataPointType(string type, bool create)
{
	dyn_dyn_string dynDynElements;
	dyn_dyn_int dynDynTypes;
	
	int result = 0;

    dynDynElements[1] = makeDynString (type , "");
    dynDynTypes[1] = makeDynInt (DPEL_STRUCT);

    if(type == "_FwInstallation_agentParametrization")
    {	
	  dynDynElements[2] = makeDynString ("","db");
	  dynDynElements[3] = makeDynString ("","", "connection");
	  dynDynElements[4] = makeDynString ("", "", "", "driver");
          dynDynElements[5] = makeDynString ("", "", "", "server");
          dynDynElements[6] = makeDynString ("", "", "", "username");
          dynDynElements[7] = makeDynString ("" , "", "", "password");          
          dynDynElements[8] = makeDynString ("" , "", "", "initialized");          
	  dynDynElements[9] = makeDynString ("", "", "useDB");
	  dynDynElements[10] = makeDynString ("", "", "projectStatus");

          dynDynTypes[2] = makeDynInt (0, DPEL_STRUCT);
	  dynDynTypes[3] = makeDynInt (0, 0, DPEL_STRUCT);
	  dynDynTypes[4] = makeDynInt (0, 0, 0, DPEL_STRING);
	  dynDynTypes[5] = makeDynInt (0, 0, 0, DPEL_STRING);
	  dynDynTypes[6] = makeDynInt (0, 0, 0, DPEL_STRING);
	  dynDynTypes[7] = makeDynInt (0, 0, 0, DPEL_STRING);
	  dynDynTypes[8] = makeDynInt (0, 0, 0, DPEL_INT);
	  dynDynTypes[9] = makeDynInt (0, 0, DPEL_INT);
	  dynDynTypes[10] = makeDynInt (0, 0, DPEL_INT);
   }
    
   // Create or change the datapoint type
   if(create)
  	 result = dpTypeCreate(dynDynElements, dynDynTypes );
   else
   	 result = dpTypeChange(dynDynElements, dynDynTypes );
	
   dynClear(dynDynElements);
   dynClear(dynDynTypes);

   return result;
}



// **************************************************************************************************************
// **************************************************************************************************************
//
//
// Functions related to the handling of the config file
//
//
// **************************************************************************************************************
// **************************************************************************************************************



// **************************************************************************************************************
//
//
// Functions related to the handling of project paths
//
//
// **************************************************************************************************************

/** The function reads all project paths from the config file into a dyn_string.

@param projPaths: dyn_string which will be filled with the project paths from the config file
@return 0 - "success"  -1 - error   -2 - no prject paths in the config file (this should not happen)
@author S. Schmeling
*/

int fwInstallation_getProjPaths( dyn_string & projPaths )
{
	string configPath = getPath(CONFIG_REL_PATH);
	string configFile = configPath + "config";

	paCfgReadValueList(configFile, "general", "proj_path", projPaths, "=");
}


/** This functions writes all project paths given in a dyn_string to the config file and overwrites existing paths exept the main project path.

@param projPaths: dyn_string with the project paths for the config file
@return 0 - "success"  -1 - error 
@author S. Schmeling
*/

int fwInstallation_setProjPaths( dyn_string projPaths )
{
	dyn_string configLines;
	
	dyn_int tempPositions;
	dyn_string tempLines;
	string tempLine;
	int i,j;
	bool sectionFound = FALSE;
	
	string configPath = getPath(CONFIG_REL_PATH);
	string configFile = configPath + "config";

	if(_fwInstallation_getConfigFile(configLines) == 0)
	{
		for (i=1; i<=dynlen(configLines); i++)
		{
			tempLine = configLines[i];
			if(strpos(tempLine, "proj_path") >= 0)
			{
				dynAppend(tempPositions,i);
			}
		}
		if(dynlen(tempPositions)>0)
		{
			sectionFound = TRUE;
			dynClear(tempLines);
			for (j=1; j<=dynlen(projPaths); j++)
			{
				tempLine = "proj_path = \"" + projPaths[j] + "\"";
				dynAppend(tempLines,tempLine);
			}
			for (j=dynlen(tempPositions); j>=1; j--)	
			{
				dynRemove(configLines,tempPositions[j]);
			}
			dynInsertAt(configLines,tempLines,tempPositions[1]);
		}
		if(sectionFound == TRUE)
		{
			return fwInstallation_saveFile(configLines, configFile);
		} else {
			return -2;
		}
	} else {
		return -1;
	}
}


/** This function add a project path to the config file.

@param projPath: string that contains the project path to be added to the config file
@param position: position of the added path in the list (n = specified position, try 999 for last before main project path)
@return 0 - "success"  -1 - error  -2 - position out of range
@author S. Schmeling
*/

int fwInstallation_addProjPath( string projPath, int position)
{
	dyn_string configLines;
	
	dyn_int tempPositions;
	string tempLine;
	int i,j;
	bool sectionFound = FALSE;
	
	string configPath = getPath(CONFIG_REL_PATH);
	string configFile = configPath + "config";

	if(_fwInstallation_getConfigFile(configLines) == 0)
	{
		for (i=1; i<dynlen(configLines); i++)
		{
			tempLine = configLines[i];
			if(strpos(tempLine, "proj_path") >= 0)
			{
				dynAppend(tempPositions,i);
			}
		}
		if(dynlen(tempPositions)>0)
		{
			sectionFound = TRUE;
			tempLine = "proj_path = \"" + projPath + "\"";
			if(position > 0) 
			{
				if(position < dynlen(tempPositions))
				{
					dynInsertAt(configLines,tempLine,tempPositions[position]);
				} else {
					dynInsertAt(configLines,tempLine,tempPositions[dynlen(tempPositions)]);
				}
			}			
		}
		if(sectionFound == TRUE)
		{
			return fwInstallation_saveFile(configLines, configFile);
		} else {
			return -2;
		}
	} else {
		return -1;
	}
}


/** This function removes the given project path from the config file.

@param projPath: string that contains the project path to be removed from the config file
@return 0 - "success"  -1 - general error   -2 - project path does not exist
@author S. Schmeling
*/

int fwInstallation_removeProjPath( string projPath )
{
	dyn_string configLines;
	
	dyn_int tempPositions;
	dyn_string tempLines;
	string tempLine;
	int i,j;
	bool sectionFound = FALSE;
	
	string configPath = getPath(CONFIG_REL_PATH);
	string configFile = configPath + "config";

	if(_fwInstallation_getConfigFile(configLines) == 0)
	{
		for (i=1; i<=dynlen(configLines); i++)
		{
			tempLine = configLines[i];
			if(strpos(tempLine, "proj_path") >= 0)
			{
				if(strpos(tempLine, projPath) >= 0)
				{
					dynAppend(tempPositions,i);
				}
			}
		}
		if(dynlen(tempPositions)>0)
		{
			sectionFound = TRUE;
			for (j=dynlen(tempPositions); j>=1; j--)	
			{
				dynRemove(configLines,tempPositions[j]);
			}
		}
		if(sectionFound == TRUE)
		{
			return fwInstallation_saveFile(configLines, configFile);
		} else {
			return -2;
		}
	} else {
		return -1;
	}
}

// **************************************************************************************************************
//
//
// Functions related to the handling of libraries in the config file
//
//
// **************************************************************************************************************

/** This function adds a library entry to a specified section of the config file.

@param section: string to define the section where the library has to be added (will be created if not existing)
@param libraryName: dyn_string containing the library names to be added
@return 0 - "success"  -1 - error 
@author S. Schmeling
*/

int fwInstallation_addLibrary( string section, dyn_string libraryName )
{
	dyn_string configLines;
	
	dyn_int tempPositions;
	string tempLine;
	int i,j,k;
	bool sectionFound = FALSE;
	
	string configPath = getPath(CONFIG_REL_PATH);
	string configFile = configPath + "config";

	j = -1;

	if(dynlen(libraryName) > 0)
	{
		if(_fwInstallation_getConfigFile(configLines) == 0)
		{
			for (i=1; i<dynlen(configLines); i++)
			{
				tempLine = configLines[i];
				if(strpos(strltrim(strrtrim(tempLine)), "["+section+"]") = 0)
				{
					j = i;
					break;
				}
			}
			if(j > 0)
			{
				sectionFound = TRUE;
				for(k=dynlen(libraryName); k>0; k--)
				{
					tempLine = "LoadCtrlLibs = \"" + libraryName[k] + "\"";
					dynInsertAt(configLines,tempLine,j+1);
				}
			}
			if(sectionFound == TRUE)
			{
				return fwInstallation_saveFile(configLines, configFile);
			} else {
				return -2;
			}
		} else {
			return -1;
		}
	}
}


// **************************************************************************************************************
//
//
// Functions related to the handling of sections
//
//
// **************************************************************************************************************

/** This function adds the given lines to a section in the config file.

@param section: string to define the section where the data has to be added (will be created if not existing)
@param configEntry: dyn_string containing the lines to be added
@return 0 - "success"  -1 - error 
@author S. Schmeling
*/

int fwInstallation_addToSection( string section, dyn_string configEntry )
{
	dyn_string configLines;
	
	dyn_int tempPositions;
	dyn_string tempLines;
	string tempLine;
	int i,j;
	bool sectionFound = FALSE;
	
	string configPath = getPath(CONFIG_REL_PATH);
	string configFile = configPath + "config";

	j = -1;

	if(_fwInstallation_getConfigFile(configLines) == 0)
	{
		for (i=1; i<=dynlen(configLines); i++)
		{
			tempLine = configLines[i];
			if(strpos(strltrim(strrtrim(tempLine)), "["+section+"]") == 0)
			{
				j = i;
				break;
			}
		}
		tempLines = configEntry;
		if(j > 0)
		{
			if(j+1 <= dynlen(configLines))
				dynInsertAt(configLines,tempLines,j+1);
			else
				dynAppend(configLines,tempLines);			
		} else {
			tempLine = "[" + section + "]";
			dynInsertAt(tempLines,tempLine,1);
			dynAppend(configLines,tempLines);
		}
		return fwInstallation_saveFile(configLines, configFile);
	} else {
		return -1;
	}
}





/** This function gets a specified section into a dyn_string

@param section: string to define the section 
@param configEntry: dyn_string that will contain the lines for the section
@return 0 - "success"  -1 - error  -2 - section does not exist
@author S. Schmeling
*/

int fwInstallation_getSection( string section, dyn_string & configEntry )
{
	dyn_string configLines;
	
	dyn_string tempLines;
	string tempLine;
	int i,j;
	bool sectionFound = FALSE;
	
	string configPath = getPath(CONFIG_REL_PATH);
	string configFile = configPath + "config";

	if(_fwInstallation_getConfigFile(configLines) == 0)
	{
		for (i=1; i<=dynlen(configLines); i++)
		{
			tempLine = configLines[i];
			if(strpos(strltrim(strrtrim(tempLine)), "["+section+"]") == 0)
			{
				if(sectionFound == FALSE)
				{
					sectionFound = TRUE;
				}
				j = 1;
				do
				{
					if(i+j <= dynlen(configLines))
					{
						tempLine = configLines[i+j];
						if(strpos(strltrim(strrtrim(tempLine)),"[") != 0)
						{
//							if(tempLine != "")
//							{
							dynAppend(tempLines,tempLine);
//							}
							j++;
						}
					}
				}
				while ((strpos(strltrim(strrtrim(tempLine)),"[") != 0) && (i+j <=dynlen(configLines)));
				i += j-1;
			}
		}
		if(sectionFound == TRUE)
		{
			configEntry = tempLines;
			return 0;
		} else {
			return -2;
		}
	} else {
		return -1;
	}
}


/** This function sets a specified section from a dyn_string

@param section: string to define the section to where the data has to written
@param configEntry: dyn_string that contains the lines for the section
@return 0 - "success"  -1 - error
@author S. Schmeling
*/

int fwInstallation_setSection( string section, dyn_string configEntry )
{
	if(fwInstallation_clearSection( section ) != -1)
	{
		return fwInstallation_addToSection( section, configEntry );	
	} else {
		return -1;
	}
}


/** This function will delete all entries of the specified section as well as all but the first header.

@param section: string to define the section which will be cleared (first header will stay)
@return 0 - "success"  -1 - error  -2 - section does not exist
@author S. Schmeling
*/

int fwInstallation_clearSection( string section )
{
	dyn_string configLines;
	
	dyn_int tempPositions;
	dyn_string tempLines;
	string tempLine;
	int i,j;
	bool sectionFound = FALSE;
	
	string configPath = getPath(CONFIG_REL_PATH);
	string configFile = configPath + "config";

	if(_fwInstallation_getConfigFile(configLines) == 0)
	{
		for (i=1; i<=dynlen(configLines); i++)
		{
			tempLine = configLines[i];
			if(strpos(strltrim(strrtrim(tempLine)), "["+section+"]") == 0)
			{
				if(sectionFound == FALSE)
				{
					sectionFound = TRUE;
				} else {
					dynAppend(tempPositions,i);
				}
				if(i < dynlen(configLines))
				{
					j = 1;
					do
					{
						tempLine = configLines[i+j];
						if(strpos(strltrim(strrtrim(tempLine)),"[") != 0)
						{
							dynAppend(tempPositions,i+j);
							if(tempLine != "")
							{
								dynAppend(tempLines,tempLine);
							}
							j++;
						}
					}
					while ((strpos(strltrim(strrtrim(tempLine)),"[") != 0) && (i+j <=dynlen(configLines)));
					i += j-1;
				}
			}
		}
		if(dynlen(tempPositions)>0)
		{
			for (i=dynlen(tempPositions); i>0; i--)
			{
				dynRemove(configLines, tempPositions[i]);
			}
		}			
		if(sectionFound == TRUE)
		{
			return fwInstallation_saveFile(configLines, configFile);
		} else {
			return -2;
		}
	} else {
		return -1;
	}
}


/** This function will put together the specified section, that consists of several distributed parts.

@param section: string to define the section which will be compacted
@return 0 - "success"  -1 - error  -2 - section does not exist
@author S. Schmeling
*/

int fwInstallation_compactSection( string section )
{
	dyn_string configLines;
	int returnValue;
	returnValue = fwInstallation_getSection(section, configLines);
	if (returnValue == 0)
	{
		fwInstallation_clearSection(section);
		returnValue = fwInstallation_setSection(section, configLines);
		return returnValue;
	} else {
		return returnValue;
	}
	
}

// **************************************************************************************************************
// **************************************************************************************************************
//
//
// Functions related to the Component Installation
//
//
// **************************************************************************************************************
// **************************************************************************************************************


/** This function returns post install files that are scheduled to run

@param allPostInstallFiles:	dyn_string to contain the list of post install files
@return 0 - "success"  -1 - error 
@author S. Schmeling
*/
int fwInstallation_postInstallToRun(dyn_string & allPostInstallFiles)
{
	dyn_string dynPostInstallFiles_all;
	if(dpExists("fwInstallationInfo"))
	{
		// get all the post install init files
		dpGet("fwInstallationInfo.postInstallFiles:_original.._value", dynPostInstallFiles_all);
		allPostInstallFiles = dynPostInstallFiles_all;
		return 0;
	} else {
		dynClear(allPostInstallFiles);
		return -1;
	}
}







/** This function returns the following property of the installed component: list of files for this component

@param componentName: string with the name of the component 
@param componentProperty: name of the requested property
@param componentInfo: variable that contains the property of the component
@return 0 - "success"  -1 - error 
@author S. Schmeling
*/

int fwInstallation_getComponentInfo( string componentName, string componentProperty, dyn_anytype & componentInfo )
{
	string temp_componentProperty, temp_string;
	float temp_float;
	dyn_anytype temp_dyn_string;
	bool temp_bool;
	int i;
	
	temp_componentProperty = strtolower(componentProperty);
	
	switch(temp_componentProperty)
	{
		case "componentfiles": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".componentFiles:_original.._value", temp_dyn_string);
			dynAppend(componentInfo, temp_dyn_string);
			return i;
			break;
		case "configGeneral": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".configFiles.configGeneral:_original.._value", temp_dyn_string);
			dynAppend(componentInfo, temp_dyn_string);
			return i;
			break;
		case "configLinux": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".configFiles.configLinux:_original.._value", temp_dyn_string);
			dynAppend(componentInfo, temp_dyn_string);
			return i;
			break;
		case "configWindows": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".configFiles.configWindows:_original.._value", temp_dyn_string);
			dynAppend(componentInfo, temp_dyn_string);
			return i;
			break;
		case "initfiles": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".initFiles:_original.._value", temp_dyn_string);
			dynAppend(componentInfo, temp_dyn_string);
			return i;
			break;
		case "postinstallfiles": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".postInstallFiles:_original.._value", temp_dyn_string);
			dynAppend(componentInfo, temp_dyn_string);
			return i;
			break;
		case "dplistfiles": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".dplistFiles:_original.._value", temp_dyn_string);
			dynAppend(componentInfo, temp_dyn_string);
			return i;
			break;
		case "requiredcomponents": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".requiredComponents:_original.._value", temp_dyn_string);
			dynAppend(componentInfo, temp_dyn_string);
			return i;
			break;
		case "subcomponents": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".subComponents:_original.._value", temp_dyn_string);
			dynAppend(componentInfo, temp_dyn_string);
			return i;
			break;
		case "scriptfiles": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".scriptFiles:_original.._value", temp_dyn_string);
			dynAppend(componentInfo, temp_dyn_string);
			return i;
			break;
		case "date": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".date:_original.._value", temp_string);
			dynAppend(componentInfo, temp_string);
			return i;
			break;
		case "descfile": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".descFile:_original.._value", temp_string);
			dynAppend(componentInfo, temp_string);
			return i;
			break;
		case "installationdirectory": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".installationDirectory:_original.._value", temp_string);
			dynAppend(componentInfo, temp_string);
			return i;
			break;
		case "componentversion": 
		case "componentversionstring": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".componentVersionString:_original.._value", temp_string);
			if(temp_string == "")
			{
				i += dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".componentVersion:_original.._value", temp_float);
				if(temp_float == floor(temp_float))
				{
					sprintf(temp_string,"%2.1f",temp_float);
				} else {
					temp_string = temp_float;
				}
				dpSetWait("fwInstallation_" + componentName + ".componentVersionString:_original.._value", componentVersionString);
			}
			dynAppend(componentInfo, temp_string);
			return i;
			break;
		case "requiredinstalled": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".requiredInstalled:_original.._value", temp_bool);
			dynAppend(componentInfo, temp_bool);
			return i;
			break;
		case "isitsubcomponent": 
			i = dpGet("fwInstallation_"+strltrim(strrtrim(componentName))+".isItSubComponent:_original.._value", temp_bool);
			dynAppend(componentInfo, temp_bool);
			return i;
			break;
		default:
			dynAppend(componentInfo, "Property not known");
			return -1;
	}			
}


/** This function gets the information about all installed components into a dyn_dyn_string structure:
	[n][1] component name
	[n][2] component version
	[n][3] path to the installation

@param componentsInfo: dyn_dyn_string that will contain all installed components and their respective version numbers
@return 0 - "success"  -1 - error   -999999 - no components installed
@author S. Schmeling
*/

int fwInstallation_getInstalledComponents(dyn_dyn_string & componentsInfo)
{
	dyn_dyn_string tempAllInfo;	
	dyn_string componentDPs;
	int i,error;
	string componentVersionString, installationDirectory;
	float componentVersion;

	componentDPs = dpNames("fwInstallation_*", "_FwInstallationComponents");

        	dynClear(tempAllInfo);
	
	error = 0;

	if(dynlen(componentDPs) == 0)
	{
		return -999999;
	} else {
		for (i=1; i<=dynlen(componentDPs); i++)
		{
			error += dpGet(componentDPs[i]+".componentVersionString:_original.._value",componentVersionString);
			error += dpGet(componentDPs[i]+".installationDirectory:_original.._value",installationDirectory);
			if(componentVersionString == "")
			{
				dpGet(componentDPs[i] + ".componentVersion:_original.._value", componentVersion);
				if(componentVersion == floor(componentVersion))
				{
					sprintf(componentVersionString,"%2.1f",componentVersion);
				} else {
					componentVersionString = componentVersion;
				}
				dpSetWait(componentDPs[i] + ".componentVersionString:_original.._value", componentVersionString);
			}
			dynAppend(tempAllInfo[i], substr(componentDPs[i],strlen(getSystemName())+15));
			dynAppend(tempAllInfo[i], componentVersionString);
			dynAppend(tempAllInfo[i], installationDirectory);
		}
		componentsInfo = tempAllInfo;
		return error;
	}
}


/** This function gets the information about all available components in the specified paths into a dyn_dyn_string structure:
	- component name
	- component version
	- subcomponent [yes/no]
	- path to the description file

@param componentPaths: dyn_string with the paths to description files
@param componentsInfo: dyn_dyn_string that will contain all installed components and their respective version numbers and their paths
@return 0 - "success"  -1 - error 
@author S. Schmeling, adapted from the panel function getInstalledComponents
*/

int fwInstallation_getAvailableComponents( dyn_string componentPaths, dyn_dyn_string & componentsInfo, string component = "*", bool scanRecursively = false)
{
 	string dirCurrentValue;
	dyn_string dynAvailableDescriptionFiles;
	string componentFileName;
	string strComponentFile;
	string tagName;
	string tagValue;

	string componentName;
	float componentVersion;
	string componentVersionString;
	
	int result;
	
	bool	fileLoaded;
	bool isItSubComponent = false;
	
	int i, j, ii, iii;

	dyn_dyn_string tempAllInfo;
	dynClear(tempAllInfo);
	iii = 0;

	if(dynlen(componentPaths) == 0)
	{
		return -1;
	}

	for(ii=1; ii<=dynlen(componentPaths); ii++)
	{
		dirCurrentValue = componentPaths[ii];
		// it the directory name is empty
		if (dirCurrentValue != "")
		{
			// read the names of files that have the .xml extension in a directory specified by dirCurrentValue
			//FVR: Do it recursively
			if(scanRecursively)
  	          dynAvailableDescriptionFiles =  fwInstallation_getFileNamesRec(dirCurrentValue, component + ".xml");
 	        else
	          dynAvailableDescriptionFiles =  getFileNames(dirCurrentValue, component + ".xml");			

			// for each component description file, read the component name, version and display it in the graphic table
			
			for( i = 1; i <= dynlen(dynAvailableDescriptionFiles); i++)
			{
				// get the file name of an .xml description file
				componentFileName = dynAvailableDescriptionFiles[i];
				
				// load the description file
				fileLoaded = fileToString(dirCurrentValue + "/" + componentFileName, strComponentFile);
				
				if (! fileLoaded )
				{
					DebugN("fwInstallation: Cannot load " + componentFileName + " file for checking");	
				}
				else 
				{
					// the description file in now in string - strComponentFile
					
					// remove the root tag <component>
					
					strreplace(strComponentFile, "<component>", "");
					strreplace(strComponentFile, "</component>", "");
					
					// while there are some tags in the string - get the next tag		
					while( fwInstallation_getTagFromString(tagName, tagValue, strComponentFile) == 1 )
					{
						switch(tagName)
						{
														// retrieve the component name
									case "name": 		componentName = tagValue;
														break;
														
														// retrieve the component version
									case "version" :	componentVersionString = tagValue;
														componentVersion = tagValue;
														break;
														
															// retrieve the information - is it subcomponent
									case "subComponent":	if((tagValue == "yes") || (tagValue == "Yes") || (tagValue == "YES"))
															{
																isItSubComponent = true;
															}
														break;
						}
					}
					
					// check whether the description file contains the component name
					
					if(componentName != "") 
					{
						iii++;
						dynAppend(tempAllInfo[iii], componentName);
						dynAppend(tempAllInfo[iii], componentVersionString);
						if(isItSubComponent)
						{
							dynAppend(tempAllInfo[iii], "yes");
						} else {
							dynAppend(tempAllInfo[iii], "no");
						}
						dynAppend(tempAllInfo[iii], dirCurrentValue + "/" + componentFileName);
					 	componentName = "";
						isItSubComponent = false;
					}
				
				}
				
			}
		}
	}
	componentsInfo = tempAllInfo;
	return 0;
}


/** This function will install the component named by the description file. No dependency checking is done!

@param componentPaths: string with the locations of the description files
@return 0 - "success"  -1 - error 
@author S. Schmeling
*/

int fwInstallation_installComponents( dyn_string componentPaths , string destinationDir )
{
	string componentsInfo;
	string descFilesInfo;
	string sourceDir;
	dyn_float dreturnf; // the return value of a child panel
	dyn_string dreturns; // the return value of a child panel
	
	dyn_string componentsNames;
	dyn_string componentFiles;
	dyn_string componentsVersions;
	dyn_string componentFilesInOrder;
	dyn_string dynNamesVersions;
	dyn_string tmpDynParamValue;
	bool componentInstalled = false;
	string strComponentFile;
	bool fileLoaded;
	
	string componentName, tagName, tagValue, componentVersionString;
	float componentVersion;
	int dpGetResult;
	
	string temp_string;
	dyn_string dynPostInstallFiles_all;	
	int i,error;

	// get all the post install init files
	dpGetResult = dpGet("fwInstallationInfo.postInstallFiles:_original.._value", dynPostInstallFiles_all);

	
	if(destinationDir == "")
	{	
		return -1;
	}
	else
	{

		dpSet("fwInstallationInfo.installationDirectoryPath:_original.._value", destinationDir);
		// retrieve the component names and the description files of the components to be installed
		for(  i = 1 ; i <= dynlen(componentPaths); i++)
		{
			temp_string = componentPaths[i];
			fileLoaded = fileToString(temp_string, strComponentFile);
			if (! fileLoaded )
			{
				DebugN("fwInstallation: Cannot load " + componentPaths[i] + " file ");	
				return -1;
			}
			else 
			{
				// the description file in now in string - strComponentFile
				// remove the root tag <component>
				strreplace(strComponentFile, "<component>", "");
				strreplace(strComponentFile, "</component>", "");
				// while there are some tags in the string - get the next tag		
				while( fwInstallation_getTagFromString(tagName, tagValue, strComponentFile) == 1 )
				{
					switch(tagName)
					{
											// retrieve the component name
						case "name": 		componentName = tagValue;
											break;
											// retrieve the component version
						case "version" :	componentVersionString = tagValue;
//											componentVersion = tagValue;
											break;
												// retrieve the information - is it subcomponent
						case "subComponent":	if((tagValue == "yes") || (tagValue == "Yes") || (tagValue == "YES"))
												{
													isItSubComponent = true;
												}
		 										break;
					}
				}
				// check whether the description file contains the component name
				if(componentName != "") 
				{
					dynAppend(componentsNames, componentName);
					dynAppend(componentsVersions, componentVersionString);
					dynAppend(componentFiles, temp_string);
				}
			}
		}
		fwInstallation_putComponentsInOrder_Install(componentsNames, componentsVersions, componentFiles, componentFilesInOrder);
		// install every component
		for (i =1; i <= dynlen(componentFilesInOrder); i++)
		{
			_fwInstallation_installComponent(componentFilesInOrder[i], false, componentName, componentInstalled, destinationDir, dynPostInstallFiles_all, error);
			if(error!=0)
			{
				return error;
			}
		}
	}
}


/** This function allows to show a timed out popup during installation

@param popupText		text to be shown
@author Sascha Schmeling
*/

int fwInstallation_popup(string popupText)
{
	dyn_float df;
	dyn_string ds;
	
	if(myManType() == UI_MAN)
	{
		ChildPanelOnCentralReturn("fwInstallation/fwInstallation_popup.pnl", "fwInstallation",
                                          makeDynString("$text:"+popupText), df, ds);
	}
}


/** This function allows to insert a manager into a project. 
The function is meant to be used in the init or postInstall scripts of a framework
component. It will popup a user interface asking whether to activate the manager if needed.

@param defActivated		FALSE - deactivated, TRUE - activated (if the user interface times out)
@param manTitle				title to be shown to the user
@param manager				name of the manager
@param startMode			{manual, once, always}
@param secKill				seconds to kill after stop
@param restartCount		number of restarts
@param resetMin				restart counter reset time (minutes)
@param commandLine		commandline for the manager
@return 1 - manager added, 2 - manager already existing, 3 - no user interface, 0 - manager addition failed
@author Sascha Schmeling
*/

int fwInstallation_appendManager(	bool defActivated, string manTitle, 
																	string manager, string startMode, 
																	int secKill, int restartCount, int resetMin,
																	string commandLine)
{
	int error;
	dyn_float df;
	dyn_string ds;
	
	bool activateManagersDisabled;
	dpGet("fwInstallationInfo.activateManagersDisabled", activateManagersDisabled);
	
	if(startMode == "manual")
			error = fwInstallation_addManager(manager, startMode, secKill, restartCount, resetMin, commandLine);
	else
		if(myManType() == UI_MAN)
		{
			ChildPanelOnCentralReturn("fwInstallation/fwInstallation_addManager.pnl", "fwInstallation", 
																makeDynString("$manTitle:"+manTitle), df, ds);
//	DebugN(df, ds);
			if((ds[1] == "timeout" && defActivated) || ds[1] == "ALLOW")
				error = fwInstallation_addManager(manager, startMode, secKill, restartCount, resetMin, commandLine);
			else
				error = fwInstallation_addManager(manager, "manual", secKill, restartCount, resetMin, commandLine);
		} else {
			if(!activateManagersDisabled)
				error = fwInstallation_addManager(manager, startMode, secKill, restartCount, resetMin, commandLine);
			else
				error = fwInstallation_addManager(manager, "manual", secKill, restartCount, resetMin, commandLine);
			if(error ==1)
			{
				DebugN("The installation tool appended a manager to your project. If you want it to start 'always' or 'once', please change the start mode in the console by hand!");
				error = 3;
			}
		}
	return error;
}


/** This function allows to insert a driver into a project. 
The function is meant to be used in the init or postInstall scripts of a framework
component. It will popup a user interface asking whether to activate the driver
or the corresponding simulator if needed.

@param defActivated		NONE - deactivated, {DRIVER,SIM} - activated the respective manager (if the user interface times out)
@param manTitle				title to be shown to the user
@param manager				name of the manager
@param startMode			{manual, once, always}
@param secKill				seconds to kill after stop
@param restartCount		number of restarts
@param resetMin				restart counter reset time (minutes)
@param commandLine		commandline for the manager
@return 1 - manager added, 2 - manager already existing, 3 - no user interface, 0 - manager addition failed
@author Sascha Schmeling
*/

int fwInstallation_appendDriver(	string defActivated, string manTitle, 
																	string manager, string startMode, 
																	int secKill, int restartCount, int resetMin,
																	string commandLine)
{
	int error;
	dyn_float df;
	dyn_string ds;
	
	bool activateManagersDisabled;
	dpGet("fwInstallationInfo.activateManagersDisabled", activateManagersDisabled);
	
	if(startMode == "manual")
	{
		error = fwInstallation_addManager(manager, startMode, secKill, restartCount, resetMin, commandLine);
		error = fwInstallation_addManager("PVSS00sim", startMode, secKill, restartCount, resetMin, commandLine);
	}	else
		if(myManType() == UI_MAN)
		{
			ChildPanelOnCentralReturn("fwInstallation/fwInstallation_addDriver.pnl", "fwInstallation", 
																makeDynString("$manTitle:"+manTitle), df, ds);
//	DebugN(df, ds);
			if(ds[1] == "timeout")
				switch(defActivated)
				{
					case "DRIVER":
						error = fwInstallation_addManager(manager, startMode, secKill, restartCount, resetMin, commandLine);
						error = fwInstallation_addManager("PVSS00sim", "manual", secKill, restartCount, resetMin, commandLine);
						break;
					case "SIM":
						error = fwInstallation_addManager(manager, "manual", secKill, restartCount, resetMin, commandLine);
						error = fwInstallation_addManager("PVSS00sim", startMode, secKill, restartCount, resetMin, commandLine);
						break;
					case "NONE":
						error = fwInstallation_addManager(manager, "manual", secKill, restartCount, resetMin, commandLine);
						error = fwInstallation_addManager("PVSS00sim", "manual", secKill, restartCount, resetMin, commandLine);
						break;
				}
			if(ds[1] == "DRIVER")
			{
				error = fwInstallation_addManager(manager, startMode, secKill, restartCount, resetMin, commandLine);
				error = fwInstallation_addManager("PVSS00sim", "manual", secKill, restartCount, resetMin, commandLine);
			}
			if(ds[1] == "SIM")
			{
				error = fwInstallation_addManager(manager, "manual", secKill, restartCount, resetMin, commandLine);
				error = fwInstallation_addManager("PVSS00sim", startMode, secKill, restartCount, resetMin, commandLine);
			}
		} else {
			if(!activateManagersDisabled) 	
			{
				switch(defActivated)
				{
					case "DRIVER":
						error = fwInstallation_addManager(manager, startMode, secKill, restartCount, resetMin, commandLine);
						error = fwInstallation_addManager("PVSS00sim", "manual", secKill, restartCount, resetMin, commandLine);
						break;
					case "SIM":
						error = fwInstallation_addManager(manager, "manual", secKill, restartCount, resetMin, commandLine);
						error = fwInstallation_addManager("PVSS00sim", startMode, secKill, restartCount, resetMin, commandLine);
						break;
					case "NONE":
						error = fwInstallation_addManager(manager, "manual", secKill, restartCount, resetMin, commandLine);
						error = fwInstallation_addManager("PVSS00sim", "manual", secKill, restartCount, resetMin, commandLine);
						break;
				}					
			} else {
				error = fwInstallation_addManager(manager, "manual", secKill, restartCount, resetMin, commandLine);
				error = fwInstallation_addManager("PVSS00sim", "manual", secKill, restartCount, resetMin, commandLine);
			}
			if(error ==1)
			{
				error = 3;
				DebugN("The installation tool appended a driver to your project. If you want it to start 'always' or 'once', please change the start mode in the console by hand!");
			}
		}
	return error;
}


/** This function allows to insert a manager into a project. It is checked before, if the 
manager already exists.

@param manager				name of the manager
@param startMode			{manual, once, always}
@param secKill				seconds to kill after stop
@param restartCount		number of restarts
@param resetMin				restart counter reset time (minutes)
@param commandLine		commandline for the manager
@return 1 - manager added, 2 - manager already existing, 3 - manager addition disabled, 0 - manager addition failed
@author Sascha Schmeling
*/

int fwInstallation_addManager(string manager, string startMode, 
															int secKill, int restartCount, int resetMin,
															string commandLine)
{
	bool failed, disabled;
	dyn_int diManPos, diStartMode, diSecKill, diRestartCount, diResetMin;
	dyn_string dsManager, dsCommandLine, dsProps;
	string str, host;
	int port, iErr = paGetProjHostPort(paGetProjName(), host, port);

	dpGet("fwInstallationInfo.addManagersDisabled", disabled);

	if(!disabled)
	{
		pmonGetManagers(failed, paGetProjName(), diManPos, dsManager, diStartMode, diSecKill, 
										diRestartCount, diResetMin, dsCommandLine);
	//	DebugN(dsManager, dsCommandLine);
	
		if(!failed)
		{
			if(dynContains(dsManager, manager))
			{
				for(int i=1; i<=dynlen(dsManager); i++)
				{
					if((dsManager[i] == manager) && (dsCommandLine[i] == commandLine))
					{
	//					DebugN("manager already existing");
						return 2;
					}
				}
				str = "##SINGLE_MGR:INS "+(dynlen(dsManager))+" "+
							manager+" "+startMode+" "+secKill+" "+restartCount+" "+
							resetMin+" "+commandLine;
	//DebugN(str);
				if(pmon_command(str, host, port, FALSE, TRUE))
				{
					return 0;
				} else {
					return 1;
				}
			} else {
				str = "##SINGLE_MGR:INS "+(dynlen(dsManager))+" "+
							manager+" "+startMode+" "+secKill+" "+restartCount+" "+
							resetMin+" "+commandLine;
	//DebugN(str);
				if(pmon_command(str, host, port, FALSE, TRUE))
				{
					return 0;
				} else {
					return 1;
				}
			}
		}	else {
			return 0;
		}
	} else
		return 3;
}




/** This function allows to find a manager in a project. 

@param manager				name of the manager
@param commandLine		commandline for the manager
@param startMode			{manual, once, always}
@return 1 - manager found, 2 manager not found, 0 - failed
@author Sascha Schmeling
*/

int fwInstallation_findManager(	string manager, string commandLine, string & startMode)
{
	bool failed;
	dyn_int diManPos, diStartMode, diSecKill, diRestartCount, diResetMin;
	dyn_string dsManager, dsCommandLine, dsProps;
	string str, host;
	int port, iErr = paGetProjHostPort(paGetProjName(), host, port);


	pmonGetManagers(failed, paGetProjName(), diManPos, dsManager, diStartMode, diSecKill, 
									diRestartCount, diResetMin, dsCommandLine);
//	DebugN(dsManager, dsCommandLine);

	if(!failed)
	{
		if(dynContains(dsManager, manager))
		{
			for(int i=1; i<=dynlen(dsManager); i++)
			{
				if((dsManager[i] == manager) && (dsCommandLine[i] == commandLine))
				{
					switch(diStartMode[i])
					{
						case 0: startMode = "manual";
										break;
						case 1: startMode = "once";
										break;
						case 2: startMode = "always";
										break;
					}
					return 1;
				}
			}
			startMode = "none";
			return 2;
		} else {
			startMode = "none";
			return 2;
		}
	} else {
		startMode = "";
		return 0;
	}
}


/** This function retrieves all managers from pmon

@param err						error code
@param projName				name of the project, "" for own
@param manPos					position of the manager
@param manager				name of the manager
@param startMode			0 manual, 1 once, 2 always
@param secKill				seconds to kill after stop
@param restartCount		number of restarts
@param resetMin				restart counter reset time (minutes)
@param commandLine		commandline for the manager
@author Sascha Schmeling

*/

pmonGetManagers(bool &err, string projName,
								dyn_int &manPos, dyn_string &manager, dyn_int &startMode, 
								dyn_int &secKill, dyn_int &restartCount, dyn_int &resetMin,
								dyn_string &commandLine)
{
	string str, host;
	int port, iErr = paGetProjHostPort(projName, host, port);
	dyn_dyn_string dsResult;
	
	if(iErr)
	{
//		pmon_warningOutput("errHostOrPort", -1);
//		err = TRUE;
//		return;
	}
	
	str = "##MGRLIST:LIST";
	
	err = pmon_query(str, host, port, dsResult, FALSE, TRUE);
//	DebugN(err, dsResult);
	
	dynClear(manPos);
	dynClear(manager);
	dynClear(startMode);
	dynClear(secKill);
	dynClear(restartCount);
	dynClear(resetMin);
	dynClear(commandLine);
	
	if(dynlen(dsResult)>0)
		for(int i=1; i<=dynlen(dsResult); i++)
		{
			dynAppend(manPos, i);
			dynAppend(manager, 			dsResult[i][1]);
			dynAppend(startMode, 		dsResult[i][2]);
			dynAppend(secKill, 			dsResult[i][3]);
			dynAppend(restartCount,	dsResult[i][4]);
			dynAppend(resetMin,			dsResult[i][5]);
			if(dynlen(dsResult[i]) == 6)
				dynAppend(commandLine,	dsResult[i][6]);
			else
				dynAppend(commandLine,  "");
		}
}
	

/** This function returns the project name

@author Sascha Schmeling

*/

string paGetProjName()
{
//	dyn_string paths = strsplit(getPath(CONFIG_REL_PATH),"/");
//	return paths[dynlen(paths)-1];
	return PROJ;
}

/** This function retrieves the system name(s) on which a certain 
"application" = component is installed.

@param applicationName	name of the application/component to be found
@param systemNames			name(s) of the system(s) with the application/component installed
@author Sascha Schmeling
*/

fwInstallation_getApplicationSystem(string applicationName, dyn_string &systemNames)
{
	string tempString;
	systemNames = dpNames("*:fwInstallation_"+applicationName, "_FwInstallationComponents");
	
	if(dynlen(systemNames) > 0)
		for(int i=1; i<=dynlen(systemNames); i++)
		{
			dpGet(systemNames[i]+".componentVersionString", tempString);
			if(tempString != "")
				systemNames[i] = dpSubStr(systemNames[i], DPSUB_SYS);
			else
				systemNames[i] = "*" + dpSubStr(systemNames[i], DPSUB_SYS) + "*";				
		}
		
	dynSortAsc(systemNames);
}

/** This function allows to make an ASCII export with specific filters.

@param fileName						path and name of the output file
@param dataPointNames			names of the dps to be put out
@param dataPointTypes			names of the dpts to be put out (default all)
@param filter							filter on dps/dpts (default dps with alerts, params, and original values)
@author Sascha Schmeling and Fernando Varela
*/

fwInstallation_createDpl(	string fileName, dyn_string dataPointNames, 
													dyn_string dataPointTypes = "",
													string filter = "DAOP")
{
	string cmdLine;
	
//  if(patternMatch("/*", fileName))
//    fileName = "." + fileName;

	if(dataPointTypes == "")
		dynClear(dataPointTypes);
	
//	cmdLine = PVSS_BIN_PATH+"PVSS00ascii -proj "+ PROJ +" -out \""+fileName+"\" -filter \""+filter+"\"";
		cmdLine = "cmd /c " + PVSS_BIN_PATH+"PVSS00ascii -proj "+ PROJ +" -out "+fileName+" -filter "+filter;
	
	if(dynlen(dataPointNames) > 0)
		for(int i=1; i<=dynlen(dataPointNames); i++){
			if(patternMatch("*&*", dataPointNames[i])){
			  dataPointNames[i] = "\"" + dataPointNames[i] + "\"";
			  //DebugN("FVR: Resulting dpName: " + dataPointNames[i]);
			}
//			cmdLine = cmdLine + " -filterDp \"" + dataPointNames[i]+"\";";
				cmdLine = cmdLine + " -filterDp " + dataPointNames[i]+";";
	  }
	if(dynlen(dataPointTypes) > 0)
		for(int i=1; i<=dynlen(dataPointTypes); i++){
//			cmdLine = cmdLine + " -filterDpType \"" + dataPointTypes[i] + "\"";
			cmdLine = cmdLine + " -filterDpType " + dataPointTypes[i];
    }	
    
//	DebugTN("command line:", cmdLine);
	system(cmdLine);											
}

/** This function creates a component description file (xml).

@param pathName						path for the output file {if empty, only create xmlDesc}
@param componentName			name of the component {required} 
@param componentVersion		version number of the component {required} 
@param componentDate			date of the component {will set current date if empty}
@param requiredComponents	list of required components (componentName=minimalVersionNumber)
@param isSubComponent			TRUE if subcomponent
@param initScripts				scripts to be run during installation 
@param deleteScripts			scripts to be run during deletion 
@param postInstallScripts	scripts to be run after installation and subsequent restart {
@param postDeleteScripts	scripts to be run after deletion and subsequent restart 
@param configFiles				config files for the project {need to be in ./config/, do not specify path}
@param asciiFiles			  	dplist files for the project {need to be in ./dplist/, do not specify path}
@param panelFiles					panel files for the project {need to be in ./panels/, do only specify sub-path}
@param scriptFiles				script files for the project {need to be in ./scripts/, do only specify sub-path}
@param libraryFiles				library files for the project {need to be in ./scripts/libs/, do only specify sub-path}
@param otherFiles					miscellaneous files for the project {do only specify sub-path from PROJ_PATH}
@param xmlDesc						complete xml description
@return 0 - xml file created, -1 - file creation failed, -2 - parameters missing
@author Sascha Schmeling and Fernando Varela.

Modified on the 27/9/2006 by FVR. Move init and post install scripts to scripts folder according to the FW Guidelines.
*/

int fwInstallation_createXml(	string pathName,
															string componentName, string componentVersion, string componentDate,
															dyn_string requiredComponents,
															bool isSubComponent,
															dyn_string initScripts, dyn_string deleteScripts,
															dyn_string postInstallScripts, dyn_string postDeleteScripts,
															dyn_string configFiles, 
															dyn_string asciiFiles,
															dyn_string panelFiles,
															dyn_string scriptFiles,
															dyn_string libraryFiles,
															dyn_string otherFiles,
															dyn_string & xmlDesc)
{
//	dyn_string xmlDesc;
	dynClear(xmlDesc);
	
	time now = getCurrentTime();
	
	dynClear(xmlDesc);

	if(requiredComponents =="")
		dynClear(requiredComponents);
	if(initScripts =="")
		dynClear(initScripts);
	if(deleteScripts =="")
		dynClear(deleteScripts);
	if(postInstallScripts =="")
		dynClear(postInstallScripts);
	if(postDeleteScripts =="")
		dynClear(postDeleteScripts);
	if(configFiles =="")
		dynClear(configFiles);
	if(asciiFiles =="")
		dynClear(asciiFiles);
	if(panelFiles =="")
		dynClear(panelFiles);
	if(scriptFiles =="")
		dynClear(scriptFiles);
	if(libraryFiles =="")
		dynClear(libraryFiles);
	if(otherFiles =="")
		dynClear(otherFiles);

	if(componentName == "")
	{
		DebugTN("Your component should have a name!");
		return -2;		
	}
	if(componentVersion == "")
	{
		DebugTN("Your component should have a version number!");
		return -2;
	}

// build xml code	
	dynAppend(xmlDesc, "<component>");
// component name
	dynAppend(xmlDesc, "<name>" + componentName + "</name>");
// component version
	dynAppend(xmlDesc, "<version>" + componentVersion + "</version>");
// release date
	if(componentDate == "")
		componentDate = year(now)+"/"+month(now)+"/"+day(now);
	dynAppend(xmlDesc, "<date>" + componentDate + "</date>");

// required components
	if(dynlen(requiredComponents) > 0)
		for(int i=1; i<=dynlen(requiredComponents); i++)
			dynAppend(xmlDesc, "<required>"+requiredComponents[i]+"</required>");

// sub-component?
	if(isSubComponent == TRUE)
		dynAppend(xmlDesc, "<subcomponent>yes</subcomponent>");


////////////////FVR: Move init and post install scripts to scripts folder according to the FW Guidelines.
// init script(s)
	if(dynlen(initScripts) > 0)
          		for(int i=1; i<=dynlen(initScripts); i++){
			  //dynAppend(xmlDesc, "<init>./config/"+initScripts[i]+"</init>");
                          dynAppend(xmlDesc, "<init>./scripts/" + componentName + "/" + initScripts[i]+"</init>");
                      }
// delete script(s)
	if(dynlen(deleteScripts) > 0)
          		for(int i=1; i<=dynlen(deleteScripts); i++){
			  //dynAppend(xmlDesc, "<delete>./config/"+deleteScripts[i]+"</delete>");
                          dynAppend(xmlDesc, "<delete>./scripts/" + componentName + "/" + deleteScripts[i]+"</delete>");
                        }
// postInstall script(s)
	if(dynlen(postInstallScripts)> 0)
          		for(int i=1; i<=dynlen(postInstallScripts); i++){
			  //dynAppend(xmlDesc, "<postInstall>./config/"+postInstallScripts[i]+"</postInstall>");
                          dynAppend(xmlDesc, "<postInstall>./scripts/" + componentName + "/" + postInstallScripts[i]+"</postInstall>");
                        }

// postDelete script(s)
	if(dynlen(postDeleteScripts) > 0)
          		for(int i=1; i<=dynlen(postDeleteScripts); i++){
			//dynAppend(xmlDesc, "<postDelete>./config/"+postDeleteScripts[i]+"</postDelete>");
                        dynAppend(xmlDesc, "<postDelete>./scripts/" + componentName + "/" + postDeleteScripts[i]+"</postDelete>");
                      }
/////////////////FVR
        
// config files
	if(dynlen(configFiles) > 0)
		for(int i=1; i<=dynlen(configFiles); i++)
			if(strpos(configFiles[i], ".windows") > 0)
				dynAppend(xmlDesc, "<config_windows>./config/"+configFiles[i]+"</config_windows>");
			else if(strpos(configFiles[i], ".linux") > 0)
				dynAppend(xmlDesc, "<config_linux>./config/"+configFiles[i]+"</config_linux>");
			else 
				dynAppend(xmlDesc, "<config>./config/"+configFiles[i]+"</config>");

// ascii import files
	if(dynlen(asciiFiles) > 0)
		for(int i=1; i<=dynlen(asciiFiles); i++)
			dynAppend(xmlDesc, "<dplist>./dplist/"+asciiFiles[i]+"</dplist>");

// panel files
	if(dynlen(panelFiles) > 0)
		for(int i=1; i<=dynlen(panelFiles); i++)
			dynAppend(xmlDesc, "<file>./panels/"+panelFiles[i]+"</file>");
// script files		
	if(dynlen(scriptFiles) > 0)
		for(int i=1; i<=dynlen(scriptFiles); i++)
			dynAppend(xmlDesc, "<file>./scripts/"+scriptFiles[i]+"</file>");
// library files
	if(dynlen(libraryFiles) > 0)
		for(int i=1; i<=dynlen(libraryFiles); i++)
			dynAppend(xmlDesc, "<file>./scripts/libs/"+libraryFiles[i]+"</file>");
// misc files
	if(dynlen(otherFiles) > 0)
		for(int i=1; i<=dynlen(otherFiles); i++)
			dynAppend(xmlDesc, "<file>./"+otherFiles[i]+"</file>");

//	DebugTN(otherFiles, componentName+ ".xml", dynContains(otherFiles, componentName+ ".xml"), xmlDesc);
	
	if(dynContains(otherFiles, componentName+ ".xml") == 0)
		dynAppend(xmlDesc, "<file>./"+componentName+ ".xml</file>");
		
	dynAppend(xmlDesc, "</component>");

// write xmlFile
	if(pathName != "")
	{
		file xmlText = fopen(pathName+"/"+componentName+".xml", "w");
		if(ferror(xmlText) != 0)
		{
			fclose(xmlText);
			return -1;
		}
		if(dynlen(xmlDesc) > 0)
		{
			for(int i=1; i<=dynlen(xmlDesc); i++)
				fputs(xmlDesc[i]+"\n", xmlText);
		}
		fclose(xmlText);
	}
}

/** This function loads a component description file (xml).

@param fileName						path and filename for the input file
@param componentName			name of the component {required} 
@param componentVersion		version number of the component {required} 
@param componentDate			date of the component {will set current date if empty}
@param requiredComponents	list of required components (componentName=minimalVersionNumber)
@param isSubComponent			TRUE if subcomponent
@param initScripts				scripts to be run during installation 
@param deleteScripts			scripts to be run during deletion 
@param postInstallScripts	scripts to be run after installation and subsequent restart
@param postDeleteScripts	scripts to be run after deletion and subsequent restart
@param configFiles				config files for the project {need to be in ./config/, do not specify path}
@param asciiFiles			  	dplist files for the project {need to be in ./dplist/, do not specify path}
@param panelFiles					panel files for the project {need to be in ./panels/, do only specify sub-path}
@param scriptFiles				script files for the project {need to be in ./scripts/, do only specify sub-path}
@param libraryFiles				library files for the project {need to be in ./scripts/libs/, do only specify sub-path}
@param otherFiles					miscellaneous files for the project {do only specify sub-path from PROJ_PATH}
@param xmlDesc						complete xml description (for experts)
@return 0 - xml file loaded, -1 - file reading failed, -2 - parameters missing
@author Sascha Schmeling
*/


//FVR Here tomorrow Tuesday: Adding a new argument to the loadXml function.//





int fwInstallation_loadXml(		string fileName,
															string & componentName, string & componentVersion, string & componentDate,
															dyn_string & requiredComponents,
															bool & isSubComponent,
															dyn_string & initScripts, dyn_string & deleteScripts,
															dyn_string & postInstallScripts, dyn_string & postDeleteScripts,
															dyn_string & configFiles, 
															dyn_string & asciiFiles,
															dyn_string & panelFiles,
															dyn_string & scriptFiles,
															dyn_string & libraryFiles,
															dyn_string & otherFiles,
															dyn_string & xmlDesc)
{

	dyn_string tempDynString;
	string strComponentFile, tagName, tagValue, tempString;
	
	bool fileLoaded = fileToString(fileName, strComponentFile);

	xmlDesc = strsplit(strComponentFile, "\n");
	
	if (! fileLoaded )
	{
		DebugN("fwInstallation: Cannot load " + fileName + " file ");	
		return -1;
	}
	else 
	{
		// the description file in now in string - strComponentFile
		// remove the root tag <component>
		strreplace(strComponentFile, "<component>", "");
		strreplace(strComponentFile, "</component>", "");
		// while there are some tags in the string - get the next tag		
		while( fwInstallation_getTagFromString(tagName, tagValue, strComponentFile) == 1 )
		{
			switch(tagName)
			{
				case "name": 					componentName = tagValue;
									break;
				case "version" :			componentVersion = tagValue;
									break;
				case "date" :					componentDate = tagValue;
									break;
				case "init" :					dynAppend(initScripts, strreplace(tagValue, "./config/", ""));
                                                                                dynAppend(initScripts, strreplace(tagValue, "./scripts/" + componentName + "/", ""));
									break;
				case "delete" :				dynAppend(deleteScripts, strreplace(tagValue, "./config/", ""));
                                                                        dynAppend(deleteScripts, strreplace(tagValue, "./scripts/" + componentName + "/", ""));

									break;
				case "postInstall" :	                dynAppend(postInstallScripts, strreplace(tagValue, "./config/", ""));
                                                                        dynAppend(postInstallScripts, strreplace(tagValue, "./scripts/" + componentName + "/", ""));
									break;
				case "postDelete" :	        	dynAppend(postDeleteScripts, strreplace(tagValue, "./config/", ""));
                                                                        dynAppend(postDeleteScripts, strreplace(tagValue, "./scripts/" + componentName + "/", ""));
									break;
				case "config" :				dynAppend(configFiles, strreplace(tagValue, "./config/", ""));
									break;
				case "config_windows" :	                dynAppend(configFiles, strreplace(tagValue, "./config/", ""));
									break;
				case "config_linux" :		        dynAppend(configFiles, strreplace(tagValue, "./config/", ""));
									break;
				case "dplist" :				dynAppend(asciiFiles, strreplace(tagValue, "./dplist/", ""));
									break;
				case "file" :				
									tempDynString = strsplit(tagValue, "/");
									tempString = "";
//									DebugTN(tagValue, tempDynString);
									switch(tempDynString[2])
									{
										case "panels":	for(int i=3; i<=dynlen(tempDynString); i++)
																			tempString += tempDynString[i]+"/";
																		dynAppend(panelFiles, strrtrim(tempString, "/"));
																		break;
										case "scripts":	if(tempDynString[3] == "libs")
																		{
																			for(int i=4; i<=dynlen(tempDynString); i++)
																				tempString += tempDynString[i]+"/";
																			dynAppend(libraryFiles, strrtrim(tempString, "/"));
																		} else {
																			for(int i=3; i<=dynlen(tempDynString); i++)
																				tempString += tempDynString[i]+"/";
																			dynAppend(scriptFiles, strrtrim(tempString, "/"));
																		}
																		break;
										default:				for(int i=2; i<=dynlen(tempDynString); i++)
																			tempString += tempDynString[i]+"/";
																		dynAppend(otherFiles, strrtrim(tempString, "/"));
																		break;
									}																		
									break;
				case "subComponent":	if((tagValue == "yes") || (tagValue == "Yes") || (tagValue == "YES"))
															{
																isSubComponent = true;
															}
									break;
			}
		}
	}	
	if(componentName =="" || componentVersion =="")
		return -2;
	else
		return 0;
}

/** This function packs a component and saves it according to an xml description.

@param destinationFolder	path for the component package (does not need to exist)
@param xmlDesc						complete xml description (for experts)
@return 0 - component completely packed, -n - n component items not found
@author Sascha Schmeling
*/

int fwInstallation_packNgo(	string destinationFolder, dyn_string xmlDesc)
{
	dyn_string tempDynString;
	int errCount = 0;

//	DebugN(xmlDesc);

	if(dynlen(xmlDesc) > 0)
	{
		if(destinationFolder != "")
		{
			for(int i=1; i<=dynlen(xmlDesc); i++)
			{
				errCount += _transferTaggedFile(xmlDesc[i], "config", PROJ_PATH, destinationFolder);
				errCount += _transferTaggedFile(xmlDesc[i], "config_linux", PROJ_PATH, destinationFolder);
				errCount += _transferTaggedFile(xmlDesc[i], "config_windows", PROJ_PATH, destinationFolder);
				errCount += _transferTaggedFile(xmlDesc[i], "init", PROJ_PATH, destinationFolder);
				errCount += _transferTaggedFile(xmlDesc[i], "delete", PROJ_PATH, destinationFolder);
				errCount += _transferTaggedFile(xmlDesc[i], "postInstall", PROJ_PATH, destinationFolder);
				errCount += _transferTaggedFile(xmlDesc[i], "postDelete", PROJ_PATH, destinationFolder);
				errCount += _transferTaggedFile(xmlDesc[i], "dpfile", PROJ_PATH, destinationFolder);
				errCount += _transferTaggedFile(xmlDesc[i], "script", PROJ_PATH, destinationFolder);
				errCount += _transferTaggedFile(xmlDesc[i], "file", PROJ_PATH, destinationFolder);
			}
		}
	}
	return errCount;
}	

/** This function retrieves the version number as well as the installed patches

@param 
@author Sascha Schmeling
*/

string fwInstallation_getPvssVersion(dyn_string & patches)
{
	string PvssVersion = VERSION;

	DebugN(PVSS_PATH);

	dynClear(patches);

	patches = getFileNames(PVSS_PATH + "/install", "P*.lst");

	return PvssVersion;
}

/** This function shows the help file associated to a component

@param componentName	name of the component in the database
@author Sascha Schmeling
*/

fwInstallation_showHelpFile(string componentName, string systemName = "")
{
	int replaced;
	string 	path, tempHelpFile, helpFile, browserCommand;
	
    if(systemName == "")
      systemName = getSystemName();
    
    if(!patternMatch("*:", systemName))
      systemName += ":";

	componentName = strltrim(componentName, "_");
	
	dpGet(systemName + "fwInstallation_" + componentName + ".installationDirectory", path);
	dpGet(systemName + "fwInstallation_" + componentName + ".helpFile", tempHelpFile);

	helpFile = path + "/help/en_US.iso88591/" + tempHelpFile;
	
	strreplace(helpFile, "\\\\", "\\");
	strreplace(helpFile, "\\", "/");
	
//	DebugTN(componentName, helpFile);

	if(_WIN32)
	{
		dpGet(systemName + "fwGeneral.help.helpBrowserCommandWindows", browserCommand);
		if(browserCommand =="")
			browserCommand = "cmd /c start iexplore $1";
		replaced = strreplace(browserCommand, "$1", helpFile);
		if(replaced == 0)
			browserCommand = browserCommand + " " + helpFile;
		DebugTN(browserCommand);
		system(browserCommand);
	}
	else
	{
		dpGet(systemName + "fwGeneral.help.helpBrowserCommandLinux", browserCommand);
		if(browserCommand =="")
			browserCommand = "mozilla $1";
		replaced = strreplace(browserCommand, "$1", helpFile);
		if(replaced == 0)
			browserCommand = browserCommand + " " + helpFile;
		system(browserCommand);
	}
}	


/* Internal functions start here!!!

	*** *   * ***** ***** ****  *   *  ***  *       ***** *   * *   *  ***  ***** ***  ***  *   *  ***
	 *  **  *   *   *     *   * **  * *   * *       *     *   * **  * *   *   *    *  *   * **  * *   *
	 *  **  *   *   *     *   * **  * *   * *       *     *   * **  * *       *    *  *   * **  * *   
   *  * * *   *   ****  ****  * * * ***** *       ****  *   * * * * *	      *    *  *   * * * *  ***
   *  *  **   *   *     *   * *  ** *   * *       *     *   * *  ** *       *    *  *   * *  **     *
   *  *  **   *   *     *   * *  ** *   * *       *     *   * *  ** *   *   *    *  *   * *  ** *   *
	*** *   *   *   ***** *   * *   * *   * *****   *      ***  *   *  ***    *   ***  ***  *   *  ***
	
*/

/** This function shows the help file associated to a component

@param componentName	name of the component in the database
@author Sascha Schmeling
*/

_fwInstallation_showHelpFile(string componentName, string systemName = "")
{
	int replaced;
	string 	path, tempHelpFile, helpFile;

    if(systemName == "")
      systemName = getSystemName();
    
    if(!patternMatch("*:", systemName))
      systemName += ":";
	
	componentName = strltrim(componentName, "_");
	
	dpGet(systemName + "fwInstallation_" + componentName + ".installationDirectory", path);
	dpGet(systemName + "fwInstallation_" + componentName + ".helpFile", tempHelpFile);

	helpFile = path + "/help/en_US.iso88591/WebHelp/" + tempHelpFile;
	
	strreplace(helpFile, "\\\\", "\\");
	strreplace(helpFile, "\\", "/");
	
	DebugTN(componentName, helpFile);
	
	if(strpos(tempHelpFile, "http") == 0)
	{
//		strreplace(tempHelpFile ,"http://", "");
//		std_help(tempHelpFile, "");
		std_miniHelp(tempHelpFile, "url", componentName);
	}	else {	
		file help = fopen(helpFile, "r");
	
		if(ferror(help) == 0)
		{
	//		std_miniHelp(helpFile, "url", componentName);
			std_help(tempHelpFile);
			fclose(help);		
		}
	}
}	


int _transferTaggedFile(string line, string tag, string sourceFolder, string destinationFolder)
{
	string source, destination;
	unsigned itemNo;
	dyn_string sourceItems;
	
	fwInstallation_getProjPaths(sourceItems);
	itemNo = dynlen(sourceItems);
	
	if(strpos(line, "<"+tag+">") == 0)
	{
//		DebugN(line);
		line = strrtrim(strltrim(line,"<"+tag),"/"+tag+">");
		line = strrtrim(strltrim(line,">."),"<");
//		DebugN(line);
		source = sourceFolder+line;
		destination = destinationFolder+line;
//		DebugN(source, destination);
		while(!copyFile(source, destination) && itemNo > 0)
		{
			source = sourceItems[itemNo]+line;
//			DebugN(source, destination, itemNo);
			itemNo--;
		}	
	}
	if(itemNo == 0)
		return -1;
	else
		return 0;
}
	


/** This function gets all entries from the config file into string structures

@param configLines: dyn_string containing the lines from the config file 

@author M. Sliwinski, adapted for library by S. Schmeling
*/
	
int _fwInstallation_getConfigFile(dyn_string & configLines)
{
	bool fileLoaded = false;
	string fileInString;
	string configPath = getPath(CONFIG_REL_PATH);
	string configFile = configPath + "config";
	
// load config file into dyn_string
	fileLoaded = fileToString(configFile, fileInString);
	if (! fileLoaded )
	{
		DebugN("fwInstallationLib: Cannot load config file");
		return -1;
	} else {
		configLines = strsplit(fileInString, "\n");
		return 0;
	}
}





/** this function saves the dyn_string  into PVSS project confg file

@param configLines: the dyn_string containing the lines from the  file
@param filename: the name of a file

@author M.Sliwinski
*/

int fwInstallation_saveFile( dyn_string & configLines, string filename)
{
	int i;
	string strLinesToSave;
	

	file fileHdlConfig;
	
	int writeResult;

	// open the file for writing
	fileHdlConfig = fopen(filename, "w");
	// if the file is not opened
	if(fileHdlConfig == 0)
	{
		DebugN("fwInstallation: File " + filename + " could not be opened");
		return -1;
	}
	else
	{
		// copy each line from a dyn_string into string and separate the lines with newline character
		for(i = 1; i <= dynlen(configLines); i++)
		{
			strLinesToSave += configLines[i] + "\n";
		}
		// save the string into the file
		writeResult = fputs(strLinesToSave , fileHdlConfig);
		// check the result of writing to a file
		if (writeResult == 0)
		{
			DebugN("fwInstallation: File " + filename + " has been saved");
		}
		// close the file					
		fclose(fileHdlConfig);
		return 0;
	}
}

/** This function installs the component. It copies the files, imports the component DPs, DPTs, updates the project config file

@param descFile: the file with the description of a component 
@param sourceDir: the root directory with the component files
@param isItSubComponent: it is false - if it is the master component; it is true if it is the sub component
@param componentName: it is the return value - the name of the installed component
@author M.Sliwinski
*/

_fwInstallation_installComponent(string descFile, bool isItSubComponent, string & componentName, bool & componentInstalled, string destinationDir, 	dyn_string  & dynPostInstallFiles_all, int & returnValue)
{

	dyn_float dreturnf; // return value of a panel
	dyn_string dreturns; // return value of a panel
	
	string componentFileName; 
	string fileToCopy;
	string asciiManager;
	string infoFile;
	string logFile;
	string cmd;
	string strComponentFile, strTestFile; 
	string asciiFile;
	string result;
	string tmpResult;
	int dpCreated;
	int iReturn; // the return value of function fwInstallation_evalScriptFile;
	int errorInstallingComponent = 1; // has value -1 if there were any errors during the component installation, 1 if everything is OK
	dyn_string dynComponentFileLines;
	dyn_string dynFileNames;
	dyn_string dynComponentFiles;
	dyn_string dynPostInstallFiles_current;
	dyn_string dynPostInstallFiles;
	dyn_string dynConfigFiles_general;
	dyn_string dynConfigFiles_linux;
	dyn_string dynConfigFiles_windows;
	dyn_string dynInitFiles;
	dyn_string dynDplistFiles;
	dyn_string resultInLines;
	dyn_string dynScriptsToBeAdded; 
	dyn_string dynDeleteFiles;
	dyn_string dynPostDeleteFiles;
	dyn_string dynScriptFiles, dynLibFiles;

	// sub Components handling
	
	dyn_string dynSubComponents;
	
	// end sub Components handling

	string componentVersion;
	string date;
	string componentConfigFile = "";
	string dplistFile;
	bool requiredInstalled = true;
	
	// component dependencies
	
	dyn_string dynRequiredComponents;
	
	string strNotInstalledNames = "";
	

	float floatInstalledVersion;
	string strInstalledName; 
	dyn_string  dynNotProperlyInstalled;
	
	// end component dependencies
	
	string tagName;
	string tagValue;
	
	bool	fileLoaded;
	bool x;
	string err;
	string componentInitFile;
	
	int i, j, k;
	int pos1, pos2, pos3, pos4;
	int dpGetResult;
	int fileCopied;

	string temp_string = descFile;
	dyn_string tempPath = strsplit(temp_string, "/");
	string sourceDir = "";	

	int error, errorCounter;
	anytype tempReturn;

	asciiManager = PVSS_BIN_PATH + "PVSS00ascii";
   	infoFile = getPath(LOG_REL_PATH) + "PVSS00ascii_info.log";
   	logFile =  getPath(LOG_REL_PATH) + "PVSS00ascii_log.log";
   	

			if(dynlen(tempPath) > 1)
			{
				for (i=1; i<dynlen(tempPath); i++)
				{
					temp_string = tempPath[i];
					sourceDir += temp_string +"/";
				}
			}

		// load the description file		
		temp_string = descFile;
		fileLoaded = fileToString(temp_string, strComponentFile);
		
		if (! fileLoaded )
		{
			DebugN("Cannot load " + descFile + " file for single installation");
			returnValue = -1;
			return;
		}
		else 
		{
			
			// add the description file to a dyn_string with file names to be copied
			dynAppend(dynFileNames, strrtrim(strltrim(tempPath[dynlen(tempPath)])));		

			_fwInstallation_decodeXML(strComponentFile,
																componentName, componentVersion, date,
																dynRequiredComponents,
																isItSubComponent,
																dynInitFiles, dynDeleteFiles,
																dynPostInstallFiles, dynPostDeleteFiles,
																dynConfigFiles_general, dynConfigFiles_windows, dynConfigFiles_linux, 
																dynDplistFiles,
																dynPanelFiles,
																dynScriptFiles,
																dynLibFiles,
																dynComponentFiles,
																dynSubComponents,
																dynScriptsToBeAdded,
																dynFileNames);

			if(dynlen(dynScriptFiles) > 0)
				for(int i=1; i<=dynlen(dynScriptFiles); i++)
					dynAppend(dynComponentFiles, "./scripts/" + dynScriptFiles[i]);

			if(dynlen(dynLibFiles) > 0)
				for(int i=1; i<=dynlen(dynLibFiles); i++)
					dynAppend(dynComponentFiles, "./scripts/libs/" + dynLibFiles[i]);

			if(dynlen(dynPostInstallFiles) > 0)
				for(int i=1; i<=dynlen(dynPostInstallFiles); i++)
					dynAppend(dynPostInstallFiles_current, destinationDir + "/" + dynPostInstallFiles[i]);


			tempDynString = dynDplistFiles;
			dynClear(dynDplistFiles);
			if(dynlen(tempDynString) > 0)
				for(int i=1; i<=dynlen(tempDynString); i++)
					dynAppend(dynDplistFiles, "./dplist/" + tempDynString[i]);

			tempDynString = dynInitFiles;
			dynClear(dynInitFiles);
			if(dynlen(tempDynString) > 0)
       				for(int i=1; i<=dynlen(tempDynString); i++){
					dynAppend(dynInitFiles, "./config/" + tempDynString[i]);
					dynAppend(dynInitFiles, "./scripts/" + componentName + "/" + tempDynString[i]);
                                }

			tempDynString = dynDeleteFiles;
			dynClear(dynDeleteFiles);
			if(dynlen(tempDynString) > 0)
  				for(int i=1; i<=dynlen(tempDynString); i++){
					dynAppend(dynDeleteFiles, "./config/" + tempDynString[i]);
					dynAppend(dynDeleteFiles, "./scripts/" + componentName + "/" + tempDynString[i]);
                                }

			tempDynString = dynPostInstallFiles;
			dynClear(dynPostInstallFiles);
			if(dynlen(tempDynString) > 0)
       				for(int i=1; i<=dynlen(tempDynString); i++){
					dynAppend(dynPostInstallFiles, "./config/" + tempDynString[i]);
					dynAppend(dynPostInstallFiles, "./scripts/" + componentName + "/" + tempDynString[i]);
                                }

			tempDynString = dynPostDeleteFiles;
			dynClear(dynPostDeleteFiles);
			if(dynlen(tempDynString) > 0)
       				for(int i=1; i<=dynlen(tempDynString); i++){
					dynAppend(dynPostDeleteFiles, "./config/" + tempDynString[i]);
					dynAppend(dynPostDeleteFiles, "./scripts/" + componentName + "/" + tempDynString[i]);
                                }
			tempDynString = dynSubComponents;
			dynClear(dynSubComponents);
			if(dynlen(tempDynString) > 0)
				for(int i=1; i<=dynlen(tempDynString); i++)
					dynAppend(dynSubComponents, sourceDir + "/" + tempDynString[i]);

	// check the component dependencies
	// retrieve the component names that are required and not installed

	fwInstallation_getNotInstalledComponentsFromRequiredComponents(dynRequiredComponents, strNotInstalledNames);
	
	// show the panel that asks if it should be installed
	
			if( strNotInstalledNames != "")
			{	
				DebugN("fwInstallation: Could not install "+ componentName +"!\n The following components are missing\n",strNotInstalledNames);
				returnValue = -1;
				return;
			}
// end check the component dependencies

//SMS check if all scripts all valid, and all directories are writeable

	errorCounter = 0;	


	if(dynlen(dynInitFiles) > 0)
	{
		for (k=1; k<=dynlen(dynInitFiles); k++)
		{
			fileLoaded = fileToString(sourceDir + dynInitFiles[k], strTestFile);
			if (! fileLoaded )
			{
				DebugN("Cannot load " + sourceDir + dynInitFiles[k] + " file for testing");
				errorCounter++;
			} else {
				error = evalScript(tempReturn, strTestFile, "", "");
				if(error!=0)
				{
					errorCounter++;
					DebugN("fwInstallation: init script "+sourceDir + dynInitFiles[k]+" of component "+componentName+" is not valid");
				}
			}
		}
	}

	if(dynlen(dynPostInstallFiles) > 0)
	{
		for (k=1; k<=dynlen(dynPostInstallFiles); k++)
		{
			fileLoaded = fileToString(sourceDir + dynPostInstallFiles[k], strTestFile);
			if (! fileLoaded )
			{
				DebugN("Cannot load " + sourceDir + dynPostInstallFiles[k] + " file for testing");
				errorCounter++;
			} else {
				error = evalScript(tempReturn, strTestFile, "", "");
				if(error!=0)
				{
					errorCounter++;
					DebugN("fwInstallation: post install script "+sourceDir + dynPostInstallFiles[k]+" of component "+componentName+" is not valid");
				}
			}
		}
	}


	if(dynlen(dynFileNames) > 0)
	{
		for (k=1; k<=dynlen(dynFileNames); k++)
		{
			error = access(_fwInstallation_baseDir(destinationDir + "/" + dynFileNames[k]),W_OK);
			if(error!=0)
			{
				errorCounter++;
				DebugN("fwInstallation: the folder "+ _fwInstallation_baseDir(destinationDir + "/" + dynFileNames[k]) +" is not write enabled");
			}
		}
	}


	if(errorCounter!=0)
	{
		DebugN("fwInstallation: error(s) found. Installation of "+componentName+" is aborted",errorCounter);
		return errorCounter;
		return;
	}

// install the 	component
		
		if(isItSubComponent)
		{
			DebugN("  Installing sub-component: " + componentName); 
		}
		else
		{
			DebugN("Installing the component: " + componentName); 
		}
// install the subcomponents if they exist

	for(i = 1; i <= dynlen(dynSubComponents); i++)
	{
		DebugN("     Installing sub Components ... ");
		_fwInstallation_installComponent(dynSubComponents[i], true, dynSubComponents[i], componentInstalled, destinationDir, dynPostInstallFiles_all, error);
	}

// end install the subcomponents if they exist
												
// copy all the files

			
			if(sourceDir == destinationDir)
			{

			}
			else
			{
				if(dynlen(dynFileNames) > 0)
				{
					DebugN("    Copying files ....");
					
					for (i=1; i <= dynlen(dynFileNames); i++)
					{
							temp_string = dynFileNames[i];
							fileToCopy = strltrim(temp_string, ".");
	
							fileCopied =  copyFile(sourceDir + fileToCopy, destinationDir + "/" + fileToCopy);
							
							
							if(!fileCopied) 
							{
								//...Error handling...; 
//SMS only print message if problems occurred
								DebugN("Error copying file - source: " + sourceDir + fileToCopy + " destination: " + destinationDir + "/" + fileToCopy);
//SMS why print success code
								errorInstallingComponent = -1;
							}
							else
							{
//SMS							DebugN(" OK", fileCopied); 
							}
					}
				}	
			}
// import the dplist files with the ASCII manager
		
		if(dynlen(dynDplistFiles) > 0)
		DebugN("    Importing dplist file: " + dplistFile + "...");
			
		
		for( i = 1; i <= dynlen(dynDplistFiles); i++)
		{	
			dplistFile = dynDplistFiles[i];

			fileToCopy = strltrim(dplistFile, ".");
					
			asciiFile = sourceDir + fileToCopy;
									
			if(asciiFile == "")
			{
					DebugN("    The " + asciiFile + 
					"file could not be found.\nCheck the path (proj_path) in your config file.\nAlso check if the file exists.");
			}
			else
			{
					cmd = asciiManager + " -in " + asciiFile + " -yes -log +stderr -log -file > "
							+ infoFile + " 2> " + logFile;								
					if (_WIN32) 
					{
							err = system("cmd /c " + cmd);
							if (err != 0)
							{
								DebugN("    Could not import properly the file " + asciiFile + " (Error number = " + err +")");
								errorInstallingComponent = -1;
							}
					}
					else  //LIN
					{
							err = system(cmd);
							if (err != 0)
							{
								DebugN("    Could not import properly the file " + asciiFile + " (Error number = " + err +")");
								errorInstallingComponent = -1;
							}
					}
									
							// Show result of the import of the current file in the log text field
							x = fileToString (logFile, result);
							resultInLines = strsplit (result, "\n");
//							DebugN(resultInLines);	
					}
		}
		
// end import the dplist files

// import config files for linux

		for(i = 1; i <= dynlen(dynConfigFiles_linux); i++)
		{
					if(! _WIN32)
					{
						DebugN("    Importing linux config file ... ");

						componentConfigFile = sourceDir + strltrim(dynConfigFiles_linux[i], ".");
						DebugN("componentConfigFile: " + componentConfigFile + "  componentName" +  componentName);
						fwInstallation_AddComponentIntoConfig( componentConfigFile,  componentName);
					}
		}
// end import config files for linux

// import config files for windows
		
		for(i = 1; i <= dynlen(dynConfigFiles_windows); i++)
		{
					if(_WIN32)
					{
							
							DebugN("    Importing windows config file ... ");
							
							componentConfigFile = sourceDir + strltrim(dynConfigFiles_windows[i], ".");
							fwInstallation_AddComponentIntoConfig( componentConfigFile,  componentName);
					}
		}
// end import config files for windows
		
// import config files

		if(dynlen(dynConfigFiles_general) > 0)
		DebugN("    Importing general config file ... ");
		
		for(i = 1; i <= dynlen(dynConfigFiles_general); i++)
		{			
					componentConfigFile = sourceDir + strltrim(dynConfigFiles_general[i], ".");
					fwInstallation_AddComponentIntoConfig( componentConfigFile,  componentName);
		}
		
// end import config files	

// add scripts to the fwScripts.lst file

	if(dynlen(dynScriptsToBeAdded) > 0)
	{
		DebugN("    Updating the fwScripts.lst file ... ");
		fwInstallation_addScriptsIntoFile(dynScriptsToBeAdded, componentName);
		
		// check if the progs file contains the fwScripts.lst file
		DebugN("checking the progs file ... if it contains fwScripts.lst");
		
		if(fwInstallation_fwScriptsAddedToProgsFile() == -1)
		{
			Debug("\n opening the fwInstallationScriptsAddedInfo.pnl");
		}
	}
	
// execute the init script
		if(dynlen(dynInitFiles) > 0)
		DebugN("    Executing the init file ... ");

		for(i =1; i <= dynlen(dynInitFiles); i++)
		{
			componentInitFile = sourceDir + strltrim(dynInitFiles[i], ".");
			// read the file and execute it
										
			fwInstallation_evalScriptFile( componentInitFile , iReturn);
			
			if(iReturn == -1)
			{
				DebugN("    	Error: executing the init file: " + componentInitFile);
				
				errorInstallingComponent = -1;


			}
		}
// end execute the init script	

// begin store the postInstall files in a datapoint
			
			// append the name of the current component post install init files
			dynAppend(dynPostInstallFiles_all, dynPostInstallFiles_current);
			
			// store all the post install init files				
			dpSet("fwInstallationInfo.postInstallFiles:_original.._value", dynPostInstallFiles_all);			


// end store the postInstall files in a datapoint

				
			// save the component info into the PVSS database
					
			DebugN("    Saving the component info into the database: ");
						
			dpCreated = dpCreate("fwInstallation_" + componentName, "_FwInstallationComponents");
			
			if (dpCreated == 0 )
			{
				dpSet("fwInstallation_" + componentName + ".componentVersion:_original.._value", componentVersion);
				dpSet("fwInstallation_" + componentName + ".componentVersionString:_original.._value", componentVersion);
				dpSet("fwInstallation_" + componentName + ".descFile:_original.._value", descFile);

				if(date != "")		
				dpSet("fwInstallation_" + componentName + ".date:_original.._value", date);
				dpSet("fwInstallation_" + componentName + ".installationDirectory:_original.._value", destinationDir);
				
				if(dynlen(dynComponentFiles) > 0)
				dpSet("fwInstallation_" + componentName + ".componentFiles:_original.._value", dynComponentFiles);
				
				if(dynlen(dynConfigFiles_general) > 0)
				dpSet("fwInstallation_" + componentName + ".configFiles.configGeneral:_original.._value", dynConfigFiles_general);
				
				if(dynlen(dynConfigFiles_linux) > 0)
				dpSet("fwInstallation_" + componentName + ".configFiles.configLinux:_original.._value", dynConfigFiles_linux);			
				
				if(dynlen(dynConfigFiles_windows) > 0)
				dpSet("fwInstallation_" + componentName + ".configFiles.configWindows:_original.._value", dynConfigFiles_windows);						
				
				if(dynlen(dynInitFiles) > 0)
				dpSet("fwInstallation_" + componentName + ".initFiles:_original.._value", dynInitFiles);
				
				if(dynlen(dynPostInstallFiles) > 0)
				dpSet("fwInstallation_" + componentName + ".postInstallFiles:_original.._value", dynPostInstallFiles);
				
				if(dynlen(dynDeleteFiles) > 0)
				dpSet("fwInstallation_" + componentName + ".deleteFiles:_original.._value", dynInitFiles);
				
				if(dynlen(dynPostDeleteFiles) > 0)
				dpSet("fwInstallation_" + componentName + ".postDeleteFiles:_original.._value", dynPostInstallFiles);
				
				if(dynlen(dynDplistFiles) > 0)
				dpSet("fwInstallation_" + componentName + ".dplistFiles:_original.._value", dynDplistFiles);
				
				if(dynlen(dynRequiredComponents) > 0)
				dpSet("fwInstallation_" + componentName + ".requiredComponents:_original.._value", dynRequiredComponents);
				
				dpSet("fwInstallation_" + componentName + ".requiredInstalled:_original.._value", requiredInstalled);
				
				if(dynlen(dynSubComponents) > 0)
				dpSet("fwInstallation_" + componentName + ".subComponents:_original.._value", dynSubComponents);
				
				dpSet("fwInstallation_" + componentName + ".isItSubComponent:_original.._value", isItSubComponent);
				
				if(dynlen(dynScriptsToBeAdded) > 0)
				dpSet("fwInstallation_" + componentName + ".scriptFiles:_original.._value", dynScriptsToBeAdded);
				
				

			}
			else
			{
				DebugN("    ERROR: The information cannot be saved into the database ");
				errorInstallingComponent = -1;
			}
			
			if(errorInstallingComponent == -1)
			{	
				DebugN("    Component installed with errors - check the log.");
			}				
			else
			{
		
		// the component has been installed - check whether it has corrected the broken dependencies
		
				fwInstallation_getListOfNotProperlyInstalledComponents(dynNotProperlyInstalled);
				
				for(i = 1; i <= dynlen(dynNotProperlyInstalled); i++)
				{
					dynClear(dynRequiredComponents);
					
					dpGet("fwInstallation_" + dynNotProperlyInstalled[i] + ".requiredComponents:_original.._value", dynRequiredComponents);

					fwInstallation_getNotInstalledComponentsFromRequiredComponents(dynRequiredComponents, strNotInstalledNames);
					
					if(strNotInstalledNames == "")
					{
						dpSet("fwInstallation_" + dynNotProperlyInstalled[i] + ".requiredInstalled:_original.._value", true);
					}	
				}
				DebugN("    Component installed.");
				componentInstalled = true;
			}
		}	
	
}

/** This function deletes the information for the component from the project config file.

@param componentName: the name of a component
@author S.Schmeling
*/

_fwInstallation_DeleteComponentFromConfig(string componentName)
{
	dyn_string configLines; // this table contains the config file - each row contains one line from config file
	string configPath = getPath(CONFIG_REL_PATH);
	string configFile = configPath + "config";
	int i;
	bool insideComponentConfiguration = FALSE;

	if(_fwInstallation_getConfigFile(configLines) == 0)
	{
		for(i=1; i<=dynlen(configLines); i++)
		{		
			if(strltrim(strrtrim(configLines[i])) == "#begin " + componentName)
			{
				insideComponentConfiguration = TRUE;
			}
			if(strltrim(strrtrim(configLines[i])) == "#end " + componentName)
			{
				insideComponentConfiguration = FALSE;
				dynRemove(configLines, i);
				i--;
			}
			if(insideComponentConfiguration)
			{
				dynRemove(configLines, i);
				i--;
			}
		}
// save the config file
		fwInstallation_saveFile(configLines, configPath + "config");
	} else {
		return -1;
	}
}


/** This function creates the Installation Component DPT and DP

@param result: this is the return value  0 - "success"   -1 - "error"

@author M.Sliwinski, adapted by S. Schmeling
*/

_fwInstallation_createDataPointTypes(bool create, int & result)
{
	int n;
	dyn_dyn_string dynDynElements;
	dyn_dyn_int dynDynTypes;
	
	result = 0;
	
	dynDynElements[1] = makeDynString ("_FwInstallationComponents", "", "");
	dynDynElements[2] = makeDynString ("", "componentFiles", "");
	dynDynElements[3] = makeDynString ("", "configFiles", "");

	dynDynElements[4] = makeDynString ("", "", "configWindows");
	dynDynElements[5] = makeDynString ("", "", "configLinux");
	dynDynElements[6] = makeDynString ("", "", "configGeneral");

	dynDynElements[7] = makeDynString ("", "initFiles", "");
	dynDynElements[8] = makeDynString ("", "postInstallFiles", "");
	
	dynDynElements[9] = makeDynString ("", "dplistFiles", "");
	dynDynElements[10] = makeDynString ("", "componentVersion", "");
	dynDynElements[11] = makeDynString ("", "date", "");
	dynDynElements[12] = makeDynString ("", "descFile", "");
	dynDynElements[13] = makeDynString ("", "installationDirectory", "");
	dynDynElements[14] = makeDynString ("", "requiredComponents", "");
	dynDynElements[15] = makeDynString ("", "requiredInstalled", "");
	dynDynElements[16] = makeDynString ("", "subComponents", "");
	dynDynElements[17] = makeDynString ("", "isItSubComponent", "");
	dynDynElements[18] = makeDynString ("", "scriptFiles", "");
	dynDynElements[19] = makeDynString ("", "componentVersionString", "");

	dynDynElements[20] = makeDynString ("", "deleteFiles", "");
	dynDynElements[21] = makeDynString ("", "postDeleteFiles", "");

	dynDynElements[22] = makeDynString ("", "helpFile", "");

	dynDynElements[23] = makeDynString ("", "sourceDir", "");

	dynDynTypes[1] = makeDynInt (DPEL_STRUCT);
	dynDynTypes[2] = makeDynInt (0, DPEL_DYN_STRING);
	dynDynTypes[3] = makeDynInt (0, DPEL_STRUCT);
	
	dynDynTypes[4] = makeDynInt (0, 0, DPEL_DYN_STRING);
	dynDynTypes[5] = makeDynInt (0, 0, DPEL_DYN_STRING);
	dynDynTypes[6] = makeDynInt (0, 0, DPEL_DYN_STRING);
	
	dynDynTypes[7] = makeDynInt (0, DPEL_DYN_STRING);
	dynDynTypes[8] = makeDynInt (0, DPEL_DYN_STRING);
	dynDynTypes[9] = makeDynInt (0, DPEL_DYN_STRING);
	dynDynTypes[10] = makeDynInt (0, DPEL_FLOAT);
	dynDynTypes[11] = makeDynInt (0, DPEL_STRING);
	dynDynTypes[12] = makeDynInt (0, DPEL_STRING);
	dynDynTypes[13] = makeDynInt (0, DPEL_STRING);
	dynDynTypes[14] = makeDynInt (0, DPEL_DYN_STRING);
	dynDynTypes[15] = makeDynInt (0, DPEL_BOOL);
	dynDynTypes[16] = makeDynInt (0, DPEL_DYN_STRING);
	dynDynTypes[17] = makeDynInt (0, DPEL_BOOL);
	dynDynTypes[18] = makeDynInt (0, DPEL_DYN_STRING);
	dynDynTypes[19] = makeDynInt (0, DPEL_STRING);

	dynDynTypes[20] = makeDynInt (0, DPEL_DYN_STRING);
	dynDynTypes[21] = makeDynInt (0, DPEL_DYN_STRING);

	dynDynTypes[22] = makeDynInt (0, DPEL_STRING);
	dynDynTypes[23] = makeDynInt (0, DPEL_STRING);

	// Create or change the datapoint type

	if(create)
	{
		n = dpTypeCreate(dynDynElements, dynDynTypes );
	} else {
		n = dpTypeChange(dynDynElements, dynDynTypes );
	}	
	// check the result of creating dpts
	if(n == -1)
	{
		result = -1;
	}

	
	dynClear(dynDynElements);
	dynClear(dynDynTypes);
	
	dynDynElements[1] = makeDynString ("_FwInstallationInformation" , "");
	dynDynElements[2] = makeDynString ("","installationDirectoryPath");
	dynDynElements[3] = makeDynString ("","postInstallFiles");
	dynDynElements[4] = makeDynString ("","postDeleteFiles", "");
	dynDynElements[5] = makeDynString ("","lastSourcePath");
	dynDynElements[6] = makeDynString ("","addManagersDisabled");
	dynDynElements[7] = makeDynString ("","activateManagersDisabled");
	dynDynElements[8] = makeDynString ("","version");

	dynDynTypes[1] = makeDynInt (DPEL_STRUCT);
	dynDynTypes[2] = makeDynInt (0, DPEL_STRING);
	dynDynTypes[3] = makeDynInt (0, DPEL_DYN_STRING);
	dynDynTypes[4] = makeDynInt (0, DPEL_DYN_STRING);
	dynDynTypes[5] = makeDynInt (0, DPEL_STRING);
	dynDynTypes[6] = makeDynInt (0, DPEL_BOOL);
	dynDynTypes[7] = makeDynInt (0, DPEL_BOOL);
	dynDynTypes[8] = makeDynInt (0, DPEL_STRING);
		
	// create or change the datapoint type	
	if(create)
	{
		n = dpTypeCreate(dynDynElements, dynDynTypes );
	} else {
		n = dpTypeChange(dynDynElements, dynDynTypes );
	}		
	
	// check the result of creating dpts
	if(n == -1)
	{
		result = -1;
	}

  ///FVR added component groups on 17/01/2007
      	dynClear(dynDynElements);
	dynClear(dynDynTypes);
	
	dynDynElements[1] = makeDynString ("_FwInstallation_componentGroups" , "");
	dynDynElements[2] = makeDynString ("","componentNames");
	dynDynElements[3] = makeDynString ("","componentVersions");
	dynDynElements[4] = makeDynString ("","isSubComponents");
	dynDynElements[5] = makeDynString ("","descriptionFiles");

	dynDynTypes[1] = makeDynInt (DPEL_STRUCT);
	dynDynTypes[2] = makeDynInt (0, DPEL_DYN_STRING);
	dynDynTypes[3] = makeDynInt (0, DPEL_DYN_STRING);
	dynDynTypes[4] = makeDynInt (0, DPEL_DYN_BOOL);
	dynDynTypes[5] = makeDynInt (0, DPEL_DYN_STRING);
		
	// create or change the datapoint type	
	if(create)
	{
		n = dpTypeCreate(dynDynElements, dynDynTypes );
	} else {
		n = dpTypeChange(dynDynElements, dynDynTypes );
	}		
	
	// check the result of creating dpts
	if(n == -1)
	{
		result = -1;
	}        
        
}

int fwInstallation_deleteComponentGroup(string groupName)
{
  strreplace(groupName, ".", "_");

  string dpName = "fwInstallation_" + groupName;  
  
  if(dpExists(dpName))
    return dpDelete(dpName);
  
  return 0;
}

int fwInstallation_createComponentGroup(string groupName, dyn_string componentNames, dyn_string componentVersions, dyn_string isSubComponents, dyn_string descriptionFiles)
{
  strreplace(groupName, ".", "_");

  groupName = "fwInstallation_" + groupName;
  
  if(!dpExists(groupName))
    dpCreate(groupName, "_FwInstallation_componentGroups");
  
  return dpSet(groupName + ".componentNames", componentNames,
               groupName + ".componentVersions", componentVersions,
               groupName + ".isSubComponents", isSubComponents,
               groupName + ".descriptionFiles", descriptionFiles);
}

dyn_string fwInstallation_getComponentGroups()
{
  dyn_string groups;
  dyn_string ds;
  
  groups = dpNames("*", "_FwInstallation_componentGroups");

  
  for(int i = 1; i <= dynlen(groups); i++)
  {
    ds = strsplit(groups[i], ":");
    if(dynlen(ds) > 1)
      groups[i] = ds[2];
    else
      groups[i] = ds[1]; 
       
    strreplace(groups[i], "fwInstallation_", "");
  }
  return groups; 
}

int fwInstallation_getGroupComponents(string groupName, dyn_string &componentNames,
                                      dyn_string &componentVersions, dyn_string &isSubComponents, dyn_string &descriptionFiles)
{
  string dpName = "fwInstallation_" + groupName;
  if(!dpExists(dpName)){
    DebugN("ERROR: fwInstallation_getGroupComponents() -> Group " + groupName + " not defined in PVSS.");
    return -1;
  }
  
  return dpGet(dpName + ".componentNames", componentNames,
               dpName + ".componentVersions", componentVersions,
               dpName + ".isSubComponents", isSubComponents,
               dpName + ".descriptionFiles", descriptionFiles);
}

int fwInstallation_createDefaultComponentGroup(string componentName, string componentVersion, bool isSubComponent, string descriptionFile)
{
  string groupName = componentName + "_" + componentVersion;
  
  return fwInstallation_createComponentGroup(groupName, makeDynString(componentName), makeDynString(componentVersion), makeDynBool(isSubComponent), makeDynString(descriptionFile));
}

string _fwInstallation_proposeInstallationDir()
{
	string path;
	dyn_string steps;
	steps = strsplit(getPath(LOG_REL_PATH), "/");
	if(dynlen(steps)>2)
		for(int i=1; i<dynlen(steps)-1; i++)
			path += steps[i] +"/";
	else
		path = steps[1] +"/";
	return path + "fwComponents_"+formatTime("%Y%m%d", getCurrentTime())+"/";
}






int _fwInstallation_decodeXML(string strComponentFile,
							string & componentName, string & componentVersion, string & date,
							dyn_string & dynRequiredComponents,
							bool & isSubComponent,
							dyn_string & dynInitFiles, dyn_string & dynDeleteFiles, 
							dyn_string & dynPostInstallFiles, dyn_string & dynPostDeleteFiles, 
							dyn_string & dynConfigFiles_general, dyn_string & dynConfigFiles_windows, dyn_string & dynConfigFiles_linux,
							dyn_string & dynDplistFiles,
							dyn_string & dynPanelFiles,
							dyn_string & dynScriptFiles,
							dyn_string & dynLibFiles,
							dyn_string & dynComponentFiles,
							dyn_string & dynSubComponents,
							dyn_string & dynScriptsToBeAdded,
							dyn_string & dynFileNames)
{
	string tagName, tagValue, tempString;

	isSubComponent = FALSE;
	
	// retrieving the values of the tags in xml file

		// delete the root tags: <component> and </component>
		
	strreplace(strComponentFile, "<component>", "");
	strreplace(strComponentFile, "</component>", "");
		
		// while the string strComponentFile contains more tags
					
		while( fwInstallation_getTagFromString(tagName, tagValue, strComponentFile) == 1 )
		{
			
			// check the tagName
			
			switch(tagName)
			{
				case "subComponent": 		if(strtoupper(tagValue) == "YES")	
																	isSubComponent = TRUE;
									break;

				case "file" : 		dynAppend(dynFileNames, tagValue);
									if(strpos(tagValue, "./scripts/libs/") == 0)
									{
										strreplace(tagValue, "./scripts/libs/", "");
										dynAppend(dynLibFiles, tagValue);
									}
									else if(strpos(tagValue, "./scripts/") == 0)
									{
										strreplace(tagValue, "./scripts/", "");
										dynAppend(dynScriptFiles, tagValue);
									}
									else if(strpos(tagValue, "./panels/") == 0)
									{
										strreplace(tagValue, "./panels/", "");
										dynAppend(dynPanelFiles, tagValue);
									}
									else
									{
										strreplace(tagValue, "./", "");
										dynAppend(dynComponentFiles, tagValue);
									}
									break;

				case "name": 		componentName = tagValue;
									break;
									
				case "version": 	componentVersion = tagValue;
									break;
									
				case "date": 		date = tagValue;
									break;
									
				case "required":	if(tagValue != "")
									dynAppend(dynRequiredComponents, tagValue);
									break;
				
				case "script": 		dynAppend(dynFileNames, tagValue);
									dynAppend(dynScriptsToBeAdded, tagValue);					
									break;
									
				case "config":		dynAppend(dynFileNames, tagValue);
									strreplace(tagValue, "./config/", "");
									dynAppend(dynConfigFiles_general, tagValue);
									break;
									
				case "init": 		dynAppend(dynFileNames, tagValue);
									strreplace(tagValue, "./config/", "");
									strreplace(tagValue, "./scripts/" + componentName + "/", "");
									dynAppend(dynInitFiles, tagValue);
									break;
				case "postInstall": dynAppend(dynFileNames, tagValue);
									strreplace(tagValue, "./config/", "");
									strreplace(tagValue, "./scripts/" + componentName + "/", "");
									dynAppend(dynPostInstallFiles, tagValue);
									break;									
				case "delete": 		dynAppend(dynFileNames, tagValue);
									strreplace(tagValue, "./config/", "");
									strreplace(tagValue, "./scripts/" + componentName + "/", "");
									dynAppend(dynDeleteFiles, tagValue);
									break;
				case "postDelete": dynAppend(dynFileNames, tagValue);
									strreplace(tagValue, "./config/", "");
									strreplace(tagValue, "./scripts/" + componentName + "/", "");
									dynAppend(dynPostDeleteFiles, tagValue);
									break;
				case "config_windows": 	dynAppend(dynFileNames, tagValue);
										dynAppend(dynConfigFiles_windows, tagValue);	
										break;
										
				case "config_linux" : 	dynAppend(dynFileNames, tagValue);
										dynAppend(dynConfigFiles_linux, tagValue);											
										break;
										
				case "dplist":		dynAppend(dynFileNames, tagValue);
									strreplace(tagValue, "./dplist/", "");
									dynAppend(dynDplistFiles, tagValue);	
									break;
									
				case "includeComponent": 	strreplace(tagValue, "./", "");
											dynAppend(dynSubComponents, tagValue);
											break;
											
			} // end switch
			
		} // end while	
}


///////////////////////////////////////FVR///////////////////////////////////////////////
/////////////////////Functions comming from fwInstallation.pnl///////////////////////////
///////////////////////////////////////FVR///////////////////////////////////////////////
///////////////////////////////////////FVR///////////////////////////////////////////////


/** This function gets the components data from the directory specified in the textBox and fills the graphic table with it.

@param tableName: the name of a graphic table to be filled with data 
@param sourceWidget: the name of a widget containing the directory from which the data about the components is taken
@return 0 - "success"  -1 - error 
@author M.Sliwinski
*/

int fwInstallation_getComponentsInfo(string tableName , string sourceWidget, string systemName = "", bool scanRecursively = false)
{


	string dirCurrentValue;
	dyn_string dynAvailableDescriptionFiles;
	string componentFileName;
	string strComponentFile;
	string tagName;
	string tagValue;

	string componentName;
	float componentVersion;
	string componentVersionString;
	
	shape shape_dirFromSourceWidget = getShape(sourceWidget);
	shape shape_destinationTable = getShape(tableName);
	int result;
	
	bool	fileLoaded;
	bool isItSubComponent = false;
	
	int i, j;
 
  if(systemName == "")
    systemName = getSystemName();
    
  if(!patternMatch("*:", systemName))
    systemName += ":";
    
   

	// clear the graphical table with the component names
	shape_destinationTable.deleteAllLines();
	
	// get the directory name where the components are located	
	dirCurrentValue = shape_dirFromSourceWidget.text;
	
	// it the directory name is empty
	if (dirCurrentValue == "")
	{
		return 0;
	}
	
	// read the names of files that have the .xml extension in a directory specified by dirCurrentValue
	//FVR: Do it recursively
	if(scanRecursively)
  	  dynAvailableDescriptionFiles =  fwInstallation_getFileNamesRec( dirCurrentValue, "*.xml");
	else
	  dynAvailableDescriptionFiles = getFileNames(dirCurrentValue, "*.xml");

	// for each component description file, read the component name, version and display it in the graphic table
	
	for( i = 1; i <= dynlen(dynAvailableDescriptionFiles); i++)
	{
		// get the file name of an .xml description file
		componentFileName = dynAvailableDescriptionFiles[i];
		
		// load the description file
		fileLoaded = fileToString(dirCurrentValue + "/" + componentFileName, strComponentFile);
		
		if (! fileLoaded )
		{
			DebugN("fwInstallation: Cannot load " + componentFileName + " file ");	
		}
		else 
		{
			// the description file in now in string - strComponentFile
			
			// remove the root tag <component>
			
			strreplace(strComponentFile, "<component>", "");
			strreplace(strComponentFile, "</component>", "");
			
			// while there are some tags in the string - get the next tag		
			while( fwInstallation_getTagFromString(tagName, tagValue, strComponentFile) == 1 )
			{
				switch(tagName)
				{
												// retrieve the component name
							case "name": 		componentName = tagValue;
												break;
												
												// retrieve the component version
							case "version" :	componentVersionString = tagValue;
												componentVersion = tagValue;
												break;
												
													// retrieve the information - is it subcomponent
							case "subComponent":	if((tagValue == "yes") || (tagValue == "Yes") || (tagValue == "YES"))
													{
														isItSubComponent = true;
													}
												break;
				}
			}
			
			// check whether the description file contains the component name
			// and whether it is a subcomponent - if it is a subcomponent - do not display it in a table with available components
			
			if((componentName != "") && ((!isItSubComponent) || (isItSubComponent && ShowSubComponents.state(0))))
			// this component can be installed - put it in the table with available components. 
			{
				//if (componentName == "") - it means that the xml file does not contain the component name
				//							 or the component file does not describe a component				
				
				// Check if the component is already installed	
				if(systemName != "*" || systemName != "*:")  //If we are not dealing with more than one system, look if component is installed
				  fwInstallation_componentInstalled(componentName, componentVersionString, result, systemName);
				
				//DebugN("FVR:::Component is already installed returns: " + result);
								
				if (result == 1) // component is installed
				{
					// display the information about the component in a table with note that it is already installed
					
					if(isItSubComponent)
						  shape_destinationTable.appendLine("componentName", "_"+componentName, "componentVersion", componentVersionString, "colStatus" , "Installed" , "descFile", dirCurrentValue + "/" + componentFileName);
					else
						  shape_destinationTable.appendLine("componentName", componentName, "componentVersion", componentVersionString, "colStatus" , "Installed" , "descFile", dirCurrentValue + "/" + componentFileName);
				}
				else // component is not installed
				{
					// display the information about the component
					if(isItSubComponent)
						shape_destinationTable.appendLine("componentName", "_"+componentName, "componentVersion", componentVersionString, "descFile", dirCurrentValue + "/" + componentFileName);
					else
					  shape_destinationTable.appendLine("componentName", componentName, "componentVersion", componentVersionString, "descFile", dirCurrentValue + "/" + componentFileName);
						
				}
			 	
			 	// reset the componentName
			 	componentName = "";
				
				
			 	
			}
		
		}
		
		// reset the isItSubComponent variable		
		isItSubComponent = false;
			
	}
}



/** This function checks if the component is already installed. It checks the PVSSDB.

@param componentName: the name of a component to be checked
@param availableComponentVersion: the version of a component to be checked
@return 1 - "component installed"  0 - "component not installed"
@author M.Sliwinski
	    16.12.2002 Sascha Schmeling		extended for unlimited version number components
*/


fwInstallation_componentInstalled(string componentName, string requestedComponentVersion, int &result, string systemName = "")
{
	string installedComponentVersion,dummy;
	float installedComponentVersionOld;


    if(systemName == "")
      systemName = getSystemName();
    
    if(!patternMatch("*:", systemName))
      systemName += ":";
  	
	// check whether the component data point exists - if it exists it is installed
	
	if(dpExists(systemName + "fwInstallation_" + componentName))
	{
		// retrieve the version of installed component
		dpGet(systemName + "fwInstallation_" + componentName + ".componentVersionString:_original.._value", installedComponentVersion);

		// Legacy
		if(installedComponentVersion == "")
		{
			DebugN("Correcting database of installed components in system" + systemName +" for "+componentName);
			dpGet(systemName + "fwInstallation_" + componentName + ".componentVersion:_original.._value", installedComponentVersionOld);
			sprintf(dummy,"%5.5f",installedComponentVersionOld);
			installedComponentVersion = strltrim(strrtrim(dummy,"0"));
			if(strpos(installedComponentVersion,".") == strlen(installedComponentVersion)-1)
			{
				installedComponentVersion += "0";
			}
			dpSet(systemName + "fwInstallation_" + componentName + ".componentVersionString", installedComponentVersion);
		}
		result = (_fwInstallation_CompareVersions(installedComponentVersion,requestedComponentVersion));
	}
	else
	{
		// return the component is not installed
		result = 0;
	}
	
}

/** This function retrieves the installed components from the PVSS database and 
fills the graphic table - "tblInstalledComponents"

@author M.Sliwinski
*/

fwInstallation_getInstalledComponentsUI(string systemName = "")
{
	dyn_string dynComponentNames;
	dyn_string systemName_Component;
	dyn_string dataPointTypes;
	
	string componentName;
	string componentVersionString;
	float componentVersion;
	bool requiredInstalled; 
	string descFile, helpFile;
	string installationDirectory;
	shape shape_destinationTable;
	bool isItSubComponent;
	
	int row, column;
	int i;	
	
	dyn_string ds;

	unsigned systemId;

    if(systemName == ""){
      systemName = getSystemName();    
    }else if(!patternMatch("*:", systemName))
    {
      systemName += ":";
    } 
  
    systemId = getSystemId(systemName);
  
   
	// get shape of the graphic table	
	shape_destinationTable = getShape("tblInstalledComponents");
	
	// clear the table
	shape_destinationTable.deleteAllLines();
    
	ErrorText.visible = FALSE;
	ErrorColor1.visible = FALSE;
	ErrorColor2.visible = FALSE;
	
	// get existing data point types

	dataPointTypes = dpTypes("*", systemId);
	
	// check whether the "_FwInstallationComponents" dpt exists
	if(dynContains(dataPointTypes, "_FwInstallationComponents") > 0)
	{
		// get the names of all installed components
		dynComponentNames = dpNames(systemName + "*", "_FwInstallationComponents");
		
		for ( i = 1; i <= dynlen(dynComponentNames); i++)
		{
			
			// remove the system name from the component name
			dynComponentNames[i] = dpSubStr( dynComponentNames[i], DPSUB_DP );
			
			// delete the fwInstallation_ prefix from the component name
			strreplace(dynComponentNames[i], "fwInstallation_" , "");
			
			// get the component name			
			componentName = dynComponentNames[i];
			
			// get the component version
			if(dpGet(systemName + "fwInstallation_" + componentName + ".componentVersionString:_original.._value", componentVersionString) != 0)
			{
				componentVersionString = "";
			}	
//SMS
			if(componentVersionString == "")
			{
				dpGet(systemName + "fwInstallation_" + componentName + ".componentVersion:_original.._value", componentVersion);
				if(componentVersion == floor(componentVersion))
				{
					sprintf(componentVersionString,"%2.1f",componentVersion);
				} else {
					componentVersionString = componentVersion;
				}
				dpSetWait(systemName + "fwInstallation_" + componentName + ".componentVersionString:_original.._value", componentVersionString);
			}
//SMS	
			
			// get the information - are the required components installed
			dpGet(systemName + "fwInstallation_" + componentName + ".requiredInstalled:_original.._value", requiredInstalled);
			
			// check if it is the sub component
			dpGet(systemName + "fwInstallation_" + componentName + ".isItSubComponent:_original.._value", isItSubComponent);
			
			// get the help file name
			dpGet(systemName + "fwInstallation_" + componentName + ".helpFile", helpFile);
			
			// if it is the subcomponent
			if(isItSubComponent)
			{
				if(ShowInstalledSubComponents.state(0))
					if(helpFile != "")
						shape_destinationTable.appendLine("componentName", "_"+componentName, 
																							"componentVersion", componentVersionString,
																							"helpFile", "HELP");
					else
						shape_destinationTable.appendLine("componentName", "_"+componentName, "componentVersion", componentVersionString);
			}
			else
			{
				// this is not the sub-component - display it in a graphic table
				if(helpFile != "")
					shape_destinationTable.appendLine("componentName", componentName, 
																						"componentVersion", componentVersionString,
																						"helpFile", "HELP");
				else
					shape_destinationTable.appendLine("componentName", componentName, "componentVersion", componentVersionString);
			}		
			
			// check if the components required by this component are installed
			if(requiredInstalled)
			{
				// do nothing
			}
			else
			{
				// set the colour of the cell into fwAlarmWarnUack colour
				
				getValue(shape_destinationTable, "currentCell", row, column);
				shape_destinationTable.currentCell(row, 0); // we set the current cell for the first column
				shape_destinationTable.cellBackCol("{255, 255, 51}");  // the values of Framework colour  fwAlarmWarnUack 
				ErrorText.visible = TRUE;
				ErrorColor1.visible = TRUE;
				ErrorColor2.visible = TRUE;
			}
		}
	}	
		
	
}

/** this functions outputs the message into the log textarea of a panel

@param message: the message to be displayed

*/
	
fwInstallation_showMessage(dyn_string message)
{

	int i, length;
	string isUI = false;
  
	if(myManType() == UI_MAN && shapeExists("list"))
    isUI = true;
 
	length = dynlen(message);
	for (i = 1; i <= length; i++)
	{		
		if(isUI){
		  list.appendItem(message[i]);	
  	  if(logFileName.text != "")
	    {
		    fwInstallation_writeToLog(message[i] + "\n");
	    }
	  }else{
	    DebugN(message[i] + "\n");
	  }
	  
	}
		if(isUI){
	    length = list.itemCount();
	    list.bottomPos(length);
	    list.selectedPos(length);
	  }
}


/** This function reads the xml tags from the component description file


@param tagName: the string with the tag name
@param tagValue: the string with the tag value
@param strComponentFile: the string with the XML info about the component
@param return: 1 it the tag was found, -1 - the tag was not found

@author M.Sliwinski
*/

int fwInstallation_getTagFromString(string & tagName, string & tagValue, string & strComponentFile)
{
	int pos1; // contains the position of an opening tag start 
	int pos2; // contains the position of an opening tag end
	int pos3; // contains the position of a closing tag start
	int pos4; // contains the position of a closing tag end
		
				// get the position of an opening tag start
				pos1 = strpos(strComponentFile, "<");
				
				// get the position of an opening tag end		
				pos2 = strpos(strComponentFile, ">");
	
				//	get the tag name			
				tagName = substr(strComponentFile, pos1 + 1, pos2 - (pos1 + 1));
				
				// get the position of an closing tag start
				pos3 = strpos(strComponentFile, "</" + tagName + ">");
				
				// get the position of an closing tag end
				pos4 = pos3 + strlen("</" + tagName + ">");
				
				// get the tag value
				tagValue = substr(strComponentFile, pos2+1, pos3-(pos2+1));
				
				// remove the bland spaces
				tagValue = strrtrim(strltrim(tagValue));
				
				// remove the tag and the tag value from the string
				strComponentFile = substr(strComponentFile, pos4);
				
				
		if(pos1 >=0)
		{
			// return that the tag was found
//SMS
//if(strtoupper(tagName) == "NAME") DebugN("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
//DebugN("Tag found : <"+tagName+"> :"+tagValue);
//SMS
			return 1;
		}
		else
		{
			// return that the tag was not found ( there are no more tags )	
			return -1;
		}				
}


/** This function executes a script from the component .init file

@param componentInitFile: the .init file with the functions to be executed
@param iReturn: sucess 0, in  the event of an error -1
@author M.Sliwinski
*/

 fwInstallation_evalScriptFile(string componentInitFile , int & iReturn)
{
	
	bool fileLoaded;
	string fileInString;
	anytype retVal;
	int result;
	
	fileLoaded = fileToString(componentInitFile, fileInString);
	
	if (! fileLoaded )
	{
	
		DebugN("fwInstallation: Cannot load " + componentInitFile);
		iReturn =  -1;
	}
	else 
	{
		
		// the evalScript 
		iReturn = evalScript(retVal, fileInString, makeDynString("$value:12345"));
		
	}
	

	
}



/** This function updates the fwScripts.lst file to be run by ctrl manager

@param dynScriptsToBeAdded: a dynamic string with file names of scripts to be added to fwScripts.lst file
@param strComponentName: the name of the component for which the scripts are added
@author M.Sliwinski
*/

fwInstallation_addScriptsIntoFile(dyn_string & dynScriptsToBeAdded, string strComponentName)
{

		dyn_string dynLoadedScripts;   // all the scripts that were loaded from fwScripts.lst
		dyn_string dynScriptsFileLines;
		string strScriptsFileName = getPath(SCRIPTS_REL_PATH) + "fwScripts.lst";
		string scriptsFileInString;
		string strScriptsFileLine;
		dyn_string dynLinesToBeAdded;
		dyn_string dynTmpDirFile;
		int idxOfScriptName;
		int i;
		bool fileLoaded;
		int canAccessFile;
				
		canAccessFile = access(strScriptsFileName, F_OK);
		
		if(canAccessFile == 0)
		{
			
			fileLoaded = fileToString(strScriptsFileName, scriptsFileInString);

			if (! fileLoaded )
			{
			
				DebugN("fwInstallation: Error loading the file: " + strScriptsFileName);
				
			}
			else 
			{
				
				dynScriptsFileLines = strsplit(scriptsFileInString, "\n");
				
				for( i = 1; i <= dynlen(dynScriptsFileLines); i++)
				{
					strScriptsFileLine = strrtrim(strltrim(dynScriptsFileLines[i]));
					
					if(strScriptsFileLine == "")
					{
						// this is a blank line
					}
					else if( strtok(strScriptsFileLine, "#") == 0)
					{
						// this is a comment
					}
					else
					{
						// this is a script entry
						dynAppend(dynLoadedScripts, strScriptsFileLine);
					}

				}
				

			}
		}
		else
		{
			DebugN("fwInstallation: The file : " + strScriptsFileName + " does not exist");
		}
		
		// what should be added
		dynAppend(dynLinesToBeAdded, "#begin " + strComponentName);
		dynAppend(dynLinesToBeAdded, "# if this parametrisation is empty that means that the scripts were already defined in the file");
		dynAppend(dynLinesToBeAdded, "");
		
		for ( i = 1; i <= dynlen(dynScriptsToBeAdded); i++)
		{
			if(dynContains(dynLoadedScripts, dynScriptsToBeAdded[i]) > 0)
			{
				// the parametrisation is already done	
				DebugN("fwInstallation: The parametrisation is already done for: " + dynScriptsToBeAdded[i]);	
			}
			else 
			{
				// update the loaded scripts - the scripts that were loaded from the old file
				dynAppend(dynLoadedScripts, dynScriptsToBeAdded[i]);
				
				// add the script file names to - the lines to be added
				
				// get the script name from the relative path
				
				dynTmpDirFile = strsplit(dynScriptsToBeAdded[i], "/");
				
				// get the index of the script file name
				idxOfScriptName = dynlen(dynTmpDirFile);
				
				// append the script file name to the lines to be added
				dynAppend(dynLinesToBeAdded, dynTmpDirFile[idxOfScriptName]);
				DebugN("fwInstallation: Adding the parametrisation for: " + dynScriptsToBeAdded[i]);	
			}
		}
		
		dynAppend(dynLinesToBeAdded, "");
		dynAppend(dynLinesToBeAdded, "#end " + strComponentName);

		// add an empty line at the end of component definition - for readability
		dynAppend(dynLinesToBeAdded, "");		 
		
		// if the fwScripts.lst is empty the PVSS00ctl manager does not start
		// the fwInstallationFakeScript.ctl is added to correct this problem
		
		if(dynContains(dynLoadedScripts, "fwInstallationFakeScript.ctl"))
		{
		
		}
		else
		{
			dynAppend(dynLinesToBeAdded, "fwInstallationFakeScript.ctl");
		}
		
		// append the new lines to the dyn_string that contains lines from old file
		
		dynAppend(dynScriptsFileLines, dynLinesToBeAdded);
		
		fwInstallation_saveFile( dynScriptsFileLines, strScriptsFileName);

}

/** This function checks the progs file if the fwScripts.lst has been added to it

@return : 0 - if fwScripts.lst is added into progs file, -1 - if fwScripts.lst does not exist in progs file
@author M.Sliwinski
*/

int fwInstallation_fwScriptsAddedToProgsFile()
{

	bool fileLoaded;
	string strScriptsFileName = getPath(CONFIG_REL_PATH) + "progs";
	string scriptsFileInString;

			fileLoaded = fileToString(strScriptsFileName, scriptsFileInString);

			if (! fileLoaded )
			{
			
				return -1;
				
			}
			else 
			{
				if(strpos(scriptsFileInString, "fwScripts.lst") >= 0)
				{
					return 0;
				}
				else
				{
					return -1;
				}
				

			}
}

///fvr is here
int _fwInstallation_CompareVersions(string installedComponentVersion, string requestedComponentVersion)
{
	dyn_uint installedComponentVersionLong,requestedComponentVersionLong;
	dyn_string installedComponentVersionStrings, requestedComponentVersionStrings;
	int result=1;
	int count,minCount,sameCount;

	requestedComponentVersionLong = strsplit(requestedComponentVersion,".");
	installedComponentVersionLong = strsplit(installedComponentVersion,".");

	if(dynlen(requestedComponentVersionLong) <= dynlen(installedComponentVersionLong))
	{
		minCount = dynlen(requestedComponentVersionLong);
		for(count=1;count<=minCount;count++)
		{
			if(installedComponentVersionLong[count] < requestedComponentVersionLong[count])
			{
				result = 0;
				break;
			}
			if(installedComponentVersionLong[count] > requestedComponentVersionLong[count])
			{
				break;
			}
		}
	} else {
		minCount = dynlen(installedComponentVersionLong);
		sameCount = 0;
		for(count=1;count<=minCount;count++)
		{
			if(installedComponentVersionLong[count] < requestedComponentVersionLong[count])
			{
				result = 0;
				break;
			}	
			if(installedComponentVersionLong[count] > requestedComponentVersionLong[count])
			{
				break;
			}
			if(installedComponentVersionLong[count] == requestedComponentVersionLong[count])
			{
				sameCount++;
			}
		}
		if((result == 1) && (sameCount == minCount))
		{
			result = 0;
		}
	}

	return result;

}		

/** This function reads the config file and retrieves the list of
proj_paths

@param proj_paths: the return value with a list of proj_paths

@author M.Sliwinski
*/

int fwInstallation_loadProjPaths(dyn_string & proj_paths)
{
	
	dyn_string parameter_Value;
	dyn_string configLines;
	
	bool fileLoaded = false;
	
	string fileInString;

	string tempLine;
	string tempParameter;
	string tempValue;
	string tempString; 

	string configPath = getPath(CONFIG_REL_PATH);
	string configFile = configPath + "config";
	
	int i, j ;
	

	
	fileLoaded = fileToString(configFile, fileInString);
	
	if (! fileLoaded )
	{
	
		Debug("\n Cannot load " + configFile + " file");
		return -1;
	}


	configLines = strsplit(fileInString, "\n");
	
// each line is loaded in a row of dyn_string configLines
	
	for(i = 1; i <= dynlen(configLines); i++)
	{
		tempLine = strltrim(strrtrim(configLines[i]));
		
		if(tempLine == "")
		{
		}
		else
		{				
				parameter_Value = strsplit(tempLine, "=");
				
				tempParameter = strltrim(strrtrim(parameter_Value[1]));
				
				if(tempParameter == "proj_path")
				{  
				
					tempValue = strltrim(strrtrim(parameter_Value[2]));
				
					strreplace(tempValue, "\"" , "");

				
				  // now value is without quotation marks
					
					dynAppend(proj_paths, tempValue);
				}
		}	
	}		
	return 1;				
}

/** this function deletes the component  files

@param componentFiles: the dyn_string with the names of the files to be deleted
@param installationDirectory: the name of the installation directory

@author M.Sliwinski
*/

int fwInstallation_deleteFiles(dyn_string & componentFiles, string installationDirectory)
{
	string fileToDelete;
	string cmd;
	string err;
	int x;
	int i;
	string result;
	int iReturn = 0;
	string logFile =  getPath(LOG_REL_PATH) + "InstallationTool.log";
	
	// Deleting the files
	
	for(i = 1; i <= dynlen(componentFiles); i++)
		{
			strreplace( installationDirectory, "//", "/" );
			
			// remove the first dot in file name
			fileToDelete = strltrim(componentFiles[i], ".");
			
//SMS			Debug("\n Deleting installationDir: " + installationDirectory + " file: " + fileToDelete + " - ");
			
			// on windows
			if (_WIN32) 
			{
				strreplace( installationDirectory , "/" , "\\");
				strreplace( fileToDelete , "/" , "\\");


				cmd = "del " +  installationDirectory + fileToDelete +  " 2> " + logFile;
				err = system("cmd /c " + cmd);
				
				if (err != 0)
				{
						fwInstallation_showMessage(makeDynString("Could not delete file " + installationDirectory + fileToDelete));
						iReturn = -1;
				}
			}
			else  // on linux
			{
				cmd = "rm " +  installationDirectory + fileToDelete +  " >> " + logFile;
				err = system(cmd);
				if (err != 0)
				{
						fwInstallation_showMessage(makeDynString("Could not delete file " + installationDirectory + fileToDelete));
						iReturn = -1;
				}
			}
			
//SMS			Debug(err);
			x = fileToString (logFile, result);
			
//SMS			Debug(x);
			
		}
		
		return iReturn;
}

fwInstallation_writeToLog(string message)
{
	file logFile;
	dyn_string tempPaths;
	string tempPath;
	int i;
	
	string fileName = logFileName.text;
	
	strreplace(fileName, "\\", "/");

	tempPaths = strsplit(fileName, "/");
	tempPath = "";
	for(i=1; i<dynlen(tempPaths); i++)
	{
		tempPath = tempPath + tempPaths[i] + "/";
	}	
	
	if( _WIN32 )
	{
		strreplace(fileName, "/", "\\");
		strreplace(tempPath, "/", "\\");
	}

	if(access(tempPath, F_OK) != 0)
	{
		system("mkdir "+tempPath);
	}
	fileName = strrtrim(fileName, "\"");
	fileName = strrtrim(fileName, "\\");
	logFile = fopen(fileName,"a");
	if(ferror(logFile) != 0)
	{
		DebugN("fwInstallation: Cannot write to LogFile "+fileName);
	} else {
		fprintf(logFile,"%s",message);
	}
	fclose(logFile);
}

fwInstallation_writeToMainLog(string message)
{
	file logFile;
	dyn_string tempPaths;
	string tempPath;
	int i;
	
	string fileName = getPath(LOG_REL_PATH) + "/fwInstallation.log";
	
	strreplace(fileName, "\\", "/");

	if( _WIN32 )
	{
		strreplace(fileName, "/", "\\");
		strreplace(tempPath, "/", "\\");
	}

	logFile = fopen(fileName,"a");
	if(ferror(logFile) != 0)
	{
		DebugN("fwInstallation: Cannot write to LogFile "+fileName);
	} else {
		fprintf(logFile,"%s\n",message);
	}
	fclose(logFile);
}

string _fwInstallation_baseDir(string filePath)
{
	string temp_string = filePath;
	dyn_string tempPath = strsplit(temp_string, "/");
	string returnString = "";	
	int i;

	if(dynlen(tempPath) > 1)
	{
		for (i=1; i<dynlen(tempPath); i++)
		{
			temp_string = tempPath[i];
			returnString += temp_string +"/";
		}
	}
	return returnString;
}

string _fwInstallation_fileName(string filePath)
{
	string temp_string = filePath;
	dyn_string tempPath = strsplit(strrtrim(strltrim(temp_string, " "), " "), "/");
	string returnString = "";	
	int i;

	if(dynlen(tempPath) > 0)
	{
		returnString = tempPath[dynlen(tempPath)];
	}
	return returnString;
}


/** This function puts the components to be installed in order in which they should be installed
The algorithm is similar to that used during deleting the components - look fwInstallation_putComponentsInOrder_Delete() function btn_ApplyDelete()
@param componentsNames: the names of the components to be installed
@param componentsVersions: the versions of components to be installed
@param componentFiles: the file names with the description of the components
@param componentFilesInOrder: the ordered file names with the description of the components

@author M.Sliwinski and F. Varela
*/

fwInstallation_putComponentsInOrder_Install(dyn_string & componentsNames, dyn_string & componentsVersions, dyn_string & componentFiles, dyn_string & componentFilesInOrder)
{
	int i, j, k;
	dyn_dyn_string dependencies; //  first column - component name, next columns - components required by this component that will be installed later
	dyn_string tempDynRequired;
	dyn_string tempParamValue;
	dyn_string orderedComponentsNames;
	string tempRequiredName;
	string tempRequiredVersion;
	string tempComponentName;
	bool emptyListExists = true;
	dyn_string dsTmp1, dsTmp2;
    int a1, b1, c1;
    int a2, b2, c2;

	// read the component requirements and fill the dependencies table
			
	for(i = 1; i <= dynlen(componentFiles); i++)
	{
		
		// put the components to be installed in 1st column of dependencies table
		dynAppend(dependencies[i] , componentsNames[i]);
		
		// get the list of components required by the component being installed
		fwInstallation_readComponentRequirements(componentFiles[i], tempDynRequired);
		
		// for each components that are required by component being installed		
		for (j =1; j <= dynlen(tempDynRequired); j++)
		{
			tempParamValue = strsplit(tempDynRequired[j], "=");
			
			// get its name
			tempRequiredName = tempParamValue[1];
			
			if(dynlen(tempParamValue) > 1)
			{
				// get its version
				tempRequiredVersion = tempParamValue[2];
			}
			else
			{
				tempRequiredVersion = 0;
			}
			// the required component will be installed later
			k = dynContains(componentsNames, tempRequiredName);
			
			if(k > 0)
			{
			    //DebugN("FVR:::::::::Comparing: " + componentsVersions[k] + " and: "+ tempRequiredVersion);
                
				dsTmp1 = strsplit(componentsVersions[k], ".");
				dsTmp2 = strsplit(tempRequiredVersion, ".");

				if(dynlen(dsTmp1) < 3 || dynlen(dsTmp2) < 3){
                  DebugN("WARNING: fwInstallation_putComponentsInOrder_install() -> Incorrect version numbers: " + componentsVersions[k] + " " + tempRequiredVersion + ". Both should have three digits");
				}else{
				  a1 = dsTmp1[1];
				  b1 = dsTmp1[2];
				  c1 = dsTmp1[3];

				  a2 = dsTmp2[1];
				  b2 = dsTmp2[2];
				  c2 = dsTmp2[3];

				}

	    		//if(componentsVersions[k] >= tempRequiredVersion)
                if(a1 >= a2 && b1 >= b2 && c1 >= c2)
				{
				    //DebugN("FFFFFFFFFFFFFF: Version is correct");
					dynAppend(dependencies[i], tempRequiredName);
				}
			}
		}	
	}
	
	k = 0;
	
	// put the components in order
	
	while(emptyListExists)
	{	
		emptyListExists = false;
				
				for(i =1; i <= dynlen(componentsNames); i++)
				{
					
					// if the component does not require any components
					if((dynlen(dependencies[i]) == 1) && (dependencies[i][1] != "EMPTY"))
					{
						
						emptyListExists = true;
						
						tempComponentName = dependencies[i][1];
						
						// set the component name to empty
						dependencies[i][1] = "EMPTY";
						
						// append the component name to a list of ordered components		
						dynAppend(componentFilesInOrder, componentFiles[i]);
						dynAppend(orderedComponentsNames, tempComponentName);
												
						for(j = 1; j <= dynlen(componentsNames); j++)
						{
						
							// update the dependencies table
							k = dynContains(dependencies[j], tempComponentName);
							
							if(k > 0)
							{
								 dynRemove(dependencies[j], k);
							}
							
						}
					}
					else
					{
					}
				} 
	}
	
		// if there were unsolved dependencies components depending on each other 
		// copy them to the end
		
		for(i = 1; i <= dynlen(componentsNames); i++)
		{
			if(dependencies[i][1] != "EMPTY")
			{
				dynAppend(componentFilesInOrder, componentFiles[i]);
			}
		}
		
		DebugN("Components put in order: " + componentFilesInOrder);
}



/** This function reads the requirements from the component description file

@param descFile: the file with the description of a component 
@param requiredComponents: the dyn_string of requiredComponents
@author M.Sliwinski
*/


 fwInstallation_readComponentRequirements(string descFile, dyn_string & dynRequiredComponents)
{
	bool	fileLoaded;
	string strComponentFile;
	string tagName;
	string tagValue;
	int i;

	// clear the required components table
	dynClear(dynRequiredComponents);
	
	if(_WIN32)
	{
		strreplace(descFile, "/", "\\");
	}	
	
	// load the description file into strComponentFile string
		
	fileLoaded = fileToString(descFile, strComponentFile);
		
		
		if (! fileLoaded )
		{
		
			DebugN("Cannot load " + descFile + " file ");
			
		}
		else 
		{
			// remove the root tag <component>
			
			strreplace(strComponentFile, "<component>", "");
			strreplace(strComponentFile, "</component>", "");
				
				
		// read the requirements tag

			// while there are tags in the string 		
			while( fwInstallation_getTagFromString(tagName, tagValue, strComponentFile) == 1 )
			{

				switch(tagName)
				{
										//
										case "required":	tagValue = strltrim(tagValue); // remove the white characters
															tagValue = strrtrim(tagValue);
															dynAppend(dynRequiredComponents, tagValue);										
															break;
				}
			}
		}	
}

/** This function forces the restart of the whole project
@author F. Varela
*/
fwInstallation_forceProjectRestart()
{
	string host;
	int port;
	int iErr = paGetProjHostPort(paGetProjName(), host, port);

	pmon_command("##RESTART_ALL:", host, port, FALSE, TRUE);
}

string fwInstallation_getComponentPath(string componentFile)
{  
  string path = "";
  dyn_string ds;
  int len = strlen(componentFile);


  ds = strsplit(componentFile, "/");
  strreplace(componentFile, ds[dynlen(ds)], "");
  path = componentFile;

  return path;
}
/** This function installs the component. It copies the files, imports the component DPs, DPTs, updates the project config file

@param descFile: the file with the description of a component 
@param sourceDir: the root directory with the component files
@param isItSubComponent: it is false - if it is the master component; it is true if it is the sub component
@param componentName: it is the return value - the name of the installed component
@param forceInstallRequired this flag indicates whether all required components must be installed provided that 
       they correct versions are found in the distribution. This is a optional parameter. The default value is false to keep the tool backwards compatible.
	   Note that the value of this parameter is set to the default value (TRUE) when a silent installation is chosen.
@param forceOverwriteFiles this flag indicates whether the files of the component must be overwritten if a previous installation of the component is detected in the target directory
       This is a optional parameter. The default value is false to keep the tool backwards compatible.
	   Note that the value of this parameter is set to the default value (FALSE) when a silent installation is chosen.
@param isSilent flag indicating whether we are dealing with a silent installation of the packages or not. The default value is false.
@return Error code: -1 if ERROR, 0 if all OK.
@author M.Sliwinski and F. Varela.
*/

int fwInstallation_installComponent(string descFile, string sourceDir, bool isItSubComponent, string & componentName, bool & componentInstalled, string subPath = "", bool forceInstallRequired = false, bool forceOverwriteFiles = false, bool isSilent = false)
{

  string destinationDir;  // the name of a directory where the component will be installed
  dyn_float df;
  dyn_string ds;
  	
  dyn_float dreturnf; // return value of a panel
  dyn_string dreturns; // return value of a panel
	
  string componentFileName; 
  string fileToCopy;
  string asciiManager;
  string infoFile;
  string logFile;
  string cmd;
  string strComponentFile; 
  string asciiFile;
  string result;
  string tmpResult;
  int dpCreated;
  int iReturn; // the return value of function fwInstallation_evalScriptFile;
  int errorInstallingComponent = 1; // has value -1 if there were any errors during the component installation, 1 if everything is OK
  dyn_string dynComponentFileLines;
  dyn_string dynFileNames;
  dyn_string dynComponentFiles;
  dyn_string dynPostDeleteFiles_current;
  dyn_string dynPostDeleteFiles_all;
  dyn_string dynPostInstallFiles_current;
  dyn_string dynPostInstallFiles_all;
  dyn_string dynPostDeleteFiles;
  dyn_string dynPostInstallFiles;
  dyn_string dynConfigFiles_general;
  dyn_string dynConfigFiles_linux;
  dyn_string dynConfigFiles_windows;
  dyn_string dynInitFiles;
  dyn_string dynDeleteFiles;
  dyn_string dynDplistFiles;
  dyn_string resultInLines;
  dyn_string dynScriptsToBeAdded; 
  string helpFile;

  // sub Components handling
  dyn_string dynSubComponents;
	
  // end sub Components handling

  string componentVersion;
  string date;
  string componentConfigFile = "";
  string dplistFile;
  bool requiredInstalled = true;
	
  // component dependencies
	
  dyn_string dynRequiredComponents;
	
  string strNotInstalledNames = "";
	

  float floatInstalledVersion;
  string strInstalledName; 
  dyn_string  dynNotProperlyInstalled;
	
  // end component dependencies
	
  string tagName;
  string tagValue;
  string filePath, filePattern, fileNameToAdd;
  dyn_string fileNames;
	
  bool	fileLoaded;
  bool x;
  string err;
  string componentInitFile;
	
  int i, j, k;
  int pos1, pos2, pos3, pos4;
  int dpGetResult;
  int fileCopied;

  int error, errorCounter;
  string errorString, strTestFile;
  dyn_string strErrors = "";
	
  bool registerOnly = FALSE;
  dyn_string proj_paths;

  dyn_string dsNotInstalledComponents;
  dyn_string dsGoodComponents;
  
  dyn_dyn_string componentsInfo;
  dyn_string currentComponentInfo;
  dyn_string dsTmp;
  string notInstalledComponentName;
  string notInstalledComponentVersion;
  string fileComponentName;
  string fileComponentVersion;
  string fileComponent;

  string componentPath; 
  
  dyn_string dsFileComponentName;
  dyn_string dsFileComponentVersion;
  dyn_string dsFileComponent;  
  
  
  dynClear(dsFileComponentName);
  dynClear(dsFileComponentVersion);
  dynClear(dsFileComponent);

  string installedVersion;

  //If silent installation then force installation of all required components and do not overwrite the files

  if(isSilent){
    forceInstallRequired = true;
    forceOverwriteFiles = false;
  }


  if(subPath == ""){
    componentPath = fwInstallation_getComponentPath(descFile);
    subPath = substr(componentPath, strlen(sourceDir));
  }

  fwInstallation_loadProjPaths(proj_paths);
  if(dynContains(proj_paths, strrtrim(sourceDir, "/")))	
		registerOnly = TRUE;

	asciiManager = PVSS_BIN_PATH + "PVSS00ascii";
   	infoFile = getPath(LOG_REL_PATH) + "PVSS00ascii_info.log";
   	logFile =  getPath(LOG_REL_PATH) + "PVSS00ascii_log.log";
   	
   	strreplace( sourceDir, "//", "/" );

	// add a control manager for the fwScripts.lst
	fwInstallation_addManager("PVSS00ctrl", "once", 30, 1, 1, "-f fwScripts.lst");

	// get the destination dir
	dpGet("fwInstallationInfo.installationDirectoryPath:_original.._value", destinationDir);


		// load the description file		
		fileLoaded = fileToString(descFile, strComponentFile);
		
		if (! fileLoaded )
		{
		
			fwInstallation_showMessage(makeDynString("Cannot load " + descFile + " file "));
			return gFwInstallationError;
			
		}
		else 
		{
			
			strreplace(descFile, sourceDir + subPath, "");
			
	  // add the description file to a dyn_string with file names to be copied
      string tmp = strrtrim(strltrim(descFile));
      if(!patternMatch("./*", tmp))
        tmp = "./" + tmp;
        
			dynAppend(dynFileNames, tmp);		
				

	// retrieving the values of the tags in xml file

		// delete the root tags: <component> and </component>
		
			strreplace(strComponentFile, "<component>", "");
			strreplace(strComponentFile, "</component>", "");
		
		// while the string strComponentFile contains more tags
					
		while( fwInstallation_getTagFromString(tagName, tagValue, strComponentFile) == 1 )
		{
			
			// check the tagName
			
			switch(tagName)
			{
				case "file" : 		
//SMS  changes to allow wildcards
									if(strpos(tagValue, "*") > 0)
									{
										filePath = _fwInstallation_baseDir(tagValue);
										filePattern = _fwInstallation_fileName(tagValue);
										fileNames = getFileNames(sourceDir + subPath + "/"+ filePath, filePattern);
//										DebugN(filePath, filePattern, getFileNames(sourceDir + subPath +"/"+ filePath, filePattern));
										for(i=1; i<=dynlen(fileNames); i++)
										{
											fileNameToAdd = filePath + "/" + fileNames[i];
											dynAppend(dynFileNames, fileNameToAdd);
											dynAppend(dynComponentFiles, fileNameToAdd);
										}
									} else {
//SMS
										dynAppend(dynFileNames, tagValue);
										dynAppend(dynComponentFiles, tagValue);
									}
									break;

				case "name": 		componentName = tagValue;
									break;
									
				case "version": 	componentVersion = tagValue;
									break;
									
				case "date": 		date = tagValue;
									break;
									
				case "help": 		helpFile = tagValue;
									break;
									
				case "required":	if(tagValue != "")
									dynAppend(dynRequiredComponents, tagValue);
									break;
				
				case "script": 		dynAppend(dynFileNames, tagValue);
									dynAppend(dynScriptsToBeAdded, tagValue);					
									break;
									
				case "config":		dynAppend(dynFileNames, tagValue);
									dynAppend(dynConfigFiles_general, tagValue);
									break;
									
				case "init": 		dynAppend(dynFileNames, tagValue);
									dynAppend(dynInitFiles, tagValue);
									break;
				case "postInstall": dynAppend(dynFileNames, tagValue);
									// this will be saved in a data point describing the component
									dynAppend(dynPostInstallFiles, tagValue);
									
									// save the full path to the init file
									tagValue = destinationDir + strltrim(tagValue, ".");
									
									// this will be saved in a data point with all the postInstall files to be run
									dynAppend(dynPostInstallFiles_current, tagValue);
									break;
									
				case "delete": 		dynAppend(dynFileNames, tagValue);
									dynAppend(dynDeleteFiles, tagValue);
									break;
				case "postDelete":	dynAppend(dynFileNames, tagValue);
									// this will be saved in a data point describing the component
									dynAppend(dynPostDeleteFiles, tagValue);
									
									// save the full path to the init file
									tagValue = destinationDir + strltrim(tagValue, ".");
									
									// this will be saved in a data point with all the postInstall files to be run
									dynAppend(dynPostDeleteFiles_current, tagValue);
									break;
									
				case "config_windows": 	dynAppend(dynFileNames, tagValue);
										dynAppend(dynConfigFiles_windows, tagValue);	
										break;
										
				case "config_linux" : 	dynAppend(dynFileNames, tagValue);
										dynAppend(dynConfigFiles_linux, tagValue);											
										break;
										
				case "dplist":		dynAppend(dynFileNames, tagValue);
									dynAppend(dynDplistFiles, tagValue);	
									break;
									
				case "includeComponent": 	strreplace(tagValue, "./", "");
											dynAppend(dynSubComponents, sourceDir + subPath + "/" + tagValue);
											break;
											
			} // end switch
			
		} // end while	



  //FVR: 31/03/2006: Check if the component already exists in the destination directory:
  //Check that the forceOverwriteFiles is not true in addition
  if(!gFwYesToAll && fwInstallation_checkTargetDirectory(destinationDir, componentName, installedVersion) && !forceOverwriteFiles)
  {
     if(!isSilent){
	   ChildPanelOnCentralReturn("fwInstallation/fwInstallation_messageInfo3", "Warning", makeDynString("$1:A previous installation (v." + installedVersion + ") of the component:\n" + componentName + " exists in the destination directory.\nDo you want to overwrite the files?"), df, ds); 
      if(df[1] < 0.){
         DebugN("INFO: fwInstallation_installComponent() -> Installation of " + componentName + " aborted by the user.");
         return 0;
      }else if(df[1] == 1.){
	     DebugN("INFO: fwInstallation_installComponent() -> Overwriting files of component" + componentName + " in directory " + destinationDir);
         registerOnly = FALSE;
      }else if(df[1] == 0.){
         DebugN("INFO: fwInstallation_installComponent() -> Registering component " + componentName + " only. Not copying files...");
         registerOnly = TRUE;
         }else{
           gFwYesToAll = TRUE;
         }
     
      } else{
       DebugN("INFO: fwInstallation_installComponent() -> Registering component " + componentName + " only. Not copying files...");
       registerOnly = TRUE;
      }
  }

	// check the component dependencies
	// retrieve the component names that are required and not installed

	fwInstallation_showMessage(makeDynString("Now installing " + componentName));

    strNotInstalledNames = "";
	fwInstallation_getNotInstalledComponentsFromRequiredComponents(dynRequiredComponents, strNotInstalledNames);
	
	// show the panel that asks if it should be installed
	
	if( strNotInstalledNames != "")
	{
	  		
	  fwInstallation_showMessage(makeDynString("Missing at installation of "+componentName+":",strNotInstalledNames));

      //If all components are available proceed with the installation otherwise cancel installation of dependent components by claering the arrays
      dsNotInstalledComponents = strsplit(strNotInstalledNames, "|");
      

      fwInstallation_checkDistribution(sourceDir, dsNotInstalledComponents, dsFileComponentName, dsFileComponentVersion, dsFileComponent);
		
	  //FVR: Check the forceInstallRequired flag is not set:
	  if(!forceInstallRequired){
				
  	    // show the panel informing the user about broken dependencies
 	    ChildPanelOnCentralReturn("fwInstallation/fwInstallationDependency.pnl", "fwInstallation_Dependency",
	                               makeDynString("$strDependentNames:" + strNotInstalledNames , "$componentName:" + componentName, "$callingPanel:" + "ToInstall", "$fileComponentName:" + dsFileComponentName, "$fileComponentVersion:" + dsFileComponentVersion), dreturnf, dreturns);
 			
	    // check the return value
	    if(dreturns[1] == "Install_Delete")
	    {
	      requiredInstalled = false;
		  fwInstallation_showMessage(makeDynString("User choice at installation of "+componentName+": INSTALL"));
		}
		else if(dreturns[1] == "DoNotInstall_DoNotDelete")
		{
		  fwInstallation_showMessage(makeDynString("User choice at installation of "+componentName+": ABORT"));
		  return gFwInstallationError;
		}
		else if(dreturns[1] == "InstallAll_DeleteAll"){  
                  forceInstallRequired = true;     //FVR: 30/03/2006: Install all required components

  	    }
	  }
      
      //Check if flag is now true -> Need of another if since the value of the flag could have changed in the previous loop
	  if(forceInstallRequired){
	    fwInstallation_showMessage(makeDynString("INFO: Forcing installation  of all required components: " + strNotInstalledNames));
  	    for(int kk = 1; kk <= dynlen(dsFileComponentName); kk++){
           componentPath = fwInstallation_getComponentPath(descFile);
           string componentSubPath = substr(componentPath, strlen(sourceDir));
		   if(fwInstallation_installComponent(dsFileComponent[kk], sourceDir, false, dsFileComponentName[kk], componentInstalled, componentSubPath, forceInstallRequired, forceOverwriteFiles, isSilent) == gFwInstallationError && isSilent)
		   {
              fwInstallation_showMessage(makeDynString("ERROR: Silent installation failed installing dependent component: " + componentName)); 
			  return gFwInstallationError;
		   }
		 }
	  }
  }
  // end check the component dependencies

	if(isItSubComponent)
	{
		fwInstallation_showMessage(makeDynString("  Installing sub-component: " + componentName)); 
	}
	else
	{
		fwInstallation_showMessage(makeDynString("Installing the component: " + componentName)); 
	}


////SMS check if all scripts all valid, and all directories are writeable and if all source files exist

	errorCounter = 0;	

	if(dynlen(dynInitFiles) > 0)
	{
		for (k=1; k<=dynlen(dynInitFiles); k++)
		{
			fileLoaded = fileToString(sourceDir + subPath + dynInitFiles[k], strTestFile);
			if (! fileLoaded )
			{
				errorString = "Cannot load " + sourceDir + subPath + dynInitFiles[k] + " file for testing";
				dynAppend(strErrors, errorString);
				fwInstallation_showMessage(makeDynString(errorString));
				errorCounter++;
			} else {  
				if(!checkScript(strTestFile))
				{
					errorCounter++;
					errorString = "fwInstallation: init script "+ sourceDir + subPath + dynInitFiles[k]+" of component "+componentName+" is not valid";
					dynAppend(strErrors, errorString);
					fwInstallation_showMessage(makeDynString(errorString));
				}
			}
		}
	}

	if(dynlen(dynPostInstallFiles) > 0)
	{
		for (k=1; k<=dynlen(dynPostInstallFiles); k++)
		{
			fileLoaded = fileToString(sourceDir + subPath + dynPostInstallFiles[k], strTestFile);
			if (! fileLoaded )
			{
				errorString = "Cannot load " + sourceDir+ subPath + dynPostInstallFiles[k] + " file for testing";
				dynAppend(strErrors, errorString);
				fwInstallation_showMessage(makeDynString(errorString));
				errorCounter++;
			} else {
				if(!checkScript(strTestFile))
				{
					errorCounter++;
					errorString = "fwInstallation: post install script "+sourceDir+ subPath + dynPostInstallFiles[k]+" of component "+componentName+" is not valid";
					dynAppend(strErrors, errorString);
					fwInstallation_showMessage(makeDynString(errorString));
				}
			}
		}
	}


	if(dynlen(dynDeleteFiles) > 0)
	{
		for (k=1; k<=dynlen(dynDeleteFiles); k++)
		{
			fileLoaded = fileToString(sourceDir+ subPath + dynDeleteFiles[k], strTestFile);
			if (! fileLoaded )
			{
				errorString = "Cannot load " + sourceDir+ subPath + dynDeleteFiles[k] + " file for testing";
				dynAppend(strErrors, errorString);
				fwInstallation_showMessage(makeDynString(errorString));
				errorCounter++;
			} else {  
				if(!checkScript(strTestFile))
				{
					errorCounter++;
					errorString = "fwInstallation: deletion script "+sourceDir+ subPath + dynDeleteFiles[k]+" of component "+componentName+" is not valid";
					dynAppend(strErrors, errorString);
					fwInstallation_showMessage(makeDynString(errorString));
				}
			}
		}
	}

	if(dynlen(dynPostDeleteFiles) > 0)
	{
		for (k=1; k<=dynlen(dynPostDeleteFiles); k++)
		{
			fileLoaded = fileToString(sourceDir+ subPath + dynPostDeleteFiles[k], strTestFile);
			if (! fileLoaded )
			{
				errorString = "Cannot load " + sourceDir+ subPath + dynPostDeleteFiles[k] + " file for testing";
				dynAppend(strErrors, errorString);
				fwInstallation_showMessage(makeDynString(errorString));
				errorCounter++;
			} else {
				if(!checkScript(strTestFile))
				{
					errorCounter++;
					errorString = "fwDeletetion: post delete script "+sourceDir + subPath+ dynPostDeleteFiles[k]+" of component "+componentName+" is not valid";
					dynAppend(strErrors, errorString);
					fwInstallation_showMessage(makeDynString(errorString));
				}
			}
		}
	}


	if(dynlen(dynFileNames) > 0)
	{
		error = access(destinationDir,W_OK);
		if(error!=0 && !registerOnly)
		{
			errorCounter++;
			errorString = "fwInstallation: the folder "+ destinationDir +" is not write enabled";
			dynAppend(strErrors, errorString);
			fwInstallation_showMessage(makeDynString(errorString));
		
		} else {
			for (k=1; k<=dynlen(dynFileNames); k++)
			{
				error = access(sourceDir + subPath+ "/" + dynFileNames[k],R_OK);
				if(error!=0)
				{
					errorCounter++;
					errorString = "fwInstallation: the file "+ sourceDir+ subPath + "/" + dynFileNames[k] +" does not exist";
					dynAppend(strErrors, errorString);
					fwInstallation_showMessage(makeDynString(errorString));
				}
				error = access(_fwInstallation_baseDir(destinationDir + "/" + dynFileNames[k]),W_OK);
				if(error!=0)
				{
					if(access(_fwInstallation_baseDir(destinationDir + "/" + dynFileNames[k]),F_OK) == 0)
					{
						errorCounter++;
						errorString = "fwInstallation: the folder "+ _fwInstallation_baseDir(destinationDir + "/" + dynFileNames[k]) +" is not write enabled";
						dynAppend(strErrors, errorString);
						fwInstallation_showMessage(makeDynString(errorString));
					}
				}
				error = access(destinationDir + "/" + dynFileNames[k],W_OK);
				if(error!=0)
				{
					error = access(destinationDir + "/" + dynFileNames[k],F_OK);
					if(error==0)
					{
						errorCounter++;
						errorString = "fwInstallation: the file "+ destinationDir + "/" + dynFileNames[k] +" is not write enabled";
						dynAppend(strErrors, errorString);
						fwInstallation_showMessage(makeDynString(errorString));
					}
				}
			}
		}
	}


	if(errorCounter!=0)
	{
		if(!isSilent){
  		  ChildPanelOnCentralReturn("fwInstallation/fwInstallationDependency.pnl", "fwInstallation_Dependency",
		  	                        makeDynString("$strDependentNames:" + strErrors , "$componentName:" + componentName, "$callingPanel:" + "Checker"), dreturnf, dreturns);
		  // check the return value
		  if(dreturns[1] == "Install_Delete")
		  {
		  	  errorString = "fwInstallation: "+errorCounter+" error(s) found. Installation of "+componentName+" will continue on user request";
			  fwInstallation_showMessage(makeDynString(errorString));
		  }
		  else if(dreturns[1] == "DoNotInstall_DoNotDelete")
		  {
			  errorString = "fwInstallation: "+errorCounter+" error(s) found. Installation of "+componentName+" is aborted";
			  fwInstallation_showMessage(makeDynString(errorString));
			  return gFwInstallationError;
		  }
	    }else{
		  errorString = "ERROR: fwInstallation: "+errorCounter+" error(s) found. Silent installation of "+componentName+" is aborted";
		  fwInstallation_showMessage(makeDynString(errorString));
		  return gFwInstallationError;          
		}
	}



// install the 	component
		
// install the subcomponents if they exist

	for(i = 1; i <= dynlen(dynSubComponents); i++)
	{
		fwInstallation_showMessage(makeDynString("     Installing sub Components ... "));
		fwInstallation_installComponent(dynSubComponents[i], sourceDir, true, dynSubComponents[i], componentInstalled);
	}

// end install the subcomponents if they exist


// copy all the files
			
			if(sourceDir == destinationDir)
			{

			}
			else
			{
				if(dynlen(dynFileNames) > 0)
				fwInstallation_showMessage(makeDynString("    Copying files ...."));
				
				for (i =1; i <= dynlen(dynFileNames); i++)
				{
						strreplace( destinationDir, "//", "/" );
						strreplace( sourceDir, "//", "/" );
						fileToCopy = strltrim(dynFileNames[i], ".");
						if(!registerOnly){
							fileCopied =  copyFile(sourceDir+ subPath + fileToCopy, destinationDir + fileToCopy);
						}
						if(!fileCopied && !registerOnly) 
						{
							//...Error handling...; 
//SMS only print message if problems occurred
							DebugN("Error copying file - source: " + sourceDir+ subPath + fileToCopy + " destination: " + destinationDir + fileToCopy);
//SMS why print success code
							fwInstallation_showMessage(makeDynString("      Error copying file: " + fileToCopy));
							errorInstallingComponent = -1;
							if(isSilent){
			                  errorString = "ERROR: fwInstallation: "+errorCounter+" error(s) found. Silent installation of "+componentName+" is aborted";
			                  fwInstallation_showMessage(makeDynString(errorString));
			                  return gFwInstallationError;
							}
						}
						else
						{
//SMS							DebugN(" OK", fileCopied); 
						}
				}
					
			}


// import the dplist files with the ASCII manager
		
		if(dynlen(dynDplistFiles) > 0)
		fwInstallation_showMessage(makeDynString("    Importing dplist file: " + dplistFile + "..."));
			
		
		for( i = 1; i <= dynlen(dynDplistFiles); i++)
		{	
			dplistFile = dynDplistFiles[i];

			fileToCopy = strltrim(dplistFile, ".");
					
			asciiFile = sourceDir+ subPath + fileToCopy;
									
			if(asciiFile == "")
			{
					fwInstallation_showMessage("    The " + asciiFile + 
					"file could not be found.\nCheck the path (proj_path) in your config file.\nAlso check if the file exists.");
	                 return gFwInstallationError;

			}
			else
			{
					cmd = asciiManager + " -in \"" + asciiFile + "\" -yes -log +stderr -log -file > "
							+ infoFile + " 2> " + logFile;								
					if (_WIN32) 
					{
							err = system("cmd /c " + cmd);
							if (err != 0)
							{
								fwInstallation_showMessage("    Could not import properly the file " + asciiFile + " (Error number = " + err +")");
								errorInstallingComponent = -1;
							}
					}
					else  //LIN
					{
							err = system(cmd);
							if (err != 0)
							{
								fwInstallation_showMessage("    Could not import properly the file " + asciiFile + " (Error number = " + err +")");
								errorInstallingComponent = -1;
							}
					}
									
							// Show result of the import of the current file in the log text field
							x = fileToString (logFile, result);
							resultInLines = strsplit (result, "\n");
							fwInstallation_showMessage(resultInLines);	
					}
			fwInstallation_showMessage(makeDynString(""));
		}
		
// end import the dplist files

// import config files for linux

		for(i = 1; i <= dynlen(dynConfigFiles_linux); i++)
		{
					if(! _WIN32)
					{
						fwInstallation_showMessage(makeDynString("    Importing linux config file ... "));

						componentConfigFile = sourceDir+ subPath + strltrim(dynConfigFiles_linux[i], ".");
						DebugN("componentConfigFile: " + componentConfigFile + "  componentName" +  componentName);
						fwInstallation_AddComponentIntoConfig( componentConfigFile,  componentName);
					}
		}
// end import config files for linux

// import config files for windows
		
		for(i = 1; i <= dynlen(dynConfigFiles_windows); i++)
		{
					if(_WIN32)
					{
							
							fwInstallation_showMessage(makeDynString("    Importing windows config file ... "));
							
							componentConfigFile = sourceDir+ subPath + strltrim(dynConfigFiles_windows[i], ".");
							fwInstallation_AddComponentIntoConfig( componentConfigFile,  componentName);
					}
		}
// end import config files for windows
		
// import config files

		if(dynlen(dynConfigFiles_general) > 0)
		fwInstallation_showMessage(makeDynString("    Importing general config file ... "));
		
		for(i = 1; i <= dynlen(dynConfigFiles_general); i++)
		{			
					componentConfigFile = sourceDir+ subPath + strltrim(dynConfigFiles_general[i], ".");
					fwInstallation_AddComponentIntoConfig( componentConfigFile,  componentName);
		}
		
// end import config files	

// add scripts to the fwScripts.lst file

	if(dynlen(dynScriptsToBeAdded) > 0)
	{
		fwInstallation_showMessage(makeDynString("    Updating the fwScripts.lst file ... "));
		fwInstallation_addScriptsIntoFile(dynScriptsToBeAdded, componentName);
		
		// check if the progs file contains the fwScripts.lst file
		DebugN("checking the progs file ... if it contains fwScripts.lst");
		
		if(fwInstallation_fwScriptsAddedToProgsFile() == -1 && !isSilent)
		{
			Debug("\n opening the fwInstallationScriptsAddedInfo.pnl");
			ChildPanelOnCentralReturn("fwInstallation/fwInstallationScriptsAddedInfo.pnl", "fwInstallation_ScriptsAddedInfo", "", dreturnf, dreturns);
		}
	}
	
// execute the init script
		if(dynlen(dynInitFiles) > 0)
		fwInstallation_showMessage(makeDynString("    Executing the init file ... "));

		for(i =1; i <= dynlen(dynInitFiles); i++)
		{
			componentInitFile = sourceDir + subPath+ strltrim(dynInitFiles[i], ".");
			// read the file and execute it
										
			fwInstallation_evalScriptFile( componentInitFile , iReturn);
			
			if(iReturn == -1)
			{
				fwInstallation_showMessage(makeDynString("    	Error: executing the init file: " + componentInitFile));
				
				errorInstallingComponent = -1;


			}
		}
// end execute the init script	

// begin store the postInstall files in a datapoint
			
			// get all the post install init files
			dpGet("fwInstallationInfo.postInstallFiles:_original.._value", dynPostInstallFiles_all);
			
			// append the name of the current component post install init files
			dynAppend(dynPostInstallFiles_all, dynPostInstallFiles_current);
			
			// store all the post install init files				
			dpSet("fwInstallationInfo.postInstallFiles:_original.._value", dynPostInstallFiles_all);			


// end store the postInstall files in a datapoint

				
			// save the component info into the PVSS database
					
			fwInstallation_showMessage(makeDynString("    Saving the component info into the database: "));
						
			dpCreated = dpCreate("fwInstallation_" + componentName, "_FwInstallationComponents");
			
			if (dpCreated == 0 )
			{
				dpSet("fwInstallation_" + componentName + ".componentVersion:_original.._value", componentVersion);
				dpSet("fwInstallation_" + componentName + ".componentVersionString:_original.._value", componentVersion);
				dpSet("fwInstallation_" + componentName + ".descFile:_original.._value", descFile);
				dpSet("fwInstallation_" + componentName + ".sourceDir:_original.._value", sourceDir);
				
				if(date != "")		
					dpSet("fwInstallation_" + componentName + ".date:_original.._value", date);
					
				if(helpFile != "")		
					dpSet("fwInstallation_" + componentName + ".helpFile:_original.._value", helpFile);
					
				dpSet("fwInstallation_" + componentName + ".installationDirectory:_original.._value", destinationDir);
				
				if(dynlen(dynComponentFiles) > 0)
				dpSet("fwInstallation_" + componentName + ".componentFiles:_original.._value", dynComponentFiles);
				
				if(dynlen(dynConfigFiles_general) > 0)
				dpSet("fwInstallation_" + componentName + ".configFiles.configGeneral:_original.._value", dynConfigFiles_general);
				
				if(dynlen(dynConfigFiles_linux) > 0)
				dpSet("fwInstallation_" + componentName + ".configFiles.configLinux:_original.._value", dynConfigFiles_linux);			
				
				if(dynlen(dynConfigFiles_windows) > 0)
				dpSet("fwInstallation_" + componentName + ".configFiles.configWindows:_original.._value", dynConfigFiles_windows);						
				
				if(dynlen(dynInitFiles) > 0)
				dpSet("fwInstallation_" + componentName + ".initFiles:_original.._value", dynInitFiles);
				
				if(dynlen(dynPostInstallFiles) > 0)
				dpSet("fwInstallation_" + componentName + ".postInstallFiles:_original.._value", dynPostInstallFiles);
				
				if(dynlen(dynDeleteFiles) > 0)
				dpSet("fwInstallation_" + componentName + ".deleteFiles:_original.._value", dynDeleteFiles);
				
				if(dynlen(dynPostDeleteFiles) > 0)
				dpSet("fwInstallation_" + componentName + ".postDeleteFiles:_original.._value", dynPostDeleteFiles);
				
				if(dynlen(dynDplistFiles) > 0)
				dpSet("fwInstallation_" + componentName + ".dplistFiles:_original.._value", dynDplistFiles);
				
				if(dynlen(dynRequiredComponents) > 0)
				dpSet("fwInstallation_" + componentName + ".requiredComponents:_original.._value", dynRequiredComponents);
				
				dpSet("fwInstallation_" + componentName + ".requiredInstalled:_original.._value", requiredInstalled);
				
				if(dynlen(dynSubComponents) > 0)
				dpSet("fwInstallation_" + componentName + ".subComponents:_original.._value", dynSubComponents);
				
				dpSet("fwInstallation_" + componentName + ".isItSubComponent:_original.._value", isItSubComponent);
				
				if(dynlen(dynScriptsToBeAdded) > 0)
				dpSet("fwInstallation_" + componentName + ".scriptFiles:_original.._value", dynScriptsToBeAdded);
				
				

			}
			else
			{
				fwInstallation_showMessage(makeDynString("    ERROR: The information cannot be saved into the database "));
				errorInstallingComponent = -1;
			}
			
			if(errorInstallingComponent == -1)
			{	
				fwInstallation_writeToMainLog(formatTime("[%Y-%m-%d_%H:%M:%S] ",getCurrentTime()) + componentName + " " + componentVersion + " installed with errors");
				fwInstallation_showMessage(makeDynString("    Component installed with errors - check the log."));
				dpSet("fwInstallation_" + componentName + ".requiredInstalled:_original.._value", false);

				return gFwInstallationError;
			}				
			else
			{
		
		// the component has been installed - check whether it has corrected the broken dependencies
		
				fwInstallation_getListOfNotProperlyInstalledComponents(dynNotProperlyInstalled);
				
				for(i = 1; i <= dynlen(dynNotProperlyInstalled); i++)
				{
					dynClear(dynRequiredComponents);
					
					dpGet("fwInstallation_" + dynNotProperlyInstalled[i] + ".requiredComponents:_original.._value", dynRequiredComponents);

					fwInstallation_getNotInstalledComponentsFromRequiredComponents(dynRequiredComponents, strNotInstalledNames);
					
					if(strNotInstalledNames == "")
					{
						dpSet("fwInstallation_" + dynNotProperlyInstalled[i] + ".requiredInstalled:_original.._value", true);
					}	
				}
				fwInstallation_writeToMainLog(formatTime("[%Y-%m-%d_%H:%M:%S] ",getCurrentTime()) + componentName + " " + componentVersion + " installed");
				fwInstallation_showMessage(makeDynString("    Component "+componentName+" installed."));
				componentInstalled = true;
			}
		}
   //Create component default group                 
   fwInstallation_createDefaultComponentGroup(componentName, componentVersion, isItSubComponent, sourceDir + "/" + descFile);
   
   bool isOK;
   if(fwInstallationDB_getUseDB() && fwInstallationDB_checkIntegrity(isOK) != 0)
     DebugN("ERROR: fwInstallation_installComponent() -> Could not check PVSS-DB contents integrity");
                	
/*
  //Register component in DB:
  if(fwInstallationDB_getUseDB()){
  
    if(fwInstallationDB_registerConfiguration(componentName, componentVersion, sourceDir, destinationDir, getUserName()) != 0)
    { 
      DebugN("ERROR: fwInstallation_installComponent() -> DB Registration failed for component: " + componentName + " v." + componentVersion);
    }

  }else{
    DebugN("INFO: fwInstallation_installComponent() -> Skipping DB registration. User flag useDB set to FALSE");
  }
*/ 

  
  return gFwInstallationOK;	

}

/** This function retrieves the list of components which were not properly installed - there was error in dependencies
and the user still wanted to install the component.

@param dynNotProperlyInstalled: the list of components which were installed with broken dependencies - .requiredInstalled:_original.._value is false
@author M.Sliwinski
*/

fwInstallation_getListOfNotProperlyInstalledComponents(dyn_string & dynNotProperlyInstalled)
{
		dyn_string dynInstalledComponents;
		int i;
		bool requiredInstalled;
		
		// retrieve all installed components
		
		dynInstalledComponents =  dpNames("*", "_FwInstallationComponents");

		
		
		for(i = 1; i <= dynlen(dynInstalledComponents); i++)
		{	
			// cut the system name from the installed components
			dynInstalledComponents[i] = dpSubStr( dynInstalledComponents[i], DPSUB_DP );
			
			// delete the fwInstallation_ prefix
			strreplace(dynInstalledComponents[i], "fwInstallation_", "");
			
			dpGet("fwInstallation_" + dynInstalledComponents[i] + ".requiredInstalled:_original.._value", requiredInstalled);
			if(!requiredInstalled)
			{
				dynAppend(dynNotProperlyInstalled, dynInstalledComponents[i]);
			}

		}		
}

/** This function checks if all the required components are installed. It returns a string of components that are not
installed and required.

@param dynRequiredComponents: the name of a componentConfigFile
@param strNotInstalledNames: the name of a component
@author M.Sliwinski
*/

fwInstallation_getNotInstalledComponentsFromRequiredComponents(dyn_string & dynRequiredComponents, string & strNotInstalledNames)
{
	int i;
	dyn_string dynInstalledComponents;
	dyn_string dynTmpNameVersion;
	string strRequiredName;
	string stringInstalledVersion;
	string stringRequiredVersion;
	
	if (dynlen(dynRequiredComponents) > 0)
	{
	
	
		// retrieve all installed components
		
		dynInstalledComponents =  dpNames("*", "_FwInstallationComponents");

		
		for(i = 1; i <= dynlen(dynInstalledComponents); i++)
		{	
		
			// cut the system name from the installed components

			dynInstalledComponents[i] = dpSubStr( dynInstalledComponents[i], DPSUB_DP );
			
			// cut the fwInstallation_ prefix 
			
			strreplace(dynInstalledComponents[i], "fwInstallation_" , "");		
		}
				
		for( i = 1; i <= dynlen(dynRequiredComponents); i++)
		{
		
			// retrieve the name and version of the component
				
			dynTmpNameVersion = strsplit(dynRequiredComponents[i] , "=");
			
			strRequiredName = strltrim(strrtrim(dynTmpNameVersion[1]));
			
			if(dynlen(dynTmpNameVersion) > 1)
			{
				stringRequiredVersion = strltrim(strrtrim(dynTmpNameVersion[2]));
			}
			else
			{	
				stringRequiredVersion = "";
			}
			
			
			// check whether the required component is installed
			
			if(dynContains(dynInstalledComponents, strRequiredName))
			{
				// the required component is installed
				
				// checking the version of the installed component
				
				 dpGet("fwInstallation_" + strRequiredName + ".componentVersionString:_original.._value", stringInstalledVersion);
				 
				 if(_fwInstallation_CompareVersions(stringInstalledVersion, stringRequiredVersion))
				 {
				 
				 	// the installed version of the component is greater than the required version - OK
				 }
				 else
				 {
				 	// the installed version is older than the required version
					
					strNotInstalledNames += strRequiredName + "=" + stringRequiredVersion + "|";

				 }
					
			}
			else
			{

				// the required component is not installed
				strNotInstalledNames += strRequiredName + "=" + stringRequiredVersion + "|";
				
			}
		}
	}
}

/** This function reads the information from the componentConfigFile and copies it into the project config file.
Function uses the following functions: fwInstallation_loadConfigIntoTables, fwInstallation_AddComponentIntoConfigIntoMemory
fwInstallation_saveFile

@param componentConfigFile: the name of a componentConfigFile
@param componentName: the name of a component
@author M.Sliwinski
*/

fwInstallation_AddComponentIntoConfig(string componentConfigFile, string componentName)
{
	
	string configPath = getPath(CONFIG_REL_PATH);
	string configFile = configPath + "config";
	
	dyn_string loadedSections;  // this table contains the sections loaded from the config file
	dyn_dyn_string configSectionTable; // the memory representation of a config file     [1][i] - [section:parameter] , [i+1][j] - [value]
	dyn_string configLines; // this table contains the config file - each row contains one line from config file
	
//	loadedSections = makeDynString("");
	dynClear(loadedSections);
	configSectionTable [1] = "";
	configSectionTable [2] = "";
	configSectionTable [3] = "";

	// load the config file into its memory representation			
	fwInstallation_loadConfigIntoTables(loadedSections, configSectionTable, configLines, configFile);

	// add component into memory if not fwInstallation
	if(componentName != gFwInstallationComponentName)
  	fwInstallation_AddComponentIntoConfigIntoMemory(loadedSections, configSectionTable, configLines, componentConfigFile , componentName);

	// save the config file
	fwInstallation_saveFile(configLines, configPath + "config");

//SMS
//SMSif(strtolower(componentName) == "fwdeviceeditornavigator")
//SMS	DebugN("<"+componentName+">: after insertion", configLines);
//SMS

}


	

/** This function reads the information from the component config file and saves the information into memory:
loadedSections and configSectionTable - the tables containing the information about the sections:parameters:values
configLines: the table containing the lines of a config file

@param loadedSections: the list of loaded sections
@param configSectionTable: the memory representation of a config file     [1][i] - [section:parameter] , [i+1][j] - [value]
@param configLines: the dyn_string containing the lines of a config file
@param fileName: the name of a config file of a component
@param componentName: the name of a component
@author M.Sliwinski
*/

int fwInstallation_AddComponentIntoConfigIntoMemory(dyn_string & loadedSections, dyn_dyn_string & configSectionTable, dyn_string & configLines, string fileName, string componentName)
{
	string fileInString;
	string tempLine;
	string currentSection = "NOT_YET_DEFINED";
	dyn_string componentLines; // contains the lines from the component config file
	dyn_dyn_string componentParameters; // [1][i] - section name; [i + 1][j] - section parameters ( lines )
	bool fileLoaded = false;
	dyn_string linesToAdd;
	string tempParameter;
	string tempValue;
	string tempString,tempStringQuotes;
	
	dyn_string strValues;
	dyn_string parameterValue;
	
	int idxOfSection;
	int sectionExistsInConfig;
	int valueExistsInConfig;
	int i, j , k;
	int returnValue;
	
	
//	componentParameters[1] = "";
//	componentParameters[2] = "";
	dynClear(componentParameters[1]);
	dynClear(componentParameters[2]);
	
	// first delete the old information about the component from the config file
	fwInstallation_deleteComponentFromConfigInMemory(loadedSections, configSectionTable, configLines, componentName);
		
	
	// load the component config file
	fileLoaded = fileToString(fileName, fileInString);
	
	if (! fileLoaded )
	{
	
		DebugN("Cannot load " + fileName + " file");
	}
	else 
	{

			componentLines = strsplit(fileInString, "\n");
			
			
			for(i = 1; i <= dynlen(componentLines); i++)
			{
				tempLine = strltrim(strrtrim(componentLines[i]));
		
		
				if (tempLine == "" )
				{
		//			Debug("\n The line is blank |"  + tempLine + "|");
				}
				else if ( strtok(tempLine, "#") == 0)
				{
					if( currentSection == "NOT_YET_DEFINED")
					{
						// we do not know in which section to add the comment
					}
					else  // add the comment into the table
					{
						idxOfSection = dynContains(componentParameters[1], currentSection);
						
						dynAppend(componentParameters[idxOfSection + 1], tempLine);
					}
				}
				else if ( strtok(tempLine, "[") == 0)  // this is a section
				{
					
					dynAppend(componentParameters[1] ,tempLine);
					currentSection = tempLine;
							
				}
				else // this is parametrization
				{
					 if(currentSection == "NOT_YET_DEFINED")
					 {
					 	DebugN("Error: the component file has errors  section is not defined. line: " + i);
					 }
					 else
					 {
					 	idxOfSection = dynContains(componentParameters[1], currentSection);
					 	
					 	dynAppend(componentParameters[idxOfSection + 1], tempLine);
					 }
				}
			} //end  for ( i = 1; i <= dynlen(componentLines)
			
			// the component information is now in the componentParameters table
		
			
		// adding the component into the config file
		
			for(i = 1; i <= dynlen(componentParameters[1]); i++)
			{
				currentSection = componentParameters[1][i];
				
		//		Debug("\n adding the lines in section: " + currentSection);
				
				sectionExistsInConfig = dynContains(loadedSections, currentSection);
				
				if(sectionExistsInConfig == 0) // the section does not exist - add the lines into the config table and into the memory
				{
					dynClear(linesToAdd);
							
					dynAppend(loadedSections, currentSection);
					dynAppend(configLines, currentSection);
					
					dynAppend(configLines, "#begin " + componentName);
					dynAppend(configLines, "#This should not be edited manually");
					dynAppend(configLines, "#if the component is empty it means that the parametrization is already done in the section\n");

					for(j =1; j <= dynlen(componentParameters[i + 1]); j++)
					{
						tempLine = strltrim(strrtrim(componentParameters[i + 1][j]));
						
						if (tempLine == "")
						{
		//					 Debug("\n The line is blank: |" + tempLine + "|");
						}
						else if( strtok(tempLine, "#") == 0)
						{
		//					Debug("\n The line is a comment: |" + tempLine + "|");
							dynAppend(linesToAdd, tempLine);
		//
						}
						else  // this line is a parametrization - check if the value already exist, if not add it
						{
								parameterValue = strsplit(tempLine, "=");
								tempParameter = strltrim(strrtrim(parameterValue[1]));
								tempValue = strltrim(strrtrim(parameterValue[2]));
								
								tempStringQuotes = tempValue; //SMS
								tempString = strltrim(strrtrim(tempValue, "\""), "\"");
								tempValue = tempString;
								
								strValues = strsplit(tempValue, ",");
								
								for(k = 1; k <= dynlen(strValues); k++)
								{
									tempValue = strltrim(strrtrim(strValues[k]));
									
									returnValue = fwInstallation_configContainsValue(configSectionTable, currentSection, tempParameter, tempValue);
									if( returnValue == 1)
									{
										// the value is already defined for this section and parameter
//SMS tobedeleted								
										Debug("\n config Contains Parameter: " + tempParameter + "  value: |" + tempValue + "|");
									}
									else
									{
		//								("\n Adding the Parameter: " + tempParameter + "  value: " + tempValue);
										if(tempString == tempStringQuotes)	//SMS only if quotes where there before, put them again
										{
											dynAppend(linesToAdd, tempParameter + " = " + tempValue );	//SMS
										} else {
											dynAppend(linesToAdd, tempParameter + " = \"" + tempValue + "\"");	//SMS the original line
										}
		
										fwInstallation_addValueIntoMemory(configSectionTable, currentSection, tempParameter, tempValue); 
									}
								}	
						}

					}
					dynAppend(configLines, linesToAdd);
					dynAppend(configLines, "#end " + componentName + "\n");
					
					// we have added the whole information into the section			
				}
				else if(sectionExistsInConfig == -1)
				{
					Debug("\n ERROR: Cannot check if section exists in loadedSections table");
				}
				else // add the lines into the config table and into the memory
				{
		//			Debug("\n section exists in the config");
					if(dynlen(componentParameters) >= i+1)
					{
						dynClear(linesToAdd);
					 	dynAppend(linesToAdd, "#begin " + componentName);
						dynAppend(linesToAdd, "#This should not be edited manually");
						dynAppend(linesToAdd, "#if the component is empty it means that the parametrization is already done in the section\n");
						
						for(j = 1; j <= dynlen(componentParameters[i + 1]); j++)
						{
							tempLine = strltrim(strrtrim(componentParameters[i + 1][j]));
							
							if (tempLine == "")
							{
			//					 Debug("\n The line is blank: |" + tempLine + "|");
							}
							else if( strtok(tempLine, "#") == 0)
							{
			//					Debug("\n The line is a comment: |" + tempLine + "|");
								dynAppend(linesToAdd, tempLine);
			//
							}
							else  // this line is a parametrization - check if the value already exist, if not add it
							{
									parameterValue = strsplit(tempLine, "=");
									tempParameter = strltrim(strrtrim(parameterValue[1]));
									tempValue = strltrim(strrtrim(parameterValue[2]));
									
									tempStringQuotes = tempValue; //SMS
									tempString = strltrim(strrtrim(tempValue, "\""), "\"");
									tempValue = tempString;
									
									strValues = strsplit(tempValue, ",");
									
									for(k = 1; k <= dynlen(strValues); k++)
									{
										tempValue = strltrim(strrtrim(strValues[k]));
										
										returnValue = fwInstallation_configContainsValue(configSectionTable, currentSection, tempParameter, tempValue);
										if( returnValue == 1)
										{
											// the value is already defined for this section and parameter
											
			//								Debug("\n config Contains Parameter: " + tempParameter + "  value: |" + tempValue + "|");
										}
										else
										{
			//								("\n Adding the Parameter: " + tempParameter + "  value: " + tempValue);
											if(tempString == tempStringQuotes)	//SMS only if quotes where there before, put them again
											{
												dynAppend(linesToAdd, tempParameter + " = " + tempValue );	//SMS
											} else {
												dynAppend(linesToAdd, tempParameter + " = \"" + tempValue + "\"");	//SMS the original line
											}
			
											fwInstallation_addValueIntoMemory(configSectionTable, currentSection, tempParameter, tempValue); 
										}
									}	
							}
						}
						
						dynAppend(linesToAdd, "#end " + componentName + "\n");
					}
		
		
		// we are adding the lines - linesToAdd   into the configLines table under  - currentSection
//SMS
//DebugN("<"+componentName+">: Inserting lines into section "+currentSection, linesToAdd);
//SMS
					fwInstallation_addLinesIntoSection(configLines, currentSection, linesToAdd);
//SMS
//if(strtolower(componentName) == "fwdeviceeditornavigator")
//	DebugN("<"+componentName+">: after insertion", configLines);
//SMS

					
				}
			}

	}
}


/** This function adds the lines from linesToAdd into the configLines under the section specified by currentSection

@param configLines: the dyn_string with file lines
@param currentSection: the name of a currentSection
@param linesToAdd: the lines to be added

@author M.Sliwinski
*/

int fwInstallation_addLinesIntoSection(dyn_string & configLines, string currentSection, dyn_string  linesToAdd)
{
	int idxOfLine;
	int i;
	int returnValue;
	
	string tempLine;
	
	for( i = 1; i <= dynlen(configLines); i++)
	{
		tempLine = strltrim(strrtrim(configLines[i]));
		
		// find the section where it should be inserted	
		if(tempLine == currentSection)
		{
			// insert the lines into the configLines
			returnValue = dynInsertAt(configLines, linesToAdd, ++i);
			
			if(returnValue == -1)
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}	
	}
	
	
}

/** This function adds [section:parameter] , [value] data into the memory representation of a config file

@param configSectionTable: the memory representation of a config file     [1][i] - [section:parameter] , [i+1][j] - [value]
@param section: the value of a section
@param parameter: the value of a parameter
@param value: the value of a "value"

@author M.Sliwinski
*/

int fwInstallation_addValueIntoMemory(dyn_dyn_string & configSectionTable, string section, string parameter, string value)
{
	int	idxSectionParam;
	int idxValue;
	int idxTemp;
	
	// get the index of section:parameter
	idxSectionParam = dynContains(configSectionTable[1], section + ":" + parameter);
	
	if (idxSectionParam == 0)
	{
		// add the section:parameter to the configSectionTable
		
		dynAppend(configSectionTable[1], section + ":" + parameter);
		
		// find the index of section:parameter
		idxTemp	= dynContains(configSectionTable[1], section + ":" + parameter);
		
		// add the value into memory
		dynAppend(configSectionTable[idxTemp + 1], value);	
	}
	else if(idxSectionParam == -1)
	{
		Debug("Error: fwInstallation_addValueIntoMemory");
		return -1;
	}
	else
	{
		idxValue = dynContains(configSectionTable[idxSectionParam + 1], value);
		
		if (idxValue == 0)
		{
			dynAppend(configSectionTable[idxSectionParam + 1], value);	
		}
		else if(idxValue == -1)
		{
			Debug("Error: fwInstallation_addValueIntoMemory");
		}
		else
		{	
			// This value already exists - do nothing
		}
	}
	return 1;

}

/** This function deletes the config info about a component from the memory representation of a config file - [section:parameter] , [value]
and from the configLines dyn_string

@param loadedSections: the list of sections loaded from the config file
@param configSectionTable: the memory representation of a config file     [1][i] - [section:parameter] , [i+1][j] - [value]
@param configLines: the dyn_string containing the lines from the config file
@param componentName: the name of a component to be deleted

@author M.Sliwinski
*/

int fwInstallation_deleteComponentFromConfigInMemory(dyn_string & loadedSections, dyn_dyn_string & configSectionTable, dyn_string & configLines, string componentName)
{
	int i, j;
	
	int configLength;
	int idxSectionParam;
	int idxSection;
	int idxValue;
	int idxCurrent;
	int removeResult;
	
	string currentSection;
	dyn_string sections;
	dyn_string configSectionRow;
	
	string tempLine;
	string tempParameter;
	string tempValue;
	string tempString;
	string tempSection;
	dyn_string parameter_Value;
	
	dyn_dyn_int components; // [1][i] index of beginning of a component definition;  [2][i] index of ending of a component definition
	dyn_string strValues;
	
	int idxBegin = 0;
	int idxEnd = 0;
	
	dynClear(sections);
	dynClear(components[1]);
	dynClear(components[2]);
	


// find out where are the components situated and in which sections
// we are interested in the indexes of #begin and #end lines

	for(i = 1; i <= dynlen(configLines); i++)
	{
	
		if ( strtok(configLines[i], "[") == 0)
		{
						currentSection = configLines[i];
		}
		
			//if(strpos(configLines[i], "#begin " + componentName) >= 0)
			if(configLines[i] == "#begin " + componentName)
			{
                                //Debug("found begin ", i);
				idxBegin = i;
			}
			
			//if(strpos(configLines[i], "#end " + componentName) >= 0)
			if(configLines[i] == "#end " + componentName)
			{
				//Debug("found end ", i);			
				idxEnd = i;
			}
			
			if(idxBegin < idxEnd)
			{
				dynAppend(components[1], idxBegin); 
				dynAppend(components[2], idxEnd);
				dynAppend(sections, currentSection);

				idxBegin = 0;
				idxEnd = 0;
			}
	}


// delete the component from the file ( the configLines table ) and from configSectionTable

	for(i = dynlen(components[1]); i >=1; i--)
	{
		idxBegin = components[1][i];
		idxEnd = components[2][i];
	
		idxCurrent = idxEnd;

		currentSection = sections[i];
		
//		Debug("\n idxBegin: " + idxBegin + " ; idxEnd: " + idxEnd + " ; currentSection: " + currentSection);
		
		for(idxCurrent = idxEnd; idxCurrent >= idxBegin; idxCurrent--)
		{
			// delete it from the configSectionTable

			
			tempLine = strltrim(strrtrim(configLines[idxCurrent]));
			
			removeResult = dynRemove(configLines, idxCurrent);
			
			if (removeResult == -1)
			{
				Debug("\n ERROR: fwInstallation_deleteComponentFromConfigInMemory(): could not remove the line from table");
			}
		
//			Debug("\n Remove result of line : ( " + idxCurrent + " ) from Config Lines: " + removeResult + " line : " + tempLine);
			
			if (tempLine == "" )
			{
//			Debug("\n The line is blank n"  + tempLine + "|");
			}
			else if ( strtok(tempLine, "#") == 0)
			{
//				Debug("\n This line is a comment"  + tempLine + "| ");
			}
			else // the templine contains parameters
			{
				parameter_Value = strsplit(tempLine, "=");
				
				tempParameter = strltrim(strrtrim(parameter_Value[1]));
				
				tempValue = strltrim(strrtrim(parameter_Value[2]));
				
				tempString = strltrim(strrtrim(tempValue, "\""));
				
				tempValue = tempString;  // now value is without quotation marks
				
				strValues = strsplit(tempValue, ",");
				
				for(j = 1 ; j <= dynlen(strValues); j++)
				{
					tempValue = strValues[j];
					strValues[j] = strrtrim(strltrim(tempValue , "\" ") , "\" ");
				}
				
				tempString = currentSection + ":" + tempParameter;
				
				// Deleting the values from memory
				
//				Debug("\n Checking if configSectionTable[1] contains: " + tempString);
				
				idxSectionParam = dynContains(configSectionTable[1], tempString);
					
				if(idxSectionParam == 0)
				{
					// The value does not exist 
				}
				else if (idxSectionParam == -1)
				{
					Debuf("\n ERROR: fwInstallation_deleteComponentFromConfigInMemory(): can not read value from configSectionTable: ");
				}
				else{
				
				 	//Debug("\n configSectionTable[1] contains: " + tempString + " on idx: " + idxSectionParam);
				 	//Debug("\n checking if strValues exist: " + strValues);
				 	
					for(j = 1; j <= dynlen(strValues); j++)
					{
						idxValue = dynContains(configSectionTable[idxSectionParam + 1], strValues[j]);
						//Debug("\n configSectionTable[idxSectionParam+1]: " + configSectionTable[idxSectionParam + 1]);
						
						if(idxValue > 0)
						{
							Debug("\n removing the value: " + strValues[j]);
							
							
							removeResult = dynRemove(configSectionTable[idxSectionParam + 1], idxValue);
							
							
//							Debug("\n removeResult dynRemove(configSectionRow[idxSectionParam + 1] , idxValue): " + removeResult);
						}					
					}

				} // else
			}
				
		}
		
	}
	
}


/** This function checks whether the section-parameter-value is defined in the memory


@param configSectionTable: the memory representation of a config file     [1][i] - [section:parameter] , [i+1][j] - [value]
@param section: the value of a section
@param parameter: the value of a parameter
@param value: the value of a "value"

@author M.Sliwinski
*/

int fwInstallation_configContainsValue(dyn_dyn_string & configSectionTable, string section , string parameter, string value)
{
	int idxOfParameter;
	int idxOfValue;
	
	idxOfParameter = dynContains(configSectionTable[1] , section + ":" + parameter);
	
	if(idxOfParameter == 0)
	{
//		Debug("\n fwInstallation_configContainsValue: There is no section_parameter:" + section + ":" + parameter);
		return 0;
	}
	else if (idxOfParameter == -1)
	{
		Debug("\n ERROR: fwInstallation_configContainsValue: error in checking section_parameter" );
		return 0;
	}
	else
	{	
		idxOfValue = dynContains(configSectionTable[idxOfParameter + 1], value);
		if(idxOfValue ==  0)
		{
			return 0;
		}
		else if (idxOfValue == -1)
		{
//			Debug("\n fwInstallation_configContainsValue: error in checking value" );
			return 0;
		}
		else
		{
//			Debug("\n fwInstallation_configContainsValue: value exists: returning 1");
			return 1;
		}
		
	}
}

						
/** This function builds the memory representation of a config file

@param loadedSections: the list of sections loaded from the config file
@param configSectionTable: the memory representation of a config file     [1][i] - [section:parameter] , [i+1][j] - [value]
@param configLines: dyn_string containing the lines from the config file 
@param fileName: the name of a config file

@author M.Sliwinski
*/
	
int fwInstallation_loadConfigIntoTables(dyn_string & loadedSections, dyn_dyn_string & configSectionTable, dyn_string & configLines, string fileName)
{
//	dyn_string knownSections = makeDynString("[ui]" , "[ctrl]");
	
	dyn_string parameter_Value;

	int idxSectionParam;
	int idxSection;
	int idxValue;
	int sectionLength;

	bool fileLoaded = false;

	string fileInString;

	string tempLine;
	string tempParameter;
	string tempValue;
	string tempString; 

	dyn_string strValues;
	
	string currentSection = "NOT_KNOWN";
	string lineTrimmed;
	
	int i, j ;
	
	fileLoaded = fileToString(fileName, fileInString);
	
	if (! fileLoaded )
	{
	
		DebugN("Cannot load " + fileName + " file");
	}
	else 
	{
//		Debug("\n" + fileName + " - file loaded");
	}

	configLines = strsplit(fileInString, "\n");
	
	
// each line is loaded in a row of dyn_string configLines
	
	for(i = 1; i <= dynlen(configLines); i++)
	{
		tempLine = strltrim(strrtrim(configLines[i]));

					
		if (tempLine == "" )
		{
//			Debug("\n The line is blank |"  + tempLine + "|");
		}
		else if ( strtok(tempLine, "#") == 0)
		{
//			Debug("\n This line is a comment |"  + tempLine + "|");
		}
		else if ( strtok(tempLine, "[") == 0)
		{
//			Debug("\n This line is a section |"  + tempLine + "|");
		//	if(dynContains(knownSections, tempLine))
		//	{
//						Debug("\n Adding new section into memory: " + tempLine);
			
					dynAppend(loadedSections ,tempLine);
					currentSection = tempLine;
					
		//	}
		//	else
		//	{
			//	Debug("\n This section is not known");
		//				currentSection = "NOT_KNOWN";
		//	}
		}
		else // this is parametrization
		{ 
			if (currentSection == "NOT_KNOWN")
			{
				// Debug("\n This section is not known: don't add parameter to memory");
			}
			else // This section is already in memory
			{
//				Debug("\n Adding the parameter into memory" + tempLine + "|");
				
				parameter_Value = strsplit(tempLine, "=");
				
				tempParameter = strltrim(strrtrim(parameter_Value[1]));
				
				tempValue = strltrim(strrtrim(parameter_Value[2]));
				
				tempString = strltrim(strrtrim(tempValue, "\""));
				
				tempValue = tempString;  // now value is without quotation marks
				
				strValues = strsplit(tempValue, ",");
				
				for(j = 1 ; j <= dynlen(strValues); j++)
				{
					tempValue = strValues[j];
					strValues[j] = strrtrim(strltrim(tempValue , "\" ") , "\" ");
				}
				
				// we hava all the values in a dyn_string - strValues
								
				idxSection = dynContains(loadedSections, currentSection);
				
				if (idxSection > 0)  // this section is in loaded sections
				{
				
					tempString = currentSection + ":" + tempParameter;
												
					idxSectionParam = dynContains(configSectionTable[1], tempString);
					
	//				Debug("\n idxSection : " + idxSection );
				
					if(idxSectionParam == 0)  // the parameter tempParameter is not defined
					{
						
						dynAppend(configSectionTable[1], tempString);
						
//						Debug("\n Adding parameter: " + tempString + " and values: " + strValues + "for the first time");
						
						idxSectionParam = dynContains(configSectionTable[1], tempString);
						
						for( j = 1; j<= dynlen(strValues); j++)
						{
							
							dynAppend(configSectionTable[idxSectionParam + 1], strValues[j]);
							
						}
							
	//					Debug("\n adding the parameter: " + tempParameter );			
					}
					else if( idxSectionParam == -1)
					{

					}
					else // the parameter is defined for the section add the value
					{
		
			
						for( j = 1; j<= dynlen(strValues); j++)
						{
									
							idxValue = dynContains(configSectionTable[idxSectionParam + 1], strValues[j]);
							
							if(idxValue == 0)
							{
								dynAppend(configSectionTable[idxSectionParam + 1], strValues[j]);
//								Debug("\n Adding parameter: " + tempString + " and values: " + strValues);
								
							}
							else if (idxValue == -1)
							{
								Debug("\n ERROR: fwInstallation_loadConfigIntoTables():  Error in adding Value into memory");
							}
							else
							{
//								Debug("\n This value already exists: " + tempValue);
							}
						}
					}
				} // if (dynContains(loadedSections, currentSection))
				else
				{	
					// This parameter is in a "not known section"
				}
				
			}
		
		} // else		
	} // end for	
        
        

}

///////////////////////////////////////////////////FVR//////////////////////////////
//////////////////FVR: Functions coming from the advanced options panel/////////////
///////////////////////////////////////////////////FVR//////////////////////////////





/** This function saves the new proj_path order into the config file

@param proj_paths: the value with a list of proj_paths

@author Sascha Schmeling
*/

int fwInstallation_changeProjPaths(dyn_string projPaths)
{
	
	dyn_string parameter_Value;
	dyn_string configLines;
	
	bool fileLoaded = false;
	
	string fileInString;

	string tempLine;
	string tempParameter;
	string tempValue;
	string tempString; 

	string configPath = getPath(CONFIG_REL_PATH);
	string configFile = configPath + "config";
	string configFileEntry;
	
	
	int i, j ;
	int numberOfProjPaths = 0;
	int indexOfProjPath = 0;
//SMS changes to accomodate commented project paths
	dyn_int projPathIndex;	

	
	fileLoaded = fileToString(configFile, fileInString);
	
	if (! fileLoaded )
	{
	
		Debug("\n Cannot load " + configFile + " file");
		return -1;
	}
	else 
	{
		Debug("\n" + configFile + " - file loaded");
	}

	configLines = strsplit(fileInString, "\n");
	
// each line is loaded in a row of dyn_string configLines

	dynClear(projPathIndex);	
	for(i = 1; i <= dynlen(configLines); i++)
	{
		tempLine = strltrim(strrtrim(configLines[i]));
		
		if(tempLine == "")
		{
		}
		else
		{				
				parameter_Value = strsplit(tempLine, "=");
				
				tempParameter = strltrim(strrtrim(parameter_Value[1]));
				
				if(tempParameter == "proj_path")
				{  
			
					numberOfProjPaths++;
					dynAppend(projPathIndex,i);		//SMS store positions of valid project paths in the config file
					if(indexOfProjPath == 0)
					{
						indexOfProjPath = i;
					}			
	
				}
		}	
	}		

	if(numberOfProjPaths!=dynlen(projPaths))
	{
		DebugN("Config file changed on disk!!!");
		return -1;
	} else {
		// exchange loaded projPaths with ordered ones
		for(i=numberOfProjPaths;i>=1;i--)
		{
			dynRemove(configLines, projPathIndex[i]);			
			configFileEntry = "proj_path = \"" + projPaths[i] + "\"";
			dynInsertAt(configLines, configFileEntry , projPathIndex[i]);
		}
		
		fwInstallation_saveFile(configLines, configFile);
	
		return 1;
	}
}

/** This function creates a project path, either creates the directory or just adds the path

@param sPath:	project path to be created

@author S.Schmeling
*/

int fwInstallation_createPath(string sPath)
{
	dyn_string projPaths;
	int i, x;
	string result;
	
	int directoryExists;
	bool state;
	string cmd, err = 0;
	shape shape_chckBox = getShape("checkCreateDirectory");
	string logFile =  getPath(LOG_REL_PATH) + "InstallationTool.log";
	
	strreplace( sPath , "//" , "/");

	Debug("\n sPath : " + sPath);

		directoryExists = access(sPath, F_OK);

	if(sPath == "")
	{
		txtWarning.text = "Directory cannot be empty";
		txtWarning.visible = true;
	}
	else if((strpos(sPath," ")>-1) || (strpos(sPath,"*")>-1) || (strpos(sPath,"?")>-1))
	{
		txtWarning.text = "Directory names shall not contain special characters [ *?]";
		txtWarning.visible = true;		
	}
	else if( directoryExists == -1)
	{

		if (shape_chckBox.state(0) == true)
		{
			// the user has chosen to create the directory
			
	//		Debug("create directory: " + sPath);
			
			//trim the last backslash in the directory name
			
			sPath = strrtrim(sPath, "/\\");
			

			if (_WIN32) 
			{
				if(strpos(sPath, ":") >= 1 || patternMatch("\\\\*", sPath))
				{	
				     	
					strreplace( sPath , "/" , "\\");
					sPath = strrtrim(sPath, "/");
					
					cmd = "mkdir " + sPath + " 2> " + logFile;
					err = system("cmd /c " + cmd);
					if(err != "0")
					{
					
						x = fileToString(logFile, result);
						Debug("\n cmd: " + cmd + "  result: " + result);
						txtWarning.text = result;
						txtWarning.visible = true;
					}
				}
				else
				{
					txtWarning.text = "You should give the whole path";
					txtWarning.visible = true;
				}
			}
			else // _LINUX
			{
				if(strpos(sPath, "/") == 0)
				{
					cmd = "mkdir " + sPath + " 2> " + logFile;
					err = system(cmd);
					if(err != "0")
					{
						x = fileToString(logFile, result);
						txtWarning.text = result;
						txtWarning.visible = true;
					}
				}
				else
				{
					txtWarning.text = "You should give the whole path";
					txtWarning.visible = true;
				}
			}
			
			//the directory has been created - add it into the config file
			if(err == 0)
			{
//FVR: Use new fwInstallation_addProjPath function
				//result = fwInstallation_addProjPath(sPath);
				result = fwInstallation_addProjPath(sPath, 999);
				
				if (result == -1)  // the project path could not be updated - too many entries
				{
					txtWarning.text = "Too many project directories defined in the config file \n" +
									"Choose directory defined in the proj_path \n" + 
									"in the project config file";
					txtWarning.visible = true;
				}
				else  // the project path has been updated
				{
			
				  	dpCreate("fwInstallationInfo" , "_FwInstallationInformation");
					dpSet("fwInstallationInfo.installationDirectoryPath:_original.._value", sPath);
					projPaths = sliProjPaths.items;
					dynInsertAt(projPaths,sPath,1);
					sliProjPaths.items = projPaths;
					sliProjPaths.selectedPos = 1;
			
//					PanelOff();
				}
			}
				
		}
		else // the user has not checked the option to create a new directory
		{
			txtWarning.text = "Directory does not exist";
			txtWarning.visible = true;
		}
	}
	else // the directory exists
	{
		 fwInstallation_loadProjPaths(projPaths);

		if(dynContains(projPaths, sPath))
		{
			// the config file contains the sPath in the proj_dir
			
			dpCreate("fwInstallationInfo" , "_FwInstallationInformation");
			dpSet("fwInstallationInfo.installationDirectoryPath:_original.._value", sPath);
			projPaths = sliProjPaths.items;
			dynInsertAt(projPaths,sPath,1);
			sliProjPaths.items = projPaths;
			sliProjPaths.selectedPos = 1;
		
//			PanelOff();
		}
		else //the directory exists but it is not in the config gile
		{
			// update the config file
			// e.g. add line: proj_path = "D:/msliwins/test/framework1.1/PVSS"
			
//	FVR: Use new fwInstallation_addProjPath
//			result = fwInstallation_addProjPath(sPath);
			result = fwInstallation_addProjPath(sPath, 999);
		 
		 	if (result == -1)  // the project path could not be updated - too many entries
			{
				txtWarning.text = "Choose directory defined in the proj_path \nin the project config file";
				txtWarning.visible = true;
			}
			else  // the project path has been updated
			{
			
			  	dpCreate("fwInstallationInformation" , "_FwInstallationInformation");
				dpSet("fwInstallationInfo.installationDirectoryPath:_original.._value", sPath);
				projPaths = sliProjPaths.items;
				dynInsertAt(projPaths,sPath,1);
				sliProjPaths.items = projPaths;
				sliProjPaths.selectedPos = 1;
	
//				PanelOff();
			}

	
		}
		
		
		}
	
}



///FVR: 29/03/2006

/** This function retrieves the component information from the PVSS DB and
	displays it in the panel

@param dcomponentName: the name of a file with component description

@author M.Sliwinski
*/

fwInstallation_getComponentDescriptionPVSSDB(string componentName)
{
	float componentVersion;
	string descFile;
	string date;
	dyn_string componentFiles;
	dyn_string configFiles_linux;
	dyn_string configFiles_windows;
	dyn_string configFiles_general;
	dyn_string initFiles;
	dyn_string dplistFiles;
	dyn_string scriptFiles;
	dyn_string requiredComponents;
	dyn_string requiredNameVersion;
	dyn_string dynSubComponents;
	dyn_string postInstallFiles;
	string componentVersionString;
	
	string requiredName;
	string requiredVersion;
	shape shape_destinationTable = getShape("tblSubComponents");

	int i;

		dpGet("fwInstallation_" + componentName + ".componentVersionString:_original.._value", componentVersionString);
		dpGet("fwInstallation_" + componentName + ".descFile:_original.._value", descFile);
		dpGet("fwInstallation_" + componentName + ".componentFiles:_original.._value", componentFiles);
		dpGet("fwInstallation_" + componentName + ".configFiles.configLinux:_original.._value", configFiles_linux);
		dpGet("fwInstallation_" + componentName + ".configFiles.configWindows:_original.._value", configFiles_windows);
		dpGet("fwInstallation_" + componentName + ".configFiles.configGeneral:_original.._value", configFiles_general);

		dpGet("fwInstallation_" + componentName + ".initFiles:_original.._value", initFiles);
		dpGet("fwInstallation_" + componentName + ".scriptFiles:_original.._value", scriptFiles);
		dpGet("fwInstallation_" + componentName + ".dplistFiles:_original.._value", dplistFiles);
		dpGet("fwInstallation_" + componentName + ".requiredComponents:_original.._value", requiredComponents);
		dpGet("fwInstallation_" + componentName + ".date:_original.._value", date);
		dpGet("fwInstallation_" + componentName + ".subComponents:_original.._value", dynSubComponents);
		dpGet("fwInstallation_" + componentName + ".postInstallFiles:_original.._value", postInstallFiles);

		TextName.text = componentName;
		TextVersion.text = componentVersionString;
		TextDate.text = date;
		
		for(i = 1; i<= dynlen(componentFiles); i++)
		{
			selectionOtherFiles.appendItem(componentFiles[i]);
		}
		
		for(i = 1; i<= dynlen(configFiles_windows); i++)
		{
			selectionConfigFiles_windows.appendItem(configFiles_windows[i]);
		}
		
		for(i = 1; i<= dynlen(configFiles_linux); i++)
		{
			selectionConfigFiles_linux.appendItem(configFiles_linux[i]);
		}
		
		for(i = 1; i<= dynlen(configFiles_general); i++)
		{
			selectionConfigFiles_general.appendItem(configFiles_general[i]); 
		}
		
		for(i = 1; i<= dynlen(initFiles); i++)
		{
			selectionInitFiles.appendItem(initFiles[i]);
		}
		
		for(i = 1; i<= dynlen(dplistFiles); i++)
		{
			selectionDplistFiles.appendItem(dplistFiles[i]);
		}
		
		for(i = 1; i<= dynlen(scriptFiles); i++)
		{
			selectionScripts.appendItem(scriptFiles[i]);
		}
		
		for(i = 1; i<= dynlen(requiredComponents); i++)
		{	
			requiredNameVersion = strsplit(requiredComponents[i], "=");
			requiredName = requiredNameVersion[1];
			
			if(dynlen(requiredNameVersion) > 1)
			{
				requiredVersion = requiredNameVersion[2];
			}
			else
			{
				requiredVersion = " ";
			}
			
			selectionRequiredComponents.appendItem(requiredName + " ver.: " + requiredVersion);
			
		}
		
		for(i = 1; i <= dynlen(postInstallFiles); i++)
		{
			selectionPostInstallFiles.appendItem(postInstallFiles[i]);
		}
		
		for(i = 1; i <= dynlen(dynSubComponents); i++)
		{
			selectionSubComponents.appendItem( dynSubComponents[i]);
		}
}


/** This function retrieves the component information from the xml file and
	displays it in the panel

@param descFile: the name of a file with component description

@author M.Sliwinski
*/

fwInstallation_getComponentDescriptionXML(string descFile)
{

	string strComponentFile;

	bool fileLoaded;
	dyn_string dynComponentFileLines;
	dyn_string dynRequiredComponents;
	
	dyn_string requiredNameVersion;
	string requiredName;
	string requiredVersion;
	
	string tagValue;
	string tagName;
	int pos1, pos2, pos3, pos4;
	string fileName;
	int i;
	
	
	int j;
	
	fileLoaded = fileToString(descFile, strComponentFile);
		
//		Debug("\nthe component file: \n\n:" + strComponentFile);
		if (! fileLoaded )
		{
		
			Debug("\nCannot load " + descFile + " file ");
			
		}
		else 
		{
//			Debug("\n" + descFile + " - file loaded");
			
			// remove the root tag : <component> and </component>
			
			strreplace(strComponentFile, "<component>", "");
			strreplace(strComponentFile, "</component>", "");
			
				pos1 = strpos(strComponentFile, "<");
			
			while(pos1 >= 0)
			{
				pos2 = strpos(strComponentFile, ">");
	
				//	find the tag name
				
				tagName = substr(strComponentFile, pos1 + 1, pos2 - (pos1 + 1));
				
				pos3 = strpos(strComponentFile, "</" + tagName + ">");
				pos4 = pos3 + strlen("</" + tagName + ">");
				
				tagValue = substr(strComponentFile, pos2+1, pos3-(pos2+1));
				tagValue = strrtrim(strltrim(tagValue));
				
				
				strComponentFile = substr(strComponentFile, pos4);
								
				pos1 = strpos(strComponentFile, "<");
	
				switch(tagName)
				{
					case "file" : 		selectionOtherFiles.appendItem(tagValue);
										break;
	
					case "name":  		TextName.text = tagValue;
										break;
										
					case "desc":		selectionDescription.appendItem(tagValue);
										break;
					
					case "version": 	TextVersion.text = tagValue;
										break;
										
					case "date": 		TextDate.text = tagValue;
										break;
										
					case "required":	if(tagValue != "")
										{
											requiredNameVersion = strsplit(tagValue, "=");
											requiredName = requiredNameVersion[1];
				
											if(dynlen(requiredNameVersion) > 1)
											{
												requiredVersion = requiredNameVersion[2];
											}
											else
											{
												requiredVersion = " ";
											}			
											selectionRequiredComponents.appendItem(requiredName + " ver.: " + requiredVersion);
										}
										break;
										
					case "config":		selectionConfigFiles_general.appendItem(tagValue);
										break;
					
					case "script": 		selectionScripts.appendItem(tagValue);
										break;
					
					case "postInstall": selectionPostInstallFiles.appendItem(tagValue);
										break;
															
					case "init": 		selectionInitFiles.appendItem(tagValue);
										break;
										
					case "config_windows": 	selectionConfigFiles_windows.appendItem(tagValue);
											break;
											
					case "config_linux" : 	selectionConfigFiles_linux.appendItem(tagValue);
											break;
											
					case "dplist":		selectionDplistFiles.appendItem(tagValue);
										break;
										
					case "includeComponent": strreplace(tagValue, "./", "");
											 selectionSubComponents.appendItem($sourceDir + "/" + tagValue);
											 break;										
												
				} // end switch
				
			} // end while	
					
		}	
}



/** This function puts the components to be deleted in order in which they should be deleted
The function only checks if the component chosen for deleting depend on each other.
The function operates on the component information contained in the PVSS DB

algorithm: suppose we have the following components to delete:  com1, com2, com3
the dependencies are following:
	com1: is required by com2
	com2: is required by com3
	com3: is nor required by them
We can first delete the com3 because it is not required by com1 i com3
	the dependencies are following:
	com1: is required by com2
	com2: is not required by any component
If there is a loop: com1 is required by com2 and com2 is required by com1 the components can not be ordered

@param componentsNames: the dyn_string of the components to be put in order before deleting 
@param componentsNamesInOrder: the dyn_string of the ordered components to be deleted

@author M.Sliwinski
*/

fwInstallation_putComponentsInOrder_Delete(dyn_string componentsNames, dyn_string & componentsNamesInOrder)
{
	dyn_dyn_string dependencies; //  first column - component name, next columns - components that require this component
	dyn_string dynDependentComponents;
	string tempComponentName;
	bool emptyListExists = true;
	int i, j, k;
	
	// build the dependencies table
	// for each compomponent
	for(i = 1; i <= dynlen(componentsNames); i++)
	{
		// build the dependencies table
		dynAppend(dependencies[i] , componentsNames[i]);
		
		// get the list of dependent components
		fwInstallation_getListOfDependentComponents(componentsNames[i], dynDependentComponents);
		
		// append the dependent components
		dynAppend(dependencies[i], dynDependentComponents);
	}
	 

	// put the components in order - algorithm is described in the comment before the function
	while(emptyListExists)
	{
		emptyListExists = false;
			
			// for each component	
			for(i = 1; i <= dynlen(componentsNames); i++)
			{
				// if it is not required by other components
				if((dynlen(dependencies[i]) == 1) && (dependencies[i][1] != "EMPTY"))
				{
					emptyListExists = true;

					tempComponentName = dependencies[i][1];
					
					// remove the component name from the dependencies table ( set it to EMPTY value )
					dependencies[i][1] = "EMPTY"; 
					
					// put it at the end of the  components in order
					dynAppend(componentsNamesInOrder, tempComponentName);
					
					// remove the component from the list
					for(j = 1; j <= dynlen(componentsNames); j++)
					{
						
						k = dynContains(dependencies[j], tempComponentName);
						
						if(k > 0)
						{
							// this component no longer requires other components
							dynRemove(dependencies[j], k);	
						}	
					}
				}
			}
	}
	
	// if there were unsolved dependencies copy the components to the end of the list
	
	for(i = 1; i <= dynlen(componentsNames); i++)
	{
		if(dependencies[i][1] != "EMPTY")
		{
			dynAppend(componentsNamesInOrder, dependencies[i][1]);
		}	
	}
		
}


/** This function gets the list of dependent components. This functions from the list of  all  installed components
 retrieves the list of components that require strComponentName

@param strComponentName: the name of the component for which we would like to find dependent components
@param dynDependentComponents: the dyn_string of components that require the strComponentName component
@author M.Sliwinski
*/

fwInstallation_getListOfDependentComponents(string strComponentName, dyn_string & dynDependentComponents)
{

	dyn_string dynInstalledComponents;
	dyn_string dynRequiredComponents;
	dyn_string dynTmpNameVersion;
	string strTmpName;
	int i, j, k;
	
	dynClear(dynDependentComponents);
	
	// get all the components installed in the system
	dynInstalledComponents =  dpNames("*", "_FwInstallationComponents");
	
	// check all the components whether they require the strComponentName
	for(i = 1; i <= dynlen(dynInstalledComponents); i++) 
	{
		
		 // remove the system name
		dynInstalledComponents[i] = dpSubStr( dynInstalledComponents[i], DPSUB_DP ); 
		
		// remove the fwInstallation_ prefix
		strreplace(dynInstalledComponents[i], "fwInstallation_", "");
		
		
		if( dynInstalledComponents[i] != strComponentName)
		{
			// retrieve the required components		
			dpGet("fwInstallation_" + dynInstalledComponents[i] + ".requiredComponents:_online.._value", dynRequiredComponents);

				
			// check whether the strComponentName is required by this component
			for(j = 1; j <= dynlen(dynRequiredComponents); j++)
			{	
				// clear the tmp dyn_string
				dynClear(dynTmpNameVersion);
				
				// get the name and version
				dynTmpNameVersion = strsplit(dynRequiredComponents[j], "=");
				
				// get the name
				strTmpName = strltrim(strrtrim(dynTmpNameVersion[1]));
				
				
				if(strTmpName == strComponentName)
				{
					// add the component to the dynDependentComponents
					 
					dynAppend(dynDependentComponents, dynInstalledComponents[i]);	
				}
			}
		}				
	}
	

}



/** this function deletes the component files, the component information from the config file
	and the internal DP created by the installation tool with the description of a component. 
	This function does not delete the component data point types ( ETM is planning to 
	add the functionality of deleting the DPT, DP from the ASCII Manager ).

@param componentName: the name of a component to be deleted
@param componentDeleted: result
@param deleteFiles: switch wether to delete also files

@author M.Sliwinski, changed by S. Schmeling and F. Varela
*/

int fwInstallation_deleteComponent(	string componentName, bool & componentDeleted,
																		bool deleteAllFiles = TRUE)
{

	dyn_string componentFiles;
	dyn_string configFiles_general;
	dyn_string configFiles_windows;
	dyn_string configFiles_linux;	
	dyn_string initFiles;
	dyn_string postInstallFiles;
	dyn_string deleteFiles;
	dyn_string postDeleteFiles;
	dyn_string dplistFiles;
	dyn_string dynDescFile;
	dyn_string dynDependentComponents;
	dyn_string dynSubComponents;
	dyn_string dynScriptFiles;
	string descFile;
	string strDependentComponentsNames;
	dyn_string dreturns;
	dyn_float dreturnf;
	int iReturn;
	int	errorDeletingComponent;
	int i,k;
	dyn_string dynFileNames;		
	string installationDirectory, tempString;
	int error;
	int errorCounter = 0;
	string errorString,descFileName;
	dyn_string strErrors = "";
	bool deletionAborted = FALSE;

	dyn_string dynPostDeleteFiles_all, dynPostDeleteFiles_current;
	string componentDeleteFile;

  string componentVersion;

  //Get component version:

  dpGet("fwInstallation_" + componentName + ".componentVersionString", componentVersion);


	// get the installation directory
	dpGet("fwInstallation_" + componentName + ".installationDirectory:_online.._value", installationDirectory);
		
	//begin check the component dependencies - if it is required by other components 

		// get a list of components that require component being deleted
		fwInstallation_getListOfDependentComponents( componentName,   dynDependentComponents);

			if( dynlen(dynDependentComponents) >= 1)
			{
				// store the dependent components in a string 
				for(i = 1; i <= dynlen(dynDependentComponents); i++)
				{
					strDependentComponentsNames += dynDependentComponents[i] + "|";
				}					
			
				fwInstallation_showMessage(makeDynString("Dependend components at deletion of "+componentName+":",strDependentComponentsNames));

				
					// ask the user if he wants to delete this component - other components are using it
					ChildPanelOnCentralReturn("fwInstallation/fwInstallationDependencyDelete.pnl", "fwInstallation_Dependency_Delete",
					 makeDynString("$strDependentNames:" + strDependentComponentsNames , "$componentName:" + componentName, "$callingPanel:" + "ToDelete"), dreturnf, dreturns);
			
						// check the return value of fwInstallationDependency .pnl
						if(dreturns[1] == "Install_Delete")
						{
							fwInstallation_showMessage(makeDynString("User choice at deletion of "+componentName+": DELETE"));
							// delete the component and update other components
							for( i = 1; i <= dynlen(dynDependentComponents); i++)
							{
								// in all dependent components set the value that the required component is not installed
								dpSet("fwInstallation_" + dynDependentComponents[i] + ".requiredInstalled:_original.._value", false);
							}

						}
						else if(dreturns[1] == "DoNotInstall_DoNotDelete")
						{
							// do not delete the component
							fwInstallation_showMessage(makeDynString("User choice at deletion of "+componentName+": ABORT"));
							deletionAborted = TRUE;
						}
			}


		// check if all files are deletable
		// FVR: Do this check only if the deleteAllFiles flag is set to true
    if(deleteAllFiles){		
			dpGet("fwInstallation_" + componentName + ".componentFiles:_online.._value", dynFileNames);
			for (k=1; k<=dynlen(dynFileNames); k++)
			{
				error = access(installationDirectory + "/" + dynFileNames[k],W_OK);
				if(error!=0)
				{
					errorCounter++;
					errorString = "fwInstallation: the file "+ installationDirectory + "/" + dynFileNames[k]+" cannot be deleted";
					dynAppend(strErrors, errorString);
					fwInstallation_showMessage(makeDynString(errorString));
				}
			}
			dpGet("fwInstallation_" + componentName + ".configFiles.configGeneral:_online.._value", dynFileNames);
			for (k=1; k<=dynlen(dynFileNames); k++)
			{
				error = access(installationDirectory + "/" + dynFileNames[k],W_OK);
				if(error!=0)
				{
					errorCounter++;
					errorString = "fwInstallation: the file "+ installationDirectory + "/" + dynFileNames[k]+" cannot be deleted";
					dynAppend(strErrors, errorString);
					fwInstallation_showMessage(makeDynString(errorString));
				}
			}
			dpGet("fwInstallation_" + componentName + ".configFiles.configLinux:_online.._value", dynFileNames);
			for (k=1; k<=dynlen(dynFileNames); k++)
			{
				error = access(installationDirectory + "/" + dynFileNames[k],W_OK);
				if(error!=0)
				{
					errorCounter++;
					errorString = "fwInstallation: the file "+ installationDirectory + "/" + dynFileNames[k]+" cannot be deleted";
					dynAppend(strErrors, errorString);
					fwInstallation_showMessage(makeDynString(errorString));
				}
			}
			dpGet("fwInstallation_" + componentName + ".configFiles.configWindows:_online.._value", dynFileNames);
			for (k=1; k<=dynlen(dynFileNames); k++)
			{
				error = access(installationDirectory + "/" + dynFileNames[k],W_OK);
				if(error!=0)
				{
					errorCounter++;
					errorString = "fwInstallation: the file "+ installationDirectory + "/" + dynFileNames[k]+" cannot be deleted";
					dynAppend(strErrors, errorString);
					fwInstallation_showMessage(makeDynString(errorString));
				}
			}
			dpGet("fwInstallation_" + componentName + ".initFiles:_online.._value", dynFileNames);
			for (k=1; k<=dynlen(dynFileNames); k++)
			{
				error = access(installationDirectory + "/" + dynFileNames[k],W_OK);
				if(error!=0)
				{
					errorCounter++;
					errorString = "fwInstallation: the file "+ installationDirectory + "/" + dynFileNames[k]+" cannot be deleted";
					dynAppend(strErrors, errorString);
					fwInstallation_showMessage(makeDynString(errorString));
				}
			}
			dpGet("fwInstallation_" + componentName + ".deleteFiles:_online.._value", dynFileNames);
			for (k=1; k<=dynlen(dynFileNames); k++)
			{
				error = access(installationDirectory + "/" + dynFileNames[k],W_OK);
				if(error!=0)
				{
					errorCounter++;
					errorString = "fwInstallation: the file "+ installationDirectory + "/" + dynFileNames[k]+" cannot be deleted";
					dynAppend(strErrors, errorString);
					fwInstallation_showMessage(makeDynString(errorString));
				}
			}
			dpGet("fwInstallation_" + componentName + ".dplistFiles:_online.._value", dynFileNames);
			for (k=1; k<=dynlen(dynFileNames); k++)
			{
				error = access(installationDirectory + "/" + dynFileNames[k],W_OK);
				if(error!=0)
				{
					errorCounter++;
					errorString = "fwInstallation: the file "+ installationDirectory + "/" + dynFileNames[k]+" cannot be deleted";
					dynAppend(strErrors, errorString);
					fwInstallation_showMessage(makeDynString(errorString));
				}
			}
			dpGet("fwInstallation_" + componentName + ".descFile:_online.._value", descFileName);
			error = access(installationDirectory + "/" + descFileName,W_OK);
			if(error!=0)
			{
				errorCounter++;
				errorString = "fwInstallation: the file "+ installationDirectory + "/" + descFileName +" cannot be deleted";
				dynAppend(strErrors, errorString);
				fwInstallation_showMessage(makeDynString(errorString));
			}
			dpGet("fwInstallation_" + componentName + ".scriptFiles:_online.._value", dynFileNames);
			for (k=1; k<=dynlen(dynFileNames); k++)
			{
				error = access(installationDirectory + "/" + dynFileNames[k],W_OK);
				if(error!=0)
				{
					errorCounter++;
					errorString = "fwInstallation: the file "+ installationDirectory + "/" + dynFileNames[k]+" cannot be deleted";
					dynAppend(strErrors, errorString);
					fwInstallation_showMessage(makeDynString(errorString));
				}
			}
			dpGet("fwInstallation_" + componentName + ".postInstallFiles:_online.._value", dynFileNames);
			for (k=1; k<=dynlen(dynFileNames); k++)
			{
				error = access(installationDirectory + "/" + dynFileNames[k],W_OK);
				if(error!=0)
				{
					errorCounter++;
					errorString = "fwInstallation: the file "+ installationDirectory + "/" + dynFileNames[k]+" cannot be deleted";
					dynAppend(strErrors, errorString);
					fwInstallation_showMessage(makeDynString(errorString));
				}
			}
			dpGet("fwInstallation_" + componentName + ".postDeleteFiles:_online.._value", dynFileNames);
			dynPostDeleteFiles_current = dynFileNames;
			for (k=1; k<=dynlen(dynFileNames); k++)
			{
				error = access(installationDirectory + "/" + dynFileNames[k],W_OK);
				if(error!=0)
				{
					errorCounter++;
					errorString = "fwInstallation: the file "+ installationDirectory + "/" + dynFileNames[k]+" cannot be deleted";
					dynAppend(strErrors, errorString);
					fwInstallation_showMessage(makeDynString(errorString));
				}
			}
    
			if(errorCounter!=0)
			{
				ChildPanelOnCentralReturn("fwInstallation/fwInstallationDependencyDelete.pnl", "fwInstallation_Dependency",
					 makeDynString("$strDependentNames:" + strErrors , "$componentName:" + componentName, "$callingPanel:" + "DeleteChecker"), dreturnf, dreturns);
				// check the return value
				if(dreturns[1] == "Install_Delete")
				{
					errorString = "fwInstallation: "+errorCounter+" error(s) found. De-Installation of "+componentName+" will continue on user request";
					fwInstallation_showMessage(makeDynString(errorString));
				}
				else if(dreturns[1] == "DoNotInstall_DoNotDelete")
				{
					errorString = "fwInstallation: "+errorCounter+" error(s) found. De-Installation of "+componentName+" is aborted";
					fwInstallation_showMessage(makeDynString(errorString));
					deletionAborted = TRUE;
				}
			}
    }//End of if(deleteAllFiles)  


//end check the component dependencies	

	if(!deletionAborted)
	{
		logFileName.text = installationDirectory + "/log/fwDeInstallation_"+componentName+"_"+formatTime("%Y%m%d_%H%M%S",getCurrentTime())+".log\"";
		fwInstallation_writeToMainLog(formatTime("[%Y-%m-%d_%H:%M:%S] ",getCurrentTime()) + "Starting to delete " + componentName);
	
		fwInstallation_showMessage(makeDynString("Deleting " + componentName + " ... ")); 
		
		
		// retrieve the information about the sub components
		
		dpGet("fwInstallation_" + componentName + ".subComponents:_online.._value", dynSubComponents);
		
		// delete all subcomponents
		for(i = 1; i <= dynlen(dynSubComponents); i++)
		{
			fwInstallation_showMessage(makeDynString("   Deleting sub components .."));
			
			// delete subcomponent
			fwInstallation_deleteComponent(dynSubComponents[i], componentDeleted);
		}	
		
		// retrieve the information about the files	to be deleted
		
		// retrieve  component files ( panels, scripts )
		
		dpGet("fwInstallation_" + componentName + ".componentFiles:_online.._value", componentFiles);
		
		// retrieve config files
		dpGet("fwInstallation_" + componentName + ".configFiles.configGeneral:_online.._value", configFiles_general);
		dpGet("fwInstallation_" + componentName + ".configFiles.configLinux:_online.._value", configFiles_linux);
		dpGet("fwInstallation_" + componentName + ".configFiles.configWindows:_online.._value", configFiles_windows);
			
		// retrieve init files
		dpGet("fwInstallation_" + componentName + ".initFiles:_online.._value", initFiles);
		
		// retrieve dplist files
		dpGet("fwInstallation_" + componentName + ".dplistFiles:_online.._value", dplistFiles);
		
		// retrieve description file
		dpGet("fwInstallation_" + componentName + ".descFile:_online.._value", descFile);
		
		// retrieve the script files
		
		dpGet("fwInstallation_" + componentName + ".scriptFiles:_online.._value", dynScriptFiles);
		
		
		// retrieve the postinstall files
		
		dpGet("fwInstallation_" + componentName + ".postInstallFiles:_online.._value", postInstallFiles);
		
		dynDescFile = makeDynString(descFile);

		// retrieve delete files
		dpGet("fwInstallation_" + componentName + ".deleteFiles:_online.._value", deleteFiles);
		// retrieve postDelete files
		dpGet("fwInstallation_" + componentName + ".postDeleteFiles:_online.._value", postDeleteFiles);
		
// begin store the postDelete files in a datapoint
			
		// get all the post delete files
		dpGet("fwInstallationInfo.postDeleteFiles:_online.._value", dynPostDeleteFiles_all);
		
		dynClear(dynPostDeleteFiles_current);
		if(dynlen(postDeleteFiles)>0)
			for(i=1; i<=dynlen(postDeleteFiles); i++)
				dynAppend(dynPostDeleteFiles_current, installationDirectory +"/"+ postDeleteFiles[i]);
		// append the name of the current component post delete files
		dynAppend(dynPostDeleteFiles_all, dynPostDeleteFiles_current);
		
		// store all the post delete files				
		dpSet("fwInstallationInfo.postDeleteFiles:_original.._value", dynPostDeleteFiles_all);			


// end store the postDelete files in a datapoint

		
		if(installationDirectory == "")
		{
			DebugN("ERROR: The installation directory for the " + componentName + " does not exist or is not specified!");
		}
		else
		{
			
// execute the delete script

			if(dynlen(deleteFiles) > 0 ) 
			{
				fwInstallation_showMessage(makeDynString("    Executing the delete file ... "));

				for(i =1; i <= dynlen(deleteFiles); i++)
				{
					componentDeleteFile = deleteFiles[i];
					// read the file and execute it
												
					fwInstallation_evalScriptFile( installationDirectory +"/"+ componentDeleteFile , iReturn);
					
					if(iReturn == -1)
					{
						fwInstallation_showMessage(makeDynString("    	Error: executing the delete file: " + componentDeleteFile));
						
						errorDeletingComponent = -1;
		
		
					}
				}
			}

// end execute the delete script	

		if(deleteAllFiles)
		{
			fwInstallation_showMessage(makeDynString("    Deleting files ..."));
	
			// delete all the files
			
			iReturn = fwInstallation_deleteFiles(componentFiles, installationDirectory);
			
			if(iReturn == -1) 
				errorDeletingComponent = -1;
				
			iReturn = fwInstallation_deleteFiles(configFiles_general, installationDirectory);
					
			if(iReturn == -1) 
				errorDeletingComponent = -1;
				
			iReturn = fwInstallation_deleteFiles(configFiles_linux, installationDirectory);
					
			if(iReturn == -1) 
				errorDeletingComponent = -1;
			
			iReturn = fwInstallation_deleteFiles(configFiles_windows, installationDirectory);
					
			if(iReturn == -1) 
				errorDeletingComponent = -1;
				
			iReturn = fwInstallation_deleteFiles(initFiles, installationDirectory);
					
			if(iReturn == -1) 
				errorDeletingComponent = -1;
			
			iReturn = fwInstallation_deleteFiles(deleteFiles, installationDirectory);
					
			if(iReturn == -1) 
				errorDeletingComponent = -1;
			
			iReturn = fwInstallation_deleteFiles(postInstallFiles, installationDirectory);
			
			if(iReturn == -1)
				errorDeletingComponent = -1;
					
			iReturn = fwInstallation_deleteFiles(dplistFiles, installationDirectory);
					
			if(iReturn == -1) 
				errorDeletingComponent = -1;
	
	
//SMS as most developers put the xml into the file list, we do not need to delete it twice			
/*			iReturn = fwInstallation_deleteFiles(dynDescFile, installationDirectory);
					
			if(iReturn == -1) 
				errorDeletingComponent = -1;
*/
			
			iReturn = fwInstallation_deleteFiles(dynScriptFiles, installationDirectory);
			
			if(iReturn == -1) 
				errorDeletingComponent = -1;
		}
						
		}
		
		// now delete the component info from the config file
		
		fwInstallation_showMessage(makeDynString("    Updating the config file ..." ));
		
		// delete the component from the config file	
		fwInstallation_deleteComponentFromConfigScriptsFile(componentName, 	getPath(CONFIG_REL_PATH) + "config");
		
		
		// delete the component from the fwScripts.lst file
		if(dynlen(dynScriptFiles) > 0)
		{
			fwInstallation_showMessage(makeDynString("    Updating the fwScripts.lst file ..." ));
	
			fwInstallation_deleteComponentFromConfigScriptsFile(componentName, 	getPath(SCRIPTS_REL_PATH) + "fwScripts.lst");
		}
		
		
		fwInstallation_showMessage(makeDynString("    Updating the component database ..."));
		
		// delete the DP
		dpDelete("fwInstallation_" + componentName);
	}

	logFileName.text = "";

	if((errorDeletingComponent == -1) || (deletionAborted))
	{
		fwInstallation_showMessage(makeDynString("	  There were errors while deleting the components - see the log"));		
		if(deletionAborted)
		{	
			fwInstallation_writeToMainLog(formatTime("[%Y-%m-%d_%H:%M:%S] ",getCurrentTime()) + componentName + " de-installation aborted");
		} else {
			fwInstallation_writeToMainLog(formatTime("[%Y-%m-%d_%H:%M:%S] ",getCurrentTime()) + componentName + " deleted with errors");
		}
		componentDeleted = FALSE;
	}
	else
	{
		fwInstallation_showMessage(makeDynString("    Component deleted."));
		fwInstallation_writeToMainLog(formatTime("[%Y-%m-%d_%H:%M:%S] ",getCurrentTime()) + componentName + " deleted");
		componentDeleted = true;
	}

	tempString = logFileName.text;
	if(tempString != "")
	{
		fwInstallation_showMessage(makeDynString("Please find the log file for this deinstallation operation in:",tempString));
		fwInstallation_writeToMainLog(formatTime("[%Y-%m-%d_%H:%M:%S] ",getCurrentTime()) + "Please find the log file for this de-installation operation in: "+tempString);
	}	
        
        //Delete component group from project:
        fwInstallation_deleteComponentGroup(componentName + "_" + componentVersion);
/*	
  //Register component in DB:
  if(fwInstallationDB_getUseDB()){
  
    if(fwInstallationDB_unregisterConfiguration(componentName, componentVersion) != 0)
    { 
      DebugN("ERROR: fwInstallation_deleteComponent() -> DB Unegistration failed for component: " + componentName + " v." + componentVersion);
    }

  }else{
    DebugN("INFO: fwInstallation_deleteComponent() -> Skipping DB unregistration. User flag useDB set to FALSE");
  }
*/	
}


/** this function deletes the component information from the PVSS project confg file.
It loads the config file into dyn_string ( each line in one row of a dyn_string )
Deletes the rows from the dyn_string and saves the dyn_string to the file

@param componentName: the name of a component to be deleted

@author M.Sliwinski
*/

fwInstallation_deleteComponentFromConfigScriptsFile(string componentName, string strFileName)
{
	int i;
	
	string fileInString;
	
	bool fileLoaded;
	
	int idxCurrent;
	int removeResult;
	dyn_string dynFileLines;
		
	dyn_dyn_int components; // 1st row - indexes of the beginning of the component in the dyn_string
							// 1nd row - indexes of the engs of the component in the dyn_string
	
	int idxBegin = 0;
	int idxEnd = 0;
	
	int scriptFileExists;

        
        //DebugN("fwInstallation_deleteComponentFromConfigScriptsFile");

	scriptFileExists = access(strFileName, F_OK);
	
	if(scriptFileExists == 0)
	{
	
		fileLoaded = fileToString(strFileName, fileInString);
		
		if (! fileLoaded )
		{
		
			Debug("\n Cannot load " + strFileName + " file");
		}
		else 
		{
	
				dynFileLines = strsplit(fileInString, "\n");

				components[1] = ""; // in this row there are indexes of the beginning of the components
				components[2] = ""; // in this row there are indexes of the ends of the components
				
			
			// find out where are the components situated and in which sections
				
				for(i = 1; i <= dynlen(dynFileLines); i++)
				{
				
					// find the beginings and ends of the components entries
					
						if(	(strpos(dynFileLines[i], "#begin " + componentName) >= 0) &&
								(strlen(dynFileLines[i]) == strlen("#begin " + componentName)))
						{
							// found the beginning of the component
							idxBegin = i;
						}
						
						if(	(strpos(dynFileLines[i], "#end " + componentName) >= 0) &&
								(strlen(dynFileLines[i]) == strlen("#end " + componentName)))
						{
							// found the end of the component
							idxEnd = i;
							
							if(dynFileLines[i+1] == "")
							{
								// this is the blanki line after #end - it should be deleted
								idxEnd++;
							}
						}
						
						if(idxBegin < idxEnd)
						{
							// save the beginning and end of the component entry
							
							dynAppend(components[1], idxBegin);
							dynAppend(components[2], idxEnd);
							
							idxBegin = 0;
							idxEnd = 0;
						}
				}
				
			// delete the component from the  configLines dyn_string
			
				for(i = dynlen(components[1]); i >=1; i--)
				{
					idxBegin = components[1][i];
					idxEnd = components[2][i];
										
					for(idxCurrent = idxEnd-1; idxCurrent >= idxBegin; idxCurrent--)
					{
						// delete it from the configSectionTable

//Debug("Deleting line no "+idxCurrent+":"+dynFileLines[idxCurrent]);			
  					removeResult = dynRemove(dynFileLines, idxCurrent);
//DebugN("Result:"+removeResult);
						if (removeResult == -1)
						{
							Debug("\n ERROR: fwInstallation_deleteComponentFromConfigScriptsFile() - could not remove the line from table");
						}
					
						
					}
					
				}
				
				// save the config file
				
				fwInstallation_saveFile(dynFileLines, strFileName);
		}
	}
}

/** This function checks if the necessary version of the required components is availabe in the distribution

@param requiredComponents: list of components to be checked
@param goodComponents: list of components with correct version in the distribution
@return 0 - "success"  -1 - error 
@author F. Varela
*/

int fwInstallation_checkDistribution(string sourceDir, dyn_string requiredComponents, dyn_string &dsFileComponentName, dyn_string &dsFileVersions, dyn_string &dsFileComponent)
{
  dyn_dyn_string componentsInfo;
  dyn_string dsTmp;
  string requiredComponentName;
  string requiredComponentVersion;
  string fileComponentName;
  string fileComponentVersion;
  string fileComponent;
   
  fwInstallation_getAvailableComponents(sourceDir, componentsInfo);
  for(int jj = 1; jj <= dynlen(requiredComponents); jj++){
    dsTmp = strsplit(requiredComponents[jj], "=");
    requiredComponentName = dsTmp[1];
    requiredComponentVersion = dsTmp[2];
                
    for(int ii = 1; ii <= dynlen(componentsInfo);ii++){
      fileComponentName = componentsInfo[ii][1];
      fileComponentVersion = componentsInfo[ii][2];
      fileComponent = componentsInfo[ii][4];
      if(patternMatch(requiredComponentName, fileComponentName)){
        DebugN("INFO: Required component found in distribution:" + requiredComponentName + ". Comparing versions");
        if(_fwInstallation_CompareVersions(fileComponentVersion, requiredComponentVersion)){
          DebugN("INFO: Distribution version OK. Proceeding with the installation: " + fileComponentVersion + " required: " +  requiredComponentVersion);
          DebugN("INFO: Component description file: " + fileComponent);
          dynAppend(dsFileComponentName, fileComponentName);
          dynAppend(dsFileComponent, fileComponent);
          dynAppend(dsFileVersions, fileComponentVersion);
        }else{
          DebugN("ERROR: Distribution version NOT OK. Aborting installation: " + fileComponentVersion + " required: " +  requiredComponentVersion);
          return -1;
        }
      }
    }
  }
  //If all components are available it all right otherwise error
  if(dynlen(dsFileComponentName) == dynlen(requiredComponents))
    return 0;
  else
	  return -1;
  
}


/** This function checks if a given component is correctly installed

@param componentName: Name of the component to be checked
@param version: Version of the component to be checked. Optional parameter: if emtpy it checks for any version.
@return 0 - Compent version or newer correctly installed  -1 - Component installed correctly or just not installed
@author F. Varela Rodriguez
*/

int fwInstallation_checkInstalledComponent(string componentName, string version = "")
{
	dyn_string componentDPs;
	string componentDP;
  bool requiredComponents;
  
	string componentVersionString, installationDirectory;
	float componentVersion;

	componentDPs = dpNames("fwInstallation_" + componentName);
  if(dynlen(componentDPs) == 0){
    //DebugN("INFO: fwInstallation_checkInstalledComponent()-> Component: " + componentName + " Version: " + version + " is not installed in this system");
    return -1;
  }else
    componentDP = componentDPs[1];
    

	
  dpGet(componentDP+".componentVersionString:_online.._value",componentVersionString,
	      componentDP+".installationDirectory:_online.._value",installationDirectory,
	      componentDP+".requiredInstalled:_online.._value", requiredComponents);


	if(componentVersionString == "")
	{
		dpGet(componentDPs[i] + ".componentVersion:_original.._value", componentVersion);
		if(componentVersion == floor(componentVersion))
		{
			sprintf(componentVersionString,"%2.1f",componentVersion);
		} else {
			componentVersionString = componentVersion;
		}
		dpSetWait(componentDPs[i] + ".componentVersionString:_original.._value", componentVersionString);
	}

  if(version != "" && !_fwInstallation_CompareVersions(componentVersionString, version)){
    DebugN("INFO: fwInstallation_checkInstalledComponent()-> An old version:"+ componentVersionString +" of the component: " + componentName + " is installed in this system. Requested version: " + version);
    return -1;	  
	}
	
	if(!requiredComponents){
    DebugN("INFO: fwInstallation_checkInstalledComponent()-> Version:"+ componentVersionString +" of the component: " + componentName + " is installed but not all required components");
    return -1;	  	
	}
  DebugN("INFO: fwInstallation_checkInstalledComponent()-> Version:"+ componentVersionString +" of the component: " + componentName + " installed in this system");
  return 0;
}

int fwInstallation_checkTargetDirectory(string destinationDir, string componentName, string &versionInstalled)
{
  string componentNameFile;  
  string componentDate;
  dyn_string requiredComponents;
  bool isSubComponent;
  dyn_string initScripts;
  dyn_string deleteScripts;
  dyn_string postInstallScripts; 
  dyn_string postDeleteScripts;
  dyn_string configFiles; 
  dyn_string asciiFiles;
  dyn_string panelFiles;
  dyn_string scriptFiles;
  dyn_string libraryFiles;
  dyn_string otherFiles;
  dyn_string xmlDesc;
  
  DebugN("INFO: Looking for previous versions of " + componentName + ".xml in " + destinationDir);

  dyn_string componentFiles = getFileNames(destinationDir, componentName + ".xml");
  
  if(dynlen(componentFiles) >0)
  {
      fwInstallation_loadXml(destinationDir + "/" + componentFiles[1],
	    			  		 componentNameFile, 
				  			 versionInstalled, 
						  	 componentDate,
							 requiredComponents,
							 isSubComponent,
							 initScripts, 
							 deleteScripts,
							 postInstallScripts, 
							 postDeleteScripts,
							 configFiles, 
							 asciiFiles,
							 panelFiles,
							 scriptFiles,
							 libraryFiles,
							 otherFiles,
							 xmlDesc);

    return 1;
  }

  return 0;
}


dyn_string fwInstallation_getFileNamesRec(string dir = ".", string pattern = "*")
{
	dyn_string tempDynString;
	dyn_string allFileNames;
	string newDir = "/*";
	
	
//	DebugTN(dir, pattern);
	
	dynClear(allFileNames);


	fwInstallation_recurserFileNames(dir, "*", allFileNames);

	if(dynlen(allFileNames) > 0)
		for(int i=1; i<=dynlen(allFileNames); i++)
		{
			strreplace(allFileNames[i], dir + "/", "");
			strreplace(allFileNames[i], "//", "/");
		}

//	DebugTN(allFileNames);
	
    if(pattern != "*")
      pattern = "*" + pattern;

	for(int i=1; i<=dynlen(allFileNames); i++){
		if(patternMatch(pattern, allFileNames[i]))
			dynAppend(tempDynString, allFileNames[i]);
	}
	return tempDynString;
}

fwInstallation_recurserFileNames(string dir, string pattern, dyn_string & fileNames)
{
	dyn_string tempDynString = getFileNames(dir + "/*", pattern);
	dyn_string tempDynString2 = getFileNames(dir, pattern);
	
//	DebugTN(dir, pattern, tempDynString);
//	delay(0,5);
	
	if(dynlen(tempDynString2) > 0)
		for(int i=1; i<=dynlen(tempDynString2); i++)
			tempDynString2[i] = dir +"/"+ tempDynString2[i];
	
	dynAppend(fileNames, tempDynString2);

	dynRemove(tempDynString, 1);
	
	if(dynlen(tempDynString) > 0)
		for(int i=1; i<=dynlen(tempDynString); i++)
			fwInstallation_recurserFileNames(dir + "/" + tempDynString[i] + "/", pattern, fileNames);
}


int fwInstallation_getDescriptionFile(string componentName, string componentVersion, string sourceDir, string &descriptionFile, bool &isItSubComponent)
{
  string fileName = componentName + ".xml";
  dyn_dyn_string componentsInfo;

  fwInstallation_getAvailableComponents(makeDynString(sourceDir), componentsInfo, componentName);

  //DebugN("List of available components: " + componentsInfo);

  for(int i =1; i <= dynlen(componentsInfo); i++){
    
	if(componentsInfo[i][1] == componentName && componentsInfo[i][2] == componentVersion){
	  //DebugN("Found...");
	  descriptionFile = componentsInfo[i][4];

	  if(componentsInfo[i][3] == "no")
	    isItSubComponent = false;
	  else
	    isItSubComponent = true;

      return 0;

	}
  }

  return -1;


}

//////////////////////////////////////////////////
int fwInstallation_isSubComponent(string xmlFile, bool &isSubComponent)
{
    
  string componentNameFile;
  string componentVersion;
  string componentDate;

  dyn_string requiredComponents;
  dyn_string initScripts;
  dyn_string deleteScripts;
  dyn_string postInstallScripts;
  dyn_string postDeleteScripts;
  dyn_string configFiles; 
  dyn_string asciiFiles;
  dyn_string panelFiles;
  dyn_string scriptFiles;
  dyn_string libraryFiles;
  dyn_string otherFiles;
  dyn_string xmlDesc;
  
  dyn_float df;
  dyn_string ds;


  DebugN("INFO: fwInstallation_isSubComponent() -> Loading file: " + xmlFile);

  if(fwInstallation_loadXml(xmlFile,
	  			  			          componentNameFile, 
				  			            componentVersion, 
						  	            componentDate,
							              requiredComponents,
							              isSubComponent,
							              initScripts, 
							              deleteScripts,
							              postInstallScripts, 
							              postDeleteScripts,
							              configFiles, 
							              asciiFiles,
							              panelFiles,
							              scriptFiles,
							              libraryFiles,
							              otherFiles,
							              xmlDesc) != 0)
  {	
     DebugN("ERROR: fwInstallation_isSubComponent() -> Could not load XML file " + xmlFile + ". Aborted.");
     return -1;
  }
  
  return 0;

}

int fwInstallation_getDistPort()
{
  int port;
 
   string filename = PROJ_PATH + "/config/config";
 
  string section = "dist";
 
  paCfgReadValue(filename,section, "distPort", port);
 
  if(port == 0)
    port = 4777;

  //DebugN(port);
              
  return port; 
 
}


void fwInstallation_runAgentCB(string actionsDpe, dyn_string actions)
{
  string commandLine = "-num " + g_manNumNV;
  string manager = "PVSS00NV";

  bool failed, disabled;
  dyn_int diManPos, diStartMode, diSecKill, diRestartCount, diResetMin;
  dyn_string dsManager, dsCommandLine, dsProps;
  string str, host;
  int port, iErr = paGetProjHostPort(paGetProjName(), host, port);
  int pos;
  int found = false;
  int i = 1;
  str = "##SINGLE_MGR:START ";
  int error;
  
  //DebugN("Pending Actions: ", actions);

  if(dynlen(actions) > 0){
    //find manager position
    //////////////

	pmonGetManagers(failed, paGetProjName(), diManPos, dsManager, diStartMode, diSecKill, 
					diRestartCount, diResetMin, dsCommandLine);

	//DebugN(dsManager, dsCommandLine);
	
	if(!failed && dynContains(dsManager, manager))
	{
	  //for(int i=1; i<=dynlen(dsManager); i++)
	  do
	  {
	    if((dsManager[i] == manager) && patternMatch("*"+commandLine + "*", dsCommandLine[i]))
	    {
	      pos = i-1;
		  found = true;
		}
	    i++;
      } while(!found && i <= dynlen(dsManager));
	}
 
	//////////////
	if(found){
	  str += pos;

	  //DebugN("FVR::::pmon command: " + str);
	  error = pmon_command(str, host, port, FALSE, TRUE);


    }else{
	  DebugN("ERROR: fwInstallation_runAgentCB() -> Could not find FW Installation Agent UI.");  
	}
  }
  if(error){
    DebugN("ERROR: Could not start the FW Installation Agent");
  }
}

