#uses "fmdCAENLibrary.ctl" 
 
FwCaenChannelEPC_initialize(string domain, string device)
{
     DebugN(">>-FwCaenChannelEPC_initialize->");   
  
}
FwCaenChannelEPC_valueChanged( string domain, string device,
      int actual_dot_status,
      bool actual_dot_remIlk,
      bool actual_dot_intFail, string &fwState )
{
	if (actual_dot_remIlk) 	{ 
		fwState = "INTERLOCK"; 
	} else if (actual_dot_intFail) { 
		fwState = "PWS_FAULT"; 
	} else  if (actual_dot_status & CAEN_TRIPMASK ) {      
		fwState = "WA_REPAIR";      
	} else if (actual_dot_status & CAEN_ERRORMASK ) {      
		fwState = "ER_REPAIR";      
	} else if (actual_dot_status & CAEN_NOCONTROLMASK ) {       
		fwState = "NO_CONTROL";       
	} else if (!(actual_dot_status ^ CAEN_OFFMASK))	{      
		fwState = "OFF";      
	} else if (!(actual_dot_status ^ CAEN_ONMASK)) 	{ 
            fwState = "ON";   
	} else       
	{      
            DebugN(">>-FwCaenChannelEPC_valueChanged->UNDEFINED STATE ! Domain"+domain+"  device:"+device+" status:"+actual_dot_status+" state:"+fwState);   
		fwState = "UNDEFINED";      
	}  
}


FwCaenChannelEPC_doCommand(string domain, string device, string command)
{
	if (command == "SWITCH_ON")
	{
		dpSet(device+".settings.onOff",true);
	}
	if (command == "RESET")
	{
		dpSetWait(device+".settings.onOff",true);
		dpSetWait(device+".settings.onOff",false);
	}
	if (command == "SWITCH_OFF")
	{
		dpSet(device+".settings.onOff",false);
	}
	if (command == "KILL")
	{
		dpSet(device+".settings.onOff",false);
	}
}


