/**@file
 *
 * This library contains all functions that serve as interface between the installation tool and a external Oracle DB.
 *
 * @author Fernando Varela Rodriguez (IT-CO/BE)
 * @date   October 2006
 */
#uses "fwInstallation.ctl"
#uses "CtrlRDBAccess"

const string csFwInstallationDBLibVersion = "3.0.1";

//Table and view names used in this version of the DB Schema:
const string FW_INSTALLATION_SCHEMA_TBL = "FW_SYS_STAT_SCHEMA";

// Required schema version:
const string FW_INSTALLATION_DB_REQUIRED_SCHEMA_VERSION = "1.0.0";

//Project status:
const int FW_INSTALLATION_DB_PROJECT_OK = 1;
const int FW_INSTALLATION_DB_PROJECT_MISSMATCH = -1;
const int FW_INSTALLATION_DB_PROJECT_NEVER_CHECKED = 0;


//Debugging options:
global bool g_fwInstallationSqlDebug = false;
global bool g_fwInstallationVerbose = false;
////////////////////////////////////////////////////////////////////////////////////////////

//DB Connection using RDBAccess.ctl
global dbConnection gFwInstallationDBConn;
  
string FW_INSTALLATION_DB_CONNECTION_NAME = "fwInstallationToolConnection";



///Beginning of executable code:

int fwInstallationDB_closeDBConnection()
{
  rdbCloseConnection(gFwInstallationDBConn); 
}

int fwInstallationDB_setupConnection()
{
  ChildPanelOnCentral("fwInstallation/fwInstallationDB_connectionSetup.pnl", "FW Installation DB connection",
                             makeDynString(""));

  return 0;
}



int fwInstallationDB_executeDBCommand(string sql, dyn_dyn_mixed data)
{
 
  dbCommand cmd;
  
  if(g_fwInstallationSqlDebug)
    DebugN(sql);

  int rc = rdbStartCommand(gFwInstallationDBConn, sql, cmd);        
  
  if(rc) {DebugN("ERROR: fwInstallationDB_executeDBCommand() -> rdbStartCommand failed"); return -1;}
  else rc = rdbBindAndExecute(cmd, data);
  
  if (rc) {
    DebugN("ERROR is BindExec",rc);
    int i1,i2,i3;
    string s1,s2;
    rc=rdbGetError(gFwInstallationDBConn, i1,i2,i3, s1,s2);
    DebugN(s1,s2);  
    dyn_errClass err=getLastError();
    DebugN(err);
    return -1;
  }
  
  rc = rdbFinishCommand(cmd);
    
  // error-handling part
  if (rc) {
      DebugN("ERROR: fwInstallationDB_executeDBCommand() -> rdbBindAndExecutefailed");
      int errorCount, errorNumber, errorNative;
      string errorDescription, SQLState;
      
      rdbGetError(gFwInstallationDBConn, errorCount, errorNumber, errorNative, 
                  errorDescription, SQLState);

      DebugN("Database error",rc,errorCount,errorNumber, errorNative, errorDescription,SQLState);
      return -1;
        
      } else if(g_fwInstallationVerbose){
        DebugN("fwInstallationDB_executeDBCommand() -> Data inserted/updated OK");
      }    
    return 0;
}


int fwInstallationDB_executeDBQuery(string sql, dyn_dyn_mixed &data, bool columnWise=FALSE, int maxRecords=0, dyn_int columnTypes=0){
  dbCommand cmd;
  int rc=rdbStartCommand(gFwInstallationDBConn, sql, cmd);

  if (rc) {DebugN("ERROR: fwInstallationDB_executeQuery() -> Error in  rdbStartCommand"); return -1;};
  
  rc=rdbExecuteCommand(cmd);
  if (rc) {DebugN("ERROR: fwInstallationDB_executeQuery() -> Error in  rdbExecuteCommand");return -1;};
  rc=rdbGetData(cmd, data, columnWise, maxRecords, columnTypes);
  
  if(g_fwInstallationVerbose)
    DebugN("data: ", data);

  rc=rdbFinishCommand(cmd);
  if (rc) {DebugN("ERROR: fwInstallationDB_executeQuery() -> Error in  rdbEFinishCommand");return -1;}; 
  
  return 0;
}

bool fwInstallationDB_getInitialized()
{
  bool value;
  dpGet("fwInstallation_agentParametrization.db.connection.initialized", value);
  
  return value;
}

int fwInstallationDB_setInitialized(bool value)
{
  return dpSet("fwInstallation_agentParametrization.db.connection.initialized", value);
}


int fwInstallationDB_deleteConnection()
{
  if(fwInstallationDB_setDBSetup("") != 0 || fwInstallationDB_setUseDB(FALSE) != 0)
  {
    DebugN("ERROR: fwInstallationDB_deleteConnection() -> Could not delete DB Connection. Sorry about that!");
    return -1;
  }
  if(g_fwInstallationVerbose)
    DebugN("INFO: fwInstallationDB_deleteConnection() -> DB connection successfully deleted.");

  return 0;
}

int fwInstallationDB_tableExists(string tableName, bool &tableExists)
{

  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql = "SELECT table_name FROM user_tables where table_name = \'" + tableName + "\'";
  
  dynClear(aRecords);
  tableExists = false;
       
  if(g_fwInstallationSqlDebug)
    DebugN(sql);
  
  if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
  {
    DebugN("ERROR: fwInstallation_tableExists() -> Could not execute the following SQL query: " + sql);
    return -1;
  }  

  if(dynlen(data) > 0 && aRecords[1][1] == tableName)
      tableExists = TRUE;
  else
    tableExists = FALSE;
  
  return 0;

}


int fwInstallationDB_viewExists(string viewName, bool &viewExists)
{

  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql = "SELECT view_name FROM user_views WHERE view_name = \'" + viewName + "\'";
  
  dynClear(aRecords);
  viewExists = false;
    
      
  if(g_fwInstallationSqlDebug)
    DebugN(sql);
  
  if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
  {
    DebugN("ERROR: fwInstallation_viewExists() -> Could not execute the following SQL query: " + sql);
    return -1;
  }  

  if(dynlen(aRecords) > 0 && aRecords[1][1] == viewName)
    viewExists = TRUE;
  else
    viewExists = FALSE;
  
  return 0;

}

int fwInstallationDB_getSchemaVersion(string &version)
{
  bool viewExists = false;
  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql = "SELECT version FROM fw_sys_stat_schema";


  if(fwInstallationDB_viewExists(FW_INSTALLATION_SCHEMA_TBL, viewExists) == 0 && !viewExists)
  {
    DebugN("WARNING: fwInstallationDB_getSchemaVersion() -> Cannot retrieve the schema version since the view " 
            + FW_INSTALLATION_SCHEMA_TBL + " does not exist.\nDB Schema does not exist");
    return -1;       
  }

  dynClear(aRecords);
    
  if(g_fwInstallationSqlDebug)
    DebugN(sql);
  
  if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
  {
    DebugN("ERROR: fwInstallation_getSchemaVersion() -> Could not execute the following SQL query: " + sql);
    return -1;
  }  
  if(dynlen(aRecords) > 0) {   
    version = aRecords[1][1];
  }
  else{
    version = "";
    return -1;
  }
 
  return 0;
}

bool fwInstallationDB_compareSchemaVersion()
{
  string dbVersion;
  int schemaOK = FALSE;
  
  if(fwInstallationDB_getSchemaVersion(dbVersion) != 0){
    DebugN("WARNING: fwInstallationDB_compareSchemaVersion() -> Could not retrieve schema version from the DB. Aborted.");
    return schemaOK;
  }
    
  if(dbVersion >= FW_INSTALLATION_DB_REQUIRED_SCHEMA_VERSION)
    schemaOK = TRUE;
    
  return schemaOK;  
}


int fwInstallationDB_registerComponentFromXml(string xmlFile)
{
  string componentName;
  string componentVersion;
  string componentDate;
  dyn_string requiredComponents;
  bool isSubComponent;
  dyn_string initScripts;
  dyn_string deleteScripts;
  dyn_string postInstallScripts;
  dyn_string postDeleteScripts;
  dyn_string configFiles;
  dyn_string asciiFiles;
  dyn_string panelFiles;
  dyn_string scriptFiles;
  dyn_string libraryFiles;
  dyn_string otherFiles;
  dyn_string xmlDesc;

  dyn_string ds;
  string defaultPath;
  int isOfficial = -1; 					//Not yet implemented in XML file
  
  int error = fwInstallation_loadXml(xmlFile, componentName, componentVersion, componentDate, 
                         requiredComponents, isSubComponent, initScripts, deleteScripts, 
                         postInstallScripts, postDeleteScripts, configFiles, asciiFiles, 
                         panelFiles, scriptFiles, libraryFiles, otherFiles, xmlDesc);

  if(error != 0)
  {
    DebugN("ERROR::fwInstallationDB_registerComponentFromXml() -> Could not load XML file: " + xmlFile + ". Aborted.");
    return -1;  
  }

  //Make path to the XML file to be the default one.
//  strreplace(xmlFile, "\\", "\/");
  ds = strsplit(xmlFile, "/");
  for(int i = 1; i <= (dynlen(ds)-1); i++){
    defaultPath += (ds[i] + "/"); 
  }  
  
  if(g_fwInstallationVerbose)
    DebugN("INFO::fwInstallationDB_registerComponentFromXml() -> Registering from file: ", componentName, componentVersion, isSubComponent, defaultPath, isOfficial);

  return fwInstallationDB_registerComponent(componentName, componentVersion, isSubComponent, defaultPath, isOfficial);

}

int fwInstallationDB_connect()
{
  string database, username, password, driver = "QOCI8";
  dyn_float df;
  dyn_string ds;
   
  dpGet("fwInstallation_agentParametrization.db.connection.server", database,
        "fwInstallation_agentParametrization.db.connection.username", username,
        "fwInstallation_agentParametrization.db.connection.password", password);
  
  if(myManType() == CTRL_MAN){
    DebugN("ERROR: DB Connection not set up.");
    return -1; 
  }
  
  if(database == "" || username == "" || password == "")
  {     
     ChildPanelOnCentralModalReturn("fwInstallation/fwInstallation_messageInfo.pnl", "DB Connection Question", makeDynString("$1:DB Connection not yet set up.\nWould you like to do it now?", "$2:Yes", "$3:No"), df, ds);
     
     //DebugN(df, ds);
     
     if(dynlen(df) && df[1] > 0.){
       
       ChildPanelOnCentralModalReturn("fwInstallation/fwInstallationDB_connectionSetup.pnl", "DB Connection Setup", makeDynString(""), df, ds);
       if(dynlen(df) == 0 || df[1] < 1.){
         return -1;
       }else{
          dpGet("fwInstallation_agentParametrization.db.connection.server", database,
                "fwInstallation_agentParametrization.db.connection.username", username,
                "fwInstallation_agentParametrization.db.connection.password", password); 
            
          if(database != "" && username != "" && password != ""){
            return fwInstallationDB_openDBConnection(database, username, password, driver);
          }else{
              return -1;
          }
       }
     }else{
       return -1;
     }
   }else{
     return fwInstallationDB_openDBConnection(database, username, password, driver);
   }
}


int fwInstallationDB_openDBConnection(string database, string username, string password, string driver = "QOCI8")
{
  
  string connectionString = "driver="+driver+";database="+database+";uid="+username+";pwd=" + password;
  
  int rc=rdbGetConnection(FW_INSTALLATION_DB_CONNECTION_NAME, gFwInstallationDBConn);
  
  if (rc) {
     // means: no connection yet... open on 
    rc=rdbOpenConnection(connectionString, gFwInstallationDBConn);
    if (rc) {
     dyn_errClass err=getLastError();
     DebugN("ERROR: fwInstallationDB_openDBConnection() -> " + err);
     return -1; 
    }
    int i1,i2,i3;
    string s1,s2;
    rc=rdbGetError(gFwInstallationDBConn, i1,i2,i3, s1,s2);
    if (s1!="") {
     DebugN("ERROR: fwInstallationDB_openDBConnection() -> There was a DB error: ",s1,s2);
     return -1; 
    }
  } 
  
  if(g_fwInstallationVerbose)
    DebugN("INFO: fwInstallationDB_openDBConnection() -> DB Connection successfully opened.");

  return 0;
}

/*
int fwInstallationDB_init()
{  
  int result = -1;
  dyn_string ds;
  dyn_float df;
  string setup; 
  string connection = "";
  dyn_string exceptionInfo;
    
  //Is FW Configuration DB installed?  
  if(fwInstallationDB_isFwConfDBInstalled()){
    if(g_fwInstallationVerbose)
      DebugN("INFO: fwInstallationDB_init() -> FW Configuration DB Installed. Initializing DB connection.");
    
    //If installed. Has the connection been configured?
    setup = fwInstallationDB_getDBSetup();
    //DebugN("DB set up is: " + setup);

     if(setup != ""){
       fwConfigurationDB_initialize(setup, exceptionInfo);

       if(dynlen(exceptionInfo) > 0 ) {fwExceptionHandling_display(exceptionInfo); return -1; }
     }
     else{
      ChildPanelOnCentralReturn("vision/MessageInfo", "Configuration DB Setup", makeDynString("$1:FW Configuration DB Component installed.\nWould you like to set up a connection to the ConfDB\nto be used by the FW Installation Tool?", "$2:Yes", "$3:Cancel"), df, ds); 
      if(df[1] == 1){
        if(fwInstallationDB_setupDBConnection() != 0){
          DebugN("ERROR: fwInstallationDB_init() -> Could not setup the DB connection");
        }else{
          fwInstallationDB_setUseDB(TRUE);	//Everything went OK. Set user flag useDB to true.
          																	//This flag is then check by the other functions to see if connection is setup
        }        
      }else{
        DebugN("WARNING: fwInstallationDB_init() ->Action aborted by the user");
        return -1;
      }
    }    
  }else{
    if(g_fwInstallationVerbose)
      DebugN("INFO: fwInstallationDB_init() -> FW Configuration DB Tool not installed or old version. Skipping DB connection...");
    
    return -1;
  }
  if(g_fwInstallationVerbose)
    DebugN("INFO: fwInstallationDB_init() -> Configuration DB connection initalized for setup: " + setup);
  
  return 0;  
}
*/
/*
int fwInstallationDB_setupDBConnection()
{
  string setup;

  dyn_float df;
  dyn_string ds;
  dyn_string exceptionInfo;

  ChildPanelOnCentralReturn("fwConfigurationDB/fwConfigurationDB_Setup.pnl", "Fw Configuration DB Setup", makeDynString(""), df, ds);
  ChildPanelOnCentralReturn("vision/MessageInfo", "Configuration DB Setup", makeDynString("$1:Using ConfDB Setup: " + fwConfigurationDB_currentSetupName + "\nClick OK to continue.", "$2:OK", "$3:Cancel"), df, ds);        

  if(df[1] == 1){
  
    setup = fwConfigurationDB_currentSetupName;
    fwInstallationDB_setDBSetup(setup);
          
    fwConfigurationDB_checkInit(exceptionInfo);
    //DebugN("***********ExceptionInfo: " + exceptionInfo);
    if(dynlen(exceptionInfo) >0 ){ fwExceptionHandling_display(exceptionInfo); return -1; }
    
    return 0;
  
  }else{

    return -1;

  }
}
*/
/*
int fwInstallationDB_isFwConfDBInstalled()
{
  int result;
  const string componentName = "fwConfigurationDB";
  const string requiredVersion = "3.0.95";

  fwInstallation_componentInstalled(componentName, requiredVersion, result, getSystemName()); 
  
  if(!result)
    fwInstallationDB_setUseDB(FALSE);  //No config DB tool isntalled or wrong version. Set user flag useDB to FALSE
    
  return result; 
}
*/

/*
string fwInstallationDB_getDBSetup()
{
  string setup = "";
  
  dpGet("fwInstallation_agentParametrization.db.setup", setup);
  if(setup == "")
    return "";
  else
    return setup;  
}
*/
/*
int fwInstallationDB_setDBSetup(string setup)
{
  return dpSet("fwInstallation_agentParametrization.db.setup", setup);
}
*/

bool fwInstallationDB_getUseDB()
{
  bool useDB = false;

  dpGet("fwInstallation_agentParametrization.db.useDB", useDB);
  
  return useDB;

}

int fwInstallationDB_setUseDB(bool useDB)
{
  return dpSet("fwInstallation_agentParametrization.db.useDB", useDB);
}


/** This one does not stop upon an error, which may be e.g. missing table, etc...
*/
int fwInstallationDB_dropDBSchema()
{
  return fwInstallationDB_executeSqlFromFile(gFwInstallationDBConn,"fwSysStat_dropSchema.sql", "DROP DB SCHEMA", FALSE);
}

int fwInstallationDB_createDBSchema(bool dropExistingSchema=FALSE, bool forceCreation = false)
{
  
  if(!forceCreation){
    if(fwInstallationDB_compareSchemaVersion()){
      if(g_fwInstallationVerbose)
        DebugN("INFO: fwInstallationDB_createDBSchema() -> DB Schema up-to-date. Nothing to be done.");    
      return 0;
    }
  }
  
  int rc=rdbCommitTransaction(gFwInstallationDBConn);

  if (dropExistingSchema) {
      DebugN("INFO: fwInstallationDB_createDBSchema() -> Dropping existing schema. Please wait...");
      
      if(fwInstallationDB_dropDBSchema())
      {
        DebugN("ERROR: fwInstallationDB_createDBSchema() -> Could not drop existing schema");
        return -1;
      }
      else {DebugN("INFO: fwInstallationDB_createDBSchema() -> DB schema successfully dropped. Recreating DB schema now. Please wait...");}
  }

  
  if(fwInstallationDB_executeSqlFromFile(gFwInstallationDBConn,"fwSysStat_createSchema.sql",
                                         "CREATE DB SCHEMA", TRUE)){
     DebugN("ERROR: fwInstallationDB_createDBSchema() -> Could not create DB schema.");
     return -1; 
  }
  
  DebugN("INFO: fwInstallationDB_createDBSchema() -> Database schema succesfully created");
  
  return 0;
}

int fwInstallationDB_registerProject(string projectName = "", string host = "", int pmon_port = 4999, string pmon_username = "", string pmon_password = "")
{
  string sql;
  string systemName;
  string systemNumber;
//  string host;
  string tool_version;
  string default_inst_path;
  
//  int pmon_port;
  int pc_id;
  int project_id;
  int system_id;
    
  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;

  if(host == "")
    host = getHostname();

  
  if(projectName == ""){
    projectName = PROJ;
    paGetProjHostPort(projectName, host, pmon_port);
  }

    
  dynClear(exceptionInfo);
  
  
  //override host since the previous function sets localhost
  host = getHostname();    

  //Check if host is registered, otherwise, register it first:
  if(fwInstallationDB_isPCRegistered(pc_id) == 0 && pc_id == -1){
  
    if(g_fwInstallationVerbose)
      DebugN("INFO: fwInstallationDB_registerProject() -> Host: " + host + " not yet registered. Registering it now...");
    
    fwInstallationDB_registerPC();
  }

  systemName = getSystemName();
  
  if(patternMatch("*:", systemName))
  {
    dyn_string ds = strsplit(systemName, ":");
    systemName = ds[1];
  }

  systemNumber = getSystemId();
  
  //Check if the pvss system is already registered, otherwise, register it first:
  if(fwInstallationDB_isSystemRegistered(system_id) == 0 && system_id == -1){
  
    if(g_fwInstallationVerbose)
      DebugN("INFO: fwInstallationDB_registerProject() -> systemName: " + systemName + "systemNumber: " + systemNumber + " not yet registered. Registering it now...");
    
    fwInstallationDB_registerSystem();
  }  
     
  fwInstallation_getToolVersion(tool_version);
  
  if(fwInstallationDB_isProjectRegistered(project_id, projectName, host) == 0 && project_id == -1){
        
    sql = "INSERT INTO fw_sys_stat_pvss_project VALUES ((fw_sys_stat_pvss_project_sq.NEXTVAL), \'" +
          PROJ+ "\', " + pc_id + ", \'" + PROJ_PATH + "\', " + pmon_port + ", \'" + pmon_username + "\', \'" + pmon_password  + "\', \'" + tool_version + "\', SYSDATE, NULL, " + system_id + ", NULL, " + FW_INSTALLATION_DB_PROJECT_NEVER_CHECKED + ", NULL)";
         
    if(g_fwInstallationSqlDebug)
      DebugN(sql);
    
    if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)) {DebugN("ERROR: fwInstallationDB_registerProject() -> Could not execute the following SQL: " + sql); return -1;};
  }
  return 0;  
}


int fwInstallationDB_synchronizeContents()
{
  dyn_string ds;
  dyn_float df;
  
  dyn_dyn_mixed commonComponentsInfo;
  dyn_dyn_mixed pvssMissingComponentsInfo;
  dyn_dyn_mixed dbMissingComponentsInfo;

  dyn_string commonComponentsInfoStr;			//String to pass a $-parameters to the panel
  dyn_string pvssMissingComponentsInfoStr;		//String to pass a $-parameters to the panel
  dyn_string dbMissingComponentsInfoStr;			//String to pass a $-parameters to the panel
    
  int error = fwInstallationDB_checkIntegrity(commonComponentsInfo, pvssMissingComponentsInfo, dbMissingComponentsInfo);
   
  if(error == -1){
    DebugN("ERROR: fwInstallationDB_synchronizeContents() -> Could not check the integrity of the DB contents.\nPlease, make sure that the DB is available. Aborted.");
    return -1;
  }

    for(int i = 1; i <= dynlen(commonComponentsInfo); i++){
      for(int j = 1; j <= dynlen(commonComponentsInfo[i]); j++)
        commonComponentsInfoStr += commonComponentsInfo[i][j] + "|";
	
	  commonComponentsInfoStr += "&";
	
	}


    for(int i = 1; i <= dynlen(pvssMissingComponentsInfo); i++){
      for(int j = 1; j <= dynlen(pvssMissingComponentsInfo[i]); j++)
        pvssMissingComponentsInfoStr += pvssMissingComponentsInfo[i][j] + "|";
	
	  pvssMissingComponentsInfoStr += "&";
	
	}

    for(int i = 1; i <= dynlen(dbMissingComponentsInfo); i++){
      for(int j = 1; j <= dynlen(dbMissingComponentsInfo[i]); j++)
        dbMissingComponentsInfoStr += dbMissingComponentsInfo[i][j] + "|";
	
	  dbMissingComponentsInfoStr += "&";
	
	}


    ChildPanelOnCentralReturn("fwInstallation/fwInstallationSynchronizeWithDB.pnl", 
                        "Configuration DB Setup", 
                        makeDynString("$commonComponentsInfo:" + commonComponentsInfoStr, 
                                      "$pvssMissingComponentsInfo:" + pvssMissingComponentsInfoStr, 
                                      "$dbMissingComponentsInfo:" + dbMissingComponentsInfoStr), df, ds); 
                                      
                                          
  dynClear(pvssMissingComponentsInfo);
  dynClear(dbMissingComponentsInfo);
  
  fwInstallationDB_checkIntegrity(commonComponentsInfo, pvssMissingComponentsInfo, dbMissingComponentsInfo);
  if(dynlen(dbMissingComponentsInfo) > 0 || dynlen(pvssMissingComponentsInfo) > 0){
    DebugN("WARNING: fwInstallationDB_synchronizeContents() -> There is still a missmatch between the PVSS and DB information.");
    return -1;
  }
 
//  }

  return 0;
}   

int fwInstallation_checkGroupComponentsIntegrity(string groupName, bool &isOK, 
                                                 dyn_string &commonComponentNames, dyn_string &commonComponentVersions, 
                                                 dyn_string &missingComponentNamesInPvss, dyn_string &missingComponentVersionsInPvss, 
                                                 dyn_string &missingComponentNamesInDB, dyn_string &missingComponentVersionsInDB)
{
  dyn_string pvssComponentNames, pvssComponentVersions;
  dyn_string dbComponentNames, dbComponentVersions;
  dyn_string pvssDescFiles;
  dyn_bool isSubComponents;
  
  isOK = false;
  
  fwInstallationDB_getGroupComponents(groupName, dbComponentNames, dbComponentVersions);
  fwInstallation_getGroupComponents(groupName, pvssComponentNames, pvssComponentVersions, isSubComponents, pvssDescFiles);

  for(int i = 1; i <= dynlen(dbComponentNames); i++)
  {
    
    if(dynContains(pvssComponentNames, dbComponentNames[i]) == dynContains(pvssComponentVersions, dbComponentVersions[i]))
    { 
      dynAppend(commonComponentNames, dbComponentNames[i]);
      dynAppend(commonComponentVersions, dbComponentVersions[i]);
       
      dynRemove(pvssComponentNames, dynContains(pvssComponentNames, dbComponentNames[i]));
      dynRemove(pvssComponentVersions, dynContains(pvssComponentVersions, dbComponentVersions[i]));
      
      dynRemove(dbComponentNames, i);
      dynRemove(dbComponentVersions, i);
    }
  }  
  
  missingComponentNamesInPvss = dbComponentNames;
  missingComponentVersionsInPvss = dbComponentVersions;
  
  missingComponentNamesInDB = pvssComponentNames;
  missingComponentVersionsInDB = pvssComponentVersions;
  
  if(dynlen(missingComponentNamesInPvss) == 0 && dynlen(missingComponentNamesInDB) == 0)
    isOK = true;

  return 0;
  
  
}//end of function



int fwInstallation_checkProjectGroupsIntegrity(bool &isOK, dyn_string &commonGroups, dyn_string &missingGroupsInPVSS, dyn_string &missingGroupsInDB)
{

  dyn_string dbGroups;
  dyn_int dbGroupIds;
  dyn_string pvssGroups;
  
  isOK = false;
  
  if(fwInstallationDB_getProjectGroups(dbGroups, dbGroupIds) != 0) //Reads groups from DB
  {
    DebugN("ERROR: fwInstallation_checkProjectGroupsIntegrity() -> Cannot talk to the DB or project is not registered.");
    return -1;
  }
  
  pvssGroups = fwInstallation_getComponentGroups(); //Reads groups from PVSS  
  
  commonGroups = dynIntersect(dbGroups, pvssGroups);
  
  for(int i = 1; i <= dynlen(commonGroups); i++)
  {
    dynRemove(pvssGroups, dynContains(pvssGroups, commonGroups[i]));
    dynRemove(dbGroups, dynContains(dbGroups, commonGroups[i]));    
  }  
  
  missingGroupsInPVSS = dbGroups;
  missingGroupsInDB = pvssGroups;
  
  if(dynlen(missingGroupsInPVSS) == 0 && dynlen(missingGroupsInDB) == 0)
    isOK = true;

  return 0;
}

    
int fwInstallationDB_checkIntegrity(bool &isOK)
{ 
  bool isProjectGroupsOK = false;
  dyn_string commonGroups;
  dyn_string missingGroupsInPVSS;
  dyn_string missingGroupsInDB;
  
  bool isGroupOK;
  dyn_string commonComponentNames;
  dyn_string commonComponentVersions;
  dyn_string missingComponentNamesInPvss;
  dyn_string missingComponentVersionsInPvss;
  dyn_string missingComponentNamesInDB;
  dyn_string missingComponentVersionsInDB;
  int status;
  
  isOK = true;

  if(fwInstallation_checkProjectGroupsIntegrity(isProjectGroupsOK, commonGroups, missingGroupsInPVSS, missingGroupsInDB) == 0)
  {
    if(!isProjectGroupsOK)
    {
      DebugN("WARNING: fwInstallationDB_checkIntegrity() -> DB and PVSS contents missmatch for project groups");
      DebugN("List of groups defined in PVSS and registered in the DB", commonGroups);
      DebugN("List of groups defined in PVSS but not registered in the DB", missingGroupsInDB);
      DebugN("List of groups registered in the DB but not existing in PVSS", missingGroupsInPVSS);
      
      isOK = false;
      
//      return 0;
      }else{              //List of groups ok, check now the contents of each group.
        for(int i = 1; i <= dynlen(commonGroups); i++)
        {
          if(fwInstallation_checkGroupComponentsIntegrity(commonGroups[i], isGroupOK, 
                                                          commonComponentNames, commonComponentVersions, 
                                                          missingComponentNamesInPvss, missingComponentVersionsInPvss, 
                                                          missingComponentNamesInDB, missingComponentVersionsInDB) != 0)
          {
            DebugN("ERROR: fwInstallationDB_checkIntegrity() -> Could not check integrity of project group: " + commonGroup[i]);
            isOK = false;
            return -1;
            }else{
              if(!isGroupOK){
                DebugN("WARNING: fwInstallationDB_checkIntegrity() -> DB and PVSS contents missmatch for project group: " + commonGroup[i]);
                DebugN("List of group components defined in PVSS and registered in the DB", commonComponentNames, commonComponentVersions);
                DebugN("List of group components defined in PVSS but not registered in the DB", missingComponentNamesInDB, missingComponentVersionsInDB);
                DebugN("List of group registered in the DB but not existing in PVSS", missingComponentNamesInPvss, missingComponentVersionsInPvss);
      
                isOK = false;
      
//                return 0;
              }
            }
        }
        
      }
  }else{
    DebugN("ERROR: fwInstallationDB_checkIntegrity() -> Could not check integrity of project groups");
    isOK = false;
    return -1;
  }
  
  if(!isOK)
    status = FW_INSTALLATION_DB_PROJECT_MISSMATCH;
  else
    status = FW_INSTALLATION_DB_PROJECT_OK;
             
  fwInstallationDB_setProjectStatus(status);
  
  return 0;
   
}


int fwInstallationDB_unregisterProjectGroup(string groupName, string projectName = "", string computerName = "")
{
  int projectGroup_id;
  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql;

  int project_id;
          
  dynClear(aRecords);

  if(projectName == "")
    projectName = PROJ;
  
  if(computerName = "")
    computerName = getHostname();

  if(fwInstallationDB_isGroupInProjectRegistered(groupName, projectGroup_id, project_id, projectName, computerName) != 0)
  {
    DebugN("ERROR: fwInstallationDB_unregisterProjectGroup() -> Could not retrieve project group information from DB");
    return -1;
    
  }else if(projectGroup_id == -1){
    DebugN("INFO: fwInstallationDB_unregisterProjectGroup() -> Group: " + groupName + " not registered in project: " + projectName + " computer: " + computerName + ". Nothing to be done.");
    return 0;      
    }else{
        
       sql = "UPDATE fw_sys_stat_project_groups SET valid_until = SYSDATE WHERE id = " + projectGroup_id;
         
       if(g_fwInstallationSqlDebug)
         DebugN(sql);
  
       if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)) {DebugN("ERROR: fwInstallationDB_unregisterProjectGroup() -> Could not execute the following SQL: " + sql); return -1;};

    }  
  
    
  return 0;
}

int fwInstallationDB_unregisterGroupComponent(string componentName, string componentVersion, string groupName, string projectName = "", string computerName = "")
{
  int groupComponent_id;
  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql;

  int project_id;
  int group_id;
  int component_id;
  bool isRegistered;
            
  dynClear(aRecords);

  if(projectName == "")
    projectName = PROJ;
  
  if(computerName = "")
    computerName = getHostname();

  if(fwInstallationDB_isComponentInGroupRegistered(componentName, componentVersion, groupName, isRegistered, component_id, group_id) != 0)
  {
    DebugN("ERROR: fwInstallationDB_unregisterGroupComponent() -> Could not retrieve project component group information from DB");
    return -1;
    
  }else if(!isRegistered){
    DebugN("INFO: fwInstallationDB_unregisterGroupComponent() -> Component: " + componentName + " v." + componentVersion + " Group: " + groupName + " not registered in project: " + projectName + " computer: " + computerName + ". Nothing to be done.");
    return 0;      
  }else{
     
     sql = "UPDATE fw_sys_stat_comp_in_groups SET valid_until = SYSDATE WHERE fw_component_id = " + component_id + " AND group_id = " + group_id + " AND valid_until IS NULL";
         
     if(g_fwInstallationSqlDebug)
       DebugN(sql);
     
     if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)) {DebugN("ERROR: fwInstallationDB_unregisterGroupComponent() -> Could not execute the following SQL: " + sql); return -1;};
   
   }      
  return 0;
}


int fwInstallationDB_unregisterInstallationPath(string installationPath, string projectName = "", string computerName = "")
{
  int groupComponent_id;
  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql;
  bool isValid;
  
  int installation_path_id;
  int project_id;
          
  dynClear(aRecords);

  if(projectName == "")
    projectName = PROJ;
  
  if(computerName = "")
    computerName = getHostname();

  if(fwInstallationDB_isInstallationPathRegistered(installationPath, installation_path_id, isValid, project_id, projectName = "", computerName = "") != 0)
  {
    DebugN("ERROR: fwInstallationDB_unregisterInstallationPath() -> Could not retrieve project installation path information from DB");
    return -1;
    
  }else if(installation_path_id == -1 || !isValid){
    DebugN("INFO: fwInstallationDB_unregisterInstallationPath() -> Path: " + installationPath + " not registered in project: " + projectName + " computer: " + computerName + ". Nothing to be done.");
    return 0;      
  }else{
         
     sql = "UPDATE fw_sys_stat_inst_path SET valid_until = SYSDATE WHERE id = " + installation_path_id;
         
     if(g_fwInstallationSqlDebug)
       DebugN(sql);
     
     if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)) {DebugN("ERROR: fwInstallationDB_unregisterInstallationPath() -> Could not execute the following SQL: " + sql); return -1;};

   }      
  return 0;
}


int fwInstallationDB_getInstallationPaths(dyn_string &dbInstallationPaths, dyn_string &dbInstallationPathsIds, string projectName = "", string computerName = "")
{
  dyn_string exceptionInfo;
  string sql;
  dyn_dyn_mixed aRecords;
  int project_id;
  
  dynClear(exceptionInfo);
  dynClear(aRecords);  

  if(projectName == "")
    projectName = PROJ;
  
  if(computerName = "")
    computerName = getHostname(); 
  
  if(fwInstallationDB_isProjectRegistered(project_id, projectName, computerName) != 0)
  {
    DebugN("ERROR: fwInstallationDB_getInstallationPaths() - > Cannot get project information from DB.");
    return -1;  
  }else if(project_id == -1)
  {  
    DebugN("ERROR: ffwInstallationDB_getInstallationPaths() - > Project: " + projectName + " in computer: " + computerName + " not registered in DB.");
    return 0;        
  }else{
 
    sql = "SELECT id, path FROM fw_sys_stat_inst_path WHERE project_id = " + project_id + " AND valid_until IS NULL";
    if(g_fwInstallationSqlDebug)
      DebugN(sql);

   if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
   {
     DebugN("ERROR: fwInstallation_getInstallationPaths() -> Could not execute the following SQL query: " + sql);
     return -1;
   }  

    for(int i = 1; i <= dynlen(aRecords); i++) {            
      dynAppend(dbInstallationPathsIds, aRecords[i][1]);
      dynAppend(dbInstallationPaths, aRecords[i][2]);
    }
  }

  return 0;    
  
}
/*
int fwInstallationDB_getComponentGroups(dyn_string &groups)
{
  dyn_string exceptionInfo;
  string sql;
  dyn_dyn_mixed aRecords;
  
  dynClear(exceptionInfo);
  dynClear(aRecords);  
    
      
  sql = "SELECT name FROM fw_sys_stat_group_of_comp";
       
  if(g_fwInstallationSqlDebug)
     DebugN(sql);

  if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
  {
    DebugN("ERROR: fwInstallation_getGroupComponents() -> Could not execute the following SQL query: " + sql);
    return -1;
  }  

  for(int i = 1; i <= dynlen(aRecords); i++) {            
    dynAppend(groups, aRecords[i][1]);
  }  
  return 0;  
}
*/

int fwInstallationDB_getGroupComponents(string groupName, dyn_string &componentNames, dyn_string &componentVersions)
{
  int group_id;
  dyn_string exceptionInfo;
  string sql;
  dyn_dyn_mixed aRecords;
  
  dynClear(exceptionInfo);
  dynClear(aRecords);  
  dynClear(componentNames);
  dynClear(componentVersions);
    
  if(fwInstallationDB_isGroupRegistered(groupName, group_id) == 0){
    if(group_id == -1){
      DebugN("ERROR: fwInstallationDB_getProjectGroups() - > Group: "+ groupName + " is not registered in DB.");
      return -1;      
    }else{    
      
      sql = "SELECT component_name, component_version FROM fw_sys_stat_component WHERE id IN (SELECT fw_component_id FROM fw_sys_stat_comp_in_groups WHERE group_id = "+ group_id +" AND valid_until IS NULL)";;
       
      if(g_fwInstallationSqlDebug)
        DebugN(sql);

      if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
      {
        DebugN("ERROR: fwInstallation_getGroupComponents() -> Could not execute the following SQL query: " + sql);
        return -1;
      }  

      for(int i = 1; i <= dynlen(aRecords); i++) {            
        dynAppend(componentNames, aRecords[i][1]);
        dynAppend(componentVersions, aRecords[i][2]);
       }
    }  
  }else{
    DebugN("ERROR: fwInstallationDB_getGroupComponents() - > Cannot access the DB.");
    return -1;
  }
  
  return 0;
  
}



int fwInstallationDB_getProjectGroups(dyn_string &groupNames, dyn_int &groupIds, bool onlyActive = true, string projectName = "", string computerName = "")
{
  int project_id;
  dyn_string exceptionInfo;
  string sql;
  dyn_dyn_mixed aRecords;
  
  dynClear(exceptionInfo);
  dynClear(aRecords);  
  dynClear(groupNames);
  dynClear(groupIds);
  
  if(projectName == "")
    projectName = PROJ;
  
  if(computerName == "")
    computerName = getHostname();
  
  if(fwInstallationDB_isProjectRegistered(project_id, projectName, computerName) == 0){
    if(project_id == -1){
      DebugN("ERROR: fwInstalltionDB_getProjectGroups() - > Project: "+ projectName + " in computer: " + " Computer: " + computerName + " is not registered in DB.");
      return -1;      
    }else{

      if(onlyActive)
       sql = "SELECT g.name, pg.group_id FROM fw_sys_stat_group_of_comp g, fw_sys_stat_project_groups pg " + 
              "WHERE g.id = pg.group_id AND pg.project_id = " + project_id + " AND valid_until IS NULL";
      else  
       sql = "SELECT g.name, pg.group_id FROM fw_sys_stat_group_of_comp g, fw_sys_stat_project_groups pg " + 
              "WHERE g.id = pg.group_id AND pg.project_id = " + project_id;
       
       if(g_fwInstallationSqlDebug)
         DebugN(sql);
  
       if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
       {
         DebugN("ERROR: fwInstallation_projectGroups() -> Could not execute the following SQL query: " + sql);
         return -1;
       }  

       for(int i = 1; i <= dynlen(aRecords); i++) {   
         
         dynAppend(groupNames, aRecords[i][1]);
         dynAppend(groupIds, aRecords[i][2]);
       }
    }  
  }else{
    DebugN("ERROR: fwInstalltionDB_getProjectGroups() - > Cannot access the DB.");
    return -1;
  }
  
  return 0;
}


int fwInstallationDB_getPvssProjects(dyn_string &projectNames, string computerName = "", bool onlyActive = true)
{
  dyn_string exceptionInfo;
  string sql;
  dyn_dyn_mixed aRecords;
  
  dynClear(exceptionInfo);
  dynClear(aRecords);  
  dynClear(projectNames);  
  
  if(computerName == "")
    computerName = getHostname();
  
  if(computerName == "ALL" && onlyActive)
     sql = "SELECT project_name FROM fw_sys_stat_pvss_project WHERE valid_until IS NULL";
  else if(computerName == "ALL" && !onlyActive) 
     sql = "SELECT project_name FROM fw_sys_stat_pvss_project";
  else if(computerName != "ALL" && onlyActive)
     sql = "SELECT project_name FROM fw_sys_stat_pvss_project " +  
           "WHERE valid_until IS NULL " + 
           "AND computer_id = (SELECT id FROM fw_sys_stat_computer WHERE hostname = \'" + computerName + "\')";
    else
     sql = "SELECT project_name FROM fw_sys_stat_pvss_project " +  
           "WHERE computer_id = (SELECT id FROM fw_sys_stat_computer WHERE hostname = \'" + computerName + "\')";
           
  if(g_fwInstallationSqlDebug)
      DebugN(sql);
  
  if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
  {
      DebugN("ERROR: fwInstallation_getPvssProjects() -> Could not execute the following SQL query: " + sql);
      return -1;
  }  

  for(int i = 1; i <= dynlen(aRecords); i++) {          
     dynAppend(projectNames, aRecords[i][1]);
  }
  
  return 0;
}

int fwInstallationDB_getComponentGroups(dyn_string &groupNames)
{
  dyn_string exceptionInfo;
  string sql;
  dyn_dyn_mixed aRecords;
  
  dynClear(exceptionInfo);
  dynClear(aRecords);  
  dynClear(groupNames);  
  

  sql = "SELECT name FROM fw_sys_stat_group_of_comp";
           
  if(g_fwInstallationSqlDebug)
      DebugN(sql);
  
  if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
  {
      DebugN("ERROR: fwInstallation_getComponentGroups() -> Could not execute the following SQL query: " + sql);
      return -1;
  }  

  for(int i = 1; i <= dynlen(aRecords); i++) {          
     dynAppend(groupNames, aRecords[i][1]);
  }
  
  return 0;
}

int fwInstallationDB_registerComponent(string componentName, string componentVersion, int isSubComponent, string defaultPath = "", int isOfficial = -1)
{
  string sql;
  int component_id;

  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;

  string defaultPathStr;
  string isOfficialStr;
    
  dynClear(exceptionInfo);

  //Check if component is already registered:
  if(fwInstallationDB_isComponentRegistered(componentName, componentVersion, component_id) == 0 && component_id != -1){
    if(g_fwInstallationVerbose)
      DebugN("INFO: FW Component already registered in the DB. Found with id = " + component_id);

    return 0;  
  }
  
  if(defaultPath != "")
    defaultPathStr = "\'" + defaultPath + "\'";
  else
    defaultPathStr = "NULL";
  
  
  if(isOfficial == -1)
    isOfficialStr = "NULL";
  else
    isOfficialStr = isOfficial;
    
  sql = "INSERT INTO fw_sys_stat_component VALUES ((fw_sys_stat_component_sq.NEXTVAL), \'" + componentName + "\', \'" + componentVersion 
        + "\', " + isSubComponent + ", " + defaultPathStr + ", " + isOfficialStr + ")";
         
  if(g_fwInstallationSqlDebug)
    DebugN(sql);
  
  if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)) {DebugN("ERROR: fwInstallationDB_registerProject() -> Could not execute the following SQL: " + sql); return -1;};

  return 0;  
}


//////////////////////
int fwInstallationDB_isComponentRegistered(string componentName, string componentVersion, int &id)
{

  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql;

  dynClear(aRecords);
    
  sql = "SELECT id FROM fw_sys_stat_component WHERE component_name = \'" + componentName +
          "\' AND component_version = \'" + componentVersion + "\'";
  
  if(g_fwInstallationSqlDebug)
    DebugN(sql);
  
  if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
  {
    DebugN("ERROR: fwInstallation_isComponentRegistered() -> Could not execute the following SQL query: " + sql);
    return -1;
  }  

  if(dynlen(aRecords) > 0) {   
    if(g_fwInstallationVerbose)
      DebugN("INFO: fwInstallationDB_isComponentRegistered() -> FW Component already registered in the DB: ", aRecords);
    
    id = aRecords[1][1];
  }
  else{
    id = -1;
  }

  return 0;
}



int fwInstallationDB_isProjectRegistered(int &project_id, string projectName = "", string computerName = "")
{

  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql;
  int pc_id;

  if(projectName == "")
    projectName = PROJ;
  
  if(computerName == "")
    computerName = getHostname();
  
      
  if(fwInstallationDB_isPCRegistered(pc_id, computerName) == 0 && pc_id != -1)
  {
  
    sql = "SELECT id FROM fw_sys_stat_pvss_project WHERE project_name = \'" + projectName +
          "\' AND computer_id = \'" + pc_id + "\' AND valid_until IS NULL";
  
    dynClear(aRecords);
      
    if(g_fwInstallationSqlDebug)
      DebugN(sql);
  
   if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
   {
     DebugN("ERROR: fwInstallation_isProjectRegistered() -> Could not execute the following SQL query: " + sql);
     return -1;
   }  

    if(dynlen(aRecords) > 0) {   
      if(g_fwInstallationVerbose)
        DebugN("INFO: fwInstallationDB_isProjectRegistered() -> Project "+ projectName + " already registered in the DB for computer: " + computerName +" with id:" +  aRecords);
        
      project_id = aRecords[1][1];
    }
    else{
      project_id = -1;
    }
  }  
  return 0;
}



int fwInstallationDB_registerPC(string host = "", string ip = "", string mac = "", string ip2 = "", string mac2= "", string bmc_ip = "", string bmc_ip2 = "")
{
  int pcId = -1;
  string sql;
//  string host;
//  string ip;
    
  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  
  dynClear(exceptionInfo);
  
  
  //override host since the previous function sets localhost
  if(host == ""){
    host = getHostname();    
    ip = getHostByName(host);
  }else if(ip == "")
  {
    ip = getHostByName(host); 
  }  
 
  //Check if already exists:
  if(fwInstallationDB_isPCRegistered(pcId, host) == 0 && pcId == -1){

    if(g_fwInstallationVerbose)
      DebugN("INFO: fwInstallationDB_registerPC() -> PC: " + host + " not yet registered in the DB. Proceeding with registration now...");  

    sql = "INSERT INTO fw_sys_stat_computer VALUES ((fw_sys_stat_computer_sq.NEXTVAL), \'" + host + "\', \'" + ip+ "\', \'" + mac + "\', \'" + ip2+ "\', \'" + mac2 + "\', \'" + bmc_ip + "\', \'" + bmc_ip2+ "\')";
         
    if(g_fwInstallationSqlDebug)
      DebugN(sql);
  
    if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)) {DebugN("ERROR: fwInstallationDB_registerProject() -> Could not execute the following SQL: " + sql); return -1;};

  }

  return 0;  
}



int fwInstallationDB_isPCRegistered(int &id, string host = "")
{
//  string host = getHostname();    
  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  
  if(host == "")
    host = getHostname();
  
  string sql = "SELECT id FROM fw_sys_stat_computer WHERE hostname = \'" + host +"\'" ;
  
  dynClear(aRecords);
  id = -1;
       
  if(g_fwInstallationSqlDebug)
    DebugN(sql);
  
  if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
  {
    DebugN("ERROR: fwInstallation_isPCRegistered() -> Could not execute the following SQL query: " + sql);
    return -1;
  }  

  if(dynlen(aRecords) > 0) {   
    if(g_fwInstallationVerbose)
      DebugN("INFO: fwInstallationDB_isPCRegistered() -> PC already registered in the DB: ", aRecords);
      
    id = aRecords[1][1];
  }
  else{
    if(g_fwInstallationVerbose)
      DebugN("INFO: fwInstallationDB_isPCRegistered() -> PC not yet registered in the DB");
    
    id = -1;
  }
  
  return 0;
}


int fwInstallationDB_registerSystem(string systemName = "", int redundancyNumber = -1)
{
//  int pcIdx = -1;
  string sql;
  string host1;
  string host2;
  
  int systemNumber;
  int systemId;
  int distPort;
  int redu_system_id;
  int redu_system;
  string redu_system_id_str;
        
  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  
  dynClear(exceptionInfo);

  if(systemName == "")
    systemName = getSystemName();  
  
  systemNumber = getSystemId(systemName);
  
  if(redundancyNumber <= 0)
  {
    dpGet(systemName + "_Connections.Data.HostNames", host1,
          systemName + "_Connections_2.Data.HostNames", host2);
    if(host1 == getHostname()){
      redundancyNumber = 1;
      redu_system = 2;
    }else{
        redu_system = 1;
        redundancyNumber = 2;    
    }
  } 
  
  //Get distribution port number:
  distPort = fwInstallation_getDistPort();

  //Check if already exists:
  if(fwInstallationDB_isSystemRegistered(systemId, systemName, redundancyNumber) == 0 && systemId == -1){

    if(g_fwInstallationVerbose)
      DebugN("INFO: fwInstallationDB_registerSystem() -> Registering system: " + systemName + " systemNumber = " + systemNumber + "" + " redundancy number = " + redundancyNumber + ".");  

    
    fwInstallationDB_isSystemRegistered(redu_system_id, systemName, redu_system);
    if(redu_system_id == -1)
      redu_system_id_str = "NULL";
    else 
      redu_system_id_str = redu_system_id;
    
    sql = "INSERT INTO fw_sys_stat_pvss_system VALUES ((fw_sys_stat_pvss_system_sq.NEXTVAL), \'" + systemName + "\', " + systemNumber + ", " + redundancyNumber + +", "+ dataPort() + ", " + eventPort() + ", " + distPort + ", NULL, " + redu_system_id_str + ")";
         
    if(g_fwInstallationSqlDebug)
      DebugN(sql);
  
    if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)) {DebugN("ERROR: fwInstallationDB_registerProject() -> Could not execute the following SQL: " + sql); return -1;};

  }

  return 0;  
}

//////////

int fwInstallationDB_isSystemRegistered(int &id, string systemName = "", int redundancyNumber = -1)
{
  string host1;
  string host2;
  
  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  
  if(systemName == "")
    systemName = getSystemName();

  if(redundancyNumber <= 0)
  {
    dpGet(systemName + "_Connections.Data.HostNames", host1,
          systemName + "_Connections_2.Data.HostNames", host2);
    if(host1 == getHostname()){
      redundancyNumber = 1;
    }else{
        redundancyNumber = 2;    
    }
  } 
  
  string sql = "SELECT id FROM fw_sys_stat_pvss_system WHERE system_name = \'" + systemName + "\' AND redundancy_number = " + redundancyNumber ;
  
  dynClear(aRecords);
  id = -1;
       
  if(g_fwInstallationSqlDebug)
    DebugN(sql);

  if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
  {
    DebugN("ERROR: fwInstallation_isSystemRegistered() -> Could not execute the following SQL query: " + sql);
    return -1;
  }  

  if(dynlen(aRecords) > 0) {   
    if(g_fwInstallationVerbose)
      DebugN("INFO: fwInstallationDB_isSystemRegistered() -> PVSS system already registered in the DB: ", aRecords);
      
    id = aRecords[1][1];
  }
  else{
    if(g_fwInstallationVerbose)
      DebugN("INFO: fwInstallationDB_isSystemRegistered() -> PVSS system not yet registered in the DB");
    
    id = -1;
  }
  
  return 0;
}




////////////////


int fwInstallationDB_isGroupRegistered(string group, int &id)
{
  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  
  strreplace(group, ".", "_");
  
  string sql = "SELECT id FROM fw_sys_stat_group_of_comp WHERE name = \'" + group +"\'" ;
  
  dynClear(aRecords);
  id = -1;
      
  if(g_fwInstallationSqlDebug)
    DebugN(sql);

  if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
  {
    DebugN("ERROR: fwInstallation_isGroupRegistered() -> Could not execute the following SQL query: " + sql);
    return -1;
  }  

  if(dynlen(aRecords) > 0) {   
    if(g_fwInstallationVerbose)
      DebugN("INFO: fwInstallationDB_isGroupRegistered() -> Group " + group + " already registered in the DB with id: ", aRecords);
      
    id = aRecords[1][1];
  }
  else{
    if(g_fwInstallationVerbose)
      DebugN("INFO: fwInstallationDB_isGroupRegistered() -> Group " + group + " not yet registered in the DB");
    
    id = -1;
  }
  
  return 0;
}


int fwInstallationDB_registerGroup(string group)
{
  string sql;
    
  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  int id;
  
  dynClear(exceptionInfo);
  
  strreplace(group, ".", "_");

  //Check if already exists:
  if(fwInstallationDB_isGroupRegistered(group, id) == 0 && id == -1){

    if(g_fwInstallationVerbose)
      DebugN("INFO: fwInstallationDB_registerGroup() -> Group: " + group + " not yet registered in the DB. Proceeding with registration now...");  

    sql = "INSERT INTO fw_sys_stat_group_of_comp VALUES ((fw_sys_stat_group_of_comp_sq.NEXTVAL), \'" + group + "\')";
         
    if(g_fwInstallationSqlDebug)
      DebugN(sql);

    if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)) {DebugN("ERROR: fwInstallationDB_registerProject() -> Could not execute the following SQL: " + sql); return -1;}; 
  
  }

  return 0;  
}


int fwInstallationDB_isComponentInGroupRegistered(string componentName, string componentVersion, string groupName, bool &isRegistered, int &component_id, int &group_id)
{

  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql;

  dynClear(aRecords);
  isRegistered = false;
  
  if(fwInstallationDB_isComponentRegistered(componentName, componentVersion, component_id) != 0)
  { 
    Debug("ERROR: fwInstallationDB_isComponentInGroupRegistered() -> Cannot talk to DB. Check DB connection parameters");
    return -1;
  }else if(component_id != -1){
    if(fwInstallationDB_isGroupRegistered(groupName, group_id) == 0 && group_id != -1)
    {
 
      sql = "SELECT fw_component_id FROM fw_sys_stat_comp_in_groups WHERE fw_component_id = " + component_id + " AND group_id = " + group_id + " AND valid_until IS NULL";      
      if(g_fwInstallationSqlDebug)
        DebugN(sql);

     if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
     {
       DebugN("ERROR: fwInstallation_isComponentInGroupRegistered() -> Could not execute the following SQL query: " + sql);
       return -1;
      }  

      if(dynlen(aRecords) > 0) {   
        if(g_fwInstallationVerbose)
          DebugN("INFO: fwInstallationDB_isComponentInGroupRegistered() -> Component "+ componentName + " v." + componentVersion + " is alaready registered in group " + groupName);

        isRegistered = true;

      }
    }else{    
      DebugN("ERROR: fwInstallationDB_isComponentInGroupRegistered() -> Group "+ groupName + " not registered in group DB");
      isRegistered = false;
      return 0;
    }
  }else{
    DebugN("WARNING: fwInstallationDB_isComponentInGroupRegistered() -> Component "+ componentName + " v." + componentVersion + " not registered in group DB");
    isRegistered = false;
    return 0;
  }
    
  return 0;
}


int fwInstallationDB_registerComponentInGroup(string componentName, string componentVersion, int isSubComponent, string groupName)
{

  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql;

  int group_id;
  int component_id;
  bool isRegistered = false;
          
  dynClear(aRecords);
  
  if(fwInstallationDB_isComponentInGroupRegistered(componentName, componentVersion, groupName, isRegistered, component_id, group_id) == 0)
  {
    if(isRegistered)
    {
      if(g_fwInstallationVerbose)
        DebugN("INFO: fwInstallationDB_registerComponentInGroup() -> Component: " + componentName + " v." + componentVersion + " already registered in group " + groupName + ". Nothing to be done...");      
        
      return 0;
    }
      
    if(g_fwInstallationVerbose){
      DebugN("INFO: fwInstallationDB_registerComponentInGroup() -> Component: " + componentName + " v." + componentVersion + " not yet registered in group " + groupName + ". Registering now...");
    }
    
    if(fwInstallationDB_isComponentRegistered(componentName, componentVersion, component_id) == 0 && component_id == -1)
    {
      if(g_fwInstallationVerbose){
        DebugN("INFO: fwInstallationDB_registerComponentInGroup() -> Component: " + componentName + " v." + componentVersion + " not yet registered in DB. Registering now...");
      }
      if(fwInstallationDB_registerComponent(componentName, componentVersion, isSubComponent) != 0){
        DebugN("ERROR: fwInstallationDB_registerComponentInGroup() -> Could not register component " + componentName + " v. " + componentVersion + " in DB.");
        return -1;
      }
      else 
        fwInstallationDB_isComponentRegistered(componentName, componentVersion, component_id);
        
    }
      
    if(fwInstallationDB_isGroupRegistered(groupName, group_id) == 0 && group_id == -1)
    {
      if(g_fwInstallationVerbose){
        DebugN("INFO: fwInstallationDB_registerComponentInGroup() -> Group: " + groupName);
      }

      if(fwInstallationDB_registerGroup(groupName) != 0){
        DebugN("ERROR: fwInstallationDB_registerComponentInGroup() -> Could not register group " + groupName + " in DB.");
        return -1;
      }
      else
        fwInstallationDB_isGroupRegistered(groupName, group_id);
    } 
    
    if(component_id != -1 && group_id != -1)
    {

      sql = "INSERT INTO fw_sys_stat_comp_in_groups VALUES(" + group_id + ", "+ component_id + ", SYSDATE, NULL)";      
      if(g_fwInstallationSqlDebug)
        DebugN(sql);
  
      if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)) {DebugN("ERROR: fwInstallationDB_registerProject() -> Could not execute the following SQL: " + sql); return -1;};

    }
  }
  return 0;
}


int fwInstallationDB_registerComponentInDefaultGroup(string componentName, string componentVersion, int isSubComponent)
{
  string groupName = componentName + "_" + componentVersion;

  return fwInstallationDB_registerComponentInGroup(componentName, componentVersion, isSubComponent, groupName);
            
}

/////////////////////
int fwInstallationDB_isGroupInProjectRegistered(string groupName, int &id, int &group_id, int &project_id, string projectName = "", string computerName = "")
{

  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql;

  int computer_id;
          
  dynClear(aRecords);

  if(projectName == "")
    projectName = PROJ;
  
  if(computerName = "")
    computerName = getHostname();

  //Check that the computer is properly registered in the DB    
  if(fwInstallationDB_isPCRegistered(computer_id, computerName) == 0)
  {
    if(computer_id == -1){
      if(g_fwInstallationVerbose){
          DebugN("INFO: fwInstallationDB_isGroupInProjectRegistered() -> Registering computer now...");
      }
      
      fwInstallationDB_registerPC(computerName);
      fwInstallationDB_isPCRegistered(computer_id);
    }
  }else{
    DebugN("ERROR: fwInstallationDB_isGroupInProjectRegistered() -> Could not retrieve the PC info from the DB. Action aborted...");
    return -1;
  }
    
  //Check that the project is properly registered in the DB  
  if(fwInstallationDB_isProjectRegistered(project_id) == 0)
  {
    if(project_id == -1){
      if(g_fwInstallationVerbose){
          DebugN("INFO: fwInstallationDB_isGroupInProjectRegistered() -> Registering project now...");
      }
      fwInstallationDB_registerProject();
      fwInstallationDB_isProjectRegistered(project_id);
    }
  }else{
    DebugN("ERROR: fwInstallationDB_isGroupInProjectRegistered() -> Could not retrieve the project info from the DB. Action aborted...");
    return -1;
  }

  //Check that the project is group is properly registered in the DB  
  if(fwInstallationDB_isGroupRegistered(groupName, group_id) == 0)
  {
    if(group_id == -1){
      if(g_fwInstallationVerbose){
        DebugN("INFO: fwInstallationDB_isGroupInProjectRegistered() -> Registering group now...");
      }
      fwInstallationDB_registerGroup(groupName);
      fwInstallationDB_isGroupRegistered(groupName, group_id);
    }
  }else{
    DebugN("ERROR: fwInstallationDB_isGroupInProjectRegistered() -> Could not retrieve the group info from the DB. Action aborted...");
    return -1;
  }

  
  if(computer_id != -1 && project_id != -1 && group_id != -1)
  {
    sql = "SELECT id FROM fw_sys_stat_project_groups WHERE project_id = " + project_id + " AND group_id = " + group_id + " AND valid_until IS NULL";      
    if(g_fwInstallationSqlDebug)
      DebugN(sql);

   if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
   {
     DebugN("ERROR: fwInstallation_isGroupInProjectRegistered() -> Could not execute the following SQL query: " + sql);
     return -1;
   }  

    if(dynlen(aRecords) > 0) {   
      if(g_fwInstallationVerbose){
        DebugN("INFO: fwInstallationDB_isGroupInProjectRegistered() -> Group "+ groupName + " already registered in project" + projectName + " in computer " + computerName);
      }

      id = aRecords[1][1];        
      }else 
        id = -1;
    }
    
  return 0;
}



int fwInstallationDB_registerGroupInProject(string groupName, int overwriteFiles = 0, int forceRequired = 1, int isSilent = 0, string scheduledInstDate = "", string projectName = "", string computerName = "")
{

  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql;
  string requestedBy = getUserName();      //To handle FW usernames
  string requestDate = getCurrentTime();
  int project_group_id;
  int group_id;
  int project_id;
             
  dynClear(aRecords);

  if(projectName == "")
    projectName = PROJ;

  if(computerName == "")
    computerName = getHostname();

  if(scheduledInstDate == "")
    scheduledInstDate = getCurrentTime();
    
  if(fwInstallationDB_isGroupInProjectRegistered(groupName, project_group_id, group_id, project_id, projectName, computerName) == 0 && group_id != -1 && project_id != -1)
  {
    if( project_group_id == -1){
 
      //sql = "INSERT INTO fw_sys_stat_project_groups VALUES((fw_sys_stat_project_groups_sq.NEXTVAL), " + group_id + ", "+ project_id + ", \'" + requestedBy + "\', to_date(" + requestDate + "), "+ overwriteFiles + ", " + forceRequired + ", " + isSilent + ", to_date(" + scheduledInstDate + "))";      
      sql = "INSERT INTO fw_sys_stat_project_groups VALUES((fw_sys_stat_project_groups_sq.NEXTVAL), " + group_id + ", "+ project_id + ", \'" + requestedBy + "\', SYSDATE, "+ overwriteFiles + ", " + forceRequired + ", " + isSilent + ", NULL, NULL)";      

      if(g_fwInstallationSqlDebug)
      DebugN(sql);
  
      if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)) {DebugN("ERROR: fwInstallationDB_registerProject() -> Could not execute the following SQL: " + sql); return -1;};
  
    }else{
      if(g_fwInstallationVerbose)
        DebugN("INFO: fwInstallationDB_registerGroupInProject() -> Group is already registered for this project");
      
      return 0;        
       
    }
  }
  else{
    DebugN("ERROR: fwInstallationDB_registerGroupInProject() -> Cannot talk to DB or either group or project are not properly registered in the DB");
    return -1;
  }
  return 0;
}

/////////////////////////////////
int fwInstallationDB_isInstallationPathRegistered(string installationPath, int &installation_path_id, bool &isValid, int &project_id, string projectName = "", string computerName = "")
{

  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql;

  int computer_id;
  
  installation_path_id = -1;        
  dynClear(aRecords);

  if(projectName == "")
    projectName = PROJ;
  
  if(computerName = "")
    computerName = getHostname();

  //Check that the computer is properly registered in the DB    
  if(fwInstallationDB_isPCRegistered(computer_id, computerName) == 0)
  {
    if(computer_id == -1){
      if(g_fwInstallationVerbose){
          DebugN("INFO: fwInstallationDB_isInstallationPathRegistered() -> Registering computer now...");
      }
      
      fwInstallationDB_registerPC(computerName);
      fwInstallationDB_isPCRegistered(computer_id);
    }
  }else{
    DebugN("ERROR: fwInstallationDB_isInstallationPathRegistered() -> Could not retrieve the PC info from the DB. Action aborted...");
    return -1;
  }
    
  //Check that the project is properly registered in the DB  
  if(fwInstallationDB_isProjectRegistered(project_id) == 0)
  {
    if(project_id == -1){
      if(g_fwInstallationVerbose){
          DebugN("INFO: fwInstallationDB_isInstallationPathRegistered() -> Registering project now...");
      }
      fwInstallationDB_registerProject();
      fwInstallationDB_isProjectRegistered(project_id);
    }
  }else{
    DebugN("ERROR: fwInstallationDB_isInstallationPathRegistered() -> Could not retrieve the project info from the DB. Action aborted...");
    return -1;
  }

  
  if(computer_id != -1 && project_id != -1)
  {

    sql = "SELECT id, valid_until FROM fw_sys_stat_inst_path WHERE path = \'" + installationPath + "\' AND project_id = " + project_id;      
    if(g_fwInstallationSqlDebug)
      DebugN(sql);

    if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
    {
      DebugN("ERROR: fwInstallation_isInstallationPathRegistered() -> Could not execute the following SQL query: " + sql);
      return -1;
    }  

    if(dynlen(aRecords) > 0) {   
      if(g_fwInstallationVerbose){
        DebugN("INFO: fwInstallationDB_isInstallationPathRegistered() -> Installation path: "+ installationPath + " already registered in project" + projectName + " for computer " + computerName);
      }

      installation_path_id = aRecords[1][1];        
      
      if(aRecords[1][2] != "")
        isValid = true;
      else
        isValid = false;
      
      }else 
        installation_path_id = -1;
    }
    
  return 0;
}

int fwInstallationDB_registerInstallationPath(string installationPath, int isDefault, string projectName = "", string computerName = "")
{

  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql;
  string requestedBy = getUserName();      //To handle FW usernames
  string requestDate = getCurrentTime();
  int project_group_id;
  int group_id;
  int project_id;
  int installation_path_id;
  bool isValid;
             
  dynClear(aRecords);

  if(projectName == "")
    projectName = PROJ;

  if(computerName == "")
    computerName = getHostname();
  
  if(fwInstallationDB_isInstallationPathRegistered(installationPath, installation_path_id, isValid, project_id, projectName, computerName) == 0 && project_id != -1)
  {
    if(installation_path_id == -1){

      if(rdbBeginTransaction(gFwInstallationDBConn)){
        DebugN("ERROR: fwInstallationDB_registerInstallationPath() -> Could notstart DB transaction");
        return -1;
      }
      
      sql = "INSERT INTO fw_sys_stat_inst_path VALUES((fw_sys_stat_inst_path_sq.NEXTVAL), " + project_id + ", \'" + installationPath + "\', SYSDATE, NULL)";      

      if(g_fwInstallationSqlDebug)
      DebugN(sql);

      if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)){
        DebugN("ERROR: fwInstallationDB_registerInstallationPath() -> Could not execute the following SQL: " + sql);
        DebugN("INFO: Rolling transaction back");
        if(rdbRollbackTransaction(gFwInstallationDBConn))
          DebugN("ERROR: fwInstallationDB_registerInstallationPath() -> Could not roll back DB transaction.");
        
        return -1;
      }
  
      //Everything OK -> Check the id that was assigned and modify the project table to set default path if required     
      if(isDefault == 1 && fwInstallationDB_isInstallationPathRegistered(installationPath, installation_path_id, project_id, projectName, computerName) == 0 && installation_path_id != -1)
      {
        if(g_fwInstallationVerbose){
          DebugN("INFO: fwInstallationDB_registerInstallationPath() -> Updating project table to set default path");
        }

        sql = "UPDATE fw_sys_stat_pvss_project SET default_inst_path_id = " + installation_path_id + " WHERE id = " + project_id;        
        
        if(g_fwInstallationSqlDebug)
          DebugN(sql);

        if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)){
          DebugN("ERROR: fwInstallationDB_registerInstallationPath() -> Could not execute the following SQL: " + sql);
          DebugN("INFO: Rolling transaction back");
          if(rdbRollbackTransaction(gFwInstallationDBConn))
            DebugN("ERROR: fwInstallationDB_registerInstallationPath() -> Could not roll back DB transaction.");
        
          return -1;
        }else if(rdbCommitTransaction(gFwInstallationDBConn))
        {
          DebugN("ERROR: fwInstallationDB_registerInstallationPath() -> Could not commit DB transaction.");
          return -1;
        }
        
        }else if(rdbCommitTransaction(gFwInstallationDBConn))
        {
          DebugN("ERROR: fwInstallationDB_registerInstallationPath() -> Could not commit DB transaction.");
          return -1;
        }
      
    }else if(!isValid)
    {

      if(rdbBeginTransaction(gFwInstallationDBConn)){
        DebugN("ERROR: fwInstallationDB_registerInstallationPath() -> Could notstart DB transaction");
        return -1;
      }
      
      if(g_fwInstallationVerbose)
        DebugN("INFO: fwInstallationDB_registerInstallationPath() -> Installation path already registered in DB. Making it valid now...");
      
      sql = "UPDATE fw_sys_stat_inst_path SET valid_until = NULL WHERE ID = " + installation_path_id;      

      if(g_fwInstallationSqlDebug)
      DebugN(sql);

      
      if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)){
        DebugN("ERROR: fwInstallationDB_registerInstallationPath() -> Could not execute the following SQL: " + sql);
        DebugN("INFO: Rolling transaction back");
        if(rdbRollbackTransaction(gFwInstallationDBConn))
          DebugN("ERROR: fwInstallationDB_registerInstallationPath() -> Could not roll back DB transaction.");
        
        return -1;
      }

      //Everything OK -> Check the id that was assigned and modify the project table to set default path if required     
      if(isDefault == 1)
      {
        if(g_fwInstallationVerbose)
          DebugN("INFO: fwInstallationDB_registerInstallationPath() -> Updating project table to set default path");
        

        sql = "UPDATE fw_sys_stat_pvss_project SET default_inst_path_id = " + installation_path_id + " WHERE id = " + project_id;        
        
        if(g_fwInstallationSqlDebug)
          DebugN(sql);

        if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)){
          DebugN("ERROR: fwInstallationDB_registerInstallationPath() -> Could not execute the following SQL: " + sql);
          DebugN("INFO: Rolling transaction back");
          if(rdbRollbackTransaction(gFwInstallationDBConn))
            DebugN("ERROR: fwInstallationDB_registerInstallationPath() -> Could not roll back DB transaction.");
        
          return -1;
        }else if(rdbCommitTransaction(gFwInstallationDBConn))
        {
          DebugN("ERROR: fwInstallationDB_registerInstallationPath() -> Could not commit DB transaction.");
          return -1;
        }
      }
      else if(rdbCommitTransaction(gFwInstallationDBConn))
      {
        DebugN("ERROR: fwInstallationDB_registerInstallationPath() -> Could not commit DB transaction.");
        return -1;
      }
    }
    else{
      if(g_fwInstallationVerbose)
        DebugN("INFO: fwInstallationDB_registerInstallationPath() -> Installation path is already registered for this project");
      
      return 0;        
       
    }
  }
  else{
    DebugN("ERROR: fwInstallationDB_registerInstallationPatht() -> Cannot talk to DB or either group or project are not properly registered in the DB");
    return -1;
  }

  return 0;
}
/////////////////////////////////

int fwInstallationDB_registerProjectConfiguration()
{
  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql;

  string defaultInstallationPath;  
  dyn_string pvssComponentGroups;
  int error = 0;
  dyn_dyn_string componentsInfo;
  
  dyn_string componentNames;
  dyn_string componentVersions;
  dyn_string componentInstDirs;
  
  bool isSubComponent;
  dyn_bool isSubComponentArray;

  dyn_string groupComponentNames, groupComponentVersions, groupDescriptionFiles;
  dyn_bool groupIsSubComponents;
  dyn_string installationPaths;
  
  bool isProjectGroupsOK;
  dyn_string commonProjectGroups, missingProjectGroupsInPVSS, missingProjectGroupsInDB;
  dyn_string dbInstallationPaths;
  dyn_int dbInstallationPathsIds; 
          
  dynClear(aRecords);
  dynClear(isSubComponentArray);
  
  DebugN("INFO: fwInstallationDB_registerProjectConfiguration() -> Updating project information in DB. Please wait...");
  
  //1.- Register PC
//  if(g_fwInstallationVerbose)
    DebugN("INFO: Step 1 of 8: fwInstallationDB_registerProjectConfiguration() -> Checking PC registration.");    
  if(fwInstallationDB_registerPC() != 0){
    DebugN("ERROR: fwInstallationDB_registerProjectConfiguration() -> Failed to register PC in the DB.");
    return -1;
  }

  //2.- Register PVSS system
//  if(g_fwInstallationVerbose)
    DebugN("INFO: Step 2 of 8: fwInstallationDB_registerProjectConfiguration() -> Checking PVSS system registration.");    
  if(fwInstallationDB_registerSystem() != 0){
    DebugN("ERROR: fwInstallationDB_registerProjectConfiguration() -> Failed to register PVSS system in the DB.");
    return -1;
  }
  
  //3.- Register PVSS project
//  if(g_fwInstallationVerbose)
    DebugN("INFO: Step 3 of 8: fwInstallationDB_registerProjectConfiguration() -> Checking PVSS project registration.");    
  if(fwInstallationDB_registerProject() != 0){
    DebugN("ERROR: fwInstallationDB_registerProjectConfiguration() -> Failed to register project in the DB.");
    return -1;
  }

  //4.- Register Installation paths
  //Default installation path first:
//  if(g_fwInstallationVerbose)
    DebugN("INFO: Step 4 of 8: fwInstallationDB_registerProjectConfiguration() -> Checking project paths registration");    
  
  dpGet("fwInstallationInfo.installationDirectoryPath", defaultInstallationPath);
  
  if(defaultInstallationPath != "")
  {
    if(fwInstallationDB_registerInstallationPath(defaultInstallationPath, true) != 0){
      DebugN("ERROR: fwInstallationDB_registerProjectConfiguration() -> Failed to register the default FW Installation path "+ defaultInstallationPath+ " in the DB. Proceeding with the next one now...");
      ++error;
    }
  }
  //Rest of the proj_paths:
  fwInstallation_loadProjPaths(installationPaths);
    
  for(int i = 1; i <= dynlen(installationPaths); i++)
  {
    if(fwInstallationDB_registerInstallationPath(installationPaths[i], false) != 0){
      DebugN("ERROR: fwInstallationDB_registerProjectConfiguration() -> Failed to register FW Installation path "+ installationPaths[i]+ " in the DB. Proceeding with the next one now...");
      ++error;
    }
  }
  
  //5.- Unregister project paths registered in the DB but not defined in PVSS:
//  if(g_fwInstallationVerbose)
    DebugN("INFO: Step 5 of 8: fwInstallationDB_registerProjectConfiguration() -> Unregistering non used project paths...");    
  
  fwInstallationDB_getInstallationPaths(dbInstallationPaths, dbInstallationPathsIds);
  
  for(int i = 1; i <= dynlen(dbInstallationPaths); i++)
  {
    if(dynContains(installationPaths, dbInstallationPaths[i]) <= 0 && dbInstallationPaths[i] != defaultInstallationPath)
    {
      if(fwInstallationDB_unregisterInstallationPath(dbInstallationPaths[i]) != 0)
      {
        DebugN("ERROR: fwInstallationDB_registerProjectConfiguration() -> Failed to unregister FW Installation path "+ dbInstallationPaths[i]+ " from the DB. Proceeding with the next one now...");
        ++error;
      }
    }
  }
  
  
  
  //6.- Get list of components and register them in DB.
//  if(g_fwInstallationVerbose)
    DebugN("INFO: Step 6 of 8: fwInstallationDB_registerProjectConfiguration() -> Checking FW Component registration.");    
  
  fwInstallation_getInstalledComponents(componentsInfo);
  for(int i = 1; i <= dynlen(componentsInfo); i++)
  {
    componentNames[i] =    componentsInfo[i][1];
    componentVersions[i] = componentsInfo[i][2];
    componentInstDirs[i] = componentsInfo[i][3];
  
    dpGet("fwInstallation_" + componentNames[i] + ".isItSubComponent", isSubComponent);
    isSubComponentArray[i] = isSubComponent;
    if(fwInstallationDB_registerComponent(componentNames[i], componentVersions[i], isSubComponent) != 0){
      DebugN("ERROR: fwInstallationDB_registerProjectConfiguration() -> Failed to register FW component "+ componentNames[i]+ " v." + componentVersions[i]);
      ++error;
    }
     
  }  
  
  //7.- a) Get list of project groups and register them
  //    b) Register group components
//  if(g_fwInstallationVerbose)
    DebugN("INFO: Step 7 of 8: fwInstallationDB_registerProjectConfiguration() -> Checking FW Component groups registration.");    
  
  pvssComponentGroups = fwInstallation_getComponentGroups();
  fwInstallation_checkProjectGroupsIntegrity(isProjectGroupsOK, commonProjectGroups, missingProjectGroupsInPVSS, missingProjectGroupsInDB);
  
  for(int i = 1; i <= dynlen(pvssComponentGroups); i++)
  {
    // a) Registering project groups
    if(fwInstallationDB_registerGroupInProject(pvssComponentGroups[i]) != 0)
    {
      ++error;
      DebugN("ERROR: fwInstallationDB_registerProjectConfiguration() -> Failed to register project component group: " + pvssComponentGroups[i] + " in DB. Proceeding with next group of components in the project...");
    }    

    // b) Registering components in groups
    fwInstallation_getGroupComponents(pvssComponentGroups[i], groupComponentNames, groupComponentVersions, groupIsSubComponents, groupDescriptionFiles);

    for(int j = 1; j <= dynlen(groupComponentNames); j++)
    {    
      if(fwInstallationDB_registerComponentInGroup(groupComponentNames[j], groupComponentVersions[j], groupIsSubComponents[j], pvssComponentGroups[i]) != 0)
      {
        ++error;
        DebugN("ERROR: fwInstallationDB_registerProjectConfiguration() -> Failed to register component: "+groupComponentNames[j] + " v." + groupComponentVersions[j]+ " group: " + pvssComponentGroups[i] + " in DB. Proceeding with next group of components in the project...");
       }    
    }
  }
  
 //8.- Unregister groups from projects if not defined in PVSS:
// if(g_fwInstallationVerbose)
  DebugN("INFO: Step 8 of 8: fwInstallationDB_registerProjectConfiguration() -> Unregistering non used project groups...");    
 
  for(int i = 1; i <= dynlen(missingProjectGroupsInPVSS); i++)
  {
    if(fwInstallationDB_unregisterProjectGroup(missingProjectGroupsInPVSS[i]) != 0)
    {
      DebugN("ERROR: fwInstallationDB_registerProjectConfiguration() -> Failed to unregister project group "+ missingProjectGroupsInPVSS[i]+ " from the DB. Proceeding with the next one now...");
      ++error;
    }
  } 
  

  if(error > 0){
    if(fwInstallationDB_setProjectStatus(FW_INSTALLATION_DB_PROJECT_MISSMATCH) != 0)
      DebugN("WARNING: fwInstallationAgentDBConsistencyChecker -> Could not update project status in DB");
      
    DebugN("WARNING: Registration of project configuration finished with errors");    
    return -1;
  }  
  
  if(fwInstallationDB_setProjectStatus(FW_INSTALLATION_DB_PROJECT_OK) != 0)
    DebugN("WARNING: fwInstallationAgentDBConsistencyChecker -> Could not update project status in DB");

  DebugN("INFO: Registration of project configuration finished successfully");    
  return 0; 
}


int fwInstallationDB_setProjectStatus(int status, string projectName = "", string computerName = "")
{
  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql;

  int project_id;
  int component_id;
          
  dynClear(aRecords);

  if(projectName == "")
    projectName = PROJ;
  
  if(computerName = "")
    computerName = getHostname();

  //
  if(fwInstallationDB_isProjectRegistered(project_id, projectName, computerName) != 0){
   DebugN("ERROR: fwInstallationDB_setProjectStatus() -> Cannot access the DB. Please check connection");
   return -1;       
  }
  else if(project_id == -1){
   DebugN("ERROR: fwInstallationDB_setProjectStatus() -> Project: " + projectName + " in computer: " + computerName + " not registered in DB.");
   return -1;      
  }else{

    sql = "UPDATE fw_sys_stat_pvss_project SET is_project_ok = " + status + ", last_time_checked = SYSDATE WHERE id = " + project_id; 
             
    if(g_fwInstallationSqlDebug)
      DebugN(sql);

    if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)) {DebugN("ERROR: fwInstallationDB_setProjectStatus() -> Could not execute the following SQL: " + sql); return -1;};
  }      
  
  return dpSet("fwInstallation_agentParametrization.db.projectStatus", status);
}


/** executes SQL statements stored in a file
 *
 * Note that the all commands in the file should be terminated by the semicolon (;)
 * character, which should be the last characted in the line.
 * The comments (lines starting with "-" character) and white-spaces are a
 * also automatically removed.
 *
 * @ingroup DBAccessFunctions
 * @param dbCon            database connection descriptor
 * @param filename          the name of the file, where the SQL statements are stored.
 *                          The file needs to be in the "config" directory of the project.
 * @param whatItIs          a short text describing the context in which the statements
 *                          are executed. Used only for printouts.
 * @param breakOnDbError    (optional, default is TRUE) determines if the function should
 *                          return terminate with an exception upon first encountered error,
 *                          or it should rather try to finish the remaining commands
 *                          (the errors encountered in a meantime will be reported at the end).
*/
int fwInstallationDB_executeSqlFromFile(dbConnection dbCon, string fileName, string whatItIs,
                                        bool breakOnDbError=TRUE)
{
    dyn_string exceptionInfo,localExceptionInfo;
    
    string sqlFile=getPath(CONFIG_REL_PATH,fileName);

    if (sqlFile=="") {
      DebugN("ERROR: fwInstallationDB_executeSqlFromFile() -> Cannot locate file with SQL statements:",fileName);
      return -1;
    }
    
    string sSql; // all statements in one string
    bool ok=fileToString(sqlFile,sSql);
    if (!ok) {
        DebugN("ERROR: fwInstallationDB_executeSqlFromFile() -> The file with SQL statements for "+whatItIs+
                          " cannot be opened");
        return -1;
    }

    // split it to individual commands - they are separated by ";" character...
    dyn_string sql=strsplit(sSql,";");

    // a separator is a semicolon followed by newline; otherwise it is a part of the same command,
    // i.e. as it is the case of the statements that refresh the views...
    for (int i=2;i<=dynlen(sql);i++) {
	if ( substr(sql[i],0,1)=="\n") {
	}else{
	    // join this line to the previous line!
	    sql[i-1]=sql[i-1]+";"+sql[i];
	    dynRemove(sql,i);
	    i--;// because we removed the current one!
	}
    }
    // commit previous transaction...
    rdbCommitTransaction(dbCon);
    int rc=rdbBeginTransaction(dbCon);
    if (rc) {
	DebugN("ERROR: fwInstallation_executeSqlFromFile() -> Cannot start transaction");
	return -1;
    };
    for (int i=1;i<=dynlen(sql);i++) {
        string stmt=sql[i];

        // skip whatever empty lines and comment lines
        dyn_string sstmt=strsplit(stmt,"\n");
        stmt="";
        for (int j=1;j<=dynlen(sstmt);j++) {
            string line=strltrim(sstmt[j]," ");
            if (line=="") continue;
            if (line[1]!="-") stmt=stmt+line+"\n";
        }
        if (stmt=="") continue;

        if(rdbExecuteSqlStatement(dbCon, stmt)){
            dynAppend(exceptionInfo,localExceptionInfo);
            rdbRollbackTransaction (dbCon);
            return -1;
        };

    };
    rc=rdbCommitTransaction(dbCon);

    if (rc) {
        DebugN("ERROR: fwInstallationDB_executeSqlFromFile() -> Cannot commit transaction","");
	return -1;
    };
    
    return 0;
}

int fwInstallationDB_getHosts(dyn_string &hostnames, dyn_string &ips, dyn_string &macs, dyn_string &ips2, dyn_string &macs2, dyn_string &bmc_ips, dyn_string &bmc_ips2)
{
  dyn_string exceptionInfo;
  string sql;
  dyn_dyn_mixed aRecords;
  
  dynClear(exceptionInfo);
  dynClear(aRecords);  

  sql = "SELECT hostname, ip, mac, ip2, mac2, bmc_ip, bmc_ip2 FROM fw_sys_stat_computer";
       
  if(g_fwInstallationSqlDebug)
    DebugN(sql);

  if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
  {
    DebugN("ERROR: fwInstallationDB_getHosts() -> Could not execute the following SQL query: " + sql);
    return -1;
  }  

  for(int i = 1; i <= dynlen(aRecords); i++) {   
    dynAppend(hostnames, aRecords[i][1]);
    dynAppend(ips, aRecords[i][2]);
    dynAppend(macs, aRecords[i][3]);
    dynAppend(ips2, aRecords[i][4]);
    dynAppend(macs2, aRecords[i][5]);
    dynAppend(bmc_ips, aRecords[i][6]);
    dynAppend(bmc_ips2, aRecords[i][7]);
  }
  
  return 0;
}

//---------Item Properties-------------

int fwInstallationDB_getComponentProperties(string component, string version, int &isSubComponent, int &isOfficial, string &defaultPath)
{  
  dyn_string exceptionInfo;
  string sql;
  dyn_dyn_mixed aRecords;
  int component_id;
  
  dynClear(exceptionInfo);
  dynClear(aRecords);  
  if(fwInstallationDB_isComponentRegistered(component, version, component_id) != 0)
  {
    DebugN("ERROR: fwInstallationDB_getComponentProperties() -> Cannot access the DB");
    return -1;
  }else if(component_id < 0)
  {
    DebugN("ERROR: fwInstallationDB_getComponentProperties() -> Component: " + component + " version" + version + " is not registered in the DB.");
    return -1;
  }  
  else{
    sql = "SELECT is_subcomponent, is_official, default_path FROM fw_sys_stat_component WHERE id = " + component_id;
    
    if(g_fwInstallationSqlDebug)
      DebugN(sql);

    if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
    {
      DebugN("ERROR: fwInstallationDB_getComponentProperties()() -> Could not execute the following SQL query: " + sql);
      return -1;
    }  
    if(dynlen(aRecords[1]) > 0){
    
    isSubComponent = aRecords[1][1];
    isOfficial =     aRecords[1][2];
    defaultPath =    aRecords[1][3];
    }
  }

  return 0;  
}

int fwInstallationDB_getHostProperties(string hostname, string &ip, string &mac, string &ip2, string &mac2, string &bmc_ip, string &bmc_ip2, bool &centrallyManaged)
{
  dyn_string exceptionInfo;
  string sql;
  dyn_dyn_mixed aRecords;
  
  dynClear(exceptionInfo);
  dynClear(aRecords);  

//  sql = "SELECT hostname, ip, mac, ip2, mac2, bmc_ip, bmc_ip2, centrally_managed FROM fw_sys_stat_computer";
  sql = "SELECT ip, mac, ip2, mac2, bmc_ip, bmc_ip2 FROM fw_sys_stat_computer WHERE hostname = \'" + hostname + "\'";
       
  if(g_fwInstallationSqlDebug)
    DebugN(sql);

  if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
  {
    DebugN("ERROR: fwInstallationDB_getHosts() -> Could not execute the following SQL query: " + sql);
    return -1;
  }

  
  if(dynlen(aRecords) > 0) {   
    ip = aRecords[1][1];
    mac = aRecords[1][2];
    ip2 = aRecords[1][3];
    mac2 = aRecords[1][4];
    bmc_ip = aRecords[1][5];
    bmc_ip2 = aRecords[1][6];
    //centrallyManaged = aRecords[1][7];
    centrallyManaged = false;
  }
  return 0;
}

int fwInstallationDB_getProjectProperties(string project, string hostname, string &pvssVersion, string &projectDir, 
                                          string &systemName, int &systemNumber, int & pPort, string &pmonUsername, 
                                          string &pmonPassword, dyn_string &projectPaths)
{
  dyn_string exceptionInfo;
  string sql;
  dyn_dyn_mixed aRecords;
  
  dynClear(exceptionInfo);
  dynClear(aRecords);  
  dynClear(projectPaths);

  pvssVersion = "N/A";
  
//  sql = "SELECT hostname, ip, mac, ip2, mac2, bmc_ip, bmc_ip2, centrally_managed FROM fw_sys_stat_computer";
  sql = "SELECT p.project_dir, s.system_name, s.system_number, p.pmon_port, p.pmon_username, p.pmon_password, p.fw_inst_tool_version, p.default_inst_path_id " +
        "FROM fw_sys_stat_pvss_project p, fw_sys_stat_pvss_system s " +
        "WHERE p.project_name = \'"+ project + "\' AND p.system_id = s.id AND computer_id = " + 
	"	(SELECT c.id from fw_sys_stat_computer c WHERE c.hostname = \'" + hostname + "')";
       
  if(g_fwInstallationSqlDebug)
    DebugN(sql);

  if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
  {
    DebugN("ERROR: fwInstallationDB_getProjectProperties() -> Could not execute the following SQL query: " + sql);
    return -1;
  }

  
  if(dynlen(aRecords) > 0) {   
    projectDir = aRecords[1][1];
    systemName = aRecords[1][2];
    systemNumber = aRecords[1][3];
    pPort = aRecords[1][4];
    pmonUsername = aRecords[1][5];
    pmonPassword = aRecords[1][6];
  }
  
  ///Project paths:
  
    sql = "select d.path from fw_sys_stat_inst_path d where valid_until IS NULL AND project_id = " +
	  "(select p.id from fw_sys_stat_pvss_project p where p.project_name = \'" + project + "\'  AND p.computer_id = "
	  "	(select c.id from fw_sys_stat_computer c where c.hostname = \'" + hostname + "'))";
       
 // if(g_fwInstallationSqlDebug)
    DebugN(sql);

  if(fwInstallationDB_executeDBQuery(sql, aRecords) != 0)
  {
    DebugN("ERROR: fwInstallationDB_getProjectProperties() -> Could not execute the following SQL query: " + sql);
    return -1;
  }

  
  for(int i = 1; i <= dynlen(aRecords); i++) {  
    
    DebugN("appending: " + aRecords[i][1]); 
    
    dynAppend(projectPaths, aRecords[i][1]);
  }
  
  DebugN("DB: ", projectPaths);
  return 0;
  
}//end of function


int fwInstallationDB_setComponentProperties(string component, string version, int isSubComponent, int isOfficial, string defaultPath)
{
  int component_id;
  dyn_string exceptionInfo;
  dyn_dyn_mixed aRecords;
  string sql;

  dynClear(aRecords);

  if(fwInstallationDB_isComponentRegistered(component, version, component_id) != 0)
  {
    DebugN("ERROR: fwInstallationDB_setComponentProperties() -> Could not access the DB");
    return -1;
    
  }else if(component_id == -1){
    DebugN("INFO: fwInstallationDB_setComponentProperties() -> Component: " + component + " v." + version + " not registered in DB.");
    return -1;      
  }else{
        
       sql = "UPDATE fw_sys_stat_component SET is_subcomponent = " + isSubComponent + ", default_path = \'" + defaultPath + "\', is_official = " + isOfficial + " WHERE id = " + component_id;
         
       if(g_fwInstallationSqlDebug)
         DebugN(sql);
  
       if(rdbExecuteSqlStatement(gFwInstallationDBConn, sql)) {DebugN("ERROR: fwInstallationDB_setComponentProperties() -> Could not execute the following SQL: " + sql); return -1;};
    }   
  return 0;
}
