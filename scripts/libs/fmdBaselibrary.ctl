// FMD BASE library 
// Author : Ga�l Renault - Niels Bohr Institute
// Ver 1.0  04/03/2007 

// ------------------------------------------
/* 			History

	 04/03/2007   - creation

*/
// ------------------------------------------
// ===================================================================
//                           VARIABLE DEFINITIONS
// ===================================================================

// Function definition for the constant
const float FLOAT_PRECISION_POS_THRESHOLD = 0.00001; // Threshold for the float comparision
const float FLOAT_PRECISION_NEG_THRESHOLD = -0.00001;

// Name of System
//const string HVSYS = "dist_1";//"hmp_hvlv";
const string SYS                   = "fmd_dcs";
const string HVSYS                 = "fmd_dcs";
const string SUFFIX_DET            = "DCS";
const string PREFIX_DET            = "FMD";
const string PREFIX_PW             = "PW";
const string PREFIX_MOD            = "MOD";
const string PREFIX_INFR           = "INFR";
const string PREFIX_RACK_ONE       = "RACK1";
const string PREFIX_RACK_TWO       = "RACK2";
const string PREFIX_HVPS           = "HVPS";
const string PREFIX_LVPS           = "LVPS";
const string PREFIX_CONTROLLER     = "CONTROLLER";
const string PREFIX_CONTROLLER0    = "CONTROLLER0";
const string PREFIX_CONTROLLER1    = "CONTROLLER1";
const string PREFIX_INNER          = "INNER";
const string PREFIX_OUTER          = "OUTER";
const string PREFIX_TOP            = "TOP";
const string PREFIX_BOTTOM         = "BOTTOM";
const string PREFIX_HV_CH          = "HvChannel";
const string PREFIX_LV_CH          = "LvChannel";
const string STOCKDP_PREFIX         = "fmd_hvSt_";
const string STOCKDPTYPENAME        = "Fmd_fwCaenChannelStockSet";

// ===================================================================
//                                IDs
// ===================================================================

const int ID_TYPE_TOP_NODE         = 1;
const int ID_TYPE_RACK_ONE         = 2;
const int ID_TYPE_RACK_TWO         = 3;
const int ID_TYPE_HVPS_CONTROLLER  = 4;
const int ID_TYPE_LVPS_CONTROLLER  = 5;
const int ID_TYPE_MOD              = 6;
const int ID_TYPE_PW               = 7;
const int ID_TYPE_HVCH             = 8;
const int ID_TYPE_LVCH             = 9;

const int ID_SIDE_INNER            = 10;  // iSideIO
const int ID_SIDE_OUTER            = 11;  // iSideIO
const int ID_SIDE_TOP              = 12;  // iSideTB
const int ID_SIDE_BOTTOM           = 13;  // iSideTB



// ===================================================================

const int DET_N_DET                = 3;//FMD1,FMD2,FMD3
const int N_HV_CHAN_I              = 4;// 0-4
const int N_HV_CHAN_O              = 9;// 0-9
const int N_LV_CHAN                = 3;// 0-3

const int CH_LVPS_CONTROLLER_0     = 0;
const int CH_LVPS_CONTROLLER_1     = 1;
const int CH_LVPS_CONTROLLER       = 99;


// ===================================================================
//                           END VARIABLE DEFINITIONS
// ===================================================================


// ----------------------------------------------------------------------------
//    
// int __fmdBase_CreatefwCaenStockSetDpe()
// {
// DebugTN("int __fmdBase_CreatefwCaenStockSetDpe()");
// int n =0;
// int tch = 0;
// dyn_string dpes;
// bool fDebug = false;
//   
// for (int n_det = 1;n_det<=3;n_det++)
//   {
//  // INNER
//   //fmd_hvSt_FMD_DCS/FMD2_MOD/FMD2_PW_OUTER_BOTTOM/HvChannel007
// DebugN("______________ INNER  HV_________________________");  
//   for (int n_hv_ch = 0; n_hv_ch <= N_HV_CHAN_I ; n_hv_ch++)
//     {// HV ch
//       string s_hv_ch;
//       sprintf(s_hv_ch,"%03d",n_hv_ch );
//       dpes = makeDynString(ROOT_SYS + PREFIX_DET + "_" + SUFFIX_DET +"/" + PREFIX_DET + n_det + "_" + PREFIX_MOD + "/" + PREFIX_DET + n_det + "_" + FSM_INNER_TOP+"/" + FSM_HV_CHAN + s_hv_ch);
//       if(fDebug) { DebugN("__fmdBase_CreatefwCaenStoreDpt : ",dpes);  }
//       else
//       {
//         if (dpExists(dpes)) DebugN("__fmdBase_CreatefwCaenStoreDpt : created DPE : ",dpes, "already exists");
//         else dpCreate(dpes ,DPT_FWCAEN_STORE ); // Create
//       }
//        
//       dpes = makeDynString(ROOT_SYS + FSMSYS+"/" + FSM_DET_NODE + n_det+"/" + FSM_INNER_BOTTOM+"/" + FSM_HV_CHAN + s_hv_ch);
//       if(fDebug){DebugN("__fmdBase_CreatefwCaenStoreDpt : ",dpes);}
//       else
//       {
//         if (dpExists(dpes)) DebugN("__fmdBase_CreatefwCaenStoreDpt : created DPE : ",dpes, "already exists");  
//         else dpCreate(dpes ,DPT_FWCAEN_STORE ); // Create
//       }
//      }// HV ch
// DebugN("______________ LV_________________________");  
//     for (int n_lv_ch = 0; n_lv_ch <= N_LV_CHAN ; n_lv_ch++)
//     {// LV ch
//       string s_lv_ch;
//       sprintf(s_lv_ch,"%03d",n_lv_ch );
//       dpes = makeDynString(ROOT_SYS + FSMSYS+"/" + FSM_DET_NODE + n_det+"/" + FSM_INNER_TOP+"/" + FSM_LV_CHAN + s_lv_ch);
//       if(fDebug) { DebugN("__fmdBase_CreatefwCaenStoreDpt : ",dpes);}
//       else
//       {
//         if (dpExists(dpes)) DebugN("__fmdBase_CreatefwCaenStoreDpt : created DPE : ",dpes, "already exists");
//         else dpCreate(dpes ,DPT_FWCAEN_STORE ); // Create
//       }
//        
//       dpes = makeDynString(ROOT_SYS + FSMSYS+"/" + FSM_DET_NODE + n_det+"/" + FSM_INNER_BOTTOM+"/" + FSM_LV_CHAN+  s_lv_ch);
//       if(fDebug){DebugN("__fmdBase_CreatefwCaenStoreDpt : ",dpes);}
//       else
//       {
//         if (dpExists(dpes)) DebugN("__fmdBase_CreatefwCaenStoreDpt : created DPE : ",dpes, "already exists");  
//         else dpCreate(dpes ,DPT_FWCAEN_STORE ); // Create
//       }
//       
//        if (n_det !=1){
//       dpes = makeDynString(ROOT_SYS + FSMSYS+"/" + FSM_DET_NODE + n_det+"/" + FSM_OUTER_TOP+"/" + FSM_LV_CHAN + s_lv_ch);
//       if(fDebug) { DebugN("__fmdBase_CreatefwCaenStoreDpt : ",dpes);}
//       else
//       {
//         if (dpExists(dpes)) DebugN("__fmdBase_CreatefwCaenStoreDpt : created DPE : ",dpes, "already exists");
//         else dpCreate(dpes ,DPT_FWCAEN_STORE ); // Create
//       }
//       
//       
//       dpes = makeDynString(ROOT_SYS + FSMSYS+"/" + FSM_DET_NODE + n_det+"/" + FSM_OUTER_BOTTOM+"/" + FSM_LV_CHAN + s_lv_ch);
//       if(fDebug) { DebugN("__fmdBase_CreatefwCaenStoreDpt : ",dpes);}
//       else
//       {
//         if (dpExists(dpes)) DebugN("__fmdBase_CreatefwCaenStoreDpt : created DPE : ",dpes, "already exists");
//         else dpCreate(dpes ,DPT_FWCAEN_STORE ); // Create
//       }
//     }
//       
//      }// LV ch
//    DebugN("_________OUTER HV______________________________"); 
// if (n_det !=1){
//   for (int n_hv_ch = 0; n_hv_ch <= N_HV_CHAN_O ; n_hv_ch++)
//     {// HV ch
//     string s_hv_ch;
//       sprintf(s_hv_ch,"%03d",n_hv_ch );
//       dpes = makeDynString(ROOT_SYS + FSMSYS+"/" + FSM_DET_NODE + n_det+"/" + FSM_OUTER_TOP+"/" + FSM_HV_CHAN + s_hv_ch);
//       if(fDebug) { DebugN("__fmdBase_CreatefwCaenStoreDpt : ",dpes);  }
//       else
//       {
//         if (dpExists(dpes)) DebugN("__fmdBase_CreatefwCaenStoreDpt : created DPE : ",dpes, "already exists");
//         else dpCreate(dpes ,DPT_FWCAEN_STORE ); // Create
//       }
//         
//       dpes = makeDynString(ROOT_SYS + FSMSYS + "/" + FSM_DET_NODE + n_det + "/" + FSM_OUTER_BOTTOM + "/" + FSM_HV_CHAN + s_hv_ch);
//       if(fDebug){DebugN("__fmdBase_CreatefwCaenStoreDpt : ",dpes);}
//       else
//       {
//         if (dpExists(dpes)) DebugN("__fmdBase_CreatefwCaenStoreDpt : created DPE : ",dpes, "already exists");  
//         else dpCreate(dpes ,DPT_FWCAEN_STORE ); // Create
//       }
//      }// HV ch
// }
//   
//  
//   }// n_det
//  
// Create the datapoint type
//n = dpTypeCreate(depes,depei);
// DebugTN ("__fmdBase_CreatefwCaenStoreDpt : created DPE : ",dpes );
// return(n);
// }

// ----------------------------------------------------------------------------
// __hmpBase_CreateWattmeterDpt (internal use only)
/*
	This function create the Wattmeter DpType. 
	
	F.A. ver 1.0   01/03/2006
	
	History
	
	  		
*/
// const string WATTMETERDPTYPENAME = "Hmp_wattMeter";
// const string WATTMETERDPNAME = "hmp_lvWattMeter";
// const float MAXDETECTORWATTS = 2000.0;
// const float MAXMODULEWATTS = 700.0;
// 
// int __fmdBase_CreateWattmeterDpt()
// {
// 
// int n;
// dyn_dyn_string depes;
// dyn_dyn_int depei;
// 
// 	// Create the data type
// 	depes[1] = makeDynString (HVSYS+":"+WATTMETERDPTYPENAME,"");
// 	depes[2] = makeDynString ("","fmdid");
// 	depes[3] = makeDynString ("","rich0");
// 	depes[4] = makeDynString ("","rich1");
// 	depes[5] = makeDynString ("","rich2");
// 	depes[6] = makeDynString ("","rich3");
// 	depes[7] = makeDynString ("","rich4");
// 	depes[8] = makeDynString ("","rich5");
// 	depes[9] = makeDynString ("","rich6");
// 
// 
// 	depei[1] = makeDynInt (DPEL_STRUCT);
// 	depei[2] = makeDynInt (0,DPEL_FLOAT);
// 	depei[3] = makeDynInt (0,DPEL_FLOAT);
// 	depei[4] = makeDynInt (0,DPEL_FLOAT);
// 	depei[5] = makeDynInt (0,DPEL_FLOAT);
// 	depei[6] = makeDynInt (0,DPEL_FLOAT);
// 	depei[7] = makeDynInt (0,DPEL_FLOAT);
// 	depei[8] = makeDynInt (0,DPEL_FLOAT);
// 	depei[9] = makeDynInt (0,DPEL_FLOAT);
// 	// Create the datapoint type
// 	n = dpTypeCreate(depes,depei);
// 	DebugTN ("__fmdBase_Wattmeter : created HMPID/Wattmeter DPT : ",n);
// 	return(n);
// }

// -------------------------

/* ------------------------------------------------

Name: 	fmdBase_RefreshBarLedIndicator(float value1,float value2)
Desc:   Display a value into the BarLedIndicator Hmp Widget
        
inp :   
out :   

Auth : G. Renault - NBI
Date : 28/02/2006
Ver. : 1.0

Hist : -

------------------------------------------------ */
void fmdBase_RefreshBarLedIndicator(string sModule, string sPanel, string sName,float value)
{
		setValue(sName+".Value","text",value);
		setInputFocus(sModule,sPanel,sName+".Value");
		return;
}
/* ------------------------------------------------

Name: 	__fmdBase_FloatEqual(float value1,float value2)
Desc:   Compare 2 float values and consider they equals
				if the difference is under a certain threshold
        
inp :   Value1, Value2 := float 
out :   Result : true := equal   false := notequal

Auth : G. Renault - NBI
Date : 30/08/2005
Ver. : 1.0

Hist : -

------------------------------------------------ */
bool __fmdBase_FloatEqual(float value1,float value2)
{
	float f;
	f = value1 - value2;
	return( (f < FLOAT_PRECISION_POS_THRESHOLD) && (f > FLOAT_PRECISION_NEG_THRESHOLD));
}
/* ------------------------------------------------

Name: 	fmdBase_Tem2Col()
Desc:   Convert a temperature value into a colour 
        string
        
inp :   Temperature := float 
out :   Colour := string  "{red,green,blu}"

Note : the range temperature varies from 8 to 60�C
       over range values are marcked with blue
       and red colours
       
Auth : G. Renault - NBI
Date : 24/06/2005
Ver. : 1.0

Hist : -

------------------------------------------------ */
string fmdBase_Tem2Col(float temp, int t_min = 0, int t_max = 130)
{
	int r,g,b;
	if(temp<t_min || temp > t_max) return("{0,0,0}");
	
	int t_delta = (t_max -t_min +1);
	int s1 = t_min + floor(t_delta * 0.16);		
	int s2 = s1 + 1;
	int s11 = t_min + floor(t_delta * 0.33);		
	int s10 = s11 - 1;
	int s8 = s11 + 1;
	int s3 = t_min + floor(t_delta * 0.49);		
	int s7 = s3 - 1;
	int s12 = t_min + floor(t_delta * 0.70);	// .66	
	int s4 = s12 - 1;
	int s9 = s12 + 1;
	int s5 = t_min + floor(t_delta * 0.90);		// .82
	int s6 = s5 - 1;
	int step = floor( 240.0 / (s11-s1+1) );
	
	r = (temp < s3) ? 0 : ( (temp > s4 && temp < s5) ? 255 : (temp > s6) ? (255-(temp-s6)*step) : ((temp-s7)*step) );
	b = (temp > s1 && temp < s8) ? 255 : ( (temp > s3) ? 0 : (temp < s2) ? (60+(temp-t_min)*step) : (255-step*(temp-s8)) );
	g = (temp < s2 || temp > s6) ? 0 : ((temp < s9 && temp > s10) ? 255 : ((temp < s11 && temp > s1) ? ((temp-s1)*step) : (255-(step*(temp-s12))) ) );	
//DebugN(">>>",s1,s11,temp,step,r,g,b);
	return("{"+r+","+g+","+b+"}");
}

// ----------------------------------------------------------------------------
// int fmdBase_getAccessLevel(bool & bOnlyDisplay)
/*
	
	F.A. ver 1.0   22/02/2006

	Return the Access Privilege Code
	
						History
	
		22/02/2006 created 
		
*/
const int HMP_ACCESS_ADMIN = 1;
const int HMP_ACCESS_EXTEND = 2;
const int HMP_ACCESS_NORMAL = 3;
const int HMP_ACCESS_ACKNOL = 4;
const int HMP_ACCESS_VISUAL = 9;
const int HMP_ACCESS_DENIED = 0;

int fmdBase_getAccessLevel(bool & bOnlyDisplay)
{

	string user,domain;
  string userFullName, description;  
  int userId;  
  bool enabled,granted;
  dyn_string groupNames, exceptionInfo;

	bOnlyDisplay = true;
	
	// Get the users 
	fwAccessControl_getUserName(user);
	fwAccessControl_getUser(user, userFullName, description, userId,
												enabled, groupNames,exceptionInfo );

	// If isn't enabled then close the panel and Exit
	if(!enabled) return(HMP_ACCESS_DENIED);	

	domain = getSystemName();
	fwAccessControl_isGranted(domain+"Administration",granted,exceptionInfo);
	if(granted)
	{
		bOnlyDisplay = false;
		return(HMP_ACCESS_ADMIN);
	}
	fwAccessControl_isGranted(domain+"Extended user level",granted,exceptionInfo);
	if(granted)
	{
		bOnlyDisplay = false;
		return(HMP_ACCESS_EXTEND);
	}
	fwAccessControl_isGranted(domain+"Normal user level",granted,exceptionInfo);
	if(granted)
	{
		return(HMP_ACCESS_NORMAL);
	}
	fwAccessControl_isGranted(domain+"Acknowledge",granted,exceptionInfo);
	if(granted)
	{
		return(HMP_ACCESS_ACKNOL);
	}
	return(HMP_ACCESS_VISUAL);
}
// ----------------------------------------------------------------------------
// string fmdBase_GetCoords(string device, int &iDet, int &iType, int &iSideIO, int &iSideTB, int &iCh)
/*
	
	G. Renault

	Return all the Coords and specification of device
	
						History
	
		28/02/2007 created
				
*/

string fmdBase_GetCoords(string device, int &iDet, int &iType, int &iSideIO, int &iSideTB, int &iCh)
{
string sLogicalName,sAppo,sEle;
dyn_string dsHier;
dyn_string dsElem;

	iDet = -1; iType = -1; iSideIO = -1; iSideTB = -1; iCh = -1;
		
	// Convert device into alias 
	sLogicalName = dpGetAlias(device+".");
	if(sLogicalName == "") sLogicalName=device;

	// Get the last element 
	dsHier = strsplit(sLogicalName,"/");
	if(dynlen(dsHier) == 0) return "";
	sAppo = dsHier[dynlen(dsHier)];

	// and create the full logical name
	sLogicalName = getSystemName()+sLogicalName;

        dyn_string dsElem = strsplit(sAppo,"_");
        string sEle = dsElem[dynlen(dsElem)];

           // Hardware Name: dist_1:CAEN/fmdSY1527/board10/channel000
//           DebugN(">>>>>----> device :",device);            // Hardware Name: dist_1:CAEN/fmdSY1527/board10/channel000
//           DebugN(">>>>>----> sLogicalName :",sLogicalName);// dist_1:FMD_DCS/FMD1_PW/FMD1_PW_INNER_TOP/HvChannel000
//           DebugN(">>>>>----> dsHier :",dsHier);            // dist_1:FMD_DCS,FMD1_PW,FMD1_PW_INNER_TOP,HvChannel00
//           DebugN(">>>>>----> sAppo :",sAppo);              // HvChannel000
//           DebugN(">>>>>----> dsElem :",dsElem);            // HvChannel000
//           DebugN(">>>>>----> sEle :",sEle);              // HvChannel000
   
   if( strpos(sEle,PREFIX_HV_CH)==0 ) {// extract HV channel number (iCh)
      string tdet = substr(sEle, strlen(PREFIX_HV_CH));
      int tret = sscanfUL(tdet, "%3d", iCh);
      iType = ID_TYPE_HVCH; 
      dsElem = strsplit(dsHier[3],"_");
      //DebugN(">>-----> fmdBase_GetCoords(): Found HV channel iCh: ", iCh);
    }
                
    if(strpos(sEle,PREFIX_LV_CH)==0){// extract LV channel numbre
      string tdet = substr(sEle, strlen(PREFIX_LV_CH));
      int tret = sscanfUL(tdet, "%3d", iCh);
      iType = ID_TYPE_LVCH; 
      dsElem = strsplit(dsHier[3],"_");
      //DebugN(">>-----> fmdBase_GetCoords(): Found LV channel iCh: ", iCh);
    }
  // --------------------------
     if( strpos(sEle,PREFIX_CONTROLLER)==0 || strpos(sEle,PREFIX_CONTROLLER0)==0 || strpos(sEle,PREFIX_CONTROLLER1)==0 ) {
       switch(sEle){
         case (PREFIX_CONTROLLER0) : iCh = CH_LVPS_CONTROLLER_0; break;
         case (PREFIX_CONTROLLER1) : iCh = CH_LVPS_CONTROLLER_1; break;
         case (PREFIX_CONTROLLER)  : iCh = CH_LVPS_CONTROLLER  ; break;
         default: DebugN(">>-----> fmdBase_GetCoords(): Error controller for power supply empty");break;
       }
       if(dynlen(dsElem)>1){
       switch(dsElem[1]){
         case (PREFIX_HVPS) : iType = ID_TYPE_HVPS_CONTROLLER; break;
         case (PREFIX_LVPS) : iType = ID_TYPE_LVPS_CONTROLLER; break;
         default: DebugN(">>-----> fmdBase_GetCoords(): Error controller for power supply empty");break;
       }
       return sLogicalName;
       }else { DebugN(">>-----> fmdBase_GetCoords(): Error: controller for power supply not found");} 
     }
     if( strpos(sEle,PREFIX_RACK_ONE)==0 ) {
       iType =  ID_TYPE_RACK_ONE;
       return sLogicalName;
     }
     if( strpos(sEle,PREFIX_RACK_TWO)==0 ) {
       iType =  ID_TYPE_RACK_TWO;
       return sLogicalName;
     }
  // --------------------------  
  // extract other params for HV & LV channels (iDet,iType,iSideIO,iSideTB)     
      iDet     = substr(dsElem[1], strlen(PREFIX_DET), 1); // iDet
      //DebugN(">>-----> fmdBase_GetCoords(): Found detector: ",iDet);
      if(iDet == 0) { // check for top node
        for(int ti = 1; dynlen(dsElem); ti++)
          if(strtok(dsElem[ti],PREFIX_DET)>=0) { iType = ID_TYPE_TOP_NODE; return sLogicalName; /* should return sNode*/}
          else {
            DebugN(">>-----> fmdBase_GetCoords(): unknown device: ", dsElem);
            }
        }
     if(dynlen(dsElem)>1 && (iType!=ID_TYPE_HVCH) && (iType!=ID_TYPE_LVCH) ){
      switch(dsElem[2]){
        case PREFIX_PW:      iType = ID_TYPE_PW;      break; 
        case PREFIX_MOD:      iType = ID_TYPE_MOD;      break; 
        case PREFIX_INFR:    iType = ID_TYPE_INFR;    break;             
        default: DebugN(">>-----> fmdBase_GetCoords(): Error in fmdBase_GetCoords() device iType not found ", dsElem ); break;}
     }
     if(dynlen(dsElem)>2){
       switch(dsElem[3]){
        case PREFIX_INNER:  iSideIO = ID_SIDE_INNER;   break; 
        case PREFIX_OUTER:  iSideIO = ID_SIDE_OUTER;   break;             
        default: DebugN(">>-----> fmdBase_GetCoords(): Error in fmdBase_GetCoords() device iSideIO not found ", dsElem );break;}
     }
     if(dynlen(dsElem)>3){
       switch(dsElem[4]){
        case PREFIX_TOP:    iSideTB = ID_SIDE_TOP;     break; 
        case PREFIX_BOTTOM: iSideTB = ID_SIDE_BOTTOM;  break;             
        default: DebugN(">>-----> fmdBase_GetCoords(): Error in fmdBase_GetCoords() device iSideIO not found ", dsElem );break;}
     }

//        DebugTN(">>-----> fmdBase_GetCoords(): Found device: ",
//                " iDet : ",iDet,                
//                " iType : ",iType,
//                " iSideIO : ",iSideIO,
//                " iSideTB: ",iSideTB,
//                " iCh : ",iCh);
           
	return(sLogicalName);
}



// ----------------------------------------------------------------------------
// fmdBase_buildLogicalPathName(int iDet, int iType, int iSideIO, int iSideTB, int iCh)
/*
	This function return the name of Logical View
	
	G. Renault
	
	History
	
		28/02/2006 created.
			

*/
string fmdBase_buildLogicalPathName(int iDet, int iType, int iSideIO, int iSideTB, int iCh){
   string tdp;
  switch(iType){
    case(ID_TYPE_TOP_NODE): tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET ;  break;
    case(ID_TYPE_MOD):      tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + iDet + "_" + PREFIX_MOD;  break; 
    case(ID_TYPE_PW):       tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + iDet + "_" + PREFIX_MOD
                                + "/" + PREFIX_DET + iDet + "_" + PREFIX_PW + "_" ;//+ inner + "_" + bottom;  break;
      switch(iSideIO) {
        case(ID_SIDE_INNER): tdp = tdp + PREFIX_INNER + "_"; break;
        case(ID_SIDE_OUTER): tdp = tdp + PREFIX_OUTER + "_"; break;
        default:  DebugN(">>-----> fmdBase_buildLogicalPathName(): Error iSideIO not found ", iSideIO);  break;}
      switch(iSideTB) {
        case(ID_SIDE_TOP):   tdp = tdp + PREFIX_TOP;         break;
        case(ID_SIDE_BOTTOM):tdp = tdp + PREFIX_BOTTOM;      break;
        default:  DebugN(">>-----> fmdBase_buildLogicalPathName(): Error iSideTB not found ", iSideTB);  break;}
      break;
    case(ID_TYPE_HVCH):     
      {string ts;
      sprintf(ts,"%03d",iCh); 
      string tPdp = "/" + PREFIX_HV_CH + ts;
      tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + iDet + "_" + PREFIX_MOD
                                + "/" + PREFIX_DET + iDet + "_" + PREFIX_PW + "_" ;//+ inner + "_" + bottom;  break;
      switch(iSideIO) {
        case(ID_SIDE_INNER): tdp = tdp + PREFIX_INNER + "_"; break;
        case(ID_SIDE_OUTER): tdp = tdp + PREFIX_OUTER + "_"; break;
        default:  DebugN(">>-----> fmdBase_buildLogicalPathName(): Error iSideIO not found ", iSideIO);  break;}
      switch(iSideTB) {
        case(ID_SIDE_TOP):   tdp = tdp + PREFIX_TOP;         break;
        case(ID_SIDE_BOTTOM):tdp = tdp + PREFIX_BOTTOM;      break;
        default:  DebugN(">>-----> fmdBase_buildLogicalPathName(): Error iSideIO not found ", iSideTB);  break;}
      tdp = tdp + tPdp;
      break;}
    case(ID_TYPE_LVCH):     
      {string ts; 
      sprintf(ts,"%03d",iCh); 
      string tPdp = "/" + PREFIX_LV_CH + ts;
      tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + iDet + "_" + PREFIX_MOD
                                + "/" + PREFIX_DET + iDet + "_" + PREFIX_PW + "_" ;//+ inner + "_" + bottom;  break;
      switch(iSideIO) {
        case(ID_SIDE_INNER): tdp = tdp + PREFIX_INNER + "_"; break;
        case(ID_SIDE_OUTER): tdp = tdp + PREFIX_OUTER + "_"; break;
        default:  DebugN(">>-----> fmdBase_buildLogicalPathName(): Error iSideIO not found ", iSideIO);  break;}
      switch(iSideTB) {
        case(ID_SIDE_TOP):   tdp = tdp + PREFIX_TOP;         break;
        case(ID_SIDE_BOTTOM):tdp = tdp + PREFIX_BOTTOM;      break;
        default:  DebugN(">>-----> fmdBase_buildLogicalPathName(): Error iSideIO not found ", iSideTB);  break;}
      tdp = tdp + tPdp;
      break;}
      
    case(ID_TYPE_RACK_ONE): tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + "_" + PREFIX_INFR + "_" + PREFIX_RACK_ONE;  break; 
    case(ID_TYPE_RACK_TWO): tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + "_" + PREFIX_INFR + "_" + PREFIX_RACK_TWO;  break; 
    case(ID_TYPE_HVPS_CONTROLLER): tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + "_" + PREFIX_INFR + "_" + PREFIX_RACK_ONE + "/" +PREFIX_HVPS+ "_" + PREFIX_CONTROLLER;  break; 
    case(ID_TYPE_LVPS_CONTROLLER): tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + "_" + PREFIX_INFR + "_" + PREFIX_RACK_TWO + "/" +PREFIX_LVPS+ "_" + PREFIX_CONTROLLER;  break;

    default: DebugN(">>-----> fmdBase_buildLogicalPathName(): Error iType not found ");  break;}
  //DebugTN(">>-----> fmdBase_GetCoords(): Found device: ", tdp);
  return tdp;

  
  }

// string fmdBase_buildLogicalPathName(int iType,int module,int sector,int extra)
// {
// 	string dpN;
// 	string dpNStock;
// 	
// 	switch(iType)
// 	{
// 		case LDT_DETECTOR:
// 			dpN = MAINSYS+":"+LOGICALVIEWROOT;
// 			break;
// 		case LDT_MODULE:
// 			dpN = MAINSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module;
// 			break;
// 		case LDT_POWERSYSTEM:
// 			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM;
// 			break;
// 		case LDT_SECTOR:
// 			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector;
// 			break;
// 		case LDT_HVSEG:
// 			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"_"+LDW_HVSEG;
// 			break;
// 		case LDT_HVGRID:
// 			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GRID;
// 			break;
// 		case LDT_LVSEG:
// 			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"_"+( (extra == LDC_ISPOSITIVE) ? LDW_FEEPSEG : LDW_FEENSEG );
// 			break;
// 		case LDT_LVRO:
// 			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_READOUT;
// 			break;
// 		case LDT_LVROSEG:
// 			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_READOUT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+( (extra == LDC_ISLEFT) ? LDW_READOUTLEFT: LDW_READOUTRIGHT );
// 			break;
// 		case LDT_LIQUID:
// 			dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_LIQUID+"_"+LDW_LOOP;
// 			break;
// 		case LDT_GAS:
// 			dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS;
// 			break;
// 		case LDT_COOL:
// 			dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING;
// 			break;
// 		case LDT_LOOP:
// 			if(extra == LDC_ISCOOLINGLOOP) dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"_"+LDW_LOOP;
// 			if(extra == LDC_ISGASLOOP) dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP;
// 			break;
// 		case LDT_PRESSURE:
// 			switch(extra)
// 			{
// 				case LDC_ISMWPC:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_MWPCPRES;
// 					break; 	
// 				case LDC_ISBOX:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_SAFEPRES;
// 					break; 	
// 				case LDC_ISENVPRES:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"_"+LDW_ENVPRES;
// 					break; 	
// 				case LDC_ISGASMAIN:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"_"+LDW_PRESSURE;
// 					break; 	
// 			}
// 			break;
// 		case LDT_TEMPERATURE:
// 			switch(extra)
// 			{
// 				case LDC_ISFEROTEMP:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"_"+LDW_FEROTEMP+sector;
// 					break; 	
// 				case LDC_ISENVTEMP:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"_"+LDW_ENVTEMP;
// 					break; 	
// 			}
// 			break;
// 			
// 		case LDT_FLOW:
// 			switch(extra)
// 			{
// 				case LDC_ISBOXFLOWIN:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_GASBOXFLOWIN;
// 					break;
// 				case LDC_ISBOXFLOWOUT:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_GASBOXFLOWOUT;
// 					break;
// 				case LDC_ISMWPCFLOWIN:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_GASMWPCFLOWIN;
// 					break;
// 				case LDC_ISMWPCFLOWOUT:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_GASMWPCFLOWOUT;
// 					break;			
// 				case LDC_ISBOXFLOWD:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_GASBOXFLOWD;
// 					break;
// 				case LDC_ISMWPCFLOWD:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_GASMWPCFLOWD;
// 					break; 
// 			} 
// 			break;		
// 		case LDT_PURITY:
// 			dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_GASPURITY;
// 			break;
// 		case LDT_STATUS:
// 			switch(extra)
// 			{
// 				case LDC_ISGASMAIN:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"_"+LDW_STATUS;
// 					break; 	
// 			}
// 			break;
// 		case LDT_MIXER:
// 			switch(extra)
// 			{
// 				case LDC_ISGASMAIN:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"_"+LDW_MIXER;
// 					break; 	
// 			}
// 			break;
// 
// 		case LDT_INFRASTRUCTURE:
// 			dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE;
// 			break;
// 		case LDT_RACKS:
// 			switch(module)
// 			{
// 				case LDC_ISCR5X07:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07;
// 					break;
// 				case LDC_ISCR5X08:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08;
// 					break;
// 				case LDC_ISCR4Y11:
// 					dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_Y11;
// 					break;
// 				case LDC_ISI0:
// 					dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I0;
// 					break;
// 				case LDC_ISI1:
// 					dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I1;
// 					break;
// 				case LDC_ISI2:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I2;
// 					break;
// 			}
// 			break;
// 
// 		case LDT_MAINPS:
// 			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_Y11+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_Y11+"_"+LDW_MAINPS;
// 			break;
// 
// 		case LDT_LVPS:
// 			switch(module)
// 			{
// 				case 0:
// 				case 1:
// 					dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I2+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I2+"_"+LDW_LVPS+module;
// 					break;
// 				case 2:
// 					dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I1+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I1+"_"+LDW_LVPS+module;
// 					break;
// 			}
// 			break;
// 
// 		case LDT_PLC:
// 			switch(extra)
// 			{
// 				case LDC_ISLIQMAIN:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"_"+LDW_PLCLIQMAIN;
// 					break;
// 				case LDC_ISLIQPUMP:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"_"+LDW_PLCLIQPUMP;
// 					break;
// 				case LDC_ISLIQTRA:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"_"+LDW_PLCLIQTRA;
// 					break;
// 				case LDC_ISPHYSICAL:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"_"+LDW_PLCPHY;
// 					break;
// 				case LDC_ISLIQMOD:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"_"+LDW_PLCLIQMOD+module;
// 					break;
// 			}
// 			break;
// 
// 		case LDT_PLANT:
// 			switch(extra)
// 			{
// 				case LDC_ISCOOLPLANT:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_COOL;
// 					break;
// 				case LDC_ISLCSPLANT:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_LCS;
// 					break;
// 				case LDC_ISGASPLANT:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS;
// 					break;
// 				case LDC_ISTRANSPLANT:
// 					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_TRANSP;
// 					break;
// 			}
// 			break;
// 		case LDT_ENVIRONMENT:
// 			dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_ENVIRONMENT;
// 			break;
// 	}
// 	return(dpN);
// }

 string fmdBase_buildLogicalName(int iDet, int iType, int iSideIO, int iSideTB, int iCh)
 {
 	string dpN;
 	int i;
 		
 	dpN = fmdBase_buildLogicalPathName(iDet,iType, iSideIO,  iSideTB,  iCh);
 	i = strpos(dpN,":");
 	if (i>0)
 		dpN = substr(dpN,i+1);
 	return(dpN);
 }

// ----------------------------------------------------------------------------
// fmdBase_buildFSMName(string sType,int module,int sector,int extra)
/*
	This function return the name of FSM Node View
	
	F.A. ver 1.0   07/09/2006
	
	History
	
		07/09/2006 created.

*/
// string fmdBase_buildFSMName(int iType,int module,int sector,int extra)
// {
// 	string dpN;
// 	string dpNStock;
// 	
// 	switch(iType)
// 	{
// 		// CUs
// 		case LDT_DETECTOR:
// 			return( LDW_PREFIX+"_"+LDW_DETECTOR);
// 			break;
// 		case LDT_MODULE:
// 			return( LDW_PREFIX+"_"+LDW_MODULE+module);
// 			break;
// 		case LDT_POWERSYSTEM:
// 			return( LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM);
// 			break;
// 		case LDT_SECTOR:
// 			return( LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector);
// 			break;
// 		case LDT_LVRO:
// 			return( LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_READOUT);
// 			break;
// 		case LDT_GAS:
// 			return( LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS);
// 			break;
// 		case LDT_COOL:
// 			return( LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING);
// 			break;
// 		case LDT_INFRASTRUCTURE:
// 			return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE);
// 			break;
// 		case LDT_RACKS:
// 			switch(module)
// 			{
// 				case LDC_ISCR5X07:
// 					return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07);
// 					break;
// 				case LDC_ISCR5X08:
// 					return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08);
// 					break;
// 				case LDC_ISCR4Y11:
// 					return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_Y11);
// 					break;
// 				case LDC_ISI0:
// 					return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I0);
// 					break;
// 				case LDC_ISI1:
// 					return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I1);
// 					break;
// 				case LDC_ISI2:
// 					return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I2);
// 					break;
// 			}
// 			break;
// 		case LDT_ENVIRONMENT:
// 			return( LDW_PREFIX+"_"+LDW_ENVIRONMENT);
// 			break;
// 
// 
// 		// DUs
// 		case LDT_HVSEG:
// 			return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"_"+LDW_HVSEG);
// 			break;
// 		case LDT_HVGRID:
// 			return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GRID);
// 			break;
// 		case LDT_LVSEG:
// 			return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"_"+( (extra == LDC_ISPOSITIVE) ? LDW_FEEPSEG : LDW_FEENSEG ) );
// 			break;
// 		case LDT_LVROSEG:
// 			return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_READOUT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+( (extra == LDC_ISLEFT) ? LDW_READOUTLEFT: LDW_READOUTRIGHT ));
// 			break;
// 		case LDT_LIQUID:
// 			return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_LIQUID+"_"+LDW_LOOP);
// 			break;
// 		case LDT_LOOP:
// 			if(extra == LDC_ISCOOLINGLOOP) return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"_"+LDW_LOOP);
// 			if(extra == LDC_ISGASLOOP) return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP);
// 			break;
// 		case LDT_PRESSURE:
// 			switch(extra)
// 			{
// 				case LDC_ISMWPC:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_MWPCPRES );
// 					break; 	
// 				case LDC_ISBOX:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_SAFEPRES );
// 					break; 	
// 				case LDC_ISENVPRES:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"_"+LDW_ENVPRES );
// 					break; 	
// 				case LDC_ISGASMAIN:
// 					return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"_"+LDW_PRESSURE );
// 					break; 	
// 			}
// 			break;
// 		case LDT_TEMPERATURE:
// 			switch(extra)
// 			{
// 				case LDC_ISFEROTEMP:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"_"+LDW_FEROTEMP+sector );
// 					break; 	
// 				case LDC_ISENVTEMP:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"_"+LDW_ENVTEMP );
// 					break; 	
// 			}
// 			break;
// 		case LDT_STATUS:
// 			switch(extra)
// 			{
// 				case LDC_ISGASMAIN:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"_"+LDW_STATUS );
// 					break; 	
// 			}
// 			break;
// 		case LDT_MAINPS:
// 			return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_Y11+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_Y11+"_"+LDW_MAINPS );
// 			break;
// 		case LDT_LVPS:
// 			switch(module)
// 			{
// 				case 0:
// 				case 1:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I2+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I2+"_"+LDW_LVPS+module);
// 					break;
// 				case 2:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I1+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I1+"_"+LDW_LVPS+module);
// 					break;
// 			}
// 			break;
// 
// 		case LDT_PLC:
// 			switch(extra)
// 			{
// 				case LDC_ISLIQMAIN:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"_"+LDW_PLCLIQMAIN );
// 					break;
// 				case LDC_ISLIQPUMP:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"_"+LDW_PLCLIQPUMP );
// 					break;
// 				case LDC_ISLIQTRA:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"_"+LDW_PLCLIQTRA );
// 					break;
// 				case LDC_ISPHYSICAL:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"_"+LDW_PLCPHY );
// 					break;
// 				case LDC_ISLIQMOD:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"_"+LDW_PLCLIQMOD+module );
// 					break;
// 			}
// 			break;
// 
// 		case LDT_PLANT:
// 			switch(extra)
// 			{
// 				case LDC_ISCOOLPLANT:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_COOL );
// 					break;
// 				case LDC_ISLCSPLANT:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_LCS );
// 					break;
// 				case LDC_ISGASPLANT:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS );
// 					break;
// 				case LDC_ISTRANSPLANT:
// 					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_TRANSP );
// 					break;
// 			}
// 			break;
// 
// 	}
// 	return("");
// }
// =========================================== EOF ================================

