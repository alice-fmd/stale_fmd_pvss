// ------------------------------------------
#uses "hmpBaselibrary.ctl"

// Hmpid CAEN library 
// Author : A.FRANCO - INFN BARI ITALY
// Ver 1.1  18/11/2005   - 

// ------------------------------------------
/*			History

			25/08/2005	- Imported from previous projects
			26/09/2005 - Changed dpSetWait -> dpSet in the StockValues exit forced status 
			20/10/2005 - Change name of functions
			15/11/2005 - Add A new FwSetCaenChannel function without the Unlock of the status 
			19/11/2005 - Redesign the interface for the Stock Values (version with strings)
			             this changes eliminate the necessity of STOCK library
			21/11/2005 - Add the "Table" Functions that Load & Store all sets
			12/12/2005 - Modify the Trip Mask 
			14/12/2005 - Added the IGNORE Mask
			20/12/2005 - Modify the GetSystemName() with HVSYS
			21/12/2005 - Modified the color map for Status2Color function
			03/10/2006 - Modified the color map for ramping States
			07/11/2006 - Modified the prefix for Stock
			18/12/2006 - Added a delay to the unlock mechanism
			
			
	Functions List	
	---------------------------------------------------------------------------------	             
	string hmpCAEN_ChannelStatusDecode(int channelstatus,bool &error,bool &trip,
							bool &On,bool &Up,bool &Down)
	string hmpCAEN_ChannelStatus2Color( int status)
	void 	 hmpCAEN_CratesTimedControlCB(string dpe, int value)
	string hmpCAEN_CrateControlStatus(string dp)
	bool   hmpCAEN_FwCaenChannelSetStock(string sLogicalChannelName, string sHardwareDevice, int iSet, bool Unlock)
	bool   hmpCAEN_LoadFwCaenChannelStock(string sLogicalChannelName, int iSet, dyn_string &dsDpEl, dyn_float &dfValues)
	bool   hmpCAEN_StoreFwCaenChannelStock(string sLogicalChannelName, int iSet, dyn_string dsDpEl, dyn_float dfValues)
	bool   hmpCAEN_LoadFwCaenChannelStockTable(string sLogicalChannelName, dyn_dyn_string &dsDpEl, dyn_dyn_float &dfValues)
	bool   hmpCAEN_StoreFwCaenChannelStockTable(string sLogicalChannelName, dyn_dyn_string dsDpEl, dyn_dyn_float dfValues)
	int  	 hmpCAEN_GetFwCaenChannelStockNumber(string sLogicalChannelName, string sHardwareDevice, string &sSetNumber)
	bool 	 hmpCAEN_GetEnabledStockFlag(string sLogicalChannelName)
	void   hmpCAEN_SetEnabledStockFlag(string sLogicalChannelName, bool bFlag)

	int 	 __hmpCAEN_CreateStockDpt()

  ---------------------------------------------------------------------------------

*/

// ------------------------------------------
// Constants definition for Crate functions
const int NO_CONTROL_TRIGGER_TIME=20; // Seconds: timeout for the Crate connection Test
const int TIMED_FUNCTION_TIME=2; // Seconds for the recall of function time

// Definitions for Stock Exchange
const int GETSET_DELAYCYCLE=500;  // milliseconds to the while cycle
const int GETSET_TIMEOUT=30; // n x GETSET_DELAYCYCLE
const int UNLOCK_DELAYCYCLE=2;// seconds waiting for the unlock this prevents a double st.change
const int STOCK_SET_ITEMS = 7;
const string STOCKDPT_PREFIX = "Hmp_";
const string STOCKDP_PREFIX = "hmp_hvSt_";
const string STOCKDPTYPENAME = "Hmp_fwCaenChannelStockSet";
const string STOCKPARAMSEPARATOR = ";";
const string STOCKVALUESEPARATOR = "=";
const bool DO_FEEDBACK_READ = true; // For disable the ReadBack controls for Settings put FALSE 


/* Status Bit Word for HV/LV Channels (Cfr. OPC Server v.2.X manual)
 0 : ON
 1 : Ramp Up
 2 : Ramp Down
 3 : Over Current
 4 : Over Voltage
 5 : Under Voltage
 6 : External Trip
 7 : Over HV Max
 8 : External Disabled
 9 : Internal Trip
10 : Calibration Error
11 : Unplugged
12 : Under Current 
13 : Over Voltage Protection
14 : Power Fail
15 : Temperature Error
------------------------------*/

// Constants for the Status detection masks
// Old Mask -->const int CAEN_TRIPMASK = 0x0B2B8;   // HVMax | ITrip | OvC | UnV | OvV | Ovp | Unc | Terr 
const int CAEN_TRIPMASK = 0x02280;   // Ovp | ITrip | HVMax  
// Old Mask -->const int CAEN_ERRORMASK = 0x00540;  // Cal | ETrip | EDis |  
const int CAEN_ERRORMASK = 0x08540;  // Cal | ETrip | EDis |  Terr
const int CAEN_NOCONTROLMASK = 0x04800;  //  Unp | PwFail  
const int CAEN_OFFMASK = 0x00000;     // This Mask Off,On,RampUp,RampDwn
const int CAEN_ONMASK = 0x00001;      // works with the XOR operation ...
const int CAEN_RAMPUPMASK = 0x00003;    
const int CAEN_RAMPDOWNMASK = 0x00005;    
const int CAEN_IGNOREDMASK = 0x01038;  //  OvC | UnV | OvV |  Unc    



// ============================================================================
//                     SECTION RELATED TO GENERAL CAEN USE FUNCTIONS
// ============================================================================

// ------------------------------------------
// hmpCAEN_ChannelStatusDecode : Decode the status word
//
// Ver 1.0 25/8/2005
//
// this function return the string description of the channel status
// and sets some boolean vars
//
// History
// 20-10-2005 - Add Power Fail Error condition
//
string hmpCAEN_ChannelStatusDecode(int channelstatus,bool &error,bool &trip,bool &On,bool &Up,bool &Down)
{

bool 	OvC,OvV,UnV,ETrip,HVMax,EDis,ITrip,Cal,Unp,Unc,Ovp,Terr,Pwfail;
string mes;

	On = getBit(channelstatus, 0 );
	Up = getBit(channelstatus, 1 );
	Down = getBit(channelstatus, 2 );
	OvC = getBit(channelstatus, 3 );
	OvV = getBit(channelstatus, 4 );
	UnV = getBit(channelstatus, 5 );
	ETrip = getBit(channelstatus, 6 );
	HVMax = getBit(channelstatus, 7 );
	EDis = getBit(channelstatus, 8 );
	ITrip = getBit(channelstatus, 9 );
	Cal = getBit(channelstatus, 10 );
	Unp = getBit(channelstatus, 11 );
	Unc = getBit(channelstatus, 12 );
	Ovp = getBit(channelstatus, 13 );
	Pwfail = getBit(channelstatus, 14 );
	Terr = getBit(channelstatus, 15 );

	// Trip condition
	trip = HVMax | ITrip | OvC | UnV | OvV | Ovp | Unc | Terr ;

	// Never goes in Error !
	error = false;

  // Made the message
	mes = (On)?"Channel On":"Channel Off";
	mes = (On && Up)?"Ramping Up":mes;
	mes = (On && Down)?"Ramping Down":mes;
	mes = (trip && Down)?"Tripping Down":mes;
	mes = (trip && !Down)?"Tripped":mes;
	mes += (OvC)?":Over Current":"";
	mes += (OvV)?":Over Voltage":"";
	mes += (Unc)?":Under Current":"";
	mes += (UnV)?":Under Voltage":"";
	mes += (Ovp)?":Over protection":"";
	mes += (Terr)?":Temperature":"";
	mes += (HVMax)?"HV Max":"";

	mes = (EDis)?"External Disabled":mes;
	mes = (ETrip)?"External Trip":mes;
	mes = (Unp)?"Channel Unplugged":mes;
	mes = (Cal)?"Calibration Error":mes;
	mes = (Pwfail)?"Power Fail Error":mes;

	return mes;
}


// ------------------------------------------
// hmpCAEN_ChannelStatus2Color: Decode the status word
//
// Ver 1.0 25/8/2005
//
// this function return the colour string of the channel status
//
// History
// 20-10-2005 - Change the color for ramping
// 21/12/2005 - Remap all colors for errors
//
/* Status Bit Word for HV/LV Channels (Cfr. OPC Server v.2.X manual)
 0 : ON
 1 : Ramp Up
 2 : Ramp Down
 3 : Over Current
 4 : Over Voltage
 5 : Under Voltage
 6 : External Trip
 7 : Over HV Max
 8 : External Disabled
 9 : Internal Trip
10 : Calibration Error
11 : Unplugged
12 : Under Current 
13 : Over Voltage Protection
14 : Power Fail
15 : Temperature Error
------------------------------- */
string hmpCAEN_ChannelStatus2Color( int status)
{
	switch(status)
	{
		case 0: // Chan OFF
			return("hmpCAENChannelOFF");
			break;
		case 1: // Chan ON
			return("hmpCAENChannelON");
			break;
		case 3:  // Ramping
			return("hmpCAENChannelRUMPUP");
			break;
		case 5:
			return("hmpCAENChannelRUMPDW");
			break;
	}

	// The unplugged condition colour
	if(getBit(status,8))
		return("hmpCAENChannelDISABLED");

	if(getBit(status,11))
		return("hmpCAENChannelUNPLUG");

	// The trip condition colour (OvC OvV UnV UnC)
	if(getBit(status,3) || getBit(status,4) || getBit(status,5) || getBit(status,12) )
		return("hmpCAENChannelOVC");

	// The trip condition colour (HVMax, ITRip, Ovp)
	if( getBit(status,7) || getBit(status,9) || getBit(status,13))
		return("hmpCAENChannelTRIP");

	// The Error condition colour (ETrip, PWf, Cal, Terr )
	if(getBit(status,10) || getBit(status,6) || getBit(status,15) || getBit(status,14))
		return("hmpCAENChannelERROR");

	return("FwDead");
}


// ============================================================================
//                     SECTION RELATED TO THE SY1527 CAEN CRATE
// ============================================================================

// ----------------------------------------------------------------------------
// hmpCAEN_CratesTimedControlCB
/*
	This function performs the control of HV Crates every 5 seconds
        and compare the time stamp of the swRelease, this do the test 
        of connection
	
	F.A. ver 1.0   28/10/2004
	
	History
	
		28/10/2004 imported from HMPIDDCS3_3 proj.
		
*/
hmpCAEN_CratesTimedControlCB(string dpe, int value)
{
	int i;
	time tLastChange, tNow;
	dyn_string dsCratesDP;
	
	// get the current time 
	tNow = getCurrentTime();
  
	//Get all SY1527 crates
	dsCratesDP = dpNames("*","FwCaenCrateSY1527");

	// for each SY test the last change time of SwRelease
	for(i=1; i<= dynlen(dsCratesDP); i++)
	{
		dpGet( dsCratesDP[i]+".Information.SwRelease:_original.._stime", tLastChange);
		if(period(getCurrentTime() - tLastChange) > NO_CONTROL_TRIGGER_TIME)
		{
			dpSet(dsCratesDP[i]+".Information.SwRelease:_original.._value","LOST_CONTROL",
						dsCratesDP[i]+".:_original.._aut_inv", true);
			// Here we put some Messages dispatch ....
			// dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|I|L|"+"Lost Connection with Crate:"+dsCratesDP[i]+"|");
		}
	}
	return;
}

// ----------------------------------------------------------------------------
// hmpCAEN_CrateControlStatus
/*
	This function performs the control of CAEN Crates 
        verify the error/warning conditions
	
	F.A. ver 1.0   28/10/2004
	
	History
	
		28/10/2004 imported from HMPIDDCS3_3 proj.
		
*/
string hmpCAEN_CrateControlStatus(string dp)
{

	int iPanIn, iPanOut,i,fault;
	string sFan,sPwS;
	dyn_string param;
	string resp = "READY";
	
	// Gets the global status indicators
	dpGet(dp+".FrontPanInP.FrontPanIn:_original.._value",iPanIn,
				dp+".PWstatus.HvPwSM:_original.._value",sPwS,	
				dp+".FanStatus.FanStat:_original.._value",sFan,
				dp+".FrontPanOutP.FrontPanOut:_original.._value",iPanOut);
				
	// Test conditions			
	if(getBit(iPanIn, 2 )) {
		// Kill Error severe
		// dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + " External Kill Signal received" +"|");
		resp = "EXTERNAL_KILL";
	}
				 
	if(getBit(iPanIn, 3 )) {
  	// Interlock Error severe
		// dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + " External Interlock Signal received" +"|");
		resp = "EXTERNAL_INTERLOCK";
	}

	// AC Severe Error
	param=strsplit(sPwS,":");
	if(param[1] == "-1") {
		// Generate the error 
		// dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + " SEVERE ERROR AC Failure !" +"|");

		// AC/DC Module stat
		fault = 0;
		for(i=2;i<6;i++) 
			if(param[i] == "-1") {
				fault++;
				// Generate the error message
				// dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + ": ERROR AC/DC module n."+(i-1)+" fault !"  +"|");
			}

		if(fault > 1) {
			// error severe
		  // dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + " SEVERE ERROR more than 1 AC/DC module failure. Kill All !" +"|");
		  resp = "ERROR_AC_FAILURE";
  	}	else if(fault == 1) {
			// error warning
			// dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + ": WARNING 1 AC/DC module failure!" +"|");
			resp = "WARNING_AC_FAILURE";
		}
	}

	// Fan Status
	param=strsplit(sFan,":");

	// fan status stat
	for(i=1;i<13;i=i+2) 
		if(param[i] == "-1") {
			fault++;
			// Generate the error message
			// dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + ": ERROR FAN n."+((i+1)/2)+" fault !"+"|");
		}

	if(fault > 2) {
		// error severe
		// dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + ": SEVERE ERROR FAN module failure. Kill All !"+"|");
		resp = "ERROR_FAN_FAILURE";
	} 
	else if(fault > 0) {
		// dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + ": WARNING 1 FAN module failure!"+"|");
		resp = "WARNING_FAN_FAILURE";
	}
		
	// Pan Out
	if(getBit(iPanOut, 9 )) {
		// Over temperature warning
		// dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + ": WARNING at least une board in Over Temperature !"+"|");
		resp = "WARNING_TEMPERATURE";
	}
	if(getBit(iPanOut, 8 )) {
		// Fan Failure Severe error
		// dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|E|H|"+"Crate " + dp + ": WARNING Fan failure error !"+"|");
		resp = "WARNING_FAN_FAILURE";
	}
	
	return(resp);
}

// ============================================================================
//                     SECTION RELATED TO STOCK EXCHANGE
// ============================================================================

// ----------------------------------------------------------------------------
// hmpCAEN_FwCaenChannelSetStock
/*
	This function performs the set into a CAEN Channel of all parameters retrived
	from the StockSet Dp. 
	
	F.A. ver 1.0   19/11/2005
	
	History
	
		19/11/2005 created ex novo.
	
	Parameters : string sLogicalChannelName := Name of channel from the logical view
	                                           if it's == "" than means that the good
	                                           name is the Hardware view Name
	             string sHardwareDevice := Name of channel from hardware view used by
	                                       FSM DU Scripts (subDomain parameter)
	             int iSet := Number of set that will be stored into Ch Dp elements
	             bool Unlock := if True force a Status Change, introduced to unlock
	                            the DUs scripts in case of SETs actions.
	                            
	Return : True if all works fine 
	  		
*/
bool hmpCAEN_FwCaenChannelSetStock(string sLogicalChannelName, string sHardwareDevice, int iSet, bool Unlock)
{
	string sStockName,sAppoggio;
  dyn_string dsSets;
  dyn_string dsDpElset,dsDpElread;
  dyn_float dfElVal,dfElReadBack;  
	int i,j,num,iDelay;
	bool bWriteFlag = true;
	dyn_errClass err;
			
	// build the name
	if(sHardwareDevice == "") 
	{
		// Convert device into alias 
		sHardwareDevice = dpAliasToName(HVSYS+":"+sLogicalChannelName);
		if(sHardwareDevice == "") {
			DebugN("hmpCAEN_FwCaenChannelSetStock : sHardwareDevice not found ! ("+sLogicalChannelName+")");
			return(false); } // Nothing to do NO LOGICAL VIEW 
	}	else
	if(sLogicalChannelName == "") // This is for FSM that pass the HW device for DUs
	{
		// Convert device into alias 
		sHardwareDevice = sHardwareDevice + ( (strpos(sHardwareDevice,".") > 0) ? "" : ".");
		sLogicalChannelName = dpGetAlias(sHardwareDevice);
		if(sLogicalChannelName == "") {
			DebugN("hmpCAEN_FwCaenChannelSetStock : sLogicalChannelName not found !");
			return(false); }
	}	
//	sStockName = getSystemName()+STOCKDP_PREFIX+sLogicalChannelName;
	sStockName = HVSYS+":"+STOCKDP_PREFIX+sLogicalChannelName;
	
	// Get the stock
	dpGet(sStockName + ".sets:_original.._value", dsSets);
	err = getLastError(); //test whether no errors
	if(dynlen(err) > 0)  {
			DebugN("hmpCAEN_FwCaenChannelSetStock : Get the stock error !");
			return(false); }
 
	// Verify the Set Number
	if (iSet > STOCK_SET_ITEMS || iSet <1 || dynlen(dsSets) < iSet ) {
			DebugN("hmpCAEN_FwCaenChannelSetStock : Set Number error ! ("+iSet+":->"+sLogicalChannelName+")");
			return(false); }

	// decode the params
	dsDpElset = strsplit(dsSets[iSet],STOCKPARAMSEPARATOR);
	num = dynlen(dsDpElset);
	for(i=1;i<=num;i++)
	{
		j = strpos(dsDpElset[i],STOCKVALUESEPARATOR);
		dfElVal[i] = substr(dsDpElset[i],j+1);
		sAppoggio = substr(dsDpElset[i],0,j);
		dsDpElset[i] = sHardwareDevice+"settings."+sAppoggio+":_original.._value";
		dsDpElread[i] = sHardwareDevice+"readBackSettings."+sAppoggio+":_original.._value";
	}
	// extract the delay
	if(strpos(dsDpElset[num],"delay") > 0)
	{
			iDelay = dfElVal[num];
			dynRemove(dfElVal,num);
			dynRemove(dsDpElset,num);
			dynRemove(dsDpElread,num);
			num--;
	}	
	// Finally Set the Caen Channel
	dpSetWait(dsDpElset,dfElVal);
	err = getLastError(); //test whether no errors
	if(dynlen(err) > 0) {
			DebugN("hmpCAEN_FwCaenChannelSetStock : Error setting the Caen Channel !");
			return(false); }
	dpSet(sStockName + ".lastStoredSet:_original.._value", iSet);
	
	// Start the feedBack loop to verify the Good setting
	i = 0; 

	// This inibits the FeedBack Read
	bWriteFlag = DO_FEEDBACK_READ;
	while( bWriteFlag )
	{
		delay(0 ,GETSET_DELAYCYCLE);
		dpGet(dsDpElread,dfElReadBack);
		err = getLastError(); //test whether no errors
		if(dynlen(err) > 0) return(false);
		if( ++i > GETSET_TIMEOUT) // more than 20 secs on waiting
		{
			//-- This rapresent a Mess
			DebugTN("hmpCAEN_FwCaenChannelSetStock : ALARM !!! TimeOut reached attempt to read parameters into:"+sLogicalChannelName);
			// This unlock the FSM ...
			if(Unlock)
			{
				dpGet(sHardwareDevice+"actual.status:_original.._value",j);		
				dpSetWait(sHardwareDevice+"actual.status:_original.._value",j);
			}		
			// Here we put some Messages dispatch ....
			// dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Channel|"+getUserName()+"|I|L|"+"Lost Connection with channel:"+device+"|");
			return(false);
		}
		bWriteFlag = false;
		for(j=1;j<=num;j++) bWriteFlag = bWriteFlag || !__hmpBase_FloatEqual(dfElVal[j],dfElReadBack[j]);  
	}	

	// And finally we wait for a stabilization time
	// if paramere Del > 0 it produce the delay and the set of status
	//        		 Del == 0 only set of state but not delay
	if(iDelay > 0)
	{
		DebugTN("I'm executing  a Programmed Delay for "+iDelay+ "sec. in device"+sHardwareDevice);
		delay(iDelay ,0);
	}

	// Force a status change
	if(Unlock)
	{
		dpGet(sHardwareDevice+"actual.status:_original.._value",j);		
		delay(UNLOCK_DELAYCYCLE ,0);
		dpGet(sHardwareDevice+"actual.status:_original.._value",i);		
		if(i == j)
			dpSetWait(sHardwareDevice+"actual.status:_original.._value",j);
	}
	return(true);
}

// ----------------------------------------------------------------------------
// hmpCAEN_LoadFwCaenChannelStock
/*
	This function performs the retriving of the parameters set from the StockSet Dp. 
	
	F.A. ver 1.0   19/11/2005
	
	History
	
		19/11/2005 created ex novo.
	
	Parameters : string sLogicalChannelName := Name of channel from the logical view
	             int iSet := Number of set that will be retrived 
							 dyn_string &dsDpEl := Array that will contains the Dp Elements, 
							                       rapresenting params
							 dyn_float &dfValues := Array that will contains the related
							                        parameters values
	                            
	Return : True if all works fine 
	  		
*/
bool hmpCAEN_LoadFwCaenChannelStock(string sLogicalChannelName, int iSet, dyn_string &dsDpEl, dyn_float &dfValues)
{
	string sStockName;
  dyn_string dsSets;
	int i,j;
	dyn_errClass err;
			
	// build the name
	// sStockName = getSystemName()+STOCKDP_PREFIX+sLogicalChannelName;
	sStockName = HVSYS+":"+STOCKDP_PREFIX+sLogicalChannelName;

	// Get the stock
	dpGet(sStockName + ".sets:_original.._value", dsSets);
	err = getLastError(); //test whether no errors
	if(dynlen(err) > 0) return(false);

	// Verify the Set Number
	if (iSet > STOCK_SET_ITEMS || iSet <1 || dynlen(dsSets) < iSet ) return(false);

	// decode the params
	dsDpEl = strsplit(dsSets[iSet],STOCKPARAMSEPARATOR);
	for(i=1;i<=dynlen(dsDpEl);i++)
	{
		j = strpos(dsDpEl[i],STOCKVALUESEPARATOR);
		dfValues[i] = substr(dsDpEl[i],j+1);
		dsDpEl[i] = substr(dsDpEl[i],0,j);
	}
	return(true);
}

// ----------------------------------------------------------------------------
// hmpCAEN_StoreFwCaenChannelStock
/*
	This function performs the storing of the parameters set to the StockSet Dp. 
	
	F.A. ver 1.0   19/11/2005
	
	History
	
		19/11/2005 created ex novo.
	
	Parameters : string sLogicalChannelName := Name of channel from the logical view
	             int iSet := Number of set that will be Stored 
							 dyn_string dsDpEl := Array that contains the Dp Elements, 
							                      rapresenting params
							 dyn_float dfValues := Array that contains the related
							                       parameters values
	                            
	Return : True if all works fine 
	  		
*/
bool hmpCAEN_StoreFwCaenChannelStock(string sLogicalChannelName, int iSet, dyn_string dsDpEl, dyn_float dfValues)
{
	string sStockName,sAppoggio;
  dyn_string dsSets;
	int i,num;
	dyn_errClass err;
			
	// build the name
//	sStockName = getSystemName()+STOCKDP_PREFIX+sLogicalChannelName;
	sStockName = HVSYS+":"+STOCKDP_PREFIX+sLogicalChannelName;

	// Verify the Set Number
	if (iSet > STOCK_SET_ITEMS || iSet <1) return(false);
	// Verify parameters
	num = dynlen(dsDpEl);
	if (num == 0) return(false);
	
	// Get the stock
	if(!dpExists(sStockName))
	{
		// This means that the dp Not Exist : Crate It
		dpCreate(STOCKDP_PREFIX+sLogicalChannelName, STOCKDPTYPENAME);
		err = getLastError(); //test whether no errors
		if(dynlen(err) > 0)
		{
			errorDialog(err);
			if( __hmpCAEN_CreateStockDpt() == 0) return(false); // and try to crete the DpType
			dpCreate(STOCKDP_PREFIX+sLogicalChannelName, STOCKDPTYPENAME); // and try to recreate DP
			err = getLastError(); //test whether no errors
			if(dynlen(err) > 0) return(false);
		}
		dsSets = makeDynString();
	}
	else
	{
		dpGet(sStockName + ".sets:_original.._value", dsSets);
		err = getLastError(); //test whether no errors
		if(dynlen(err) > 0) return(false);
	}

	// code the params
	for(i=1;i<=num;i++) sAppoggio = sAppoggio + dsDpEl[i] + STOCKVALUESEPARATOR +dfValues[i] + STOCKPARAMSEPARATOR;
	dsSets[iSet] = substr(sAppoggio,0,strlen(sAppoggio)-1);
		
	// write into Stock dp
	dpSetWait(sStockName + ".sets:_original.._value", dsSets);
	err = getLastError(); //test whether no errors
	if(dynlen(err) > 0) return(false);
	
	// End
	return(true);
	
}
// ----------------------------------------------------------------------------
// hmpCAEN_LoadFwCaenChannelStockTable
/*
	This function performs the retriving of the parameters set from the 
	StockSet Dp. for all the Sets
	
	F.A. ver 1.0   21/11/2005
	
	History
	
		21/11/2005 created ex novo.
	
	Parameters : string sLogicalChannelName := Name of channel from the logical view
							 dyn_dyn_string &dsDpEl := Array that will contains the Dp Elements, 
							                       rapresenting params
							 dyn_dyn_float &dfValues := Array that will contains the related
							                        parameters values
	                            
	Return : True if all works fine 
	  		
*/
bool hmpCAEN_LoadFwCaenChannelStockTable(string sLogicalChannelName, dyn_dyn_string &dsDpEl, dyn_dyn_float &dfValues)
{
	string sStockName;
  dyn_string dsSets;
	int k,i,j,iNset;
	dyn_errClass err;
			
	// build the name
//	sStockName = getSystemName()+STOCKDP_PREFIX+sLogicalChannelName;
	sStockName = HVSYS+":"+STOCKDP_PREFIX+sLogicalChannelName;

	dynClear(dfValues);
	dynClear(dsDpEl);
	
	// Get the stock
	dpGet(sStockName + ".sets:_original.._value", dsSets);
	err = getLastError(); //test whether no errors
	if(dynlen(err) > 0) return(false);

	// reset the arrays
	dynClear(dsDpEl);
	dynClear(dfValues);
	
	// for each sets
	iNset = dynlen(dsSets);
	for(k=1;k<=iNset;k++)
	{
		// decode the params
		dsDpEl[k] = strsplit(dsSets[k],STOCKPARAMSEPARATOR);
		for(i=1;i<=dynlen(dsDpEl[k]);i++)
		{
			j = strpos(dsDpEl[k][i],STOCKVALUESEPARATOR);
			dfValues[k][i] = substr(dsDpEl[k][i],j+1);
			dsDpEl[k][i] = substr(dsDpEl[k][i],0,j);
		}
	}
	return(true);
}

// ----------------------------------------------------------------------------
// hmpCAEN_StoreFwCaenChannelStockTable
/*
	This function performs the storing of the parameters set to the 
	StockSet Dp. for all sets
	
	F.A. ver 1.0   21/11/2005
	
	History
	
		21/11/2005 created ex novo.
	
	Parameters : string sLogicalChannelName := Name of channel from the logical view
							 dyn_dyn_string dsDpEl := Array that contains the Dp Elements, 
							                      rapresenting params
							 dyn_dyn_float dfValues := Array that contains the related
							                       parameters values
	                            
	Return : True if all works fine 
	  		
*/
bool hmpCAEN_StoreFwCaenChannelStockTable(string sLogicalChannelName, dyn_dyn_string dsDpEl, dyn_dyn_float dfValues)
{
	string sStockName,sAppoggio;
  dyn_string dsSets;
	int j,i,iNset,num;
	dyn_errClass err;
			
	// build the name
//	sStockName = getSystemName()+STOCKDP_PREFIX+sLogicalChannelName;
	sStockName = HVSYS+":"+STOCKDP_PREFIX+sLogicalChannelName;

	// Verify number off sets
	iNset = dynlen(dsDpEl);
	if (iNset == 0) return(false);
	
	// Get the stock
	if(!dpExists(sStockName))
	{
		// This means that the dp Not Exist : Crate It
		dpCreate(STOCKDP_PREFIX+sLogicalChannelName, STOCKDPTYPENAME);
		err = getLastError(); //test whether no errors
		if(dynlen(err) > 0)
		{
			errorDialog(err);
			if( __hmpCAEN_CreateStockDpt() == 0) return(false); // and try to crete the DpType
			dpCreate(STOCKDP_PREFIX+sLogicalChannelName, STOCKDPTYPENAME); // and try to recreate DP
			err = getLastError(); //test whether no errors
			if(dynlen(err) > 0) return(false);
		}
	}
	dsSets = makeDynString();

	// For each set
	for(j=1;j<=iNset;j++)
	{
		// Verify number of parma
		num = dynlen(dsDpEl[j]);
		if (num >0 ) 
		{
			// code the params
			sAppoggio = "";
			for(i=1;i<=num;i++)
				sAppoggio = sAppoggio + dsDpEl[j][i] + STOCKVALUESEPARATOR +dfValues[j][i] + STOCKPARAMSEPARATOR;
			dsSets[j] = substr(sAppoggio,0,strlen(sAppoggio)-1);
		}
	}
	// write into Stock dp
	dpSetWait(sStockName + ".sets:_original.._value", dsSets);
	err = getLastError(); //test whether no errors
	if(dynlen(err) > 0) return(false);
	
	// End
	return(true);

}



// ----------------------------------------------------------------------------
// __hmpCAEN_CreateStockDpt (internal use only)
/*
	This function create the StockSet DpType. 
	
	F.A. ver 1.0   19/11/2005
	
	History
	
		19/11/2005 created ex novo.
	  		
*/
int __hmpCAEN_CreateStockDpt()
{

int n;
dyn_dyn_string depes;
dyn_dyn_int depei;

	// Create the data type
	depes[1] = makeDynString (STOCKDPTYPENAME,"");
	depes[2] = makeDynString ("","enabled");
	depes[3] = makeDynString ("","sets");
	depes[4] = makeDynString ("","lastStoredSet");

	depei[1] = makeDynInt (DPEL_STRUCT);
	depei[2] = makeDynInt (0,DPEL_BOOL);
	depei[3] = makeDynInt (0,DPEL_DYN_STRING);
	depei[4] = makeDynInt (0,DPEL_INT);
	// Create the datapoint type
	n = dpTypeCreate(depes,depei);
	DebugTN ("__hmpCAEN_StoreFwCaenChannelStock : created HMPID/Stock DPT : ",n);
	return(n);
}

// -------------------------

// ----------------------------------------------------------------------------
// hmpCAEN_GetFwCaenChannelStockNumber
/*
	This function performs the attempt to define what is the stock number 
	stored into the FwCaenChannel DP  
	
	F.A. ver 1.0   19/11/2005
	
	History
	
		19/11/2005 created ex novo.
	
	Parameters : string sLogicalChannelName := Name of channel from the logical view
	             int iSet := Number of set that will be Stored 
							 dyn_string dsDpEl := Array that contains the Dp Elements, 
							                      rapresenting params
							 dyn_float dfValues := Array that contains the related
							                       parameters values
	                            
	Return : True if all works fine 
	  		
*/
int hmpCAEN_GetFwCaenChannelStockNumber(string sLogicalChannelName, string sHardwareDevice, string &sSetNumber)
{
  DebugTN("hmpCAEN_GetFwCaenChannelStockNumber here we go...logi, hard",sLogicalChannelName,sHardwareDevice);
	string sStockName;
	dyn_string dsSets;
	int iSet;
	dyn_errClass err;
	
	sSetNumber = "";

	// build the name
	if(sLogicalChannelName == "") // This is for FSM that pass the HW device for DUs
	{
		// Convert device into alias 
		sLogicalChannelName = dpGetAlias(sHardwareDevice+".");
		if(sLogicalChannelName == "") return(0); // Nothing to do NO LOGICAL VIEW
	}	
//	sStockName = getSystemName()+STOCKDP_PREFIX+sLogicalChannelName;
	sStockName = HVSYS+":"+STOCKDP_PREFIX+sLogicalChannelName;
//	sStockName = sLogicalChannelName;
        
	dpGet(sStockName + ".lastStoredSet:_original.._value", iSet);
	err = getLastError(); //test whether no errors
	if(dynlen(err) > 0) return(0);

	sSetNumber = iSet; // String parameter for the FSM DUs scripts

	return(iSet);
	
	/* -----------------  Pay attention ------------
  In case of disconnected of SCADA and changed of 
  the configuration out of the STOCK mechanism 
  the stock set value could be not exactly match
  the "lastStoredSet" value. In this scenario 
  could be necessary try to detect the stock number 
  comparing the values stored.
 			 
	float I0,V0,RU,RD;TR;
	dyn_string dsElem,dsSets;
	dyn_string dsCatch;
	dyn_float dfVal,dfCatch;
	dyn_int diScore;
	int i,j,k;
			
	dsCatch = makeDynString("v0,i0,rUp,rDw,tripTime");
	dfCatch = makeDynFloat(0.0,0.0,0.0,0.0,0.0);
	diScore = makeDynString(0,0,0,0,0,0,0);
	
	// read the StockParam	& Actual set
	dpGet(sStockName + ".sets:_original.._value", dsSets,
				getSystemName()+sLogicalChannelName + ".readBackSettings.v0:_original.._value", dfCatch[1],
				getSystemName()+sLogicalChannelName + ".readBackSettings.i0:_original.._value", dfCatch[2],
				getSystemName()+sLogicalChannelName + ".readBackSettings.rUp:_original.._value", dfCatch[3],
				getSystemName()+sLogicalChannelName + ".readBackSettings.rDw:_original.._value", dfCatch[4],
				getSystemName()+sLogicalChannelName + ".readBackSettings.tripTime:_original.._value", dfCatch[5]);
	err = getLastError(); //test whether no errors
	if(dynlen(err) > 0) return(false);
 
 	// An probabilistich approch to solve this ... last hope
	for(i=1;i<=dynlen(dsSets);i++) {
		dsElem = strsplit(dsSet[i],STOCKPARAMSEPARATOR);
		for(j=1;j<=dynlen(dsElem);j++) {
			dfVal[j] = substr(dsElem[j],strpos(dsElem[j],STOCKVALUESEPARATOR)+1);
			dsElem[j] = substr(dsElem[j],0,strpos(dsElem[j],STOCKVALUESEPARATOR)-1);
			k = dynContains(dsCatch,dsElem[j]);
			if(k > 0) diScore[i] = ( (__hmpBase_FloatEqual(dfCatch[k],dfVal[j]) ? diScore[i]+1 : -99);
		}
	}
	iSet = dynContains(diScore,dynMax(diScore));
	sSetNumber = iSet;
	return(iSet);
	-------------------------------------------------------------   */

}

// ----------------------------------------------------------------------------
// hmpCAEN_GetEnabledStockFlag
/*
	This function gets the enabled flag from the StockSet Dp. 
	
	F.A. ver 1.0   19/11/2005
	
	History
	
		19/11/2005 created ex novo.
	
	Parameters : string sLogicalChannelName := Name of channel from the logical view
	                            
	Return : The enabled flag 
	  		
*/
bool hmpCAEN_GetEnabledStockFlag(string sLogicalChannelName)
{
string sStockName;
bool bFlag;
dyn_errClass err;

	// build the name
//	sStockName = getSystemName()+STOCKDP_PREFIX+sLogicalChannelName;
	sStockName = HVSYS+":"+STOCKDP_PREFIX+sLogicalChannelName;

	// Get the stock enabled
	dpGet(sStockName + ".enabled:_original.._value", bFlag);
	err = getLastError(); //test whether no errors
	if(dynlen(err) > 0) return(false);

	return(bFlag);
}

// ----------------------------------------------------------------------------
// hmpCAEN_SetEnabledStockFlag
/*
	This function sets the enabled flag to the StockSet Dp. 
	
	F.A. ver 1.0   19/11/2005
	
	History
	
		19/11/2005 created ex novo.
	
	Parameters : string sLogicalChannelName := Name of channel from the logical view
	             bool bFlag := Value of enabled flag (true = enabled, false = disabled)               

	Return : nothing 
	  		
*/
void hmpCAEN_SetEnabledStockFlag(string sLogicalChannelName, bool bFlag)
{
string sStockName;
dyn_errClass err;

	// build the name
//	sStockName = getSystemName()+STOCKDP_PREFIX+sLogicalChannelName;
	sStockName = HVSYS+":"+STOCKDP_PREFIX+sLogicalChannelName;

	// Set the stock enabled
	if(!dpExists(sStockName))
	{
		// This means that the dp Not Exist : Crate It
		dpCreate(sStockName, STOCKDPTYPENAME);
		err = getLastError(); //test whether no errors
		if(dynlen(err) > 0) return(false);
	}
	dpSetWait(sStockName + ".enabled:_original.._value", bFlag);
	err = getLastError(); //test whether no errors
	if(dynlen(err) > 0) return(false);

	return;
}

// ============================================================================
// OBSOLETE FUNCTIONS : WILL BE DELETED IN NEXT VERSION
// ============================================================================

// ----------------------------------------------------------------------------
// hmpCAEN_ChannelSetUnlock
/*
	This function performs the set of a parameter stock into a HV Channel and 
	unlock the device unit with read/write of status word
	
	F.A. ver 1.0   12/11/2004
	
	History
	
		12/11/2004 created.
		22/11/2004 add the writing of status to forcing the exit after a delay
		12/04/2005 modified the -1 delay param
		15/04/2005 put the FeedBack Of Write->Read dpset
		25/08/2005 Now it work with the HV/LV CAEN Channels
*/
hmpCAEN_ChannelSetUnlock(string device,float v0, float i0,float rUp, float rDown, float trip,int del)
{
	int status, iCount;
	float rbv0,rbi0, vnew,inew;

	vnew = v0;
	inew = i0;

	dpSetWait(device+".settings.v0:_original.._value",vnew,
			device+".settings.i0:_original.._value",inew,
			device+".settings.rUp:_original.._value",rUp,
			device+".settings.rDwn:_original.._value",rDown,
			device+".settings.tripTime:_original.._value",trip);

//DebugN(">>>--hmpidCAENchannelset-->>",device+".settings.v0",vnew);

	// Start the feedBack loop to verify the Good setting
	rbv0=0;
	rbi0=0;
	iCount = 0;
	while( !__hmpBase_FloatEqual(vnew,rbv0) || !__hmpBase_FloatEqual(inew,rbi0) )
	{
		delay(0 ,GETSET_DELAYCYCLE);
		dpGet(device+".readBackSettings.v0:_original.._value",rbv0,
		    device+".readBackSettings.i0:_original.._value",rbi0);

		if( ++iCount > GETSET_TIMEOUT) // more than 20 secs ow waiting
		{
			//-- This rapresent a Mess
			DebugN(">>>-hmpidCAENchannelset-> ALARM !!! hmpidCAENchannelset : TimeOut reached");
			// This unlock the FSM ...
			dpGet(device+".actual.status:_original.._value",status);		
			dpSetWait(device+".actual.status:_original.._value",status);
			// Here we put some Messages dispatch ....
			// dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Channel|"+getUserName()+"|I|L|"+"Lost Connection with channel:"+device+"|");
			return;
		}
	}
	// And finally we wait for a stabilization time
	// if paramere Del > 0 it produce the delay and the set of status
	//        		 Del == 0 only set of state but not delay
	//        		 Del == -1 no forcing of status, we wait for a stat change
	if(del >= 0)
	{
		delay(del ,0);
		dpGet(device+".actual.status:_original.._value",status);		
		dpSetWait(device+".actual.status:_original.._value",status);
	}
}

// ----------------------------------------------------------------------------
// hmpCAEN_ChannelSet
/*
	This function performs the set of a parameter stock into a HV Channel 
	without forcing the status change
	
	F.A. ver 1.0   17/11/2005
	
	History
	
		17/11/2005 created.
*/
hmpCAEN_ChannelSet(string device,float v0, float i0,float rUp, float rDown, float trip,int del)
{
	int status, iCount;
	float rbv0,rbi0, vnew,inew;

	vnew = v0;
	inew = i0;

	dpSetWait(device+".settings.v0:_original.._value",vnew,
			device+".settings.i0:_original.._value",inew,
			device+".settings.rUp:_original.._value",rUp,
			device+".settings.rDwn:_original.._value",rDown,
			device+".settings.tripTime:_original.._value",trip);

	// Start the feedBack loop to verify the Good setting
	rbv0=0;
	rbi0=0;
	iCount = 0;
	while( !__hmpBase_FloatEqual(vnew,rbv0) || !__hmpBase_FloatEqual(inew,rbi0) )
	{
		delay(0 ,GETSET_DELAYCYCLE);
		dpGet(device+".readBackSettings.v0:_original.._value",rbv0,
		    device+".readBackSettings.i0:_original.._value",rbi0);
		if( ++iCount > GETSET_TIMEOUT) // more than 20 secs ow waiting
		{
			//-- This rapresent a Mess
			DebugN(">>>-hmpidCAENchannelset-> ALARM !!! hmpidCAENchannelset : TimeOut reached");
			// Here we put some Messages dispatch ....
			// dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Channel|"+getUserName()+"|I|L|"+"Lost Connection with channel:"+device+"|");
			return;
		}
	}
	// And finally we wait for a stabilization time
	// if paramere Del > 0 it produce the delay and the set of status
	//        		 Del == 0 only set of state but not delay
	if(del >= 0) delay(del ,0);
}

// =========================================== EOF ================================


