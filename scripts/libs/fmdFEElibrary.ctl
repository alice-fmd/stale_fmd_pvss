#uses "fmdBaselibrary.ctl"
#uses "Rcuxx"

// FMD FEE library 
// Author : Ga�l Renault
// Ver 1.0  04/03/2007 

// ------------------------------------------
/*			History
                        
      04/03/2007   - creation
			
			
			
	Functions List	
	---------------------------------------------------------------------------------	
        string     Unsigned2HexString(unsigned u)
        unsigned   HexString2Unsigned(string s)        
        int        dec2bin(unsigned dec, dyn_int &binary)
        int        bin2dec(dyn_int binary, unsigned &dec)
*/
// ------------------------------------------
string Unsigned2HexString(unsigned u, int digits){
  string s;
  string format = "0x%0" + digits + "x";
  sprintf(s, format ,u);
  return s;
  }
// ------------------------------------------
unsigned HexString2Unsigned(string s){
  int  ti;
  sscanf(s, "0x%x", ti);
  bit32 tb;
  tb = ti;
  unsigned tu;
  tu = tb; 
  return tu;
  }
// ------------------------------------------
int dec2bin(unsigned dec, dyn_int &binary){
  int remain = 0;
  int k = 0;
  binary = makeDynInt("0","0","0","0","0","0","0","0",
                        "0","0","0","0","0","0","0","0",
                        "0","0","0","0","0","0","0","0",
                        "0","0","0","0","0","0","0","0");
  
  while( dec > 0){
    remain  = fmod(dec,2);
    dec = dec / 2.;
    k = k +1;
    binary[k] = (remain == 0 ? 0 : 1);
    }
  
   return 0;
}
// ------------------------------------------
int bin2dec(dyn_int binary, unsigned &dec){
  int ret = -1;
  dec = 0;
  
  for(int ti = 0; ti<dynlen(binary); ti++){
   // DebugN(" ti = ", ti, " add ",binary[ti+1]*(1<<ti));
    dec = dec + binary[ti+1]*(1<<ti);
    }
  
  return ret;
  }
// ------------------------------------------
int fmdRcuPMEM_Execute(int handle, unsigned cmd, unsigned address, bool bblock, dyn_uint &data){
  int ret = 0;

  //dyn_uint data;
  data[1] = 0x0;
  data[2] = 0x390000;

  unsigned block = (bblock == true ? 1 << 13 : 0);
  data[1]  = (cmd + address + (block ? (1 << 13) : 0));

  ret = rcuWriteIMEM(handle, address, data);
  DebugN("fmdFEElibrary: rcuWriteIMEM returned ", ret);

  return ret;
  
  }
// ------------------------------------------
// 
// ----------------------------------------------------------------------------
// ____fmdBase_CreateALTRODpt (internal use only)
// /*
// 	This function create the Wattmeter DpType. 
// 	
// 	ver 1.0   18/03/2007
// 	
// 	History
//         
// */
//  const string ALTRODPTYPENAME = "Fmd_ALTRO";
// 
//  int __fmdBase_CreateALTRODpt()
//  {
//  
//  int n;
//  dyn_dyn_string depes;
//  dyn_dyn_int depei;
//  
//  	// Create the data type
//  DebugN(" dpt name: ", ALTRODPTYPENAME);
//  	depes[1] = makeDynString (ALTRODPTYPENAME,"");
//  	depes[2] = makeDynString ("","address");
//  	depes[3] = makeDynString ("","aclMemory");
//  	
//  
//  	depei[1] = makeDynInt (DPEL_STRUCT);
//  	depei[2] = makeDynInt (0,DPEL_UINT);
//  	depei[3] = makeDynInt (0,DPEL_UINT);
//  	
//  	// Create the datapoint type
//  	n = dpTypeCreate(depes,depei);
//  	DebugTN ("__fmdBase_CreateALTRODpt : created FMD/ALTRO DPT : ",n);
//  	return(n);
//  }

// ----------------------------------------------------------------------------
// __fmdBase_CreateALTRODpe (internal use only)
// /*
// 	This function create the Wattmeter DpType. 
// 	
// 	ver 1.0   18/03/2007
// 	
// 	History
//         
// 	
// */
//  int __fmdBase_CreateALTRODpe(){
//    int n;
//    dyn_string dpes;
//    int i =  0;
//    dpes = makeDynString(PREFIX_DET + "_ALTRO_" + i);
//    n = dpCreate(dpes ,ALTRODPTYPENAME );
//    return(n);
//    }
//  
// ------------------------------------------
// EOF
//
