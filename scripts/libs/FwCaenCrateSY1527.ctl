#uses "fmdCAENlibrary.ctl"  
string sPreviousState;  
 
FwCaenCrateSY1527_initialize(string domain, string device)
{
 
// Create and set the Timed Function dp   
// and link the timer variable to the CB function for a period of 5 second  
dyn_errClass err;  
DebugN(">>-----> FwCaenCrateSY1527_initialize :"+domain+" "+device+"  Timed Function is installing");  
if (!dpExists("fmd"+device+"_timer"))   
{  
	dpCreate("fmd"+device+"_timer", "_TimedFunc");  
	err = getLastError();  
	if (dynlen(err) > 0) errorDialog(err);  
}   
dpSet(getSystemName() +"fmd"+ device+"_timer.validFrom:_original.._value",0,  
      getSystemName() +"fmd"+ device+"_timer.validUntil:_original.._value",0,  
      getSystemName() +"fmd"+ device+"_timer.time:_original.._value",makeDynInt(),  
      getSystemName() +"fmd"+ device+"_timer.interval:_original.._value",TIMED_FUNCTION_TIME,  
	getSystemName() +"fmd"+ device+"_timer.syncTime:_original.._value",-1);  
timedFunc("fmdCAEN_CratesTimedControlCB",getSystemName()+"fmd"+device+"_timer");  
err = getLastError();  
if (dynlen(err) > 0) errorDialog(err);  
  
DebugN(">>-----> FwCaenCrateSY1527_initialize :"+domain+" "+device+"  Timed Function installed");  
  
sPreviousState = "";  
  
}
FwCaenCrateSY1527_valueChanged( string domain, string device,
      string Information_dot_SwRelease , string &fwState)
{
  
string st;  
  
	if(Information_dot_SwRelease == "LOST_CONTROL") 
	{ 
		fwState = "NO_CONTROL";  
            st = fwState; 
	} else if(Information_dot_SwRelease == "KILL")  
	{  
		fwState = "WA_REPAIR";   
            st = fwState;  
	} else if(Information_dot_SwRelease == "CLEAR")  
	{  
		fwState = "WA_REPAIR";   
            st = fwState;  
	} else  
	{  
            st = fmdCAEN_CrateControlStatus(dpSubStr(device, DPSUB_SYS_DP));  
            if (sPreviousState != st)   
            {  
               switch(st)  
               {  
               case "READY":  
                  fwState = "ON";  
                  break;  
               case "EXTERNAL_INTERLOCK":  
               case "EXTERNAL_KILL":  
                  fwState = "INTERLOCK";  
                  break;  
               case "ERROR_AC_FAILURE":  
               case "ERROR_FAN_FAILURE":  
                  fwState = "PWS_FAULT";  
                  break;  
               case "WARNING_AC_FAILURE":  
               case "WARNING_FAN_FAILURE":  
                  fwState = "ER_REPAIR";   
                  break;   
               case "WARNING_TEMPERATURE":  
                  fwState = "WA_REPAIR";  
                  break;  
               default:  
                  fwState = "";  
                  break;  
               }   
            } 
	}  
      sPreviousState = st;  
}


FwCaenCrateSY1527_doCommand(string domain, string device, string command)
{ 
device = dpSubStr(device, DPSUB_SYS_DP);  
  
	if (command == "RESET") 
	{ 
		dpSet(device+".Commands.ClearAlarm:_original.._value",true); 
		dpSetWait(device+".Information.SwRelease:_original.._value","CLEAR");   
//  	      dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|I|H|"+"Clear Alarm command send to Crate:"+device+"|");  
  
	} else if (command == "KILL")  
	{  
		dpSetWait(device+".Commands.Kill:_original.._value",true);   
		dpSetWait(device+".Information.SwRelease:_original.._value","KILL");   
//  	      dpSet(getSystemName()+"log_1.SendMess:_original.._value","CAEN_Crate|"+getUserName()+"|I|H|"+"KILL ALL command send to Crate:"+device+"|");  
	}  

}


