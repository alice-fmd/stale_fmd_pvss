#uses "fmdCAENlibrary.ctl"  
 
string fwPreviousState; 

FwCaenChannelLV_initialize(string domain, string device)
{ 
int status;  
string fwState,SetNumber;  
 
// and set the new status  
//dpGet(device+".actual.status:_original.._value",status); 
//FwCaenChannelLV_valueChanged(domain, device, status, fwState);  
fwDU_setState(domain, device, fwPreviousState);  
  

}

FwCaenChannelLV_valueChanged( string domain, string device, 
      int actual_dot_status, string &fwState ) 
{ 
int SetNum;  
string SetNumber;  
dyn_float dfValues;
    
	if (actual_dot_status & CAEN_TRIPMASK )     
	{     
		fwState = "TRIPPED";     
	}    
	else if (actual_dot_status & CAEN_ERRORMASK )     
	{     
		fwState = "CHAN_FAULT";     
	}   
	else if (actual_dot_status & CAEN_NOCONTROLMASK )      
	{      
		fwState = "NO_CONTROL";      
	}      
	else if (actual_dot_status & CAEN_IGNOREDMASK)        
	{        
            fwState = (fwPreviousState != "") ? fwPreviousState : (actual_dot_status ^ CAEN_ONMASK) ? "ON" : "OFF" ;   
            DebugN(">>-FwCaenChannelLV_valueChanged->IGNORED STATE ! Domain"+domain+"  device:"+device+" status:"+actual_dot_status+" state:"+fwState);   
	}        
	else if (!(actual_dot_status ^ CAEN_OFFMASK))
	{     
		fwState = "OFF";     
	}     
	else if (!(actual_dot_status ^ CAEN_ONMASK))
	{       
            fwState = "ON";  
            fmdCAEN_FwCaenChannelSetStock("",device,2,false); 
 	}     
	else      
	{     
            DebugN(">>-FwCaenChannel_valueChanged->UNDEFINED STATE ! Domain"+domain+"  device:"+device+" status:"+actual_dot_status+" state:"+fwState);  
		fwState = "UNDEFINED";     
	}    
     fwPreviousState = fwState;         

}


FwCaenChannelLV_doCommand(string domain, string device, string command) 
{  
string number;  
int num;  
string fwState;  
dyn_float dfValues; 
 
      switch(command) 
      {
      	case "SWITCH_ON":
			dpSet(device+".settings.onOff",TRUE);  
                  break; 
            case "SWITCH_OFF":
 		      dpSet(device+".settings.onOff",FALSE); 
                  break; 
            case "SET":
                  fwDU_getCommandParameter(domain,device,"SetNum",number);    
                  fmdCAEN_FwCaenChannelSetStock("",device,number,true); 
                  break;
            case "GO_READY":
                  fmdCAEN_FwCaenChannelSetStock("",device,1,false); 
                  dpSet(device+".settings.onOff",TRUE); 
                  break; 
	      case "GO_STANDBY":
                  fmdCAEN_FwCaenChannelSetStock("",device,3,false); 
                  dpSet(device+".settings.onOff",FALSE);   
                  break;
	      case "RESET": 
                  // This is a complex Reset procedure 
                  // put in order to perform an FSM exit 
                  // in the tripped state 
                  // 
                  // first set to 0 the Voltage 
                  dpSetWait(device+".settings.v0",0.0);     
                  // then perform the SwitchON/OFF 
                  dpSetWait(device+".settings.onOff",TRUE);     
                  dpSetWait(device+".settings.onOff",FALSE);     
                  // finally set the Channel with the set 1 
                  fmdCAEN_FwCaenChannelSetStock("",device,1,true);  
                  // ----------------------------- 
                  break; 
      }   
} 
