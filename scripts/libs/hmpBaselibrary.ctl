// ------------------------------------------
/* 					History

	7/12/2006	-	Modified Def. in order to include Gas Flow In & Out

*/

// Function definition for the constant
const float FLOAT_PRECISION_POS_THRESHOLD = 0.00001; // Threshold for the float comparision
const float FLOAT_PRECISION_NEG_THRESHOLD = -0.00001;

// Name of System
const string HVSYS = "dist_1";//"hmp_hvlv";
const string LGCSYS = "hmp_main";
const string MAINSYS = "hmp_main";

// Naming for Logical/Detector View
const string LDW_PREFIX = "FMD";
const string LOGICALVIEWROOT = "FMD_DCS";
const string LDW_DETECTOR = "DET";
const string LDW_MODULE = "MP";
const string LDW_POWERSYSTEM = "PW";
const string LDW_SECTOR = "SEC";
const string LDW_HVSEG = "HV";
const string LDW_FEEPSEG = "FEP";
const string LDW_FEENSEG = "FEN";
const string LDW_READOUT = "RO";
const string LDW_READOUTLEFT = "ROL";
const string LDW_READOUTRIGHT = "ROR";
// const string LDW_GRID = "GRID";
// const string LDW_GAS = "GAS";
// const string LDW_MWPCPRES = "PMWPC";
// const string LDW_SAFEPRES = "PBOX";
// const string LDW_GASBOXFLOWIN = "FINBOX";
// const string LDW_GASMWPCFLOWIN = "FINMWPC";
// const string LDW_GASBOXFLOWD = "FDBOX";
// const string LDW_GASMWPCFLOWD = "FDMWPC";
// const string LDW_GASBOXFLOWOUT = "FOUTBOX";
// const string LDW_GASMWPCFLOWOUT = "FOUTMWPC";
// const string LDW_GASPURITY = "PUR";
// const string LDW_COOLING = "COOL";
const string LDW_FEROTEMP = "TINBOX";
const string LDW_LOOP = "LOOP";
const string LDW_MIXER = "MIXER";
const string LDW_LIQUID = "LIQ";
const string LDW_INFRASTRUCTURE = "INFR";
const string LDW_RACK_X07 = "CR5X07";
const string LDW_PLCPHY = "PLCPTPAR";
const string LDW_PLCLIQMAIN = "PLCLCSMAIN";
const string LDW_PLCLIQTRA = "PLCLCSTRAN";
const string LDW_RACK_X08 = "CR5X08";
// const string LDW_PLCLIQMOD = "PLCLCSM";
// const string LDW_PLCLIQPUMP = "PLCLCSPUMP";
const string LDW_RACK_COOL = "COOLPLANT";
const string LDW_RACK_LCS = "LCSPLANT";
const string LDW_RACK_GAS = "GASPLANT";
const string LDW_RACK_TRANSP = "TRANPLANT";
const string LDW_RACK_Y11 = "CR4Y11";
const string LDW_MAINPS = "MAINPWS";
const string LDW_RACK_I0 = "I0";
const string LDW_LVPS = "EASY";
const string LDW_RACK_I1 = "I1";
const string LDW_RACK_I2 = "I2";
const string LDW_LHC = "LHC";
const string LDW_ENVIRONMENT = "ENV";
const string LDW_ENVTEMP = "TENV";
const string LDW_ENVPRES = "PENV";
const string LDW_STATUS = "STATE";
const string LDW_PRESSURE = "PRESS";

// Naming for Logical View Types
const int LDT_DETECTOR = 1;
const int LDT_MODULE = 2;
const int LDT_POWERSYSTEM = 3;
const int LDT_SECTOR = 4;
const int LDT_HVSEG = 5;
const int LDT_LVSEG = 6;
const int LDT_LVRO = 7;
const int LDT_LVROSEG = 8;
const int LDT_HVGRID = 9;
const int LDT_LIQUID = 10;
const int LDT_RADIATOR = 11;
const int LDT_GAS = 12;
const int LDT_COOL = 13;
const int LDT_TEMPERATURE = 14;
const int LDT_PRESSURE = 15;
const int LDT_FLOW = 16;
const int LDT_PURITY = 17;
const int LDT_LOOP = 18;
const int LDT_INFRASTRUCTURE = 19;
const int LDT_RACKS = 20;
const int LDT_MAINPS = 21;
const int LDT_LVPS = 22;
const int LDT_PLC = 23;
const int LDT_PLANT = 24;
const int LDT_CRATE = 25;
const int LDT_ENVIRONMENT = 26;
const int LDT_LHC = 27;
const int LDT_STATUS = 28;
const int LDT_MIXER = 29;

// extra coords const definition
const int LDC_ISLEFT = 1;
const int LDC_ISRIGHT = 2;

const int LDC_ISPOSITIVE = 1;
const int LDC_ISNEGATIVE = 2;

const int LDC_ISCONTROL = 1;
const int LDC_ISHEADER = 2;
const int LDC_ISRADIATOR = 3;
const int LDC_ISINLET = 4;
const int LDC_ISOUTLET = 5;
const int LDC_ISGASREF = 6;

const int LDC_ISMWPC = 7;
const int LDC_ISBOX = 8;

const int LDC_ISFEROTEMP = 10;

const int LDC_ISLIQMAIN = 12;
const int LDC_ISLIQMOD = 13;
const int LDC_ISLIQPUMP = 14;
const int LDC_ISLIQTRA = 15;
const int LDC_ISPHYSICAL = 18;

const int LDC_ISCOOLINGLOOP = 30;
const int LDC_ISGASLOOP = 31;
const int LDC_ISENVPRES = 32;
const int LDC_ISENVTEMP = 33;

const int LDC_ISCOOLPLANT = 40;
const int LDC_ISLCSPLANT = 41;
const int LDC_ISGASPLANT = 42;
const int LDC_ISTRANSPLANT = 43;

const int LDC_ISBOXFLOWIN = 45;
const int LDC_ISBOXFLOWOUT = 46;
const int LDC_ISMWPCFLOWIN = 47;
const int LDC_ISMWPCFLOWOUT = 48;
const int LDC_ISMWPCFLOWD = 49;
const int LDC_ISBOXFLOWD = 50;

const int LDC_ISGASMAIN = 51;

const int LDC_ISCR4Y11 = 11;
const int LDC_ISCR5X07 = 7;
const int LDC_ISCR5X08 = 8;
const int LDC_ISI0 = 0;
const int LDC_ISI1 = 1;
const int LDC_ISI2 = 2;


// ------------------------------------------------
// Names of DPT Used
const string DPT_PWCRATE = "FwCaenCrateSY1527";
const string DPT_HVCHANNEL = "FwCaenChannel";
const string DPT_LVCHANNEL = "FwCaenChannel";
// const string DPT_TEMPERATURE = "Hmp_SiemensComplexTemperature";
// const string DPT_PRESSURE = "Hmp_SiemensComplexPressure";
// const string DPT_LIQMODULE = "Hmp_SiemensComplexLCMod";
// const string DPT_PLC = "Hmp_SiemensPlc";
// const string DPT_COOLINGLOOP = "Hmp_CoolingLoop";
// const string DPT_GASLOOP = "Hmp_GasLoop";



// ----------------------------------------------------------------------------
// __hmpBase_CreateWattmeterDpt (internal use only)
/*
	This function create the Wattmeter DpType. 
	
	F.A. ver 1.0   01/03/2006
	
	History
	
	  		
*/
const string WATTMETERDPTYPENAME = "Hmp_wattMeter";
const string WATTMETERDPNAME = "hmp_lvWattMeter";
const float MAXDETECTORWATTS = 2000.0;
const float MAXMODULEWATTS = 700.0;

int __hmpBase_CreateWattmeterDpt()
{

int n;
dyn_dyn_string depes;
dyn_dyn_int depei;

	// Create the data type
	depes[1] = makeDynString (HVSYS+":"+WATTMETERDPTYPENAME,"");
	depes[2] = makeDynString ("","hmpid");
	depes[3] = makeDynString ("","rich0");
	depes[4] = makeDynString ("","rich1");
	depes[5] = makeDynString ("","rich2");
	depes[6] = makeDynString ("","rich3");
	depes[7] = makeDynString ("","rich4");
	depes[8] = makeDynString ("","rich5");
	depes[9] = makeDynString ("","rich6");


	depei[1] = makeDynInt (DPEL_STRUCT);
	depei[2] = makeDynInt (0,DPEL_FLOAT);
	depei[3] = makeDynInt (0,DPEL_FLOAT);
	depei[4] = makeDynInt (0,DPEL_FLOAT);
	depei[5] = makeDynInt (0,DPEL_FLOAT);
	depei[6] = makeDynInt (0,DPEL_FLOAT);
	depei[7] = makeDynInt (0,DPEL_FLOAT);
	depei[8] = makeDynInt (0,DPEL_FLOAT);
	depei[9] = makeDynInt (0,DPEL_FLOAT);
	// Create the datapoint type
	n = dpTypeCreate(depes,depei);
	DebugTN ("__hmpBase_Wattmeter : created HMPID/Wattmeter DPT : ",n);
	return(n);
}

// -------------------------

/* ------------------------------------------------

Name: 	hmpBase_RefreshBarLedIndicator(float value1,float value2)
Desc:   Display a value into the BarLedIndicator Hmp Widget
        
inp :   
out :   

Auth : A.Franco - INFN Bari
Date : 28/02/2006
Ver. : 1.0

Hist : -

------------------------------------------------ */
void hmpBase_RefreshBarLedIndicator(string sModule, string sPanel, string sName,float value)
{
		setValue(sName+".Value","text",value);
		setInputFocus(sModule,sPanel,sName+".Value");
		return;
}
/* ------------------------------------------------

Name: 	__hmpBase_FloatEqual(float value1,float value2)
Desc:   Compare 2 float values and consider they equals
				if the difference is under a certain threshold
        
inp :   Value1, Value2 := float 
out :   Result : true := equal   false := notequal

Auth : A.Franco - INFN Bari
Date : 30/08/2005
Ver. : 1.0

Hist : -

------------------------------------------------ */
bool __hmpBase_FloatEqual(float value1,float value2)
{
	float f;
	f = value1 - value2;
	return( (f < FLOAT_PRECISION_POS_THRESHOLD) && (f > FLOAT_PRECISION_NEG_THRESHOLD));
}
/* ------------------------------------------------

Name: 	hmpBase_Tem2Col()
Desc:   Convert a temperature value into a colour 
        string
        
inp :   Temperature := float 
out :   Colour := string  "{red,green,blu}"

Note : the range temperature varies from 8 to 60�C
       over range values are marcked with blue
       and red colours
       
Auth : A.Franco - INFN Bari
Date : 24/06/2005
Ver. : 1.0

Hist : -

------------------------------------------------ */
string hmpBase_Tem2Col(float temp, int t_min = 0, int t_max = 130)
{
	int r,g,b;
	if(temp<t_min || temp > t_max) return("{0,0,0}");
	
	int t_delta = (t_max -t_min +1);
	int s1 = t_min + floor(t_delta * 0.16);		
	int s2 = s1 + 1;
	int s11 = t_min + floor(t_delta * 0.33);		
	int s10 = s11 - 1;
	int s8 = s11 + 1;
	int s3 = t_min + floor(t_delta * 0.49);		
	int s7 = s3 - 1;
	int s12 = t_min + floor(t_delta * 0.70);	// .66	
	int s4 = s12 - 1;
	int s9 = s12 + 1;
	int s5 = t_min + floor(t_delta * 0.90);		// .82
	int s6 = s5 - 1;
	int step = floor( 240.0 / (s11-s1+1) );
	
	r = (temp < s3) ? 0 : ( (temp > s4 && temp < s5) ? 255 : (temp > s6) ? (255-(temp-s6)*step) : ((temp-s7)*step) );
	b = (temp > s1 && temp < s8) ? 255 : ( (temp > s3) ? 0 : (temp < s2) ? (60+(temp-t_min)*step) : (255-step*(temp-s8)) );
	g = (temp < s2 || temp > s6) ? 0 : ((temp < s9 && temp > s10) ? 255 : ((temp < s11 && temp > s1) ? ((temp-s1)*step) : (255-(step*(temp-s12))) ) );	
//DebugN(">>>",s1,s11,temp,step,r,g,b);
	return("{"+r+","+g+","+b+"}");
}

// ----------------------------------------------------------------------------
// int hmpBase_getAccessLevel(bool & bOnlyDisplay)
/*
	
	F.A. ver 1.0   22/02/2006

	Return the Access Privilege Code
	
						History
	
		22/02/2006 created 
		
*/
const int HMP_ACCESS_ADMIN = 1;
const int HMP_ACCESS_EXTEND = 2;
const int HMP_ACCESS_NORMAL = 3;
const int HMP_ACCESS_ACKNOL = 4;
const int HMP_ACCESS_VISUAL = 9;
const int HMP_ACCESS_DENIED = 0;

int hmpBase_getAccessLevel(bool & bOnlyDisplay)
{

	string user,domain;
  string userFullName, description;  
  int userId;  
  bool enabled,granted;
  dyn_string groupNames, exceptionInfo;

	bOnlyDisplay = true;
	
	// Get the users 
	fwAccessControl_getUserName(user);
	fwAccessControl_getUser(user, userFullName, description, userId,
												enabled, groupNames,exceptionInfo );

	// If isn't enabled then close the panel and Exit
	if(!enabled) return(HMP_ACCESS_DENIED);	

	domain = getSystemName();
	fwAccessControl_isGranted(domain+"Administration",granted,exceptionInfo);
	if(granted)
	{
		bOnlyDisplay = false;
		return(HMP_ACCESS_ADMIN);
	}
	fwAccessControl_isGranted(domain+"Extended user level",granted,exceptionInfo);
	if(granted)
	{
		bOnlyDisplay = false;
		return(HMP_ACCESS_EXTEND);
	}
	fwAccessControl_isGranted(domain+"Normal user level",granted,exceptionInfo);
	if(granted)
	{
		return(HMP_ACCESS_NORMAL);
	}
	fwAccessControl_isGranted(domain+"Acknowledge",granted,exceptionInfo);
	if(granted)
	{
		return(HMP_ACCESS_ACKNOL);
	}
	return(HMP_ACCESS_VISUAL);
}
// ----------------------------------------------------------------------------
// string hmpBase_GetCoords(string device, int &iModule, int &iSector, int &extra, int &iType)
/*
	
	F.A. ver 1.0   21/11/2005

	Return all the Coords and specification of device
	
						History
	
		19/11/2005 created ex novo.
		07/12/2005 add the loop node for gas and cooling
		19/07/2006 Renew all the names
				
*/
string hmpBase_GetCoords(string device, int &iModule, int &iSector, int &extra, int &iType)
{
string sLogicalName,sAppo,sEle;
dyn_string dsHier;
dyn_string dsElem;



	iModule = 0; iSector = 0; iType = 0; extra = 0;
		
	// Convert device into alias 
	sLogicalName = dpGetAlias(device+".");
	if(sLogicalName == "") sLogicalName=device;

	// Get the last element 
	dsHier = strsplit(sLogicalName,"/");
	if(dynlen(dsHier) == 0) return "";
	sAppo = dsHier[dynlen(dsHier)];

	// and create the full logical name
	sLogicalName = getSystemName()+sLogicalName;

	DebugN(">>>>>---->",device,sLogicalName,dsHier,sAppo);
	// split the element
	dsElem = strsplit(sAppo,"_");
	sEle = dsElem[dynlen(dsElem)];
	
	// Is the detector
	if(strpos(sEle,LDW_DETECTOR) == 0)
		{ iType =LDT_DETECTOR; return(sLogicalName);}

	// IS a Plant
	switch(sEle)
	{
		case LDW_RACK_COOL: 
			iType =LDT_PLANT;
			extra = LDC_ISCOOLPLANT;
			return(sLogicalName);
			break;
		case LDW_RACK_LCS: 
			iType =LDT_PLANT;
			extra = LDC_ISLCSPLANT;
			return(sLogicalName);
			break;
		case LDW_RACK_GAS: 
			iType =LDT_PLANT;
			extra = LDC_ISGASPLANT;
			return(sLogicalName);
			break;
		case LDW_RACK_TRANSP: 
			iType =LDT_PLANT;
			extra = LDC_ISTRANSPLANT;
			return(sLogicalName);
			break;
	}

	// Is a Module
	if(strpos(sEle,LDW_MODULE) == 0)
	{	iModule = substr(sEle,strlen(sEle)-1,1);
		iType =LDT_MODULE;
		return(sLogicalName);
	}
	
	// Is a Power Sub System
	if(strpos(sEle,LDW_POWERSYSTEM) == 0)
	{	iModule = substr(dsElem[dynlen(dsElem)-1],strlen(dsElem[dynlen(dsElem)-1])-1,1);
		iType =LDT_POWERSYSTEM;
		return(sLogicalName);
	}

	// Is a Sector	
	if(strpos(sEle,LDW_SECTOR) == 0)  
	{
		iSector = substr(sEle,strlen(sEle)-1,1);
		iModule = substr(dsElem[dynlen(dsElem)-1],strlen(dsElem[dynlen(dsElem)-1])-1,1);
		iType =LDT_SECTOR;
		return(sLogicalName);
	}
	
	// Is a HV Channel
	if(strpos(sEle,LDW_HVSEG) == 0)  
	{
		iSector = substr(dsElem[dynlen(dsElem)-1],strlen(dsElem[dynlen(dsElem)-1])-1,1);
		iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
		iType =LDT_HVSEG;
		return(sLogicalName);
	}
	
	// Is a LV Negative Channel
	if(strpos(sEle,LDW_FEENSEG) == 0)  
	{
		iSector = substr(dsElem[dynlen(dsElem)-1],strlen(dsElem[dynlen(dsElem)-1])-1,1);
		iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
		extra = LDC_ISNEGATIVE;
		iType =LDT_LVSEG;
		return(sLogicalName);
	}
	// Is a LV Positive Channel
	if(strpos(sEle,LDW_FEEPSEG) == 0)  
	{
		iSector = substr(dsElem[dynlen(dsElem)-1],strlen(dsElem[dynlen(dsElem)-1])-1,1);
		iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
		extra = LDC_ISPOSITIVE;
		iType =LDT_LVSEG;
		return(sLogicalName);
	}

	// Is a LV Channel
	if(strpos(sEle,LDW_READOUTLEFT) == 0)  
	{
		iModule = substr(dsElem[dynlen(dsElem)-1],strlen(dsElem[dynlen(dsElem)-1])-1,1);
		iSector = 0;
		extra = LDC_ISLEFT;
		iType =LDT_LVROSEG;
		return(sLogicalName);
	}
	// Is a LV Channel
	if(strpos(sEle,LDW_READOUTRIGHT) == 0)  
	{
		iModule = substr(dsElem[dynlen(dsElem)-1],strlen(dsElem[dynlen(dsElem)-1])-1,1);
		iSector = 0;
		extra = LDC_ISRIGHT;
		iType =LDT_LVROSEG;
		return(sLogicalName);
	}

	// Is a ReadOut
	if(strpos(sEle,LDW_READOUT) == 0)  
	{
		iModule = substr(dsElem[dynlen(dsElem)-1],strlen(dsElem[dynlen(dsElem)-1])-1,1);
		iType =LDT_LVRO;
		return(sLogicalName);
	}
	
	// Is a Grid HV Channel
	if(strpos(sEle,LDW_GRID) == 0)  
	{
		iModule = substr(dsElem[dynlen(dsElem)-1],strlen(dsElem[dynlen(dsElem)-1])-1,1);
		iType =LDT_HVGRID;
		return(sLogicalName);
	}	


	// Is a Gas Module
	if(strpos(sEle,LDW_GAS) == 0)  
	{
		iModule = substr(dsElem[dynlen(dsElem)-1],strlen(dsElem[dynlen(dsElem)-1])-1,1);
		iType =LDT_GAS;
		return(sLogicalName);
	}			
	// Is a Pressure Sensor MWPC
	if(strpos(sEle,LDW_MWPCPRES) == 0)  
	{
		iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
		extra = LDC_ISMWPC;
		iType =LDT_PRESSURE;
		return(sLogicalName);
	}			
	// Is a Pressure Sensor BOX
	if(strpos(sEle,LDW_SAFEPRES) == 0)  
	{
		iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
		extra = LDC_ISBOX;
		iType =LDT_PRESSURE;
		return(sLogicalName);
	}	
	// Is a Gas Flow
	if(strpos(sEle,LDW_GASBOXFLOWIN) == 0)  
	{
		iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
		iType =LDT_FLOW;
		extra = LDC_ISBOXFLOWIN;
		return(sLogicalName);
	}	
	if(strpos(sEle,LDW_GASMWPCFLOWIN) == 0)  
	{
		iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
		iType =LDT_FLOW;
		extra = LDC_ISMWPCFLOWIN;
		return(sLogicalName);
	}	
	if(strpos(sEle,LDW_GASBOXFLOWOUT) == 0)  
	{
		iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
		iType =LDT_FLOW;
		extra = LDC_ISBOXFLOWOUT;
		return(sLogicalName);
	}	
	if(strpos(sEle,LDW_GASMWPCFLOWOUT) == 0)  
	{
		iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
		iType =LDT_FLOW;
		extra = LDC_ISMWPCFLOWOUT;
		return(sLogicalName);
	}	
	if(strpos(sEle,LDW_GASBOXFLOWD) == 0)  
	{
		iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
		iType =LDT_FLOW;
		extra = LDC_ISBOXFLOWD;
		return(sLogicalName);
	}	
	if(strpos(sEle,LDW_GASMWPCFLOWD) == 0)  
	{
		iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
		iType =LDT_FLOW;
		extra = LDC_ISMWPCFLOWD;
		return(sLogicalName);
	}	

	// Is a Gas Purity
	if(strpos(sEle,LDW_GASPURITY) == 0)  
	{
		iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
		extra = LDC_ISMWPC;
		iType =LDT_PURITY;
		return(sLogicalName);
	}	
	// Is a Gas Pressure
	if(strpos(sEle,LDW_PRESSURE) == 0)  
	{
		if(dsElem[dynlen(dsElem)-1] == LDW_RACK_GAS)
		{
			iModule = 0;
			iType = LDT_PRESSURE;
			extra = LDC_ISGASMAIN;
			return(sLogicalName);
		}
	}	

	// Is a Gas STATE
	if(strpos(sEle,LDW_STATUS) == 0)  
	{
		if(dsElem[dynlen(dsElem)-1] == LDW_RACK_GAS)
		{
			iModule = 0;
			iType = LDT_STATUS;
			extra = LDC_ISGASMAIN;
			return(sLogicalName);
		}
	}	
	// Is a Gas MIXER
	if(strpos(sEle,LDW_MIXER) == 0)  
	{
		if(dsElem[dynlen(dsElem)-1] == LDW_RACK_GAS)
		{
			iModule = 0;
			iType = LDT_MIXER;
			extra = LDC_ISGASMAIN;
			return(sLogicalName);
		}
	}	

	// Is a Cooling Module
	if(strpos(sEle,LDW_COOLING) == 0)  
	{
		iModule = substr(dsElem[dynlen(dsElem)-1],strlen(dsElem[dynlen(dsElem)-1])-1,1);
		iType =LDT_COOL;
		return(sLogicalName);
	}			
	// Is a Temperature Sensor
	if(strpos(sEle,LDW_FEROTEMP) == 0)  
	{
		iSector = substr(dsElem[dynlen(dsElem)],strlen(dsElem[dynlen(dsElem)])-1,1);
		iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
		extra = LDC_ISFEROTEMP;
		iType =LDT_TEMPERATURE;
		return(sLogicalName);
	}			
	// Is a Loop Gas/Cooling Module
	if(strpos(sEle,LDW_LOOP) == 0)  
	{
		if(dsElem[dynlen(dsElem)-1] == LDW_COOLING)
		{
			iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
			extra = LDC_ISCOOLINGLOOP;
			iType =LDT_LOOP;
			return(sLogicalName);
		}
		if(dsElem[dynlen(dsElem)-1] == LDW_GAS)
		{
			iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
			extra = LDC_ISGASLOOP;
			iType =LDT_LOOP;
			return(sLogicalName);
		}
		if(dsElem[dynlen(dsElem)-1] == LDW_LIQUID)
		{
			iModule = substr(dsElem[dynlen(dsElem)-2],strlen(dsElem[dynlen(dsElem)-2])-1,1);
			extra = LDC_ISCONTROL;
			iType =LDT_LIQUID;
			return(sLogicalName);
		}
	}			
	// Is a Infrastructure Group
	if(strpos(sEle,LDW_INFRASTRUCTURE) == 0)  
	{
		iType =LDT_INFRASTRUCTURE;
		return(sLogicalName);
	}	
	// Is a Rack
	switch(sEle)
	{
		case LDW_RACK_X07:
			iModule = LDC_ISCR5X07;
			iType =LDT_RACKS;
			return(sLogicalName);
			break;
		case LDW_RACK_X08:
			iModule = LDC_ISCR5X08;
			iType =LDT_RACKS;
			return(sLogicalName);
			break;
		case LDW_RACK_Y11:
			iModule = LDC_ISCR4Y11;
			iType =LDT_RACKS;
			return(sLogicalName);
			break;
		case LDW_RACK_I0:
			iModule = LDC_ISI0;
			iType =LDT_RACKS;
			return(sLogicalName);
			break;
		case LDW_RACK_I1:
			iModule = LDC_ISI1;
			iType =LDT_RACKS;
			return(sLogicalName);
			break;
		case LDW_RACK_I2:
			iModule = LDC_ISI2;
			iType =LDT_RACKS;
			return(sLogicalName);
			break;
	}
	// Is a Crate	SY1527
	if(strpos(sEle,LDW_MAINPS) == 0)  
	{
		iType =LDT_MAINPS;
		return(sLogicalName);
	}	
	// Is a Crate	 EASY
	if(strpos(sEle,LDW_LVPS) == 0)  
	{
		iType =LDT_LVPS;
		iModule = substr(sEle,strlen(sEle)-1,1);
		return(sLogicalName);
	}	
	// Is a PLC 
	switch(sEle)
	{
		case LDW_PLCLIQMAIN: 
			iModule = 7;
			iType =LDT_PLC;
			extra = LDC_ISLIQMAIN;
			return(sLogicalName);
			break;
		case LDW_PLCLIQPUMP:
			iModule = 8;
			iType =LDT_PLC;
			extra = LDC_ISLIQPUMP;
			return(sLogicalName);
			break;
		case LDW_PLCLIQTRA:  
			iModule = 7;
			iType =LDT_PLC;
			extra = LDC_ISLIQTRA;
			return(sLogicalName);
			break;
		case LDW_PLCPHY:
			iModule = 7;
			iType =LDT_PLC;
			extra = LDC_ISPHYSICAL;
			return(sLogicalName);
			break;
	}	
	// Is a PLC x Module Liq
	if(strpos(sEle,LDW_PLCLIQMOD) == 0)  
	{	iModule = 8;
		iSector = substr(sEle,strlen(sEle)-1,1);
		iType =LDT_PLC;
		extra = LDC_ISLIQMOD;
		return(sLogicalName);
	}	
	
	// Is a Environment Node
	if(strpos(sEle,LDW_ENVIRONMENT) == 0)  
	{	iType =LDT_ENVIRONMENT;
		extra = 0;
		return(sLogicalName);
	}	
	// Is a Environment Temperature
	if(strpos(sEle,LDW_ENVTEMP) == 0)  
	{	iType =LDT_TEMPERATURE;
		extra = LDC_ISENVTEMP;
		return(sLogicalName);
	}	
	// Is a Environment Pressure
	if(strpos(sEle,LDW_ENVPRES) == 0)  
	{	iType =LDT_PRESSURE;
		extra = LDC_ISENVPRES;
		return(sLogicalName);
	}

	// Is a LHC ?
	if(strpos(sEle,LDW_LHC) == 0)  
	{	iType =LDT_LHC;
		return(sLogicalName);
	}


	iType = 0;
	return(sLogicalName);
}



// ----------------------------------------------------------------------------
// hmpBase_buildLogicalPathName(string sType,int module,int sector,int extra)
/*
	This function return the name of Logical View
	
	F.A. ver 1.0   21/11/2005
	
	History
	
		21/11/2005 created.
		07/12/2005 add Gas/Cool Loop node
		19/07/2006	New naming	

*/
string hmpBase_buildLogicalPathName(int iType,int module,int sector,int extra)
{
	string dpN;
	string dpNStock;
	
	switch(iType)
	{
		case LDT_DETECTOR:
			dpN = MAINSYS+":"+LOGICALVIEWROOT;
			break;
		case LDT_MODULE:
			dpN = MAINSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module;
			break;
		case LDT_POWERSYSTEM:
			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM;
			break;
		case LDT_SECTOR:
			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector;
			break;
		case LDT_HVSEG:
			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"_"+LDW_HVSEG;
			break;
		case LDT_HVGRID:
			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GRID;
			break;
		case LDT_LVSEG:
			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"_"+( (extra == LDC_ISPOSITIVE) ? LDW_FEEPSEG : LDW_FEENSEG );
			break;
		case LDT_LVRO:
			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_READOUT;
			break;
		case LDT_LVROSEG:
			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_READOUT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+( (extra == LDC_ISLEFT) ? LDW_READOUTLEFT: LDW_READOUTRIGHT );
			break;
		case LDT_LIQUID:
			dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_LIQUID+"_"+LDW_LOOP;
			break;
		case LDT_GAS:
			dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS;
			break;
		case LDT_COOL:
			dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING;
			break;
		case LDT_LOOP:
			if(extra == LDC_ISCOOLINGLOOP) dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"_"+LDW_LOOP;
			if(extra == LDC_ISGASLOOP) dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP;
			break;
		case LDT_PRESSURE:
			switch(extra)
			{
				case LDC_ISMWPC:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_MWPCPRES;
					break; 	
				case LDC_ISBOX:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_SAFEPRES;
					break; 	
				case LDC_ISENVPRES:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"_"+LDW_ENVPRES;
					break; 	
				case LDC_ISGASMAIN:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"_"+LDW_PRESSURE;
					break; 	
			}
			break;
		case LDT_TEMPERATURE:
			switch(extra)
			{
				case LDC_ISFEROTEMP:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"_"+LDW_FEROTEMP+sector;
					break; 	
				case LDC_ISENVTEMP:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"_"+LDW_ENVTEMP;
					break; 	
			}
			break;
			
		case LDT_FLOW:
			switch(extra)
			{
				case LDC_ISBOXFLOWIN:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_GASBOXFLOWIN;
					break;
				case LDC_ISBOXFLOWOUT:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_GASBOXFLOWOUT;
					break;
				case LDC_ISMWPCFLOWIN:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_GASMWPCFLOWIN;
					break;
				case LDC_ISMWPCFLOWOUT:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_GASMWPCFLOWOUT;
					break;			
				case LDC_ISBOXFLOWD:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_GASBOXFLOWD;
					break;
				case LDC_ISMWPCFLOWD:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_GASMWPCFLOWD;
					break; 
			} 
			break;		
		case LDT_PURITY:
			dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_GASPURITY;
			break;
		case LDT_STATUS:
			switch(extra)
			{
				case LDC_ISGASMAIN:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"_"+LDW_STATUS;
					break; 	
			}
			break;
		case LDT_MIXER:
			switch(extra)
			{
				case LDC_ISGASMAIN:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"_"+LDW_MIXER;
					break; 	
			}
			break;

		case LDT_INFRASTRUCTURE:
			dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE;
			break;
		case LDT_RACKS:
			switch(module)
			{
				case LDC_ISCR5X07:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07;
					break;
				case LDC_ISCR5X08:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08;
					break;
				case LDC_ISCR4Y11:
					dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_Y11;
					break;
				case LDC_ISI0:
					dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I0;
					break;
				case LDC_ISI1:
					dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I1;
					break;
				case LDC_ISI2:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I2;
					break;
			}
			break;

		case LDT_MAINPS:
			dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_Y11+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_Y11+"_"+LDW_MAINPS;
			break;

		case LDT_LVPS:
			switch(module)
			{
				case 0:
				case 1:
					dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I2+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I2+"_"+LDW_LVPS+module;
					break;
				case 2:
					dpN = HVSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I1+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I1+"_"+LDW_LVPS+module;
					break;
			}
			break;

		case LDT_PLC:
			switch(extra)
			{
				case LDC_ISLIQMAIN:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"_"+LDW_PLCLIQMAIN;
					break;
				case LDC_ISLIQPUMP:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"_"+LDW_PLCLIQPUMP;
					break;
				case LDC_ISLIQTRA:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"_"+LDW_PLCLIQTRA;
					break;
				case LDC_ISPHYSICAL:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"_"+LDW_PLCPHY;
					break;
				case LDC_ISLIQMOD:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"_"+LDW_PLCLIQMOD+module;
					break;
			}
			break;

		case LDT_PLANT:
			switch(extra)
			{
				case LDC_ISCOOLPLANT:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_COOL;
					break;
				case LDC_ISLCSPLANT:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_LCS;
					break;
				case LDC_ISGASPLANT:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS;
					break;
				case LDC_ISTRANSPLANT:
					dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_TRANSP;
					break;
			}
			break;
		case LDT_ENVIRONMENT:
			dpN = LGCSYS+":"+LOGICALVIEWROOT+"/"+LDW_ENVIRONMENT;
			break;
	}
	return(dpN);
}

string hmpBase_buildLogicalName(int iType,int module,int sector,int extra)
{
	string dpN;
	int i;
		
	dpN = hmpBase_buildLogicalPathName(iType,module,sector,extra);
	i = strpos(dpN,":");
	if (i>0)
		dpN = substr(dpN,i+1);
	return(dpN);
}

// ----------------------------------------------------------------------------
// hmpBase_buildFSMName(string sType,int module,int sector,int extra)
/*
	This function return the name of FSM Node View
	
	F.A. ver 1.0   07/09/2006
	
	History
	
		07/09/2006 created.

*/
string hmpBase_buildFSMName(int iType,int module,int sector,int extra)
{
	string dpN;
	string dpNStock;
	
	switch(iType)
	{
		// CUs
		case LDT_DETECTOR:
			return( LDW_PREFIX+"_"+LDW_DETECTOR);
			break;
		case LDT_MODULE:
			return( LDW_PREFIX+"_"+LDW_MODULE+module);
			break;
		case LDT_POWERSYSTEM:
			return( LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM);
			break;
		case LDT_SECTOR:
			return( LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector);
			break;
		case LDT_LVRO:
			return( LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_READOUT);
			break;
		case LDT_GAS:
			return( LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS);
			break;
		case LDT_COOL:
			return( LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING);
			break;
		case LDT_INFRASTRUCTURE:
			return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE);
			break;
		case LDT_RACKS:
			switch(module)
			{
				case LDC_ISCR5X07:
					return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07);
					break;
				case LDC_ISCR5X08:
					return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08);
					break;
				case LDC_ISCR4Y11:
					return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_Y11);
					break;
				case LDC_ISI0:
					return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I0);
					break;
				case LDC_ISI1:
					return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I1);
					break;
				case LDC_ISI2:
					return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I2);
					break;
			}
			break;
		case LDT_ENVIRONMENT:
			return( LDW_PREFIX+"_"+LDW_ENVIRONMENT);
			break;


		// DUs
		case LDT_HVSEG:
			return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"_"+LDW_HVSEG);
			break;
		case LDT_HVGRID:
			return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GRID);
			break;
		case LDT_LVSEG:
			return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_SECTOR+sector+"_"+( (extra == LDC_ISPOSITIVE) ? LDW_FEEPSEG : LDW_FEENSEG ) );
			break;
		case LDT_LVROSEG:
			return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_POWERSYSTEM+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_READOUT+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+( (extra == LDC_ISLEFT) ? LDW_READOUTLEFT: LDW_READOUTRIGHT ));
			break;
		case LDT_LIQUID:
			return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_LIQUID+"_"+LDW_LOOP);
			break;
		case LDT_LOOP:
			if(extra == LDC_ISCOOLINGLOOP) return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"_"+LDW_LOOP);
			if(extra == LDC_ISGASLOOP) return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_LOOP);
			break;
		case LDT_PRESSURE:
			switch(extra)
			{
				case LDC_ISMWPC:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_MWPCPRES );
					break; 	
				case LDC_ISBOX:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_GAS+"_"+LDW_SAFEPRES );
					break; 	
				case LDC_ISENVPRES:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"_"+LDW_ENVPRES );
					break; 	
				case LDC_ISGASMAIN:
					return( LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"_"+LDW_PRESSURE );
					break; 	
			}
			break;
		case LDT_TEMPERATURE:
			switch(extra)
			{
				case LDC_ISFEROTEMP:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"/"+LDW_PREFIX+"_"+LDW_MODULE+module+"_"+LDW_COOLING+"_"+LDW_FEROTEMP+sector );
					break; 	
				case LDC_ISENVTEMP:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"/"+LDW_PREFIX+"_"+LDW_ENVIRONMENT+"_"+LDW_ENVTEMP );
					break; 	
			}
			break;
		case LDT_STATUS:
			switch(extra)
			{
				case LDC_ISGASMAIN:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS+"_"+LDW_STATUS );
					break; 	
			}
			break;
		case LDT_MAINPS:
			return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_Y11+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_Y11+"_"+LDW_MAINPS );
			break;
		case LDT_LVPS:
			switch(module)
			{
				case 0:
				case 1:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I2+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I2+"_"+LDW_LVPS+module);
					break;
				case 2:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I1+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_I1+"_"+LDW_LVPS+module);
					break;
			}
			break;

		case LDT_PLC:
			switch(extra)
			{
				case LDC_ISLIQMAIN:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"_"+LDW_PLCLIQMAIN );
					break;
				case LDC_ISLIQPUMP:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"_"+LDW_PLCLIQPUMP );
					break;
				case LDC_ISLIQTRA:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"_"+LDW_PLCLIQTRA );
					break;
				case LDC_ISPHYSICAL:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X07+"_"+LDW_PLCPHY );
					break;
				case LDC_ISLIQMOD:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_X08+"_"+LDW_PLCLIQMOD+module );
					break;
			}
			break;

		case LDT_PLANT:
			switch(extra)
			{
				case LDC_ISCOOLPLANT:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_COOL );
					break;
				case LDC_ISLCSPLANT:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_LCS );
					break;
				case LDC_ISGASPLANT:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_GAS );
					break;
				case LDC_ISTRANSPLANT:
					return( LDW_PREFIX+"_"+LDW_DETECTOR+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"/"+LDW_PREFIX+"_"+LDW_INFRASTRUCTURE+"_"+LDW_RACK_TRANSP );
					break;
			}
			break;

	}
	return("");
}
// =========================================== EOF ================================

