#uses "fmdCAENlibrary.ctl"
#uses "fmdFEElibrary.ctl"
// ===================================================================
//                            MAIN PROG
// ===================================================================
int main()
{
  int ret = -1;
  //ret = __fmdCAEN_CreateStockDpt();
  //DebugN(">> fmdSetUp.ctl: __fmdCAEN_CreateStockDpt() returns : ", ret);
  
  ret = __fmdBase_CreateALTRODpt();
  DebugN(">> fmdSetUp.ctl: __fmdBase_CreateALTRODpt returns : ", ret);
  
   ret = __fmdBase_CreateALTRODpe();
   DebugN(">> fmdSetUp.ctl: __fmdBase_CreateALTRODpe returns : ", ret);
   
  return 0;
}
