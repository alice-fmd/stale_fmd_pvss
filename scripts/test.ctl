//#uses "fmdBaselibrary.ctl"



// ===================================================================
//                           VARIABLE DEFINITIONS
// ===================================================================
const string SYS                   = "dist_1";
const string SUFFIX_DET            = "DCS";
const string PREFIX_DET            = "FMD";
const string PREFIX_PW             = "PW";
const string PREFIX_MOD            = "MOD";
const string PREFIX_INFR           = "INFR";
const string PREFIX_RACK_ONE       = "RACK1";
const string PREFIX_RACK_TWO       = "RACK2";
const string PREFIX_HVPS           = "HVPS";
const string PREFIX_LVPS           = "LVPS";
const string PREFIX_CONTROLLER     = "CONTROLLER";
const string PREFIX_INNER          = "INNER";
const string PREFIX_OUTER          = "OUTER";
const string PREFIX_TOP            = "TOP";
const string PREFIX_BOTTOM         = "BOTTOM";
const string PREFIX_HV_CH          = "HvChannel";
const string PREFIX_LV_CH          = "LvChannel";

const string ROOT_SYS              = "fmd_hvSt_";

const int ID_TYPE_TOP_NODE         = 6;
const int ID_TYPE_RACK_ONE         = 12;
const int ID_TYPE_RACK_TWO         = 13;
const int ID_TYPE_HVPS_CONTROLLER  = 14;
const int ID_TYPE_LVPS_CONTROLLER  = 15;
const int ID_TYPE_MOD              = 1;
const int ID_TYPE_PW               = 2;
const int ID_TYPE_HVCH             = 3;
const int ID_TYPE_LVCH             = 4;

const int ID_SIDE_INNER            = 7;   // iSideD
const int ID_SIDE_OUTER            = 8;  // iSideIO
const int ID_SIDE_TOP              = 9;  // iSideTB
const int ID_SIDE_BOTTOM           = 10;  // iSideTB

// ===================================================================
//                           END VARIABLE DEFINITIONS
// ===================================================================

// ===================================================================
//                                FUNCTIONS
// ===================================================================
string fmdBase_GetCoords(string sLogicalName, int &iDet, int &iType, int &iSideIO, int &iSideTB, int &iCh)
{
  
dyn_string dsHier  = strsplit(sLogicalName,"/");;
string sAppo = dsHier[dynlen(dsHier)];

dyn_string dsElem = strsplit(sAppo,"_");
string sEle = dsElem[dynlen(dsElem)];

           // Hardware Name: dist_1:CAEN/fmdSY1527/board10/channel000
DebugN(">>>>>----> sLogicalName :",sLogicalName);// dist_1:FMD_DCS/FMD1_PW/FMD1_PW_INNER_TOP/HvChannel000
DebugN(">>>>>----> dsHier :",dsHier);            // dist_1:FMD_DCS,FMD1_PW,FMD1_PW_INNER_TOP,HvChannel00
DebugN(">>>>>----> sAppo :",sAppo);              // HvChannel000
DebugN(">>>>>----> dsElem :",dsElem);            // HvChannel000
DebugN(">>>>>----> sEle :",sEle);              // HvChannel000
 
  //bool tIsChannel = false;
   
   if( strpos(sEle,PREFIX_HV_CH)==0 ) {// extract HV channel number (iCh)
      string tdet = substr(sEle, strlen(PREFIX_HV_CH));
      int tret = sscanfUL(tdet, "%3d", iCh);
      iType = ID_TYPE_HVCH; 
     // tIsChannel = true;
      dsElem = strsplit(dsHier[3],"_");
      DebugN(">>-----> fmdBase_GetCoords(): Found HV channel iCh: ", iCh);
    }
                
    if(strpos(sEle,PREFIX_LV_CH)==0){// extract LV channel numbre
      string tdet = substr(sEle, strlen(PREFIX_LV_CH));
      int tret = sscanfUL(tdet, "%3d", iCh);
      iType = ID_TYPE_LVCH; 
     // tIsChannel = true;
      dsElem = strsplit(dsHier[3],"_");
      DebugN(">>-----> fmdBase_GetCoords(): Found LV channel iCh: ", iCh);
    }
  // --------------------------
     if( strpos(sEle,PREFIX_CONTROLLER)==0 ) {
       if(dynlen(dsElem)>1){
       switch(dsElem[1]){
         case (PREFIX_HVPS) : iType = ID_TYPE_HVPS_CONTROLLER; break;
         case (PREFIX_LVPS) : iType = ID_TYPE_LVPS_CONTROLLER; break;
         default: DebugN(">>-----> fmdBase_GetCoords(): Error controller for power supply empty");break;
       }
       return sLogicalName;
       } else { DebugN(">>-----> fmdBase_GetCoords(): Error: controller for power supply not found");} 
     }
     if( strpos(sEle,PREFIX_RACK_ONE)==0 ) {
       iType =  ID_TYPE_RACK_ONE;
       return sLogicalName;
     }
     if( strpos(sEle,PREFIX_RACK_TWO)==0 ) {
       iType =  ID_TYPE_RACK_TWO;
       return sLogicalName;
     }
  // --------------------------  
  //  if (tIsChannel == true){// extract other params for HV & LV channels (iDet,iType,iSideIO,iSideTB)     
      iDet     = substr(dsElem[1], strlen(PREFIX_DET), 1); // iDet
      DebugN(">>-----> fmdBase_GetCoords(): Found detector: ",iDet);
      if(iDet == 0) { // check for top node
        for(int ti = 1; dynlen(dsElem); ti++)
          if(strtok(dsElem[ti],PREFIX_DET)>=0) { iType = ID_TYPE_TOP_NODE; return sLogicalName; /* should return sNode*/}
          else {
            DebugN(">>-----> fmdBase_GetCoords(): unknown device: ", dsElem);
            }
        }
     if(dynlen(dsElem)>1 && (iType!=ID_TYPE_HVCH) && (iType!=ID_TYPE_LVCH) ){
      switch(dsElem[2]){
        case PREFIX_PW:      iType = ID_TYPE_PW;      break; 
        case PREFIX_MOD:      iType = ID_TYPE_MOD;      break; 
        case PREFIX_INFR:    iType = ID_TYPE_INFR;    break;             
        default: DebugN(">>-----> fmdBase_GetCoords(): Error in fmdBase_GetCoords() device iType not found ", dsElem ); break;}
     }
     if(dynlen(dsElem)>2){
       switch(dsElem[3]){
        case PREFIX_INNER:  iSideIO = ID_SIDE_INNER;   break; 
        case PREFIX_OUTER:  iSideIO = ID_SIDE_OUTER;   break;             
        default: DebugN(">>-----> fmdBase_GetCoords(): Error in fmdBase_GetCoords() device iSideIO not found ", dsElem );break;}
     }
     if(dynlen(dsElem)>3){
       switch(dsElem[4]){
        case PREFIX_TOP:    iSideTB = ID_SIDE_TOP;     break; 
        case PREFIX_BOTTOM: iSideTB = ID_SIDE_BOTTOM;  break;             
        default: DebugN(">>-----> fmdBase_GetCoords(): Error in fmdBase_GetCoords() device iSideIO not found ", dsElem );break;}
     }

       DebugTN(">>-----> fmdBase_GetCoords(): Found device: ",
               " iDet : ",iDet,                
               " iType : ",iType,
               " iSideIO : ",iSideIO,
               " iSideTB: ",iSideTB,
               " iCh : ",iCh);
 return sLogicalName;
}
// ===================================================================
//           GET LOGICAL PATH
// ===================================================================
string fmdBase_buildLogicalPathName(int iDet, int iType, int iSideIO, int iSideTB, int iCh){
  string tdp;
  switch(iType){
    case(ID_TYPE_TOP_NODE): tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET ;  break;
    case(ID_TYPE_MOD):      tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + iDet + "_" + PREFIX_MOD;  break; 
    case(ID_TYPE_PW):       tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + iDet + "_" + PREFIX_MOD
                                + "/" + PREFIX_DET + iDet + "_" + PREFIX_PW + "_" ;//+ inner + "_" + bottom;  break;
      switch(iSideIO) {
        case(ID_SIDE_INNER): tdp = tdp + PREFIX_INNER + "_"; break;
        case(ID_SIDE_OUTER): tdp = tdp + PREFIX_OUTER + "_"; break;
        default:  DebugN(">>-----> fmdBase_buildLogicalPathName(): Error iSideIO not found ", iSideIO);  break;}
      switch(iSideTB) {
        case(ID_SIDE_TOP):   tdp = tdp + PREFIX_TOP;         break;
        case(ID_SIDE_BOTTOM):tdp = tdp + PREFIX_BOTTOM;      break;
        default:  DebugN(">>-----> fmdBase_buildLogicalPathName(): Error iSideTB not found ", iSideTB);  break;}
      break;
    case(ID_TYPE_HVCH):     
      {string ts;
      sprintf(ts,"%03d",iCh); 
      string tPdp = "/" + PREFIX_HV_CH + ts;
      tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + iDet + "_" + PREFIX_MOD
                                + "/" + PREFIX_DET + iDet + "_" + PREFIX_PW + "_" ;//+ inner + "_" + bottom;  break;
      switch(iSideIO) {
        case(ID_SIDE_INNER): tdp = tdp + PREFIX_INNER + "_"; break;
        case(ID_SIDE_OUTER): tdp = tdp + PREFIX_OUTER + "_"; break;
        default:  DebugN(">>-----> fmdBase_buildLogicalPathName(): Error iSideIO not found ", iSideIO);  break;}
      switch(iSideTB) {
        case(ID_SIDE_TOP):   tdp = tdp + PREFIX_TOP;         break;
        case(ID_SIDE_BOTTOM):tdp = tdp + PREFIX_BOTTOM;      break;
        default:  DebugN(">>-----> fmdBase_buildLogicalPathName(): Error iSideIO not found ", iSideTB);  break;}
      tdp = tdp + tPdp;
      break;}
    case(ID_TYPE_LVCH):     
      {string ts; 
      sprintf(ts,"%03d",iCh); 
      string tPdp = "/" + PREFIX_LV_CH + ts;
      tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + iDet + "_" + PREFIX_MOD
                                + "/" + PREFIX_DET + iDet + "_" + PREFIX_PW + "_" ;//+ inner + "_" + bottom;  break;
      switch(iSideIO) {
        case(ID_SIDE_INNER): tdp = tdp + PREFIX_INNER + "_"; break;
        case(ID_SIDE_OUTER): tdp = tdp + PREFIX_OUTER + "_"; break;
        default:  DebugN(">>-----> fmdBase_buildLogicalPathName(): Error iSideIO not found ", iSideIO);  break;}
      switch(iSideTB) {
        case(ID_SIDE_TOP):   tdp = tdp + PREFIX_TOP;         break;
        case(ID_SIDE_BOTTOM):tdp = tdp + PREFIX_BOTTOM;      break;
        default:  DebugN(">>-----> fmdBase_buildLogicalPathName(): Error iSideIO not found ", iSideTB);  break;}
      tdp = tdp + tPdp;
      break;}
      
    case(ID_TYPE_RACK_ONE): tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + "_" + PREFIX_INFR + "_" + PREFIX_RACK_ONE;  break; 
    case(ID_TYPE_RACK_TWO): tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + "_" + PREFIX_INFR + "_" + PREFIX_RACK_TWO;  break; 
    case(ID_TYPE_HVPS_CONTROLLER): tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + "_" + PREFIX_INFR + "_" + PREFIX_RACK_ONE + "/" +PREFIX_HVPS+ "_" + PREFIX_CONTROLLER;  break; 
    case(ID_TYPE_LVPS_CONTROLLER): tdp = SYS + ":" + PREFIX_DET + "_" + SUFFIX_DET + "/" + PREFIX_DET + "_" + PREFIX_INFR + "_" + PREFIX_RACK_TWO + "/" +PREFIX_LVPS+ "_" + PREFIX_CONTROLLER;  break;

    default: DebugN(">>-----> fmdBase_buildLogicalPathName(): Error iType not found ");  break;}
  DebugTN(">>-----> fmdBase_GetCoords(): Found device: ", tdp);
  return tdp;
}
// ===================================================================
void hexa(string &str,unsigned nombre){
    
    sprintf(str, "%016X", nombre);
}

// ===================================================================
int dec2bin(unsigned dec, dyn_int &binary){
  int ret = -1;
  int remain = 0;
  unsigned old_dec;
  int k = 0;
   binary = makeDynInt("0","0","0","0","0","0","0","0",
                        "0","0","0","0","0","0","0","0",
                        "0","0","0","0","0","0","0","0",
                        "0","0","0","0","0","0","0","0");
   
  //DebugN(">> dec2bin get ",dec, " binary=", binary);
  
  while( dec > 0){
    old_dec = dec;
    remain  = fmod(dec,2);
    dec = dec / 2.;
    //DebugN(">> ",old_dec," / 2 = ",dec , " remain ", remain);
    k = k +1;
    binary[k] = (remain == 0 ? 0 : 1);
    //DebugN("binary[",k,"] = ", binary[k]);
    }
   return ret;
  }
// ===================================================================
int bin2dec(dyn_int binary, unsigned &dec){
  int ret = -1;
  dec = 0;
  
  for(int ti = 0; ti<dynlen(binary); ti++){
    DebugN(" ti = ", ti, " add ",binary[ti+1]*(1<<ti));
    unsigned tmp = (1<<ti);
    dec = dec + binary[ti+1]*tmp;
    }
  
  return ret;
  }
// ===================================================================
//                            MAIN PROG
// ===================================================================
int main()
{
  unsigned iInt = 7;
  dyn_int sInt ;
                              
  dec2bin(iInt,sInt);

  DebugN(">>--------->  dec2bin:  IN  iInt: ",  iInt );
  DebugN(">>--------->  dec2bin: OUT  sInt: ",  sInt );

  iInt = 0;
  sInt = makeDynInt("1","1","1","1","1","1","1","1",
                    "1","1","1","1","1","1","1","1",
                    "1","1","1","1","1","1","1","1",
                    "1","1","1","1","1","1","1","1");
  bin2dec(sInt,iInt);
  
  
  DebugN(">>--------->  bin2dec: IN  sInt: ",  sInt );
  DebugN(">>--------->  bin2dec: OUT iInt: ",  iInt );
  
int iDet     = -1 ;
int iType    = -1;
int iSideIO   = -1;
int iSideTB   = -1;
int iCh      = -1;

//string sLogicalName ;

//string sLogicalName = "dist_1:FMD_DCS";
//string sLogicalName = "dist_1:FMD_DCS/FMD_INFR_RACK1";
//string sLogicalName = "dist_1:FMD_DCS/FMD_INFR_RACK2";
//string sLogicalName = "dist_1:FMD_DCS/FMD_INFR_RACK1/HVPS_CONTROLLER";
//string sLogicalName = "dist_1:FMD_DCS/FMD_INFR_RACK2/LVPS_CONTROLLER";
//string sLogicalName = "dist_1:FMD_DCS/FMD3_MOD";
//string sLogicalName = "dist_1:FMD_DCS/FMD1_MOD/FMD1_PW_INNER_BOTTOM";
//string sLogicalName = "dist_1:FMD_DCS/FMD1_MOD/FMD1_PW_INNER_TOP/HvChannel000";
//string sLogicalName = "dist_1:FMD_DCS/FMD2_MOD/FMD2_PW_INNER_TOP/LvChannel010";


// ============= CODE =============
//string sret = fmdBase_GetCoords( sLogicalName, iDet, iType, iSideIO, iSideTB, iCh);
 // ============ DECODE ============
string sLogicalPath ;
iType = ID_TYPE_HVCH;
iCh = 3;
//sLogicalPath = fmdBase_buildLogicalPathName(iDet, iType,  iSideIO,  iSideTB,  iCh);

//DebugN("..---------> IN: ", sLogicalName );
//DebugN("..--------->OUT: ", sLogicalPath  );       
 
//sLogicalPath = fmdBase_buildLogicalPathName(iDet, iType,  iSideIO,  iSideTB,  iCh);



  return 0;
}
