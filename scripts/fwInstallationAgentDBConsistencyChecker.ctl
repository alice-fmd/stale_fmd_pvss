#uses "fwInstallationDB.ctl"

main()
{
  bool isOK;
  bool connectionOpened = false;
  int sleepFor = 60; //time to sleep in mins 
  
  if(fwInstallationDB_getUseDB() && fwInstallationDB_connect() != 0)
    DebugN("ERROR: fwInstallationDBConsistencyChecker script -> Could not connect to DB. Next attempt in "+ sleepFor + " mins."); 
  else
    connectionOpened = true;
  
  while(1){
    if(fwInstallationDB_getUseDB()){
      if(!connectionOpened && fwInstallationDB_connect() != 0)
      DebugN("ERROR: fwInstallationDBConsistencyChecker script -> Could not connect to DB. Next attempt in "+ sleepFor + " mins."); 
      else
        connectionOpened = true;
    
      //DebugN("INFO: fwInstallationAgentDBConsistencyChecker -> Starting to check consistency of DB contents for the project at ", getCurrentTime());    
      if(fwInstallationDB_checkIntegrity(isOK) != 0)
      {
        DebugN("ERROR: fwInstallationAgentDBConsistencyChecker -> DB consistency check failed for project. Check DB connection...");
      }  
      else{
        if(isOK){
          fwInstallationDB_setProjectStatus(FW_INSTALLATION_DB_PROJECT_OK);

          //DebugN("INFO: fwInstallationAgentDBConsistencyChecker -> DB consistency successfully check for project: " + PROJ);
        }
        else{
           fwInstallationDB_setProjectStatus(FW_INSTALLATION_DB_PROJECT_MISSMATCH);
           
          DebugN("ERROR: fwInstallationAgentDBConsistencyChecker -> DB consistency failed for project: " + PROJ);
        }
      }    
      //DebugN("INFO: fwInstallationAgentDBConsistencyChecker -> End of consistency check at ", getCurrentTime());
    }
    delay(sleepFor*60);
  }//end while(1)
}//end of main
